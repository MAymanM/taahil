package com.tkmsoft.taahel.adapters.addingItems;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.addingItems.CentersAddModel;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MahmoudAyman on 27/05/2018.
 */
public class CentersAddAdapter extends RecyclerView.Adapter<CentersAddAdapter.MyViewHolder> {

    private Context context;
    private ListAllListeners listAllListeners;
    private ArrayList<CentersAddModel> mDataList;

    public CentersAddAdapter(Context context, ArrayList<CentersAddModel> mDataList,
                           ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_centers_addings_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        ListSharedPreference listSharedPreference = new ListSharedPreference(context);

        holder.itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(holder.getAdapterPosition()),
                holder.getAdapterPosition()));

        holder.deleteBtn.setOnClickListener(view -> listAllListeners.onDeleteButtonClick(view, mDataList.get(holder.getAdapterPosition()), holder.getAdapterPosition()));

        holder.editBtn.setOnClickListener(view -> listAllListeners.onEditButtonClick(view, mDataList.get(holder.getAdapterPosition()), holder.getAdapterPosition()));

    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(CentersAddModel centersAddModel, int adapterPosition);
        void onDeleteButtonClick(View v, CentersAddModel centersAddModel, int position);
        void onEditButtonClick(View v, CentersAddModel centersAddModel, int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        //TextView nameTV, priceTV, totalTV;
        ImageButton editBtn, deleteBtn;

        MyViewHolder(View itemView) {
            super(itemView);

            editBtn = itemView.findViewById(R.id.editBtn);
            deleteBtn = itemView.findViewById(R.id.deleteBtn);

//            nameTV = itemView.findViewById(R.id.nameTV);
//            priceTV = itemView.findViewById(R.id.priceTV);
        }
    }//end class MyViewHolder

}//end class