package com.tkmsoft.taahel.adapters.bills;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.bill.Datum;

import java.security.InvalidParameterException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 24/02/2019.
 */
public class BillsAdapter extends RecyclerView.Adapter<BillsAdapter.MyViewHolder> {
    private List<Datum> datumList;
    private ListAllClickListeners listAllClickListeners;

    public BillsAdapter(@NonNull List<Datum> datumList, ListAllClickListeners listAllClickListeners) {
        this.datumList = datumList;
        this.listAllClickListeners = listAllClickListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.single_item_bills_view, parent, false);
        Context c = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Datum movie = datumList.get(position);
        holder.onBind(movie);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void replaceData(List<Datum> movies) {
        this.datumList.clear();
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= datumList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return datumList.get(position);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }


    public interface ListAllClickListeners {
        void onItemClick(Datum datumList, int pos);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.codeTV, R.id.dateTV})
        List<TextView> textViewList;
        TextView codeTV, dateTV;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            codeTV = textViewList.get(0);
            dateTV = textViewList.get(1);
        }


        void onBind(Datum data) {

            codeTV.setText(String.valueOf(data.getId()));
            dateTV.setText(data.getDate());
            itemView.setOnClickListener(v ->
                    listAllClickListeners.onItemClick(getItem(getAdapterPosition()), getAdapterPosition())
            );
        }

    }
}