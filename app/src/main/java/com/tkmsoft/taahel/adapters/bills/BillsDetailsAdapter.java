package com.tkmsoft.taahel.adapters.bills;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 25/02/2019.
 */

public class BillsDetailsAdapter extends RecyclerView.Adapter<BillsDetailsAdapter.MyViewHolder> {
    private List<String> pNameList;
    private List<String> pQuantityList;
    private List<String> pPriceList;
    private List<String> pTotalList;

    private Context context;

    public BillsDetailsAdapter(List<String> pNmaeList, List<String> pQuantity, List<String> pPriceList, ArrayList<String> pTotalList) {
        this.pNameList = pNmaeList;
        this.pPriceList = pPriceList;
        this.pQuantityList = pQuantity;
        this.pTotalList = pTotalList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.single_item_bill_products_view, parent, false);
        context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String name = pNameList.get(position);
        String quantity = pQuantityList.get(position);
        String price = pPriceList.get(position);
        String total = pTotalList.get(position);
        holder.onBind(name, quantity, price, total);
    }

    @Override
    public int getItemCount() {
        return pNameList.size();
    }

    public void replaceData(List<String> movies) {
        this.pNameList.clear();
        this.pNameList.addAll(movies);
        notifyDataSetChanged();
    }

    public String getItem(int position) {
        if (position < 0 || position >= pNameList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return pNameList.get(position);
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.nameTV, R.id.quantityTV, R.id.priceTV, R.id.totalTV})
        List<TextView> textViewList;

        TextView nameTV, quantityTV, priceTV, totalTV;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ListSharedPreference listSharedPreference = new ListSharedPreference(context);
            nameTV = textViewList.get(0);
            quantityTV = textViewList.get(1);
            priceTV = textViewList.get(2);
            totalTV = textViewList.get(3);
        }

        void onBind(String name, String quantity, String price, String total) {
            nameTV.setText(name);
            quantityTV.setText(quantity);
            priceTV.setText(price);
            totalTV.setText(total);
        }
    }
}