package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.fragments.main.doctor.RequestDrFragment1;
import com.tkmsoft.taahel.model.api.order.drTimes.DayTime;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 30/05/2018.
 */
public class DrDayTimesAdapter extends RecyclerView.Adapter<DrDayTimesAdapter.MyViewHolder> {

    private Context context;
    private ListAllListeners listAllListeners;
    private List<DayTime> mDaysList;
    private ListSharedPreference listSharedPreference;
    private boolean onBind;

    public DrDayTimesAdapter(Context context, List<DayTime> mDaysList,
                             ListAllListeners listAllListeners) {
        this.mDaysList = mDaysList;
        this.listAllListeners = listAllListeners;
        this.context = context;
        listSharedPreference = new ListSharedPreference(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_select_days_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDaysList != null)
            return mDaysList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        onBind = true;
        holder.bind();
        onBind = false;
    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemChecked(DayTime dayTime, int adapterPosition, boolean b);
    }

    public DayTime getItem(int position) {
        if (position < 0 || position >= mDaysList.size()) {
            Log.d(RequestDrFragment1.class.getSimpleName(), "getItem: " + "INVALID INDEX");
        }
        return mDaysList.get(position);
    }

    public void replaceData(List<DayTime> dayTimes) {
        this.mDaysList.clear();
        this.mDaysList.addAll(dayTimes);
        notifyDataSetChanged();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.checkBox)
        CheckBox checkBox;
        @BindView(R.id.typeTV)
        TextView typeTV;
        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind() {
            setDaysTextName(checkBox, getAdapterPosition());

            if (listSharedPreference.getDrTimeCheckPos().equals(getAdapterPosition())) {
                checkBox.setChecked(true);
                listAllListeners.onItemChecked(getItem(getAdapterPosition()), getAdapterPosition(), true);
            } else
                checkBox.setChecked(false);

            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                if (!onBind) {
                    if (b) {
                        listSharedPreference.setDrTimeCheckPos(getAdapterPosition());
                        listAllListeners.onItemChecked(getItem(getAdapterPosition()), getAdapterPosition(), true);
                        notifyDataSetChanged();
                    }
                }
            });

            if (mDaysList.get(getAdapterPosition()).getFree().equals("0"))
                typeTV.setText(context.getString(R.string.for_Money));
            else
                typeTV.setText(context.getString(R.string.free));
        }

        private void setDaysTextName(CheckBox checkBox, int pos) {
            switch (mDaysList.get(pos).getTime()) {
                case "0":
                    checkBox.setText(R.string.am12_1);
                    break;
                case "1":
                    checkBox.setText(R.string.am1_2);
                    break;
                case "2":
                    checkBox.setText(R.string.am2_3);
                    break;
                case "3":
                    checkBox.setText(R.string.am3_4);
                    break;
                case "4":
                    checkBox.setText(R.string.am4_5);
                    break;
                case "5":
                    checkBox.setText(R.string.am5_6);
                    break;
                case "6":
                    checkBox.setText(R.string.am6_7);
                    break;
                case "7":
                    checkBox.setText(R.string.am7_8);
                    break;
                case "8":
                    checkBox.setText(R.string.am8_9);
                    break;
                case "9":
                    checkBox.setText(R.string.am9_10);
                    break;
                case "10":
                    checkBox.setText(R.string.am10_11);
                    break;
                case "11":
                    checkBox.setText(R.string.am11_12);
                    break;
                case "12":
                    checkBox.setText(R.string.pm12_1);
                    break;
                case "13":
                    checkBox.setText(R.string.pm1_2);
                    break;
                case "14":
                    checkBox.setText(R.string.pm2_3);
                    break;
                case "15":
                    checkBox.setText(R.string.pm3_4);
                    break;
                case "16":
                    checkBox.setText(R.string.pm4_5);
                    break;
                case "17":
                    checkBox.setText(R.string.pm5_6);
                    break;
                case "18":
                    checkBox.setText(R.string.pm6_7);
                    break;
                case "19":
                    checkBox.setText(R.string.pm7_8);
                    break;
                case "20":
                    checkBox.setText(R.string.pm8_9);
                    break;
                case "21":
                    checkBox.setText(R.string.pm9_10);
                    break;
                case "22":
                    checkBox.setText(R.string.pm10_11);
                    break;
                case "23":
                    checkBox.setText(R.string.pm11_12);
                    break;
            }
        }

    }//end class MyViewHolder


}//end class