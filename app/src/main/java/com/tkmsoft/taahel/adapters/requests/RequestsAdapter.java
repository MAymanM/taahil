package com.tkmsoft.taahel.adapters.requests;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.order.RequestsModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MahmoudAyman on 21/05/2018.
 */
public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.MyViewHolder> {

    private ListAllListeners listAllListeners;

    private ArrayList<RequestsModel> mDataList;


    public RequestsAdapter(Context context, ArrayList<RequestsModel> mDataList,
                           ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_requests_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        holder.nameTV.setText(mDataList.get(holder.getAdapterPosition()).getName());

        holder.counterTV.setText(mDataList.get(holder.getAdapterPosition()).getCount());

        holder.itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(holder.getAdapterPosition()), holder.getAdapterPosition()));

    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(RequestsModel requestsModel, int adapterPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView nameTV, counterTV;

        MyViewHolder(View itemView) {
            super(itemView);

            nameTV = itemView.findViewById(R.id.nameTV);
            counterTV = itemView.findViewById(R.id.counterTV);
        }
    }//end class MyViewHolder

}//end class
