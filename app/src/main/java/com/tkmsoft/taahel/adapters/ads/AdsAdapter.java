package com.tkmsoft.taahel.adapters.ads;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.AdsModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by MahmoudAyman on 25/04/2018.
 */
public class AdsAdapter extends PagerAdapter {

    private List<AdsModel.DataBean.AdsInfoBean> mDataList;
    private LayoutInflater inflater;
    private ListAllListeners listAllListeners;

    public AdsAdapter(Context context, List<AdsModel.DataBean.AdsInfoBean> mDataList, ListAllListeners listAllListeners) {
        Context context1 = context;
        this.mDataList = mDataList;
        inflater = LayoutInflater.from(context);
        this.listAllListeners = listAllListeners;
    }

    public interface ListAllListeners {
        void onItemViewClick(List<AdsModel.DataBean.AdsInfoBean> images, int position);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.single_image_slide_view, view, false);

        ImageView myImage = myImageLayout.findViewById(R.id.image);

        Picasso.get()
                .load(mDataList.get(position).getPhoto())
                .placeholder(R.drawable.place_holder)
                .error(R.drawable.place_holder)
                .into(myImage);

        myImageLayout.setOnClickListener(view1 -> listAllListeners.onItemViewClick(mDataList, position));

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
}
