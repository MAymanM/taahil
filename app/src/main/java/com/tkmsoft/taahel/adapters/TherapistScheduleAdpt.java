package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.TherapistScheduleModel;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;

/**
 * Created by MahmoudAyman on 25/04/2018.
 */
public class TherapistScheduleAdpt extends RecyclerView.Adapter<TherapistScheduleAdpt.MyViewHolder> {
    private Context context;
    private ClickListListeners clickListListeners;
    private ListSharedPreference listSharedPreference;
    private ArrayList<TherapistScheduleModel> mDataList;
    private String dayNum;

    public TherapistScheduleAdpt(Context context, String dayNum, ArrayList<TherapistScheduleModel> mDataList,
                                 ClickListListeners clickListListeners) {
        this.mDataList = mDataList;
        this.clickListListeners = clickListListeners;
        this.context = context;
        this.dayNum = dayNum;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_therapist_schedular_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.bind();
    }//end onBindViewHolder()

    public interface ClickListListeners {
        void onItemCheck(String checkBoxName, int position, boolean isFree);

        void onItemUncheck(String checkBoxName, int position);

        void onFreeCheck(String checkBoxName, String name, int pos);

        //void onFreeUncheck(String name, int pos);
        void onPaidCheck(String checkBoxName, String name, int pos);
        //void onPaidUncheck(String name, int pos);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;
        RadioButton freeRB, paidRB;
        MyViewHolder(View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.timeCheckBox);
            freeRB = itemView.findViewById(R.id.freeRadioBtn);
            paidRB = itemView.findViewById(R.id.paidRadioBtn);
            listSharedPreference = new ListSharedPreference(context);

        }

        public void bind() {

            checkBox.setOnCheckedChangeListener(null);
            freeRB.setOnCheckedChangeListener(null);
            paidRB.setOnCheckedChangeListener(null);

            //Check Box
            checkBox.setText(mDataList.get(getAdapterPosition()).getTime());

            if (mDataList.get(getAdapterPosition()).getChecked())
                checkBox.setChecked(true);
            else
                checkBox.setChecked(false);


            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    if (listSharedPreference.getIsFreeChoosen()) {
                        listSharedPreference.setIsFreeChoosen(true);
                        mDataList.get(getAdapterPosition()).setChecked(true);
                        freeRB.setChecked(true);
                        mDataList.get(getAdapterPosition()).setFree(true);
                        clickListListeners.onItemCheck(checkBox.getText().toString(), getAdapterPosition(), true);

                        listSharedPreference.setCheckedBox(dayNum, checkBox.getText().toString(), checkBox.getText().toString());
                        listSharedPreference.setRadioFree(dayNum, checkBox.getText().toString(), true);

                        if (listSharedPreference.getRadioFree(dayNum, checkBox.getText().toString()))
                            listSharedPreference.setRadioFree(dayNum, checkBox.getText().toString(), true);
                        else
                            listSharedPreference.setRadioFree(dayNum, checkBox.getText().toString(), false);

                    }else
                    {
                        mDataList.get(getAdapterPosition()).setChecked(true);
                        paidRB.setChecked(true);
                        mDataList.get(getAdapterPosition()).setPaid(true);
                        clickListListeners.onItemCheck(checkBox.getText().toString(), getAdapterPosition(), false);

                        listSharedPreference.setCheckedBox(dayNum, checkBox.getText().toString(), checkBox.getText().toString());
                        if (listSharedPreference.getRadioFree(dayNum, checkBox.getText().toString())) {
                            listSharedPreference.setRadioFree(dayNum, checkBox.getText().toString(), true);
                        }
                        else
                            listSharedPreference.setRadioFree(dayNum, checkBox.getText().toString(), false);
                    }

                } else {
                    mDataList.get(getAdapterPosition()).setChecked(false);
                    if (freeRB.isChecked())
                        listSharedPreference.setIsFreeChoosen(false);
                    clickListListeners.onItemUncheck(checkBox.getText().toString(), getAdapterPosition());
                    freeRB.setChecked(false);
                    paidRB.setChecked(false);
                    mDataList.get(getAdapterPosition()).setPaid(false);
                    mDataList.get(getAdapterPosition()).setFree(false);

                    listSharedPreference.deleteCheckBox(dayNum, checkBox.getText().toString());
                }
            });

            //Radio Buttons
            //Free
            if (mDataList.get(getAdapterPosition()).isFree()) {
                freeRB.setChecked(true);
                listSharedPreference.setIsFreeChoosen(true);
            }
            else
                freeRB.setChecked(false);

            freeRB.setOnClickListener(view -> {
                if (checkBox.isChecked()) {
                    if (listSharedPreference.getIsFreeChoosen()) {
                        if (paidRB.isChecked()) {
                            paidRB.setChecked(false);
                            mDataList.get(getAdapterPosition()).setPaid(false);
                        }
                        if (freeRB.isChecked()) {
                            listSharedPreference.setIsFreeChoosen(true);
                            mDataList.get(getAdapterPosition()).setFree(true);
                            clickListListeners.onFreeCheck(checkBox.getText().toString(), freeRB.getText().toString(), getAdapterPosition());
                        }
                        listSharedPreference.setRadioFree(dayNum, checkBox.getText().toString(), true);
                    }
                    else {
                        freeRB.setChecked(false);
                        Toast.makeText(context, context.getString(R.string.cannot_set_a_free_day), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    freeRB.setChecked(false);
                    mDataList.get(getAdapterPosition()).setFree(false);
                    Toast.makeText(context, context.getString(R.string.plz_choose_time_first), Toast.LENGTH_SHORT).show();
                }
            });

            //Paid
            if (mDataList.get(getAdapterPosition()).isPaid())
                paidRB.setChecked(true);
            else
                paidRB.setChecked(false);

            paidRB.setOnClickListener(view -> {

                if (checkBox.isChecked()) {
                    if (freeRB.isChecked()) {
                        freeRB.setChecked(false);
                        listSharedPreference.setIsFreeChoosen(false);
                        mDataList.get(getAdapterPosition()).setFree(false);
                    }
                    if (paidRB.isChecked()) {
                        mDataList.get(getAdapterPosition()).setPaid(true);
                        clickListListeners.onPaidCheck(checkBox.getText().toString(), paidRB.getText().toString(), getAdapterPosition());
                        Toast.makeText(context, "paid", Toast.LENGTH_SHORT).show();
                    }
                    listSharedPreference.setRadioFree(dayNum, checkBox.getText().toString(), false);
                } else {
                    paidRB.setChecked(false);
                    mDataList.get(getAdapterPosition()).setPaid(false);
                    Toast.makeText(context, context.getString(R.string.plz_choose_time_first), Toast.LENGTH_SHORT).show();
                }
            });


///////////////////////Setting value if exit///////////////////
            if (listSharedPreference.getCheckedBox(dayNum, checkBox.getText().toString()).equals(checkBox.getText().toString())) {
                checkBox.setChecked(true);
                Log.d("bgbb", "keyon: " + dayNum + "*" + checkBox.getText().toString() + "\n" + "val: " + checkBox.getText().toString());
                if (listSharedPreference.getRadioFree(dayNum, checkBox.getText().toString())) {
                    freeRB.setChecked(true);
                    paidRB.setChecked(false);
                    Log.d("bgbb", "free: " + listSharedPreference.getRadioFree(dayNum, checkBox.getText().toString()));
                } else {
                    paidRB.setChecked(true);
                    freeRB.setChecked(false);
                    Log.d("bgbb", "free: " + listSharedPreference.getRadioFree(dayNum, checkBox.getText().toString()));
                }
            } else {
                checkBox.setChecked(false);

            }

        }
    }//end class MyViewHolder

}//end class