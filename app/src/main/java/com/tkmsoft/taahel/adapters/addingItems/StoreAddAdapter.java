package com.tkmsoft.taahel.adapters.addingItems;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.store.add.StoreAddModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MahmoudAyman on 24/05/2018.
 */
public class StoreAddAdapter extends RecyclerView.Adapter<StoreAddAdapter.MyViewHolder>{
    private ListAllListeners listAllListeners;
    private ArrayList<StoreAddModel> mDataList;

    public StoreAddAdapter(Context context, ArrayList<StoreAddModel> mDataList,
                           ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_store_addings_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(holder.getAdapterPosition()),
                holder.getAdapterPosition()));
//        boolean isFav = mDataList.get(holder.getAdapterPosition()).isFav();
//        if (!isFav) {
//            holder.favBtn.setImageResource(R.drawable.ic_favorite_empty_24dp);
//        } else {
//            holder.favBtn.setImageResource(R.drawable.ic_favorite_full_24dp);
//        }
//        holder.favBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mDataList.get(holder.getAdapterPosition()).isFav()) {
//                    holder.favBtn.setImageResource(R.drawable.ic_favorite_empty_24dp);
//                    listAllListeners.onFavButtonClick(view, mDataList.get(holder.getAdapterPosition()),holder.getAdapterPosition(), false);
//                } else {
//                    holder.favBtn.setImageResource(R.drawable.ic_favorite_full_24dp);
//                    listAllListeners.onFavButtonClick(view, mDataList.get(holder.getAdapterPosition()),holder.getAdapterPosition(), true);
//                }
//            }
//        });

        holder.deleteBtn.setOnClickListener(view -> listAllListeners.onDeleteButtonClick(view, mDataList.get(holder.getAdapterPosition()), holder.getAdapterPosition()));

        holder.editBtn.setOnClickListener(view -> listAllListeners.onEditButtonClick(view, mDataList.get(holder.getAdapterPosition()), holder.getAdapterPosition()));

    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(StoreAddModel storeAddModel, int adapterPosition);
        void onDeleteButtonClick(View v, StoreAddModel storeAddModel, int position);
        void onEditButtonClick(View v, StoreAddModel storeAddModel, int position);
        //void onFavButtonClick(View v, AddingItemsModel addingItemsModel,int position, boolean isFav);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        //TextView nameTV, priceTV, totalTV;
        ImageButton favBtn, editBtn, deleteBtn;

        MyViewHolder(View itemView) {
            super(itemView);

            favBtn = itemView.findViewById(R.id.favBtn);
            editBtn = itemView.findViewById(R.id.editBtn);
            deleteBtn = itemView.findViewById(R.id.deleteBtn);

//            nameTV = itemView.findViewById(R.id.nameTV);
//            priceTV = itemView.findViewById(R.id.priceTV);
        }
    }//end class MyViewHolder

}//end class
