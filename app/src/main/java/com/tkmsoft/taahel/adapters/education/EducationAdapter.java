package com.tkmsoft.taahel.adapters.education;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.education.view.Datum;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Created by MahmoudAyman on 10/10/2018.
 **/
public class EducationAdapter extends RecyclerView.Adapter<EduViewHolder> {
    private List<Datum> movies;
    private ListAllListeners listAllListeners;

    public EducationAdapter(@NonNull List<Datum> movies, ListAllListeners listAllListeners) {
        this.movies = movies;
        this.listAllListeners = listAllListeners;
    }

    @NonNull
    @Override
    public EduViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.single_item_education_view, parent, false);
        Context context = parent.getContext();
        return new EduViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final EduViewHolder holder, int position) {
        final Datum movie = movies.get(position);

        holder.onBind(movie, holder);
        holder.itemView.setOnClickListener(v -> listAllListeners.onItemClick(movie, holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void replaceData(List<Datum> movies) {
        this.movies.clear();
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= movies.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return movies.get(position);
    }

    public void clearData() {
        movies.clear();
        notifyDataSetChanged();
    }


    public interface ListAllListeners {
        void onItemClick(Datum datum, int pos);
    }
}

