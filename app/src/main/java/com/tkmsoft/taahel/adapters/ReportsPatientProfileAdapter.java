package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.reports.patient.profile.Datum;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 30/09/2018.
 */
public class ReportsPatientProfileAdapter extends RecyclerView.Adapter<ReportsPatientProfileAdapter.MyViewHolder>{


    private ListAllListeners listAllListeners;
    private List<Datum> mDataList;

    public ReportsPatientProfileAdapter(Context context, List<Datum> mDataList,
                           ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_reports_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.bind();
    }//end onBindViewHolder()

    public interface ListAllListeners {
        void onItemViewClick(Datum reportsModel, int adapterPosition);

        void onDownloadClick(String repLink, int adapterPosition);

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageBtn)
        ImageButton imageBtn;
        @BindView(R.id.codeTV)
        TextView codeTV;
        @BindView(R.id.descriptionTV)
        TextView descriptionTV;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind() {
            codeTV.setText(mDataList.get(getAdapterPosition()).getPatientOrderId());
            descriptionTV.setText(mDataList.get(getAdapterPosition()).getNotes());
            itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(getAdapterPosition()), getAdapterPosition()));
            imageBtn.setOnClickListener(view -> listAllListeners.onDownloadClick(mDataList.get(getAdapterPosition()).getReport(), getAdapterPosition()));
        }
    }//end class MyViewHolder

}
