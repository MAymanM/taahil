package com.tkmsoft.taahel.adapters.requests.doctor;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.order.drs.Order;

import java.util.List;

/**
 * Created by MahmoudAyman on 21/05/2018.
 */
public class SubRequestAdapterDr extends RecyclerView.Adapter<SubRequestAdapterDr.MyViewHolder> {

    private ListAllListeners listAllListeners;
    private List<Order> mDataList;

    public SubRequestAdapterDr(Context context, List<Order> mDataList,
                               ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_sub_request_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.bind();
    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemDrOrderClick(Order order, int adapterPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView codeTV, dateTV;

        MyViewHolder(View itemView) {
            super(itemView);

            codeTV = itemView.findViewById(R.id.codeTV);
            dateTV = itemView.findViewById(R.id.dateTV);
        }

        public void bind() {
            if (mDataList.get(getAdapterPosition()).getId() != null)
                codeTV.setText(String.valueOf(mDataList.get(getAdapterPosition()).getId()));
            if (mDataList.get(getAdapterPosition()).getDate() != null)
                dateTV.setText(mDataList.get(getAdapterPosition()).getDate());
            itemView.setOnClickListener(view -> listAllListeners.onItemDrOrderClick(mDataList.get(getAdapterPosition()), getAdapterPosition()));
        }
    }//end class MyViewHolder

}//end class