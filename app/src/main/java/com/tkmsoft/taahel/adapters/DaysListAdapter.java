package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.DaysModel;

import java.util.ArrayList;

/**
 * Created by MahmoudAyman on 13/05/2018.
 */
public class DaysListAdapter extends RecyclerView.Adapter<DaysListAdapter.MyViewHolder> {
    private ListAllListeners listAllListeners;
    private ArrayList<DaysModel> mDataList;


    public DaysListAdapter(Context context, ArrayList<DaysModel> mDataList,
                           ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_days_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        holder.nameTV.setText(mDataList.get(holder.getAdapterPosition()).getDay());

        holder.itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(holder.getAdapterPosition()),
                holder.getAdapterPosition()));
    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(DaysModel daysModel, int adapterPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView nameTV;

        MyViewHolder(View itemView) {
            super(itemView);
            nameTV = itemView.findViewById(R.id.nameTV);
        }
    }//end class MyViewHolder

}//end class
