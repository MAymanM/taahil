package com.tkmsoft.taahel.adapters.favs;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.favourite.view.Datum;
import com.tkmsoft.taahel.model.api.favourite.view.Medical;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by MahmoudAyman on 19/05/2018.
 */
public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.MyViewHolder> {
    private ListAllListeners listAllListeners;
    private ArrayList<Datum> mDataList;
    private ListSharedPreference listSharedPreference;

    public FavouritesAdapter(ArrayList<Datum> mDataList, ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_favourites_view, parent, false);
        Context context = parent.getContext();
        listSharedPreference = new ListSharedPreference(context);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        Datum datum = getItem(position);
        holder.onBind(datum.getMedical());
    }//end onBindViewHolder()

    public void replaceData(List<Datum> movies) {
        this.mDataList.clear();
        this.mDataList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.mDataList.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= mDataList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return mDataList.get(position);
    }

    public void clearData() {
        mDataList.clear();
        notifyDataSetChanged();
    }

    public interface ListAllListeners {
        void onItemViewClick(Medical purchaseDetailModel, int adapterPosition);

        void onFavBtnClick(Medical datum, int pos, ImageButton imageButton);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        SelectableRoundedImageView imageView;
        @BindViews({R.id.nameTV, R.id.priceTV})
        List<TextView> textViewList;
        TextView nameTV, priceTV;

        @BindView(R.id.favBtn)
        ImageButton favBtn;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            nameTV = textViewList.get(0);
            priceTV = textViewList.get(1);
        }

        public void onBind(final Medical datum) {
            if (getLang().equals("ar")) {
                nameTV.setText(datum.getNameAr());
                priceTV.setText(datum.getPrice() + " " + datum.getCurrency().getNameAr());
            } else {
                nameTV.setText(datum.getNameEn());
                priceTV.setText(datum.getPrice() + " " + datum.getCurrency().getNameEn());
            }

            favBtn.setImageResource(R.drawable.ic_favorite_full_24dp);

            itemView.setOnClickListener(v ->
                    listAllListeners.onItemViewClick(datum, getAdapterPosition()));

        }

        private String getLang() {
            return listSharedPreference.getLanguage();
        }

        @OnClick(R.id.favBtn)
        void onFavBtnClick() {
            listAllListeners.onFavBtnClick(mDataList.get(getAdapterPosition()).getMedical(), getAdapterPosition(), favBtn);
        }

    }//end class MyViewHolder
}//end class
