package com.tkmsoft.taahel.adapters.addingItems;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.addingItems.AddingItemsModel;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MahmoudAyman on 23/05/2018.
 */
public class AddingItemsAdapter extends RecyclerView.Adapter<AddingItemsAdapter.MyViewHolder> {
    private Context context;
    private ListAllListeners listAllListeners;
    private ArrayList<AddingItemsModel> mDataList;

    public AddingItemsAdapter(Context context, ArrayList<AddingItemsModel> mDataList,
                              ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_addings_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        ListSharedPreference listSharedPreference = new ListSharedPreference(context);

        holder.nameTV.setText(mDataList.get(holder.getAdapterPosition()).getName());
        Picasso.get().load(mDataList.get(holder.getAdapterPosition()).getImage()).into(holder.imageView);
        holder.itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(holder.getAdapterPosition()),
                holder.getAdapterPosition()));

    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(AddingItemsModel addingItemsModel, int adapterPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView nameTV;
        SelectableRoundedImageView imageView;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            nameTV = itemView.findViewById(R.id.nameTV);
        }
    }//end class MyViewHolder

}//end class
