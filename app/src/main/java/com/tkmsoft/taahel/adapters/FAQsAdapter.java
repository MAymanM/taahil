package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.FAQModel;

import java.security.InvalidParameterException;
import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 10/19/2018.
 **/
public class FAQsAdapter extends RecyclerView.Adapter<FAQsAdapter.MyViewHolder> {

    private List<FAQModel.DataBean.FaqsBean> datumList;
    private ListAllClickListeners listAllClickListeners;

    public FAQsAdapter(List<FAQModel.DataBean.FaqsBean> faqs, ListAllClickListeners listAllListeners) {
        this.datumList = faqs;
        this.listAllClickListeners = listAllListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.sigle_item_faqs_view, parent, false);
        Context context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        FAQModel.DataBean.FaqsBean movie = datumList.get(position);
        holder.onBind(movie);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void replaceData(List<FAQModel.DataBean.FaqsBean> faqsBeans) {
        if (datumList.size() > 0) {
            this.datumList.clear();
        }
        this.datumList.addAll(faqsBeans);
        notifyDataSetChanged();
    }

    public void updateData(List<FAQModel.DataBean.FaqsBean> faqsBeans) {
        this.datumList.addAll(faqsBeans);
        notifyDataSetChanged();
    }

    public FAQModel.DataBean.FaqsBean getItem(int position) {
        if (position < 0 || position >= datumList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return datumList.get(position);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindViews({R.id.questionTV, R.id.answerTV})
        List<TextView> listTextView;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        void onBind(FAQModel.DataBean.FaqsBean faqsBean) {
                listTextView.get(0).setText(faqsBean.getQuestion());
                listTextView.get(1).setText(faqsBean.getAnswer());
            itemView.setOnClickListener(view -> listAllClickListeners.onItemClick(getItem(getAdapterPosition()), getAdapterPosition()));
        }
    }//end class MyViewHolder

    public interface ListAllClickListeners {
        void onItemClick(FAQModel.DataBean.FaqsBean datumList, int pos);
    }
}
