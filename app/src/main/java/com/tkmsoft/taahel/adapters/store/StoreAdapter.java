package com.tkmsoft.taahel.adapters.store;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.tkmsoft.taahel.R;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.model.api.store.view.Datum;

import java.security.InvalidParameterException;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by MahmoudAyman on 25/04/2018.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> {
    private List<Datum> datumList;
    private ListAllClickListeners listAllClickListeners;

    public StoreAdapter(@NonNull List<Datum> datumList, ListAllClickListeners listAllClickListeners) {
        this.datumList = datumList;
        this.listAllClickListeners = listAllClickListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.single_item_store_view, parent, false);
        Context c = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Datum movie = datumList.get(position);
        holder.bind(movie);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void replaceData(List<Datum> movies) {
        this.datumList.clear();
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= datumList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return datumList.get(position);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }


    public interface ListAllClickListeners {
        void onItemClick(Datum datumList, int pos);

        void onFavBtnClick(ImageButton favBtn, Datum datum, int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        SelectableRoundedImageView imageView;
        @BindViews({R.id.nameTV, R.id.priceTV, R.id.currencyTV, R.id.viewsTV, R.id.favsTV})
        List<TextView> textViewList;
        TextView nameTV, priceTV, currencyTV, viewsTV, favsTV;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;
        @BindView(R.id.favBtn)
        ImageButton favBtn;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            nameTV = textViewList.get(0);
            priceTV = textViewList.get(1);
            currencyTV = textViewList.get(2);
            viewsTV = textViewList.get(3);
            favsTV = textViewList.get(4);
        }

        public void bind(final Datum datum) {
            Picasso.get().load(datum.getMainPhoto())
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.place_holder)
                    .resize(500,300)
                    .into(imageView);

            if (datum.getFav()!= null) {
                if (datum.getFav())
                    favBtn.setImageResource(R.drawable.ic_favorite_full_24dp);
                else
                    favBtn.setImageResource(R.drawable.ic_favorite_empty_24dp);
            }

            ratingBar.setRating(Float.valueOf(datum.getCountRate()));
            nameTV.setText(datum.getNameAr());
            priceTV.setText(datum.getPrice());
            currencyTV.setText(datum.getCurrency().getNameAr());
            viewsTV.setText(datum.getViews());
            favsTV.setText(datum.getCountWishlists());
            itemView.setOnClickListener(view -> listAllClickListeners.onItemClick(datum, getAdapterPosition()));

        }

        @OnClick(R.id.favBtn)
        void onFavClick() {
            listAllClickListeners.onFavBtnClick(favBtn, datumList.get(getAdapterPosition()), getAdapterPosition());
        }

    }//end class MyViewHolder
}