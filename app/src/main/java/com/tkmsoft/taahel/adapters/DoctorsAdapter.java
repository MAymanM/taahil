package com.tkmsoft.taahel.adapters;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.getDoctors.Datum;
import com.squareup.picasso.Picasso;

import java.security.InvalidParameterException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by MahmoudAyman on 19/04/2018.
 */
public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.MyViewHolder> {
    private List<Datum> datumList;
    private Context context;
    private ListAllClickListeners listAllClickListeners;

    public DoctorsAdapter(@NonNull List<Datum> datumList, ListAllClickListeners listAllClickListeners) {
        this.datumList = datumList;
        this.listAllClickListeners = listAllClickListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.sigle_item_therapist_view, parent, false);
        context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Datum movie = datumList.get(position);
        holder.onBind(movie);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void replaceData(List<Datum> movies) {
        this.datumList.clear();
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= datumList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return datumList.get(position);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }




    private void setImageSpecs(CircleImageView imageView, String gender, int adapterPosition) {
        if (gender.equals("0")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                imageView.setBorderColor(context.getColor(R.color.blue_male));
            } else {
                imageView.setBorderColor(context.getResources().getColor(R.color.blue_male));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                imageView.setBorderColor(context.getColor(R.color.pink_female));
            } else {
                imageView.setBorderColor(context.getResources().getColor(R.color.pink_female));
            }
        }
        String IMAGE_BASE_URL = "https://www.taahel.com/storage/uploads/members/avatars/";
        Picasso.get()
                .load(IMAGE_BASE_URL + getItem(adapterPosition).getAvatar())
                .resize(100, 100)
                .placeholder(R.drawable.place_holder)
                .error(R.drawable.place_holder)
                .into(imageView);
    }



    class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView imageView;
        TextView nameTV, deptTV, yearsEXP_TV, priceTV, currencyTV;
        RatingBar ratingBar;
        @BindView(R.id.cityTV)
        TextView cityTV;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            nameTV = itemView.findViewById(R.id.nameTV);
            imageView = itemView.findViewById(R.id.circularImageView);
            deptTV = itemView.findViewById(R.id.deptTV);
            yearsEXP_TV = itemView.findViewById(R.id.yearsOfExpTV);
            priceTV = itemView.findViewById(R.id.cost);
            currencyTV = itemView.findViewById(R.id.currency);
            ratingBar = itemView.findViewById(R.id.starsRating);
        }


        void onBind(Datum movie) {
            nameTV.setText(movie.getName());
            priceTV.setText(movie.getDoctor().getPrice());
            currencyTV.setText(movie.getCurrency().getNameAr());
            yearsEXP_TV.setText(movie.getDoctor().getExperiences());
            deptTV.setText(movie.getSpecializations().get(0).getNameAr());
            cityTV.setText(movie.getCity().getNameAr());
            setImageSpecs(imageView, movie.getGender(), getAdapterPosition());

            ratingBar.setRating(movie.getRate().floatValue());
            itemView.setOnClickListener(view -> listAllClickListeners.onItemClick(getItem(getAdapterPosition()), getAdapterPosition()));
        }
    }//end class MyViewHolder

    public interface ListAllClickListeners {
        void onItemClick(Datum datumList, int pos);
    }
}//end class