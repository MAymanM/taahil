package com.tkmsoft.taahel.adapters.calculations;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.order.drs.Order;

import java.security.InvalidParameterException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by MahmoudAyman on 31/12/2018.
 */
public class CalcsAdapter extends RecyclerView.Adapter<CalcsAdapter.MyViewHolder> {

    private ListAllListeners listAllListeners;
    private List<Order> mDataList;

    public CalcsAdapter(List<Order> mDataList,
                        ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_sub_request_view, parent, false);
        Context context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.bind();
    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(Order order, int adapterPosition);
    }

    public void replaceData(List<Order> movies) {
        this.mDataList.clear();
        this.mDataList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Order> movies) {
        this.mDataList.addAll(movies);
        notifyDataSetChanged();
    }

    public Order getItem(int position) {
        if (position < 0 || position >= mDataList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return mDataList.get(position);
    }

    public void clearData() {
        mDataList.clear();
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView codeTV, dateTV;

        MyViewHolder(View itemView) {
            super(itemView);

            codeTV = itemView.findViewById(R.id.codeTV);
            dateTV = itemView.findViewById(R.id.dateTV);
        }

        public void bind() {
            if (mDataList.get(getAdapterPosition()).getId() != null)
                codeTV.setText(String.valueOf(mDataList.get(getAdapterPosition()).getId()));
            if (mDataList.get(getAdapterPosition()).getDate() != null)
                dateTV.setText(mDataList.get(getAdapterPosition()).getDate());
            itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(getAdapterPosition()), getAdapterPosition()));
        }
    }//end class MyViewHolder

}//end class