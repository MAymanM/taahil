package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.specialization.Specialization;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 23/06/2018.
 */
public class StringAdapter extends RecyclerView.Adapter<StringAdapter.MyViewHolder> {

    private Context context;
    private ListAllListeners listAllListeners;
    private List<Specialization> mDataList;
    private ListSharedPreference listSharedPreference;

    public StringAdapter(Context context, List<Specialization> mDataList, ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        this.context = context;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_dept_checkbox_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.bind();
    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onCheck(Specialization stringModel, int position);

        void onUnCheck(Specialization stringModel, int position);

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkBox)
        CheckBox checkBox;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            listSharedPreference = new ListSharedPreference(context);
        }

        public void bind() {

            if (listSharedPreference.getDeptCheck("checkBox:" + getAdapterPosition())) {
                listSharedPreference.setDeptCheck("checkBox:" + getAdapterPosition(), true);
                checkBox.setChecked(true);
            } else {
                listSharedPreference.setDeptCheck("checkBox:" + getAdapterPosition(), false);
                checkBox.setChecked(false);
            }

            checkBox.setText(mDataList.get(getAdapterPosition()).getNameAr());

            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    listSharedPreference.setDeptCheck("checkBox:" + getAdapterPosition(), true);
                    listAllListeners.onCheck(mDataList.get(getAdapterPosition()), getAdapterPosition());
//                        Toast.makeText(context, "" + listSharedPreference.getDeptCheck("checkBox:" + getAdapterPosition()), Toast.LENGTH_SHORT).show();
                } else {
                    listSharedPreference.setDeptCheck("checkBox:" + getAdapterPosition(), false);
                    listAllListeners.onUnCheck(mDataList.get(getAdapterPosition()), getAdapterPosition());
//                        Toast.makeText(context, "" + listSharedPreference.getDeptCheck("checkBox:" + getAdapterPosition()), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }//end class MyViewHolder


}//end class