package com.tkmsoft.taahel.adapters.cart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.cart.view.Cart;
import com.tkmsoft.taahel.model.api.cart.view.Product;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.security.InvalidParameterException;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by MahmoudAyman on 13/01/2019.
 */
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    private List<Cart> mDataList;
    private Context context;
    private ListAllClickListeners listAllClickListeners;
    private ListSharedPreference listSharedPreference;

    public CartAdapter(@NonNull List<Cart> mDataList,
                        ListAllClickListeners listAllClickListeners) {
        this.mDataList = mDataList;
        this.listAllClickListeners = listAllClickListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.single_item_cart_view, parent, false);
        context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Product product = mDataList.get(position).getProduct();
        holder.onBind(product);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void replaceData(List<Cart> movies) {
        this.mDataList.clear();
        this.mDataList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Cart> movies) {
        this.mDataList.addAll(movies);
        notifyDataSetChanged();
    }

    public Cart getItem(int position) {
        if (position < 0 || position >= mDataList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return mDataList.get(position);
    }

    public void clearData() {
        mDataList.clear();
        notifyDataSetChanged();
    }

    public interface ListAllClickListeners {
        void onItemClick(Product productsList, int pos);

        void onRemoveCartClick(Product productsList, int pos);

        void onAddQuantityClick(int counter, String total, int price, int pos);

        void onRemoveQuantityClick(int counter, String total, int price, int pos);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        SelectableRoundedImageView imageView;
        @BindViews({R.id.nameTV, R.id.costTV, R.id.totalTV, R.id.counterTV})
        List<TextView> textViewList;
        TextView nameTV, costTV, totalTV, counterTV;
        @BindViews({R.id.addCounterIB, R.id.removeCounterIB, R.id.deleteIB})
        List<ImageButton> imageButtonList;
        ImageButton addCounterIB, removeCounterIB, deleteIB;
        int counter;
        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            listSharedPreference = new ListSharedPreference(context);
            //textView
            nameTV = textViewList.get(0);
            costTV = textViewList.get(1);
            totalTV = textViewList.get(2);
            counterTV = textViewList.get(3);

            //ImageButtons
            addCounterIB = imageButtonList.get(0);
            removeCounterIB = imageButtonList.get(1);
            deleteIB = imageButtonList.get(2);
            //
            counter = 1;
        }

        void onBind(final Product data) {
            Picasso.get().load(data.getMainPhoto())
                    .resize(500, 500)
                    .error(R.drawable.place_holder)
                    .into(imageView);

            if (getLang().equals("ar"))
                nameTV.setText(data.getNameAr());
            else
                nameTV.setText(data.getNameEn());
            costTV.setText(data.getPrice());
            counterTV.setText(String.valueOf(counter));
            totalTV.setText(String.valueOf(data.getPrice()));

            itemView.setOnClickListener(v -> listAllClickListeners.onItemClick(data, getAdapterPosition()));
        }

        private String getLang() {
            return listSharedPreference.getLanguage();
        }

        @OnClick(R.id.deleteIB)
        void onDeleteClick() {
            listAllClickListeners.onRemoveCartClick(getItem(getAdapterPosition()).getProduct(), getAdapterPosition());
        }

        @OnClick(R.id.addCounterIB)
        void onAddCounterClick() {
            Product product = getItem(getAdapterPosition()).getProduct();
            int quantity = 1000;
            int price = Integer.parseInt(product.getPrice());
            updateCounterAdd(counter);
            if (counter <= quantity) {
                counterTV.setText(String.valueOf(counter));
                String total = String.valueOf(price * counter);
                totalTV.setText(total);
                listAllClickListeners.onAddQuantityClick(counter, total, price, getAdapterPosition());
            } else {
                String total = String.valueOf(price);
                totalTV.setText(total);
                listAllClickListeners.onAddQuantityClick(counter, total, price, getAdapterPosition());
                Toast.makeText(context, R.string.max_reached_quantity, Toast.LENGTH_SHORT).show();
            }
        }

        @OnClick(R.id.removeCounterIB)
        void onRemoveCounterClick() {
            Product product = getItem(getAdapterPosition()).getProduct();
            int price = Integer.parseInt(product.getPrice());
            if (counter > 1) {
                updateCounterMinus(counter);
                counterTV.setText(String.valueOf(counter));
                if (counter != 1) {
                    String total = String.valueOf(price * counter);
                    totalTV.setText(total);
                    listAllClickListeners.onRemoveQuantityClick(counter, total, price, getAdapterPosition());
                } else {
                    String total = String.valueOf(price);
                    totalTV.setText(total);
                    counterTV.setText("1");
                    listAllClickListeners.onRemoveQuantityClick(1, total, price, getAdapterPosition());
//                    Toast.makeText(context, R.string.min_reached_quantity, Toast.LENGTH_SHORT).show();
                }
            }
        }

        private void updateCounterAdd(int counter) {
            this.counter = counter + 1;
        }

        private void updateCounterMinus(int counter) {
            this.counter = counter - 1;
        }


    }
}