package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.activities.view.products.Datum;

import java.security.InvalidParameterException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 25/04/2018.
 */
public class SubActivityAdapter extends RecyclerView.Adapter<SubActivityAdapter.MyViewHolder> {
    private List<Datum> data;

    private ListAllListeners listAllListeners;

    public SubActivityAdapter(@NonNull List<Datum> data, ListAllListeners listAllListeners) {
        this.data = data;
        this.listAllListeners = listAllListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item_activities_view, parent, false);
        Context context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Datum datum = data.get(position);
        holder.onBind(datum);
        holder.setClicks(datum);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void replaceData(List<Datum> movies) {
        this.data.clear();
        this.data.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.data.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= data.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return data.get(position);
    }

    public void clearData() {
        data.clear();
        notifyDataSetChanged();
    }

    public interface ListAllListeners {
        void onItemClick(Datum data, int pos);
    }

   public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        SelectableRoundedImageView imageView;
        @BindView(R.id.nameTV)
        TextView nameTV;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void onBind(Datum datum) {
            Picasso.get().load(datum.getPhoto())
                    .error(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .into(imageView);

            nameTV.setText(datum.getNameAr());
        }

        void setClicks(final Datum datum) {
            itemView.setOnClickListener(v -> listAllListeners.onItemClick(datum, getAdapterPosition()));
       }
   }//end class MyViewHolder
}