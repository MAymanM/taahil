package com.tkmsoft.taahel.adapters.drugs;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.drugs.view.Datum;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Created by MahmoudAyman on 25/04/2018.
 */
public class DrugsAdapter extends RecyclerView.Adapter<DrugsViewHolder> {
    private List<Datum> datumList;
    private ListAllClickListeners listAllClickListeners;

    public DrugsAdapter(@NonNull List<Datum> datumList, ListAllClickListeners listAllClickListeners) {
        this.datumList = datumList;
        this.listAllClickListeners = listAllClickListeners;
    }

    @NonNull
    @Override
    public DrugsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.single_item_centers_view, parent, false);
        Context c = parent.getContext();
        return new DrugsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DrugsViewHolder holder, int position) {
        Datum movie = datumList.get(position);
        OnBind(movie, holder);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void replaceData(List<Datum> movies) {
        this.datumList.clear();
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= datumList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return datumList.get(position);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }

    private void OnBind(Datum data, final DrugsViewHolder viewHolder) {
        Picasso.get().load(data.getPhoto())
                .error(R.drawable.place_holder)
                .placeholder(R.drawable.place_holder)
                .into(viewHolder.imageView);
        viewHolder.nameTV.setText(data.getNameAr());

        viewHolder.itemView.setOnClickListener(v -> listAllClickListeners.onItemClick(getItem(viewHolder.getAdapterPosition()), viewHolder.getAdapterPosition()));
    }


    public interface ListAllClickListeners {
        void onItemClick(Datum datumList, int pos);
    }
}