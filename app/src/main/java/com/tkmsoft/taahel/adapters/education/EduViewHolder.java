package com.tkmsoft.taahel.adapters.education;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.education.view.Datum;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 10/10/2018.
 **/
class EduViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.imageView)
    SelectableRoundedImageView imageView;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.item_progressBar)
    ProgressBar progressBar;

    EduViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    void onBind(Datum movie, EduViewHolder holder) {
        holder.nameTV.setText(movie.getNameAr());
        Picasso.get().load(movie.getPhoto()).error(R.drawable.place_holder).into(imageView);
    }
}
