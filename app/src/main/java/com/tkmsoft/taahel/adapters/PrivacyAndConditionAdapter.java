package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.PrivacyAndConditionModel;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.security.InvalidParameterException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 10/19/2018.
 **/
public class PrivacyAndConditionAdapter extends RecyclerView.Adapter<PrivacyAndConditionAdapter.MyViewHolder> {
    private List<PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean> datumList;
    private ListAllClickListeners listAllClickListeners;
    private ListSharedPreference listSharedPreference;

    public PrivacyAndConditionAdapter(Context appContext,List<PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean> datumList, ListAllClickListeners listAllClickListeners) {
        this.datumList = datumList;
        this.listAllClickListeners = listAllClickListeners;
        listSharedPreference = new ListSharedPreference(appContext);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.sigle_item_privacy_and_condition_view, parent, false);
        Context context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean movie = datumList.get(position);
        holder.onBind(movie);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void replaceData(List<PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean> movies) {
        this.datumList.clear();
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean> movies) {
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean getItem(int position) {
        if (position < 0 || position >= datumList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return datumList.get(position);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameTV)
        TextView nameTV;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        void onBind(PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean movie) {
            if (listSharedPreference.getLanguage().equals("en"))
                nameTV.setText(movie.getCon_en());
            else
                nameTV.setText(movie.getCon_en());
            itemView.setOnClickListener(view -> listAllClickListeners.onItemClick(getItem(getAdapterPosition()), getAdapterPosition()));
        }
    }//end class MyViewHolder

    public interface ListAllClickListeners {
        void onItemClick(PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean datumList, int pos);
    }
}
