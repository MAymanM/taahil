package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.ReviewsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by MahmoudAyman on 25/04/2018.
 */
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {
    private ListAllListeners listAllListeners;
    private ArrayList<ReviewsModel> mDataList;

    public ReviewsAdapter(Context context, ArrayList<ReviewsModel> mDataList,
                          ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_review_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mDataList != null ? mDataList.size() : 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        ReviewsModel reviewsModel = mDataList.get(position);
        holder.bind(reviewsModel);
    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(ReviewsModel reviewsModel, int adapterPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        CircleImageView imageView;
        @BindViews({R.id.nameTV, R.id.commentTV})
        List<TextView> textViews;
        @BindView(R.id.rateBar)
        RatingBar ratingBar;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(final ReviewsModel reviewsModel) {
            textViews.get(0).setText(reviewsModel.getName());
            textViews.get(1).setText(reviewsModel.getComment());

            Picasso.get().load(reviewsModel.getImage())
                    .error(R.drawable.place_holder)
                    .into(imageView);

            ratingBar.setRating(Float.parseFloat(reviewsModel.getRate()));

            itemView.setOnClickListener(view ->
                    listAllListeners.onItemViewClick(reviewsModel, getAdapterPosition())
            );
        }
    }//end class MyViewHolder

}//end class