package com.tkmsoft.taahel.adapters.activities;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.activities.view.category.ActivityCategModel;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 25/04/2018.
 */
public class ActivitiesCategAdapter extends RecyclerView.Adapter<ActivitiesCategAdapter.MyViewHolder> {
    private Context context;

    private ListAllListeners listAllListeners;

    private List<ActivityCategModel.DataBean.CategoriesBean> mDataList;
    List<Integer> images;

    public ActivitiesCategAdapter(Context context, List<Integer> images,
                                  List<ActivityCategModel.DataBean.CategoriesBean> mDataList,
                                  ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        this.images = images;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_activities_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mDataList != null ? mDataList.size() : 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        ListSharedPreference listSharedPreference = new ListSharedPreference(context);

        holder.nameTV.setText(mDataList.get(holder.getAdapterPosition()).getName_ar());

            Picasso.get()
                    .load(images.get(holder.getAdapterPosition()))
                    .error(R.drawable.place_holder)
                    .into(holder.imageView);

        holder.itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(holder.getAdapterPosition()), holder.getAdapterPosition()));
    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(ActivityCategModel.DataBean.CategoriesBean categoriesBean, int adapterPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        SelectableRoundedImageView imageView;
        @BindView(R.id.nameTV)
        TextView nameTV;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }//end class MyViewHolder

}//end class