package com.tkmsoft.taahel.adapters.requests.mostafed;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.order.patient.Order;

import java.util.List;

/**
 * Created by MahmoudAyman on 12/09/2018.
 */
public class SubRequestAdapterPatient extends RecyclerView.Adapter<SubRequestAdapterPatient.MyViewHolder>{

    private ListAllListeners listAllListeners;
    private List<Order> mDataList;

    public SubRequestAdapterPatient(Context context, List<Order> mDataList,
                               ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_sub_request_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.bind();
    }//end onBindViewHolder()

    public interface ListAllListeners {
        void onItemPatientOrderClick(Order order, int adapterPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView codeTV, dateTV;

        MyViewHolder(View itemView) {
            super(itemView);
            codeTV = itemView.findViewById(R.id.codeTV);
            dateTV = itemView.findViewById(R.id.dateTV);
        }

        public void bind() {

            codeTV.setText(String.valueOf(mDataList.get(getAdapterPosition()).getId()));
            String[] splitStr = mDataList.get(getAdapterPosition()).getDate().split("\\s+");
            dateTV.setText(splitStr[0]);
            itemView.setOnClickListener(view -> listAllListeners.onItemPatientOrderClick(mDataList.get(getAdapterPosition()), getAdapterPosition()));
        }
    }//end class MyViewHolder
}
