package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.TherapistDetailDeptsModel;

import java.util.ArrayList;

/**
 * Created by MahmoudAyman on 29/05/2018.
 */
public class TherapistDetailDeptAdapter extends RecyclerView.Adapter<TherapistDetailDeptAdapter.MyViewHolder>{
    private ArrayList<TherapistDetailDeptsModel> mDataList;


    public TherapistDetailDeptAdapter(Context context, ArrayList<TherapistDetailDeptsModel> mDataList) {
        this.mDataList = mDataList;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_thearapist_detail_dept_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        holder.radioButton.setText(mDataList.get(holder.getAdapterPosition()).getDept());

    }//end onBindViewHolder()



    class MyViewHolder extends RecyclerView.ViewHolder {

        RadioButton radioButton;

        MyViewHolder(View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.radioButton);
        }
    }//end class MyViewHolder

}//end class