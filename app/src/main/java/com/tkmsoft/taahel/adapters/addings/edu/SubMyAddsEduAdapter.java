package com.tkmsoft.taahel.adapters.addings.edu;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.adds.edu.Datum;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.security.InvalidParameterException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 27/01/2019.
 */

public class SubMyAddsEduAdapter extends RecyclerView.Adapter<SubMyAddsEduAdapter.MyViewHolder> {
    private List<Datum> datumList;
    private Context context;
    private ListAllClickListeners listAllClickListeners;

    public SubMyAddsEduAdapter(Context context, List<Datum> datumList, ListAllClickListeners listAllClickListeners) {
        this.datumList = datumList;
        this.listAllClickListeners = listAllClickListeners;
        this.context = context;
        ListSharedPreference listSharedPreference = new ListSharedPreference(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.single_item_myadds_education_view, parent, false);
        context = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Datum movie = datumList.get(position);
        holder.OnBind(movie);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void replaceData(List<Datum> movies) {
        this.datumList.clear();
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= datumList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return datumList.get(position);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
         SelectableRoundedImageView imageView;
        @BindView(R.id.nameTV)
         TextView nameTV;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void OnBind(Datum data) {

            Picasso.get().load(data.getPhoto())
                    .error(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .into(imageView);

            nameTV.setText(data.getNameAr());

            itemView.setOnClickListener(v -> listAllClickListeners.onItemClick(getItem(getAdapterPosition()),getAdapterPosition()));
        }

    }
    public interface ListAllClickListeners {
        void onItemClick(Datum datumList, int pos);
    }
}