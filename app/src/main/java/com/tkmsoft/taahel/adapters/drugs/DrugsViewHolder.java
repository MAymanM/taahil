package com.tkmsoft.taahel.adapters.drugs;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.tkmsoft.taahel.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 10/10/2018.
 **/
class DrugsViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.imageView)
    SelectableRoundedImageView imageView;
    @BindView(R.id.nameTV)
    TextView nameTV;

    DrugsViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
