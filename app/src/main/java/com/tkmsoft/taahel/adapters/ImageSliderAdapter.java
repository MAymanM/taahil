package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.SliderModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MahmoudAyman on 25/04/2018.
 */
public class ImageSliderAdapter extends PagerAdapter {

    private List<SliderModel.DataBean.SlidersBean> mDataList;
    private LayoutInflater inflater;

    public ImageSliderAdapter(Context context, List<SliderModel.DataBean.SlidersBean> mDataList, ListAllListeners listAllListeners) {
        Context context1 = context;
        this.mDataList = mDataList;
        inflater = LayoutInflater.from(context);
        ListAllListeners listAllListeners1 = listAllListeners;
    }

    public interface ListAllListeners {
        void onItemViewClick(ArrayList<SliderModel.DataBean.SlidersBean> images, int position);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.single_image_slide_view, view, false);

        ImageView myImage = myImageLayout.findViewById(R.id.image);

        Picasso.get()
                .load(mDataList.get(position).getPhoto())
                .placeholder(R.drawable.place_holder)
                .error(R.drawable.place_holder)
                .into(myImage);


        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
}
