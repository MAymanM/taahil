package com.tkmsoft.taahel.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.PurchaseDetailModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by MahmoudAyman on 16/05/2018.
 */
public class PurchaseDetailAdapter extends RecyclerView.Adapter<PurchaseDetailAdapter.MyViewHolder> {
    private ListAllListeners listAllListeners;
    private ArrayList<PurchaseDetailModel> mDataList;


    public PurchaseDetailAdapter(Context context, ArrayList<PurchaseDetailModel> mDataList, ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_purchase_detail_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {


        Picasso.get()
                .load(mDataList.get(holder.getAdapterPosition()).getImage())
                .into(holder.imageView);

        holder.priceTV.setText(mDataList.get(holder.getAdapterPosition()).getPrice());
        holder.nameTV.setText(mDataList.get(holder.getAdapterPosition()).getName());
        holder.quantityTV.setText(mDataList.get(holder.getAdapterPosition()).getQuantity());
        holder.totalTV.setText(mDataList.get(holder.getAdapterPosition()).getTotal());

        holder.itemView.setOnClickListener(view -> listAllListeners.onItemViewClick(mDataList.get(holder.getAdapterPosition()),
                holder.getAdapterPosition()));
    }//end onBindViewHolder()


    public interface ListAllListeners {
        void onItemViewClick(PurchaseDetailModel purchaseDetailModel, int adapterPosition);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView nameTV, priceTV, quantityTV, totalTV;

        MyViewHolder(View itemView) {
            super(itemView);
            priceTV = itemView.findViewById(R.id.priceTV);
            nameTV = itemView.findViewById(R.id.nameTV);
            quantityTV = itemView.findViewById(R.id.quantityTV);
            totalTV = itemView.findViewById(R.id.totalTV);

            imageView = itemView.findViewById(R.id.imageView);
        }
    }//end class MyViewHolder

}//end class