package com.tkmsoft.taahel.adapters.reports;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.reports.patient.permission.Datum;

import java.security.InvalidParameterException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 30/09/2018.
 */
public class ReportsPermissionAdapter extends RecyclerView.Adapter<ReportsPermissionAdapter.MyViewHolder> {
    private ListAllListeners listAllListeners;
    private List<Datum> mDataList;

    public ReportsPermissionAdapter(Context context, List<Datum> mDataList, ListAllListeners listAllListeners) {
        this.mDataList = mDataList;
        this.listAllListeners = listAllListeners;
        Context context1 = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.single_item_reports_permisiion_view, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        if (mDataList != null)
            return mDataList.size();
        else
            return 0;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.bind();
    }//end onBindViewHolder()

    public interface ListAllListeners {

        void onSwitchChangeListener(Datum data, int adapterPosition, boolean isChecked);

    }

    private Datum getItem(int position){
        if (position < 0 || position >= mDataList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return mDataList.get(position);
    }
    
    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.switchBtn)
        Switch switchBtn;
        @BindView(R.id.nameTV)
        TextView nameTV;
//        @BindView(R.id.codeTV)
//        TextView codeTV;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind() {
            nameTV.setText(getItem(getAdapterPosition()).getDoctor().getName());
//            codeTV.setText(String.valueOf(getItem(getAdapterPosition()).getId()));

            if (getItem(getAdapterPosition()).getApprove().equals("1")) {
                switchBtn.setChecked(true);
            } else
                switchBtn.setChecked(false);

            switchBtn.setOnCheckedChangeListener((buttonView, isChecked) -> listAllListeners.onSwitchChangeListener(getItem(getAdapterPosition()), getAdapterPosition(), isChecked));
        }


    }//end class MyViewHolder
}
