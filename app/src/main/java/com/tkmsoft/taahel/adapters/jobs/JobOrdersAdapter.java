package com.tkmsoft.taahel.adapters.jobs;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.model.api.joborders.Datum;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 05/02/2019.
 */
public class JobOrdersAdapter extends RecyclerView.Adapter<JobOrdersAdapter.MyViewHolder> {
    private List<Datum> datumList;
    private ListAllClickListeners listAllClickListeners;

    public JobOrdersAdapter(@NonNull ArrayList<com.tkmsoft.taahel.model.api.joborders.Datum> datumList, ListAllClickListeners listAllClickListeners) {
        this.datumList = datumList;
        this.listAllClickListeners = listAllClickListeners;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.single_item_centers_view, parent, false);
        Context c = parent.getContext();
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Datum movie = datumList.get(position);
        holder.onBind(movie);
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public void replaceData(List<Datum> movies) {
        this.datumList.clear();
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public void updateData(List<Datum> movies) {
        this.datumList.addAll(movies);
        notifyDataSetChanged();
    }

    public Datum getItem(int position) {
        if (position < 0 || position >= datumList.size()) {
            throw new InvalidParameterException("INVALID IDX");
        }
        return datumList.get(position);
    }

    public void clearData() {
        datumList.clear();
        notifyDataSetChanged();
    }



    public interface ListAllClickListeners {
        void onItemClick(Datum datumList, int pos);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView)
        SelectableRoundedImageView imageView;
        @BindView(R.id.nameTV)
        TextView nameTV;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


         void onBind(Datum data) {
            Picasso.get().load(data.getPhoto())
                    .error(R.drawable.place_holder)
                    .placeholder(R.drawable.place_holder)
                    .into(imageView);

            nameTV.setText(data.getName());

            itemView.setOnClickListener(v -> listAllClickListeners.onItemClick(getItem(getAdapterPosition()), getAdapterPosition()));
        }

    }
}