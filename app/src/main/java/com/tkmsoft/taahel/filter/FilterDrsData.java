package com.tkmsoft.taahel.filter;

import com.tkmsoft.taahel.adapters.DoctorsAdapter;
import com.tkmsoft.taahel.model.api.getDoctors.Datum;
import com.tkmsoft.taahel.model.api.getDoctors.Specialization;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MahmoudAyman on 10/21/2018.
 **/
public class FilterDrsData {
    private String countryCode;
    private String cityCode;
    private String rate;
    private String genderCode;
    private String specialtyId;
    private DoctorsAdapter doctorsAdapter;

    public FilterDrsData(String countryCode, String cityCode, String rate, String genderCode, String specialtyId, DoctorsAdapter doctorsAdapter) {
        this.countryCode = countryCode;
        this.cityCode = cityCode;
        this.rate = rate;
        this.genderCode = genderCode;
        this.specialtyId = specialtyId;
        this.doctorsAdapter = doctorsAdapter;
    }

    public void attemptFilter(List<Datum> results, int pos) {
        int rateInt;
        if (!rate.equals("null"))
            rateInt = Integer.valueOf(rate);
        else
            rateInt = 0;
        List<Datum> newArray = new ArrayList<>();
        for (int i = 0; i < results.size(); i++) {
            //set into vars
            String resCountry = String.valueOf(results.get(i).getCountry().getId());
            String resCity = String.valueOf(results.get(i).getCity().getId());
            String resGender = results.get(i).getGender();
            List<Specialization> resSpecialList = results.get(i).getSpecializations();
            int resRate = (results.get(i).getRate()).intValue();
            String resSpecialtyId;
            //begin filter
            if (!countryCode.equals("null")) {
                if (!cityCode.equals("null")) {
                    if (!rate.equals("null")) {
                        if (!genderCode.equals("null")) {
                            if (!specialtyId.equals("null")) {
                                if (resCountry.equals(countryCode)) {
                                    if (resCity.equals(cityCode)) {
                                        if (resRate >= rateInt) {
                                            if (resGender.equals(genderCode)) {
                                                for (int s = 0; s < resSpecialList.size(); s++) {
                                                    resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                                                    if (resSpecialtyId.equals(specialtyId)) {
                                                        newArray.add(results.get(i));
                                                    }
                                                }//end for
                                            }
                                        }
                                    }
                                }
                            }//end C & c & rate & gender & special
                            else if (resCountry.equals(countryCode)) {
                                if (resCity.equals(cityCode)) {
                                    if (resRate >= rateInt) {
                                        if (resGender.equals(genderCode)) {
                                            newArray.add(results.get(i));
                                        }
                                    }
                                }
                            }
                        }//end C & c & rate & gender
                        else if (!specialtyId.equals("null")) {
                            if (resCountry.equals(countryCode)) {
                                if (resCity.equals(cityCode)) {
                                    if (resGender.equals(genderCode)) {
                                        for (int s = 0; s < resSpecialList.size(); s++) {
                                            resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                                            if (resSpecialtyId.equals(specialtyId)) {
                                                newArray.add(results.get(i));
                                            }
                                        }
                                    }
                                }
                            }//end for
                        }//end C & c & rate & special
                        else if (resCity.equals(cityCode) &&
                                resCountry.equals(countryCode) &&
                                resRate >= rateInt) {

                            newArray.add(results.get(i));
                        }
                    }//end C & c & rate
                    else if (!genderCode.equals("null")) {
                        if (!specialtyId.equals("null")) {
                            if (resCountry.equals(countryCode)) {
                                if (resCity.equals(cityCode)) {
                                    if (resGender.equals(genderCode)) {
                                        for (int s = 0; s < resSpecialList.size(); s++) {
                                            resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                                            if (resSpecialtyId.equals(specialtyId)) {
                                                newArray.add(results.get(i));
                                            }
                                        }
                                    }
                                }
                            }//end for
                        }//C & c & gender & special
                        else if (resCountry.equals(countryCode)) {
                            if (resCity.equals(cityCode)) {
                                if (resGender.equals(genderCode)) {
                                    newArray.add(results.get(i));
                                }
                            }
                        }// put C & c & gender.
                    }// end C & c & gender.
                    else if (!specialtyId.equals("null")) {
                        if (resCountry.equals(countryCode)) {
                            if (resCity.equals(cityCode)) {
                                for (int s = 0; s < resSpecialList.size(); s++) {
                                    resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                                    if (resSpecialtyId.equals(specialtyId)) {
                                        newArray.add(results.get(i));
                                    }
                                }
                            }
                        }
                    }// end C & c & special
                    else if (resCountry.equals(countryCode)) {
                        if (resCity.equals(cityCode)) {
                            newArray.add(results.get(i));
                        }
                    }//put C & c
                }//end C & c
                else if (!rate.equals("null")) {
                    if (resCountry.equals(countryCode)) {
                        if (resRate >= rateInt) {
                            newArray.add(results.get(i));
                        }
                    }
                }//end C & rate
                else if (!genderCode.equals("null")) {
                    if (resCountry.equals(countryCode) &&
                            resGender.equals(genderCode)) {
                        newArray.add(results.get(i));
                    }
                }//end C & gender
                else if (!specialtyId.equals("null")) {
                    for (int s = 0; s < resSpecialList.size(); s++) {
                        resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                        if (resCountry.equals(countryCode) &&
                                resSpecialtyId.equals(specialtyId)) {
                            newArray.add(results.get(i));
                        }
                    }
                }//end C & special
                else if (resCountry.equals(countryCode)) {
                    newArray.add(results.get(i));
                }//put C
            }//end C
            else if (!rate.equals("null")) {
                if (!genderCode.equals("null")) {
                    if (!specialtyId.equals("null")) {
                        if (resRate >= rateInt) {
                            if (resGender.equals(genderCode)) {
                                for (int s = 0; s < resSpecialList.size(); s++) {
                                    resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                                    if (resSpecialtyId.equals(specialtyId)) {
                                        newArray.add(results.get(i));
                                    }
                                }
                            }
                        }
                    }//end rate & gender & special
                    else if (resRate >= rateInt) {
                        if (resGender.equals(genderCode)) {
                            newArray.add(results.get(i));
                        }
                    }
                }// end rate & gender
                else if (!specialtyId.equals("null")) {
                    if (resRate >= rateInt) {
                        for (int s = 0; s < resSpecialList.size(); s++) {
                            resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                            if (resSpecialtyId.equals(specialtyId)) {
                                newArray.add(results.get(i));
                            }
                        }
                    }
                }//end rate & special
                else if (resRate >= rateInt) {
                    newArray.add(results.get(i));
                }//end put rate
            }//end rate
            else if (!genderCode.equals("null")) {
                if (!specialtyId.equals("null")) {
                    if (resGender.equals(genderCode)) {
                        for (int s = 0; s < resSpecialList.size(); s++) {
                            resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                            if (resSpecialtyId.equals(specialtyId)) {
                                newArray.add(results.get(i));
                            }
                        }
                    }
                }//end gender & special
                else if (resGender.equals(genderCode)) {
                    newArray.add(results.get(i));
                }//end put gender
            }//end gender
            else if (!specialtyId.equals("null")) {
                for (int s = 0; s < resSpecialList.size(); s++) {
                    resSpecialtyId = String.valueOf(resSpecialList.get(s).getId());
                    if (resSpecialtyId.equals(specialtyId)) {
                        newArray.add(results.get(i));
                    }
                }//end put special
            }//end special
        }//end for loop all
        if (pos == 1)
            doctorsAdapter.replaceData(newArray);
        else
            doctorsAdapter.updateData(newArray);
    }

}
