package com.tkmsoft.taahel.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.ReportsPatientProfileAdapter;
import com.tkmsoft.taahel.adapters.reports.ReportsAdapter;
import com.tkmsoft.taahel.model.api.order.GetPermissionModel;
import com.tkmsoft.taahel.model.api.reports.patient.profile.Datum;
import com.tkmsoft.taahel.model.api.reports.patient.profile.PatientOrderProfileModel;
import com.tkmsoft.taahel.model.api.reports.patient.profile.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.application.MyContextWrapper;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientOrderProfileActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.imageView)
    CircleImageView imageView;
    @BindView(R.id.genderTV)
    TextView genderTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.emailTV)
    TextView emailTV;
    @BindView(R.id.mobileTV)
    TextView mobileTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.sendBtn)
    Button sendBtn;
    @BindView(R.id.card_reports)
    CardView card_reports;

    //    private ArrayList<ReportsModel> mDataList;
    private ReportsAdapter reportsAdapter;
    private String TAG = getClass().getSimpleName();
    private ListSharedPreference listSharedPreference;
    private String gender, avatar, birthdate, name, email, phone, country, city;
    private String pId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_order_profile);
        listSharedPreference = new ListSharedPreference(PatientOrderProfileActivity.this.getApplicationContext());
        ButterKnife.bind(this);
        setLayoutLanguage(listSharedPreference.getLanguage());
        initUI();

    }

    private void setLayoutLanguage(String language) {
        if (language.equals("ar"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase, "ar"));
    }

    private void getData() throws NullPointerException {
        Intent intent = getIntent();
        pId = intent.getStringExtra("pId");
        gender = intent.getStringExtra("gender");
        avatar = intent.getStringExtra("avatar");
        birthdate = intent.getStringExtra("birthdate");
        name = intent.getStringExtra("name");
        email = intent.getStringExtra("email");
        phone = intent.getStringExtra("phone");
        country = intent.getStringExtra("country");
        city = intent.getStringExtra("city");
    }

    private void initUI() {
//        Toast.makeText(this, "" + gender, Toast.LENGTH_SHORT).show();
        getData();
        setData();
        initRecyclerView();
    }

    @Override
    protected void onStart() {
        checkForReports();
        super.onStart();
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(PatientOrderProfileActivity.this, 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(PatientOrderProfileActivity.this, 1));
        }
    }

    private void checkForReports() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
            Call<PatientOrderProfileModel> registerCall = retrofit.checkForReport(listSharedPreference.getToken(), pId);
//            Log.d(TAG, "serverComplete: " + "orderId: " + orderId + "\n notes: " + notesRB);
            registerCall.enqueue(new Callback<PatientOrderProfileModel>() {
                @Override
                public void onResponse(@NonNull Call<PatientOrderProfileModel> call, @NonNull Response<PatientOrderProfileModel> response) {
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Status status = response.body().getStatus();
                                if (status.getType().equals("success")) {
//                                    Toast.makeText(PatientOrderProfileActivity.this, response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                    initAdapter(response.body().getData().getData());
                                    sendBtn.setVisibility(View.GONE);
                                    card_reports.setVisibility(View.VISIBLE);
                                } else {
                                    if (status.getTitle().equals("تم ارسال الطلب من قبل")) {
                                        sendBtn.setBackgroundResource(R.drawable.btn_bground_disable);
                                        sendBtn.setTextColor(getResources().getColor(R.color.colorAccent));
                                        sendBtn.setText(getString(R.string.done_sending_request));
                                        sendBtn.setClickable(false);
                                    } else {
                                        Toast.makeText(PatientOrderProfileActivity.this, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                        sendBtn.setBackgroundResource(R.drawable.btn_bground);
                                        sendBtn.setTextColor(getResources().getColor(R.color.white));
                                        sendBtn.setText(getString(R.string.request_report_permission));
                                        sendBtn.setClickable(true);
                                        sendBtn.setVisibility(View.VISIBLE);
                                        card_reports.setVisibility(View.GONE);
                                    }
                                }
                            }
                        }
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<PatientOrderProfileModel> call, @NonNull Throwable t) {
                    try {
//                        Toast.makeText(PatientOrderProfileActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                        Log.d(TAG, "on fail: " + t.getMessage());
                    } catch (Exception r) {
                        Log.d(TAG, "excepetion: " + r.getMessage());
                    }
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception exc) {
//            Toast.makeText(PatientOrderProfileActivity.this, "" + exc, Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void initAdapter(List<Datum> data) {
        ReportsPatientProfileAdapter reportsPAdapter = new ReportsPatientProfileAdapter(this, data,
                new ReportsPatientProfileAdapter.ListAllListeners() {
                    @Override
                    public void onItemViewClick(Datum reportsModel, int adapterPosition) {

                    }

                    @Override
                    public void onDownloadClick(String repLink, int adapterPosition) {

                    }
                });

        recyclerView.setAdapter(reportsPAdapter);
        reportsPAdapter.notifyDataSetChanged();
    }

    private void setData() {
        genderTV.setText(gender);
        try {
            if (URLUtil.isValidUrl(avatar))
                Glide.with(PatientOrderProfileActivity.this)
                        .load(avatar).into(imageView);
            else
                imageView.setImageResource(R.drawable.place_holder);
        } catch (Exception e) {
            Toast.makeText(this, "setuserdata: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            imageView.setImageResource(R.drawable.place_holder);
        }
        dateTV.setText(birthdate);
        nameTV.setText(name);
        emailTV.setText(email);
        mobileTV.setText(phone);
        countryTV.setText(country);
        cityTV.setText(city);
    }

    @OnClick(R.id.sendBtn)
    protected void onRequest_permissionBtnClick() {
        serverGetReportRequest();
    }

    private void serverGetReportRequest() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
            Call<GetPermissionModel> registerCall = retrofit.getPermission(listSharedPreference.getToken(), pId);
//            Log.d(TAG, "serverComplete: " + "orderId: " + orderId + "\n notes: " + notesRB);
            registerCall.enqueue(new Callback<GetPermissionModel>() {
                @Override
                public void onResponse(@NonNull Call<GetPermissionModel> call, @NonNull Response<GetPermissionModel> response) {
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getStatus().getType().equals("success")) {
                                    Toast.makeText(PatientOrderProfileActivity.this, response.body().getStatus().getTitle(),
                                            Toast.LENGTH_SHORT).show();
                                    sendBtn.setBackgroundResource(R.drawable.btn_bground_disable);
                                    sendBtn.setTextColor(getResources().getColor(R.color.colorAccent));
                                    sendBtn.setText(getString(R.string.done_sending_request));
                                    sendBtn.setClickable(false);
                                } else
                                    Toast.makeText(PatientOrderProfileActivity.this, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception exc) {
                        Toast.makeText(PatientOrderProfileActivity.this, "" + exc.getMessage(), Toast.LENGTH_SHORT).show();
                        exc.printStackTrace();
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<GetPermissionModel> call, @NonNull Throwable t) {
                    try {
                        Toast.makeText(PatientOrderProfileActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                        Log.d(TAG, "on fail: " + t.getMessage());
                    } catch (Exception r) {
                        Log.d(TAG, "excepetion: " + r.getMessage());
                    }
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception exc) {
            Toast.makeText(PatientOrderProfileActivity.this, "" + exc, Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    }

}
