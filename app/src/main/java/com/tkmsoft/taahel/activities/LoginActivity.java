package com.tkmsoft.taahel.activities;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.fragments.login.LoginFragment;
import com.tkmsoft.taahel.interfaces.IOnBackPressed;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyContextWrapper;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    private ListSharedPreference listSharedPreference;
    protected IOnBackPressed onBackPressedListener;
    private String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setLayoutLanguage(getLang());
        ButterKnife.bind(LoginActivity.this);
        MoveToFragment moveFragment = new MoveToFragment(this);
        moveFragment.moveInLogin(new LoginFragment());
    }

    private void setLayoutLanguage(String language) {
        if (language.equals("ar"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        listSharedPreference = new ListSharedPreference(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, getLang()));
    }

    private String getLang() {
        return listSharedPreference.getLanguage();
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null)
            onBackPressedListener.onBackPressed();
        else
            super.onBackPressed();
    }

    public void setOnBackPressedListener(IOnBackPressed onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }
}
