package com.tkmsoft.taahel.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends Activity {

    private boolean isFirstRun;
    ListSharedPreference listSharedPreference;

    @BindView(R.id.splach_screen)
    ImageView imageView;
    @BindViews({R.id.btn_en, R.id.btn_ar})
    List<Button> buttonList;
    private boolean isLogged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        ButterKnife.bind(SplashActivity.this);
        listSharedPreference = new ListSharedPreference(SplashActivity.this);
        isFirstRun = listSharedPreference.getIsFirstRun();
        isLogged = listSharedPreference.getIsLogged();

        initUI();

        RelativeLayout relativeLayout = findViewById(R.id.splash_rela);
        relativeLayout.setBackgroundResource(R.color.colorPrimary);

        insertAnimation();
    }

    private void initUI() {
        Button button_en = buttonList.get(0);
        Button button_ar = buttonList.get(1);
        Picasso.get().load(R.drawable.app_logo).into(imageView);
    }


    @OnClick(R.id.btn_en)
    void onBtnEnClick() {
        listSharedPreference.setLanguage("en");
        listSharedPreference.setIsFirstRun(false);
        goToActivity(new LoginActivity());
    }

    @OnClick(R.id.btn_ar)
    void onBtnArClick() {
        listSharedPreference.setLanguage("ar");
        listSharedPreference.setIsFirstRun(false);

        goToActivity(new LoginActivity());
    }

    private void insertAnimation() {
        Animation animation = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.splash_anim);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);

        ImageView splash = findViewById(R.id.splach_screen);
        splash.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                showHideButtons();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }//end insert anim

    private void proceedToTransact() {
        if (isLogged) {
            goToActivity(new MainActivity());
        } else {
            goToActivity(new LoginActivity());
        }
    }

    private void showHideButtons() {
        if (isFirstRun) {
//            button_ar.setVisibility(View.VISIBLE);
//            button_en.setVisibility(View.VISIBLE);
            listSharedPreference.setLanguage("ar");
            listSharedPreference.setIsFirstRun(false);

            goToActivity(new LoginActivity());
        } else {
//            button_ar.setVisibility(View.GONE);
//            button_en.setVisibility(View.GONE);
            proceedToTransact();
        }
    }

    private void goToActivity(Activity activity) {
        Intent intent = new Intent(MyApp.getContext().getApplicationContext(), activity.getClass());
        startActivity(intent);
        finish();
    }


}
