package com.tkmsoft.taahel.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.application.MyContextWrapper;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.DialogueHelper;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;
import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.os.Build.VERSION_CODES.M;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MapsActivity extends FragmentActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMyLocationButtonClickListener {

    private GoogleMap mMap;
    private MarkerOptions markerOptions = new MarkerOptions();
    private Marker marker;
    private Double currentLat, currentLongX, newLat, newLong;
    private String currentAddress, newAddress;
    ListSharedPreference listSharedPreference;
    @BindView(R.id.cardView)
    CardView cardView;
    private static final String TAG = MapsActivity.class.getName();
    DialogueHelper dialogueHelper;

    private GoogleApiClient mGoogleApiClient;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationRequest mLocationRequest;
    private ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setLayoutLanguage(listSharedPreference.getLanguage());
        ButterKnife.bind(this);
        listSharedPreference = new ListSharedPreference(this);
        dialogueHelper = new DialogueHelper(this);
        connectionDetector = new ConnectionDetector(this);
        cardView.setVisibility(GONE);
        initGService();
        initGMaps();

    }

    private boolean isGPSEnabled() {
        try {
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            assert manager != null;
            return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            Log.e(TAG, "isGPSEnabled: " + e.getMessage());
            return false;
        }
    }

    private void initGService() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds

        turnFindLocationOn();
    }

    private void setLayoutLanguage(String language) {
        if (language.equals("ar"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        listSharedPreference = new ListSharedPreference(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, listSharedPreference.getLanguage()));
    }

    private void turnFindLocOff() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    private void turnFindLocationOn() {
        if (connectionDetector.isConnectingToInternet()) {
            if (isGPSEnabled()) {
                mGoogleApiClient.connect();
            } else {
                dialogueHelper.showGPSEnableDialogue();
            }
        } else {
            Toast.makeText(this, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initGMaps() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        newLat = latLng.latitude;
        newLong = latLng.longitude;
        setMarkerAtLoc(latLng);
        cardView.setVisibility(VISIBLE);
    }

    @OnClick({R.id.proceedBtn, R.id.cancelBtn})
    void onButtonsClick(Button button) {
        switch (button.getId()) {
            case R.id.proceedBtn:
                listSharedPreference.setLat(marker.getPosition().latitude);
                listSharedPreference.setLong(marker.getPosition().longitude);
                //listSharedPreference.setLocAddress(newAddress);
                Log.e(TAG, "onButtonsClick: " +
                        "\n newLat:" + newLat +
                        "\n newlong:" + newLong
                        + "\ncurrent lat=" + currentLat +
                        "\ncurrent longX=" + currentLongX +
                        "\nmarkerLat: " + marker.getPosition().latitude +
                        "\nmarkerLong:" + marker.getPosition().longitude);
                finish();
                break;
            case R.id.cancelBtn:
                cardView.setVisibility(GONE);
                break;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Log.d(TAG, "onMyLocationButtonClick: ");
        setMarkerAtLoc(getCurrentLatLong());
        cardView.setVisibility(VISIBLE);
        return true;
    }

    private LatLng getCurrentLatLong() {
        return new LatLng(currentLat, currentLongX);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: " + location);
        handelNewLocation(location);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "onConnected: ");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                        22);
            } else {
                Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if (location != null) {
                    handelNewLocation(location);
                } else
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        } else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location != null) {
                handelNewLocation(location);
            } else
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void handelNewLocation(Location location) {
        currentLat = location.getLatitude();
        currentLongX = location.getLongitude();
        Log.d(TAG, "handelNewLocation: " + location
                + "\nlat: " + currentLat
                + "\nlong: " + currentLongX
                + "\naddress: null");
        marker = mMap.addMarker(markerOptions.position(getCurrentLatLong()));
        setMarkerAtLoc(getCurrentLatLong());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: ");
        mMap = googleMap;
        addMyLocationEnabledCircle();
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setOnMapClickListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
    }

    private void addMyLocationEnabledCircle() {
        if (Build.VERSION.SDK_INT >= M) {
            if (checkSelfPermission(ACCESS_FINE_LOCATION) != PERMISSION_GRANTED &&
                    checkSelfPermission(ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{ACCESS_FINE_LOCATION},
                        22);
            } else {
                mMap.setMyLocationEnabled(true);
            }
        } else {
            mMap.setMyLocationEnabled(true);
        }
    }


    private void setMarkerAtLoc(LatLng latLng) {
        marker.setPosition(latLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: " + connectionResult);
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        turnFindLocOff();
    }
}
