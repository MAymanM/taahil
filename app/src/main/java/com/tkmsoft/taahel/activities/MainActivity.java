package com.tkmsoft.taahel.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.fragments.main.AboutFragment;
import com.tkmsoft.taahel.fragments.main.FAQFragment;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.fragments.main.PrivacyConditionFrag;
import com.tkmsoft.taahel.fragments.main.activities.SubActivityFragment;
import com.tkmsoft.taahel.fragments.main.adds.MyAddsFragment;
import com.tkmsoft.taahel.fragments.main.bills.BillsFragment;
import com.tkmsoft.taahel.fragments.main.calculations.CalculationsFragment;
import com.tkmsoft.taahel.fragments.main.cart.CartFragment;
import com.tkmsoft.taahel.fragments.main.centers.CenterGovernmentFragment;
import com.tkmsoft.taahel.fragments.main.centers.CenterPrivateFragment;
import com.tkmsoft.taahel.fragments.main.centers.FilterCentersFragment;
import com.tkmsoft.taahel.fragments.main.doctor.DoctorsFragment;
import com.tkmsoft.taahel.fragments.main.doctor.RequestDrFragment1;
import com.tkmsoft.taahel.fragments.main.education.EduPrivateFragment;
import com.tkmsoft.taahel.fragments.main.education.FilterEducationFragment;
import com.tkmsoft.taahel.fragments.main.favourite.FavouritesFragment;
import com.tkmsoft.taahel.fragments.main.jobOrders.JobOrdersFragment;
import com.tkmsoft.taahel.fragments.main.jobs.JobsFragment;
import com.tkmsoft.taahel.fragments.main.profile.doctor.DrProfileFragment;
import com.tkmsoft.taahel.fragments.main.profile.doctor.EditDoctorFragment;
import com.tkmsoft.taahel.fragments.main.profile.doctor.EditDrSchedulerFragment2;
import com.tkmsoft.taahel.fragments.main.profile.patient.EditPatientFragment;
import com.tkmsoft.taahel.fragments.main.profile.patient.PatientProfileFragment;
import com.tkmsoft.taahel.fragments.main.reports.ReportsPermissionFragment;
import com.tkmsoft.taahel.fragments.main.reports.ReportsFragment;
import com.tkmsoft.taahel.fragments.main.orders.RequestsFragment;
import com.tkmsoft.taahel.fragments.main.store.StoreFragment;
import com.tkmsoft.taahel.helper.InsertBottomBarModels;
import com.tkmsoft.taahel.interfaces.IOnBackPressed;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.interfaces.SendDateCallBack;
import com.tkmsoft.taahel.interfaces.SendPatientProfData;
import com.tkmsoft.taahel.model.api.profile.UpdateAvatarModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyContextWrapper;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import devlight.io.library.ntb.NavigationTabBar;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MainViewsCallBack, SendDateCallBack, SendPatientProfData, OneSignal.NotificationOpenedHandler {

    ImageButton toolbarFilterButton;
    TextView toolbarTitle;
    private Locale locale;

    ImageButton imageButton_sendOrder;
    RelativeLayout relativeLayout_sendOrder;

    AlertDialog.Builder alertDialogBuilder;

    ListSharedPreference listSharedPreference;
    IOnBackPressed onBackPressedListener;

    private boolean mIsLogged;

    private DrawerLayout mDrawer;
    public Toolbar mToolbar;
    FloatingActionButton fab;
    private int destination;
    private String TAG = getClass().getSimpleName();

    TextView headerName, headerEmail;
    CircleImageView headerImageView;
    View header;
    private int REQUEST_GALLERY_CODE = 1478;
    private NavigationView mNavigationView;
    private MoveToFragment moveToFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        moveToFragment = new MoveToFragment(this);
        getSharedPrefData();
        setLayoutLanguage(getLang());
        mToolbar = findViewById(R.id.main_toolbar);
        initNavigationDrawer();
        initBottomTabBar();
        initUI();

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(this)
                .init();

        moveToFragment.moveInMain(new HomeFragment());
    }


    private void pickImage() {
        Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
        openGalleryIntent.setType("image/*");
        startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
    }

    private void getSharedPrefData() {
        mIsLogged = listSharedPreference.getIsLogged();
        String mToken = listSharedPreference.getToken();
    }

    private void initUI() {
        toolbarTitle = findViewById(R.id.toolbar_title);
        toolbarFilterButton = findViewById(R.id.toolbar_filter_button);
        fab = findViewById(R.id.main_fab);
        imageButton_sendOrder = findViewById(R.id.imageButton_sendOrder);
        relativeLayout_sendOrder = findViewById(R.id.relativeLayout_sendOrder);
        headerName = header.findViewById(R.id.headerName);
        headerEmail = header.findViewById(R.id.headerEmail);

        headerImageView = header.findViewById(R.id.imageView);
        headerImageView.setOnClickListener(v -> {
            if (mIsLogged)
                pickImage();
            else
                setAlertDialog(2);
        });

        if (mIsLogged) {
            setUserData(listSharedPreference.getUAvatar(),
                    listSharedPreference.getUName(),
                    listSharedPreference.getUEmail());
        }

        setDrawerMenuItems();
    }//init

    private void setDrawerMenuItems() {
        if (!mIsLogged) {
            //LoggedOut
            Menu menu = mNavigationView.getMenu();
            MenuItem nav_logout = menu.findItem(R.id.logout_nav);
            nav_logout.setTitle(getString(R.string.login));
            nav_logout.setIcon(R.drawable.ic_power_nav_24dp);

        } else {
            //LoggedIn
            Menu menu = mNavigationView.getMenu();
            MenuItem nav_logout = menu.findItem(R.id.logout_nav);
            nav_logout.setTitle(getString(R.string.logout));

            MenuItem nav_scheduler = menu.findItem(R.id.scheduler_edit_nav);
            MenuItem nav_orders = menu.findItem(R.id.requests_nav);
            MenuItem nav_reports = menu.findItem(R.id.reports_nav);
            MenuItem nav_calcs = menu.findItem(R.id.calculations_nav);
            if (listSharedPreference.getUType().equals("0")) {
                nav_orders.setTitle(getString(R.string.patient_orders));
                nav_scheduler.setTitle(getString(R.string.edit_scheduler));
                nav_reports.setTitle(getString(R.string.p_reports));
                nav_calcs.setVisible(true);
            } else {
                nav_orders.setTitle(getString(R.string.my_requests));
                nav_scheduler.setTitle(getString(R.string.reports_permission));
                nav_reports.setTitle(getString(R.string.my_reports));
                nav_calcs.setVisible(false);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            String image_path = getRealPathFromUri(uri, MainActivity.this);
            serverUpdateProfileImage(image_path);
            Glide.with(MainActivity.this).load(uri).into(headerImageView);
        }
    }

    private void serverUpdateProfileImage(String image_path) {
        Toast.makeText(this, R.string.loading, Toast.LENGTH_SHORT).show();
        ApiLink retrofit = MyRetrofitClient.getProfile().create(ApiLink.class);

        File file1 = new File(image_path);
        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);

        MultipartBody.Part image1 = MultipartBody.Part.createFormData("avatar", file1.getName(), mFile1);

        final Call<UpdateAvatarModel> updateProfileImageCall = retrofit.uploadAvatar(listSharedPreference.getToken(), image1);

        updateProfileImageCall.enqueue(new Callback<UpdateAvatarModel>() {
            @Override
            public void onResponse(@NonNull Call<UpdateAvatarModel> call, @NonNull Response<UpdateAvatarModel> response) {
                try {
                    if (response.isSuccessful()) {
                        listSharedPreference.setUAvatar(response.body().getData().getAvatar_path());
                        Glide.with(MainActivity.this).load(listSharedPreference.getUAvatar()).into(headerImageView);
                    } else {
                        Toast.makeText(MainActivity.this, "" + getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
                        Glide.with(MainActivity.this).load(R.drawable.place_holder).into(headerImageView);
                    }
                } catch (Exception ignored) {
                    Toast.makeText(MainActivity.this, "exc", Toast.LENGTH_SHORT).show();
                    Glide.with(MainActivity.this).load(R.drawable.place_holder).into(headerImageView);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateAvatarModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                Glide.with(MainActivity.this).load(R.drawable.place_holder).into(headerImageView);
            }
        });
    }


    public static String getRealPathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    private void setUserData(String avatar, String name, String email) {
        try {
            if (URLUtil.isValidUrl(avatar))
                Glide.with(MainActivity.this).load(avatar).into(headerImageView);
            else
                headerImageView.setImageResource(R.drawable.place_holder);
        } catch (Exception e) {
//            Toast.makeText(this, "setuserdata: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            headerImageView.setImageResource(R.drawable.place_holder);
        }
        headerName.setText(name);
        headerEmail.setText(email);
    }

    private void initNavigationDrawer() {
        mDrawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        header = mNavigationView.getHeaderView(0);
    }

    private void initBottomTabBar() {
        final InsertBottomBarModels insertBottomBarModels = new InsertBottomBarModels(MainActivity.this);

        final String[] colors = getResources().getStringArray(R.array.tab_bar_colors);

        NavigationTabBar navigationTabBar = findViewById(R.id.ntb_horizontal);

        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        if (getLang().equals("en"))
            insertBottomBarModels.modelsInEnglish(models, colors);
        else
            insertBottomBarModels.modelsInArabic(models, colors);
        navigationTabBar.setModels(models);

        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(NavigationTabBar.Model model, int index) {
                if (getLang().equals("en")) {
                    insertBottomBarModels.beginFragmentTransactionsEnglish(index);

                } else {
                    insertBottomBarModels.beginFragmentTransactionsArabic(index);
                }
            }

            @Override
            public void onEndTabSelected(NavigationTabBar.Model model, int index) {
                //addToolbarTitleAndIcons(index);
            }
        });

        //background color
        navigationTabBar.setBgColor(Color.parseColor(colors[1]));

    }//end initUi

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (onBackPressedListener != null)
                onBackPressedListener.onBackPressed();
            else
                super.onBackPressed();
        }
    }

    private static void restartActivity(Activity activity) {
        activity.recreate();
    }

    private void setLayoutLanguage(String language) {
        if (language.equals("ar"))
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        listSharedPreference = new ListSharedPreference(newBase);
        super.attachBaseContext(MyContextWrapper.wrap(newBase, getLang()));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.profile_nav) {
            if (mIsLogged) {
                if (listSharedPreference.getUType().equals("0")) {
                    listSharedPreference.setIsProfile(true);
                    moveToFragment.moveInMain(new DrProfileFragment());
                } else {
                    moveToFragment.moveInMain(new PatientProfileFragment());
                }
            } else {
                goToLogin();
            }
        } else if (id == R.id.profile_edit_nav) {
            if (mIsLogged) {
                if (listSharedPreference.getUType().equals("0")) {
                    moveToFragment.moveInMain(new EditDoctorFragment());
                } else {
                    moveToFragment.moveInMain(new EditPatientFragment());
                }
            } else
                goToLogin();
        } else if (id == R.id.scheduler_edit_nav) {
            if (mIsLogged) {
                if (listSharedPreference.getUType().equals("0")) {
                    moveToFragment.moveInMain(new EditDrSchedulerFragment2());
                } else {
                    moveToFragment.moveInMain(new ReportsPermissionFragment());
                }
            }
        } else if (id == R.id.requests_nav) {
            if (mIsLogged) {
                moveToFragment.moveInMain(new RequestsFragment());
            } else
                goToLogin();
        }
//        else if (id == R.id.notifications_nav) {
////            getFragmentManager().beginTransaction()
////                    .replace(R.id.main_frameLayout, new NotificationFragment()).commit();
//        } else if (id == R.id.adding_items_nav) {
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.main_frameLayout, new AddingItemsFragment()).commit();
//        }
        else if (id == R.id.favourite_nav) {
            if (mIsLogged)
                moveToFragment.moveInMain(new FavouritesFragment());
            else
                goToLogin();
        } else if (id == R.id.cart_nav) {
            moveToFragment.moveInMain(new CartFragment());
        } else if (id == R.id.bills_nav) {
            moveToFragment.moveInMain(new BillsFragment());
        } else if (id == R.id.nav_job_orders) {
            moveToFragment.moveInMain(new JobOrdersFragment());
        } else if (id == R.id.reports_nav) {
            if (mIsLogged)
                moveToFragment.moveInMain(new ReportsFragment());
            else
                goToLogin();
        }
//        else if (id == R.id.language_nav) {
//            setAlertDialog(1);
//            AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
//        }
        else if (id == R.id.nav_faq) {
            moveToFragment.moveInMain(new FAQFragment());
        } else if (id == R.id.nav_privacy_policy) {
            moveToFragment.moveInMain(new PrivacyConditionFrag());
        } else if (id == R.id.logout_nav) {
            listSharedPreference.deleteAllSharedPref();
            listSharedPreference.setLoginStatus(false);
            goToLogin();
        } else if (id == R.id.nav_about) {
            moveToFragment.moveInMain(new AboutFragment());

        } else if (id == R.id.nav_jobs) {
            moveToFragment.moveInMain(new JobsFragment());

        } else if (id == R.id.calculations_nav) {
            moveToFragment.moveInMain(new CalculationsFragment());
        } else if (id == R.id.my_adds_nav) {
            if (mIsLogged)
                moveToFragment.moveInMain(new MyAddsFragment());
            else
                goToLogin();
        }

        mDrawer = findViewById(R.id.drawer_layout);
        mDrawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void goToLogin() {
        Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(myIntent);
        this.finish();
    }

    private void setAlertDialog(int i) {
        switch (i) {
            case 1:
                alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage(getString(R.string.change_language_prompt));
                alertDialogBuilder.setPositiveButton(getString(R.string.yes), (arg0, arg1) ->
                {
                    //important to restart
                    moveToFragment.moveInMain(new HomeFragment());
                    if (listSharedPreference.getLanguage().equals("en")) {
                        listSharedPreference.setLanguage("ar");
                        restartActivity(MainActivity.this);
                    } else {
                        listSharedPreference.setLanguage("en");
                        restartActivity(MainActivity.this);
                    }
                });

                alertDialogBuilder.setNegativeButton(getString(R.string.no), (dialog, which) ->
                        dialog.dismiss());
                break;
            case 2:
                alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle(getString(R.string.please_login_first));
                alertDialogBuilder.setPositiveButton(getString(R.string.login), (arg0, arg1) -> {
                    Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(myIntent);
                    finish();
                });

                alertDialogBuilder.setNegativeButton(getString(R.string.skip_now), (dialog, which)
                        -> dialog.dismiss()
                );
                break;
            default:
                break;
        }
    }

    public void setOnBackPressedListener(IOnBackPressed onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the locale has changed
        if (!locale.equals(newConfig.locale)) {
            locale = newConfig.locale;

            this.setContentView(R.layout.activity_main);
            NavigationView navigationView = this.findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(MainActivity.this);
        }
    }

    @Override
    public void serToolbarTitle(String title) {
        toolbarTitle.setText(title);
    }

    @Override
    public void showFilterBtn(boolean visibility) {
        if (visibility) {
            toolbarFilterButton.setVisibility(View.VISIBLE);
        } else {
            toolbarFilterButton.setVisibility(View.INVISIBLE);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void showAddFab(boolean visibility) {
        if (visibility) {
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }
    }

    @Override
    public void showSendRequestView(boolean visibility) {

        if (relativeLayout_sendOrder.getVisibility() == View.VISIBLE) {
            relativeLayout_sendOrder.setVisibility(View.GONE);
        } else {
            relativeLayout_sendOrder.setVisibility(View.GONE);

        }
    }

    @Override
    public void filterDoctor(String countryCode, String cityCode, String genderCode, String specialtyId, String rate, boolean isFilter) {
        Fragment drFragment = new DoctorsFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putString("country", countryCode);
        bundle1.putString("city", cityCode);
        bundle1.putString("gender", genderCode);
        bundle1.putString("speciality", specialtyId);
        bundle1.putString("rate", rate);
        bundle1.putBoolean("isFilter", isFilter);
        drFragment.setArguments(bundle1);

        moveToFragment.moveInMain(drFragment);
    }

    @Override
    public void filterCenter(String countryCode, String cityCode, String rate, String categCode, boolean isFilter) {
        Fragment canterFrag;
        if (categCode.equals("1"))
            canterFrag = new CenterGovernmentFragment();
        else
            canterFrag = new CenterPrivateFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putString("country", countryCode);
        bundle1.putString("city", cityCode);
        bundle1.putString("rate", rate);
        bundle1.putBoolean("isFilter", isFilter);
        canterFrag.setArguments(bundle1);

        moveToFragment.moveInMain(canterFrag);
    }

    @Override
    public void sendCategNum(String categNum, String type) {
        Fragment mn = new HomeFragment();
        switch (type) {
            case "center":
                mn = new FilterCentersFragment();
                break;
            case "edu":
                mn = new FilterEducationFragment();
                break;
            default:
                break;
        }
        Bundle bundle = new Bundle();
        bundle.putString("numb", categNum);
        mn.setArguments(bundle);

        moveToFragment.moveInMain(mn);
    }

    @Override
    public void filterEdu(String countryCode, String cityCode, String rate, String categCode, boolean isFilter) {
        Fragment eduFrag = new BillsFragment();
        if (categCode.equals("1"))
            Toast.makeText(this, "1", Toast.LENGTH_SHORT).show();
        else
            eduFrag = new EduPrivateFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putString("country", countryCode);
        bundle1.putString("city", cityCode);
        bundle1.putString("rate", rate);
        bundle1.putBoolean("isFilter", isFilter);
        eduFrag.setArguments(bundle1);

        moveToFragment.moveInMain(eduFrag);
    }

    @Override
    public void filterActivity(String countryCode, String cityCode, String rate, boolean isFilter) {
        Fragment subActivityFrag = new SubActivityFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putString("country", countryCode);
        bundle1.putString("city", cityCode);
        bundle1.putString("rate", rate);
        bundle1.putBoolean("isFilter", isFilter);
        subActivityFrag.setArguments(bundle1);

        moveToFragment.moveInMain(subActivityFrag);
    }

    @Override
    public void filterStore(Integer countryCode, Integer cityCode, Float rate, Integer fieldCode, boolean isFilter) {
        Fragment subActivityFrag = new StoreFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putInt("country", countryCode);
        bundle1.putInt("city", cityCode);
        bundle1.putInt("field", fieldCode);
        bundle1.putFloat("rate", rate);
        bundle1.putBoolean("isFilter", isFilter);
        subActivityFrag.setArguments(bundle1);

        moveToFragment.moveInMain(subActivityFrag);
    }

    private String getLang() {
        return listSharedPreference.getLanguage();
    }

    @Override
    public void setDate(String fullDate, String date, int dayNum) {
        Fragment fragment = new RequestDrFragment1();
        Bundle bundle1 = new Bundle();
        bundle1.putString("fullDate", fullDate);
        bundle1.putString("date", date);
        bundle1.putInt("dayNum", dayNum);
        fragment.setArguments(bundle1);
        listSharedPreference.deleteDrTimeCheckPos();

        moveToFragment.moveInMain(fragment);
    }

    @Override
    public void sendData(String id, String gender, String avatar, String birthDate, String
            name, String email, String phone, String country, String city) {

        Intent intent = new Intent(MainActivity.this,
                PatientOrderProfileActivity.class);
        intent.putExtra("gender", gender);
        intent.putExtra("avatar", avatar);
        intent.putExtra("birthdate", birthDate);
        intent.putExtra("name", name);
        intent.putExtra("email", email);
        intent.putExtra("phone", phone);
        intent.putExtra("country", country);
        intent.putExtra("city", city);
        intent.putExtra("pId", id);
        startActivity(intent);
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        Log.d(TAG, "notificationOpened: " + result);
        Log.d(TAG, "notificationOpened: " + result.notification);
        Log.d(TAG, "notificationOpened: " + result.notification.androidNotificationId);
        Log.d(TAG, "notificationOpened: " + result.notification.groupedNotifications);
        Log.d(TAG, "notificationOpened: " + result.notification.displayType);
        Log.d(TAG, "notificationOpened: " + result.notification.payload.body);
        Log.d(TAG, "notificationOpened: " + result.notification.payload.notificationID);
        Log.d(TAG, "notificationOpened: " + result.notification.payload.title);
        Log.d(TAG, "notificationOpened: " + result.action.type);
        Log.d(TAG, "notificationOpened: " + result.action.actionID);
        Log.d(TAG, "notificationOpened: josn:");
        Log.d(TAG, "notificationOpened: \n" + result.notification.toJSONObject());
        Log.d(TAG, "notificationOpened: \n" + result.notification.payload.toJSONObject());

        moveToFragment.moveInMain(new RequestsFragment());

    }



}

