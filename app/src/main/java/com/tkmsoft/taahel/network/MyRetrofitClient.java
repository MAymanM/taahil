package com.tkmsoft.taahel.network;

import androidx.annotation.NonNull;

import com.androidnetworking.interceptors.HttpLoggingInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by MahmoudAyman on 07/02/2018.
 */

public class MyRetrofitClient {

   private static String DOMAIN = "https://taahel.com/";

    public MyRetrofitClient() {

    }

    private static OkHttpClient getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    @NonNull
    public static Retrofit getBase() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit auth() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/auth/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit getOrder() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/order/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit getBaseActivity() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/activity/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit getDrug() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/drug/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit getEducation() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/education/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit getStore() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/medical/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit getProfile() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/profile/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit getJobs() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/order-jobs/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    @NonNull
    public static Retrofit downloadJobOrder() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "storage/uploads/members/jobOrder/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    public static Retrofit getPayment() {
        return new Retrofit.Builder().baseUrl(DOMAIN + "api/order/payment/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }
}