package com.tkmsoft.taahel.network;

/**
 * Created by MahmoudAyman on 05/08/2018.
 */
public class Links {

    private static String DOMAIN = "https://taahel.com/";

    public static class Auth {

        public static String login() {
            return DOMAIN + "api/auth/login";
        }

        public static String register() {
            return DOMAIN + "api/auth/register";
        }

        public static String userInfo() {
            return DOMAIN + "api/auth/user_info";
        }

        public static String forget() {
            return DOMAIN + "api/auth/forget";
        }

        public static String confirm() {
            return DOMAIN + "api/auth/confirm";
        }

        public static String reset() {
            return DOMAIN + "api/auth/reset";
        }
    }

    /////////////////////////////////////////////////////
    public static class Base {

        public static String register() {
            return DOMAIN + "api/auth/register";
        }

        public static String reg_patient() {
            return DOMAIN + "api/auth/complete-register-patient";
        }

        public static String getDoctors() {
            return DOMAIN + "api/doctors?country_id={country_id}&city_id={city_id}&gender={gender}&specialty_id={specialty_id}&rate={rate}&page={page}";
        }

        public static String getSpecialization() {
            return DOMAIN + "api/specializations?page={page}";
        }

        public static String getReports() {
            return DOMAIN + "api/getProfile/reports";
        }
    }

    ///////////////////////////////////////////////
    public static class Profile {

        public static String getCalenderTimes() {
            return DOMAIN + "api/getProfile/getCalender?day={day}";
        }
    }

    ///////////////////////////////////////////////////
    public static class Order {
        public static String postDoctorDayTimes() {
            return DOMAIN + "api/order/postDoctorDayTimes";
        }

        public static String requestADoctor() {
            return DOMAIN + "api/order/requestADoctor";
        }

        public static String getOrders() {
            return DOMAIN + "api/order/getOrders?type={type}";
        }

        public static String acceptOrderDr() {
            return DOMAIN + "api/order/doctor?id={id}";
        }

        public static String refuseOrderDr() {
            return DOMAIN + "api/order/doctor/refuse?id={id}";
        }

        public static String deleteOrderPatient() {
            return DOMAIN + "api/order/patient/delete?id={id}";
        }

        public static String patientCompleteOrder() {
            return "api/order/patient/complete?id={id}";
        }
    }

    ///////////////////////////////////////////////////////
    public static class Medical {
        public static String getProducts() {
            return DOMAIN + "api/medical/medicals?country_id=null&city_id=null&type_id=null&rate=null&min_price=null&max_price=null";
        }
    }

}
