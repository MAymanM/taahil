package com.tkmsoft.taahel.network;

import com.tkmsoft.taahel.model.DrTimesBody;
import com.tkmsoft.taahel.model.api.FAQModel;
import com.tkmsoft.taahel.model.api.OpinionModel;
import com.tkmsoft.taahel.model.api.PrivacyAndConditionModel;
import com.tkmsoft.taahel.model.api.activities.view.category.ActivityCategModel;
import com.tkmsoft.taahel.model.api.activities.view.products.ActivityProductsModel;
import com.tkmsoft.taahel.model.api.adds.activity.MyActivitiesModel;
import com.tkmsoft.taahel.model.api.adds.center.MyCentersModel;
import com.tkmsoft.taahel.model.api.adds.edu.MyEducationModel;
import com.tkmsoft.taahel.model.api.adds.store.MyStoreModel;
import com.tkmsoft.taahel.model.api.auth.confirm.ConfirmModel;
import com.tkmsoft.taahel.model.api.auth.forgetpass.ForgetPassModel;
import com.tkmsoft.taahel.model.api.auth.login.LoginModel;
import com.tkmsoft.taahel.model.api.auth.register.doctor.dates.DrTimesRegisterModel;
import com.tkmsoft.taahel.model.api.AdsModel;
import com.tkmsoft.taahel.model.api.auth.updatePlayerId.UpdatePlayerIDModel;
import com.tkmsoft.taahel.model.api.bill.BillsModel;
import com.tkmsoft.taahel.model.api.cart.cash.CashCartPayModel;
import com.tkmsoft.taahel.model.api.cart.change.ChangeCartModel;
import com.tkmsoft.taahel.model.api.cart.view.CartModel;
import com.tkmsoft.taahel.model.api.checkout.checkoutId.GetCheckOutIDModel;
import com.tkmsoft.taahel.model.api.comments.add.AddCommentModel;
import com.tkmsoft.taahel.model.api.adds.edit.EditModel;
import com.tkmsoft.taahel.model.api.defaultRes.DefaultResponse;
import com.tkmsoft.taahel.model.api.deletecart.DeleteCartModel;
import com.tkmsoft.taahel.model.api.drugs.view.DrugsModel;
import com.tkmsoft.taahel.model.api.education.view.EducationModel;
import com.tkmsoft.taahel.model.api.favourite.change.ChangeFavModel;
import com.tkmsoft.taahel.model.api.favourite.view.FavouritesModel;
import com.tkmsoft.taahel.model.api.joborders.JobOrdersModel;
import com.tkmsoft.taahel.model.api.jobs.disabilities.DisabilitiesModel;
import com.tkmsoft.taahel.model.api.jobs.fields.FieldsJobModel;
import com.tkmsoft.taahel.model.api.jobs.view.JobsModel;
import com.tkmsoft.taahel.model.api.order.GetPermissionModel;
import com.tkmsoft.taahel.model.api.order.drTimes.DoctorTimesModel;
import com.tkmsoft.taahel.model.api.order.drs.DoctorOrdersModel;
import com.tkmsoft.taahel.model.api.order.orderDetails.dr.accept.AcceptDrModel;
import com.tkmsoft.taahel.model.api.order.orderDetails.patient.PatientCompleteOrderModel;
import com.tkmsoft.taahel.model.api.order.patient.PatientOrderModel;
import com.tkmsoft.taahel.model.api.order.payment.PaymentModel;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.getCalender.GetCalenderTimesModel;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.updateCalender.UpdateCalenderModel;
import com.tkmsoft.taahel.model.api.profile.patient.UpdatePatientModel;
import com.tkmsoft.taahel.model.api.getDoctors.DoctorsModel;
import com.tkmsoft.taahel.model.api.SliderModel;
import com.tkmsoft.taahel.model.api.activities.AddActivityModel;
import com.tkmsoft.taahel.model.api.auth.register.doctor.CompleteRegDoc1;
import com.tkmsoft.taahel.model.api.auth.register.patient.RegisterPatientModel;
import com.tkmsoft.taahel.model.api.auth.register.RegisterModel;
import com.tkmsoft.taahel.model.api.drugs.AddCenterModel;
import com.tkmsoft.taahel.model.api.education.AddEducationsModel;
import com.tkmsoft.taahel.model.api.order.requestADr.RequestADoctorModel;
import com.tkmsoft.taahel.model.api.profile.UpdateAvatarModel;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.UpdateDoctorModel;
import com.tkmsoft.taahel.model.api.reports.ReportsModel;
import com.tkmsoft.taahel.model.api.reports.patient.changepermission.RepChangePermissionModel;
import com.tkmsoft.taahel.model.api.reports.patient.permission.ReportsPermissionModel;
import com.tkmsoft.taahel.model.api.reports.patient.profile.PatientOrderProfileModel;
import com.tkmsoft.taahel.model.api.store.view.StoreModel;
import com.tkmsoft.taahel.model.api.store.add.StoreAddModel;
import com.tkmsoft.taahel.model.api.store.spinner.type.TypeStoreModel;
import com.tkmsoft.taahel.model.api.specialization.SpecializationsModel;
import com.tkmsoft.taahel.model.body.UpdateDoctorBody;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.model.spinners.CurrencyModel;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by MahmoudAyman on 12/06/2018.
 */

public interface ApiLink {
    
    @FormUrlEncoded
    @POST("login")
    Call<LoginModel> login(@Field("phone") String phone,
                           @Field("password") String password);
    
    @FormUrlEncoded
    @POST("register")
    Call<RegisterModel> register(@Field("type") String type,
                                 @Field("name") String name,
                                 @Field("phone") String phone,
                                 @Field("phoneKey") String phoneKey,
                                 @Field("password") String password,
                                 @Field("password_confirmation") String password_confirmation);
    
    @FormUrlEncoded
    @POST("confirm")
    Call<ConfirmModel> confirm(@Header("token") String token,
                               @Field("code") String code);
    
    @Multipart
    @POST("complete-register-doctor")
    Call<CompleteRegDoc1> complete_register_doctor_Qe(@Header("token") String token,
                                                      @Part MultipartBody.Part licence,
                                                      @PartMap() Map<String, RequestBody> specialties,
                                                      @Part("country_id") RequestBody country_id,
                                                      @Part("city_id") RequestBody city_id,
                                                      @Part("email") RequestBody email,
                                                      @Part("price") RequestBody price,
                                                      @Part("currency_id") RequestBody currency_id,
                                                      @Part("experiences") RequestBody experiences,
                                                      @Part("education") RequestBody education,
                                                      @Part("about") RequestBody about,
                                                      @Part("gender") RequestBody gender);
    
    
    @FormUrlEncoded
    @POST("complete-register-doctor")
    Call<CompleteRegDoc1> complete_register_doctor_null_license(@Header("token") String token,
                                                                @FieldMap Map<String, String> specialties,
                                                                @Field("country_id") String country_id,
                                                                @Field("city_id") String city_id,
                                                                @Field("email") String email,
                                                                @Field("price") String price,
                                                                @Field("currency_id") String currency_id,
                                                                @Field("experiences") String experiences,
                                                                @Field("education") String education,
                                                                @Field("about") String about,
                                                                @Field("gender") String gender);
    
    
    @Multipart
    @POST("uploadAvatar")
    Call<UpdateAvatarModel> uploadAvatar(@Header("token") String token,
                                         @Part MultipartBody.Part avatar);
    
    @Multipart
    @POST("complete-register-patient")
    Call<RegisterPatientModel> complete_register_patient(@Header("token") String token,
                                                         @Part MultipartBody.Part report,
                                                         @Part("country_id") RequestBody country_id,
                                                         @Part("city_id") RequestBody city_id,
                                                         @Part("email") RequestBody email,
                                                         @Part("share") RequestBody share,
                                                         @Part("gender") RequestBody gender,
                                                         @Part("day") RequestBody day,
                                                         @Part("month") RequestBody month,
                                                         @Part("year") RequestBody year);
    
    @FormUrlEncoded
    @POST("complete-register-patient")
    Call<RegisterPatientModel> complete_register_patient_null_report(@Header("token") String token,
                                                                     @Field("country_id") String country_id,
                                                                     @Field("city_id") String city_id,
                                                                     @Field("email") String email,
                                                                     @Field("share") String share,
                                                                     @Field("gender") String gender,
                                                                     @Field("day") String day,
                                                                     @Field("month") String month,
                                                                     @Field("year") String year,
                                                                     @Field("lat") String lat,
                                                                     @Field("long") String xLong);
    
    @Multipart
    @POST("updatePatient")
    Call<UpdatePatientModel> updatePatient(@Header("token") String token,
                                           @Part MultipartBody.Part report,
                                           @Part("name") RequestBody name,
                                           @Part("username") RequestBody userName,
                                           @Part("city_id") RequestBody city_id,
                                           @Part("email") RequestBody email,
                                           @Part("phone") RequestBody phone,
                                           @Part("phoneKey") RequestBody phoneKey,
                                           @Part("gender") RequestBody gender,
                                           @Part("lat") RequestBody lat,
                                           @Part("long") RequestBody xLong,
                                           @Part("day") RequestBody user_day,
                                           @Part("month") RequestBody user_month,
                                           @Part("year") RequestBody user_year,
                                           @Part("old_password") RequestBody oldPass,
                                           @Part("password") RequestBody newPass,
                                           @Part("password_confirmation") RequestBody newPassConfirm);
    
    @FormUrlEncoded
    @POST("updatePatient")
    Call<UpdatePatientModel> updatePatientNullReport(@Header("token") String token,
                                                     @Field("name") String name,
                                                     @Field("username") String userName,
                                                     @Field("city_id") String city_id,
                                                     @Field("email") String email,
                                                     @Field("phone") String phone,
                                                     @Field("phoneKey") String phoneKey,
                                                     @Field("gender") String gender,
                                                     @Field("lat") String lat,
                                                     @Field("long") String xLong,
                                                     @Field("day") String user_day,
                                                     @Field("month") String user_month,
                                                     @Field("year") String user_year,
                                                     @Field("old_password") String oldPass,
                                                     @Field("password") String newPass,
                                                     @Field("password_confirmation") String newPassConfirm);
    
    
    @FormUrlEncoded
    @POST("requestADoctor")
    Call<RequestADoctorModel> requestADoctor(@Header("token") String token,
                                             @Field("job_date_id") String job_date_id,
                                             @Field("date") String date,
                                             @Field("desc") String desc,
                                             @Field("address") String address,
                                             @FieldMap Map<String, String> hashMap);
    
    //GET
    @GET("countries")
    Call<CountriesModel> getCountries();
    
    @GET("currencies")
    Call<CurrencyModel> getCurrency();
    
    @GET("getCategories")
    Call<ActivityCategModel> getActivitiesCategs();
    
    @GET("getCategory/{categ_id}")
    Call<DrugsModel> getDrugs(@Path("categ_id") String categ_id,
                              @Query("page") int page);
    
    @GET("getCategory/{categ_id}")
    Call<EducationModel> getEducations(@Path("categ_id") String categ_id,
                                       @Query("page") int page);
    
    @GET("?country_id=null&city_id=null&type_id=null&rate=null&min_price=null&max_price=null")
    Call<StoreModel> getStoreProducts();
    
    
    @GET("getCategories")
    Call<TypeStoreModel> getStoreTypes();
    
    
    @GET("slider")
    Call<SliderModel> getSliderImages();
    
    @GET("job")
    Call<JobsModel> getJobs(@Query("page") Integer page);
    
    @GET("wishList")
    Call<FavouritesModel> getWishLists(@Header("token") String token, @Query("page") Integer page);
    
    @GET
    Call<AdsModel> getAds(@Url String url);
    
    
    @FormUrlEncoded
    @POST("update-player-id")
    Call<UpdatePlayerIDModel> updatePlayerId(@Header("token") String token, @Field("player_id") String player_id);
    
    @POST("complete-doctor-dates")
    Call<DrTimesRegisterModel> complete_doctor_dates(@Header("token") String token, @Body DrTimesBody drTimesBody);
    
    @FormUrlEncoded
    @POST("complete-doctor-dates")
    Call<DrTimesRegisterModel> complete_doctor_dates_er(@Header("token") String token, @Field("day") String day, @FieldMap Map<String, String> drTimes, @FieldMap Map<String, String> drType);
    
    @FormUrlEncoded
    @POST("updateCalender")
    Call<UpdateCalenderModel> updateCalender(@Header("token") String token,
                                             @Field("day") String day,
                                             @FieldMap Map<String, String> drTimes,
                                             @FieldMap Map<String, String> drType);
    
    @FormUrlEncoded
    @POST("members")
    Call<DoctorsModel> getMembers(@Field("type") String type);
    
    @FormUrlEncoded
    @POST("postDoctorDayTimes")
    Call<DoctorTimesModel> postDrTimes(@Field("doctor_id") int doctorId,
                                       @Field("day") String dayNum,
                                       @Field("date") String date);
    
    @POST("updateDoctor")
    Call<UpdateDoctorModel> updateDoctor(@Header("token") String token,
                                         @Body UpdateDoctorBody updateDoctorBody);
    
    @FormUrlEncoded
    @POST("addComment")
    Call<AddCommentModel> addComment(@Header("token") String token,
                                     @Field("product_id") String product_id,
                                     @Field("content") String content,
                                     @Field("rate") String rate,
                                     @Field("type") String type);
    
    //AddActivity
    @Multipart
    @POST("add")
    Call<AddActivityModel> addActivity(@Header("token") String api_token,
                                       @Part MultipartBody.Part photo,
                                       @Part("name_ar") RequestBody name_ar,
                                       @Part("name_en") RequestBody name_en,
                                       @Part("type_id") RequestBody type_id,
                                       @Part("start_date") RequestBody start_date,
                                       @Part("duration") RequestBody duration,
                                       @Part("city_id") RequestBody city_id,
                                       @Part("address_ar") RequestBody address_ar,
                                       @Part("address_en") RequestBody address_en,
                                       @Part("link") RequestBody link,
                                       @Part("static_phone") RequestBody static_phone,
                                       @Part("phone") RequestBody phone,
                                       @Part("lat") RequestBody lat,
                                       @Part("long") RequestBody longX);
    
    //addCenter
    @Multipart
    @POST("add")
    Call<AddCenterModel> addCenter(@Header("token") String token,
                                   @Part MultipartBody.Part photo,
                                   @Part("name_ar") RequestBody name_ar,
                                   @Part("name_en") RequestBody name_en,
                                   @Part("type_id") RequestBody type_id,
                                   @Part("city_id") RequestBody city_id,
                                   @Part("address_ar") RequestBody address_ar,
                                   @Part("address_en") RequestBody address_en,
                                   @Part("web") RequestBody link,
                                   @Part("static_phone") RequestBody static_phone,
                                   @Part("phone") RequestBody phone,
                                   @Part("lat") RequestBody lat,
                                   @Part("long") RequestBody longX);
    
    //addEducation
    @Multipart
    @POST("add")
    Call<AddEducationsModel> addEducation(@Header("token") String token,
                                          @Part MultipartBody.Part photo,
                                          @Part("name_ar") RequestBody name_ar,
                                          @Part("name_en") RequestBody name_en,
                                          @Part("type_id") RequestBody type_id,
                                          @Part("city_id") RequestBody city_id,
                                          @Part("address_ar") RequestBody address_ar,
                                          @Part("address_en") RequestBody address_en,
                                          @Part("phone") RequestBody phone,
                                          @Part("lat") RequestBody lat,
                                          @Part("long") RequestBody longX);
    
    @Multipart
    @POST("doctor/complete")
    Call<AcceptDrModel> complete_dr_order(@Query("id") String orderId,
                                          @Part MultipartBody.Part report,
                                          @Part("notes") RequestBody notesRB);
    
    @FormUrlEncoded
    @POST("doctor/complete")
    Call<AcceptDrModel> complete_dr_order(@Query("id") String orderId,
                                          @Field("notes") String notesRB);
    
    @GET("reports")
    Call<ReportsModel> getReports(@Header("token") String token);
    
    
    @GET("doctors?country_id=null&city_id=null&gender=null&specialty_id=null&rate=null")
    Call<DoctorsModel> getDoctors(@Query("page") int page);
//    @GET("doctors?country_id=null&city_id=null&gender=null&specialty_id=null&rate=null&page=1")
//    Call<DoctorsModel> getDoctors_o();
    
    @GET("report/permission")
    Call<GetPermissionModel> getPermission(@Header("token") String token,
                                           @Query("id") String pId);
    
    @GET("report/reports-permission")
    Call<ReportsPermissionModel> getPermissionReports(@Header("token") String token);
    
    @GET("report/check-reports")
    Call<PatientOrderProfileModel> checkForReport(@Header("token") String token,
                                                  @Query("id") String id);
    
    @GET("report/reports-permission/change")
    Call<RepChangePermissionModel> changePermission(@Query("id") String reportId);
    
    //addStore
    @Multipart
    @POST("add")
    Call<StoreAddModel> addMedical(@Header("token") String token,
                                   @Part MultipartBody.Part photo1,
                                   @Part("name_ar") RequestBody name_ar,
                                   @Part("name_en") RequestBody name_en,
                                   @Part("type_id") RequestBody type_id,
                                   @Part("city_id") RequestBody city_id,
                                   @Part("price") RequestBody price,
                                   @Part("currency_id") RequestBody currency_id,
                                   @Part("size") RequestBody size,
                                   @Part("age_from") RequestBody age_from,
                                   @Part("age_to") RequestBody age_to,
                                   @Part("desc_ar") RequestBody desc_ar,
                                   @Part("desc_en") RequestBody desc_en,
                                   @Part("lat") RequestBody lat,
                                   @Part("long") RequestBody longX);
    
    @Multipart
    @POST("add")
    Call<StoreAddModel> addMedical2(@Header("token") String token,
                                    @Part MultipartBody.Part photo1,
                                    @Part MultipartBody.Part second_photo,
                                    @Part("name_ar") RequestBody name_ar,
                                    @Part("name_en") RequestBody name_en,
                                    @Part("type_id") RequestBody type_id,
                                    @Part("city_id") RequestBody city_id,
                                    @Part("price") RequestBody price,
                                    @Part("currency_id") RequestBody currency_id,
                                    @Part("size") RequestBody size,
                                    @Part("age_from") RequestBody age_from,
                                    @Part("age_to") RequestBody age_to,
                                    @Part("desc_ar") RequestBody desc_ar,
                                    @Part("desc_en") RequestBody desc_en,
                                    @Part("lat") RequestBody lat,
                                    @Part("long") RequestBody longX);
    
    @Multipart
    @POST("add")
    Call<StoreAddModel> addMedical3(@Header("token") String token,
                                    @Part MultipartBody.Part photo1,
                                    @Part MultipartBody.Part third_photo,
                                    @Part("name_ar") RequestBody name_ar,
                                    @Part("name_en") RequestBody name_en,
                                    @Part("type_id") RequestBody type_id,
                                    @Part("city_id") RequestBody city_id,
                                    @Part("price") RequestBody price,
                                    @Part("currency_id") RequestBody currency_id,
                                    @Part("size") RequestBody size,
                                    @Part("age_from") RequestBody age_from,
                                    @Part("age_to") RequestBody age_to,
                                    @Part("desc_ar") RequestBody desc_ar,
                                    @Part("desc_en") RequestBody desc_en,
                                    @Part("lat") RequestBody lat,
                                    @Part("long") RequestBody longX);
    
    @Multipart
    @POST("add")
    Call<StoreAddModel> addMedicalAll(@Header("token") String token,
                                      @Part MultipartBody.Part main_photo,
                                      @Part MultipartBody.Part second_photo,
                                      @Part MultipartBody.Part third_photo,
                                      @Part("name_ar") RequestBody name_ar,
                                      @Part("name_en") RequestBody name_en,
                                      @Part("type_id") RequestBody type_id,
                                      @Part("city_id") RequestBody city_id,
                                      @Part("price") RequestBody price,
                                      @Part("currency_id") RequestBody currency_id,
                                      @Part("size") RequestBody size,
                                      @Part("age_from") RequestBody age_from,
                                      @Part("age_to") RequestBody age_to,
                                      @Part("desc_ar") RequestBody desc_ar,
                                      @Part("desc_en") RequestBody desc_en,
                                      @Part("lat") RequestBody lat,
                                      @Part("long") RequestBody longX);
    
    @FormUrlEncoded
    @POST("postDoctorDayTimes")
    Call<DoctorTimesModel> getDoctorDayTimes(@Field("doctor_id") String doctor_id,
                                             @Field("day") String day,
                                             @Field("date") String date);
    
    @FormUrlEncoded
    @POST("sentOpinion")
    Call<OpinionModel> postOpinion(@Field("opinion") String opinion);
    
    @GET("getCategory/{category_id}")
    Call<ActivityProductsModel> getActivityProducts(@Path("category_id") String categ_id,
                                                    @Query("page") int page);
    
    @FormUrlEncoded
    @POST("patient/complete")
    Call<PatientCompleteOrderModel> patientCompleteOrderModel(@Query("id") String orderId, @Field("rate") String rateValue, @Field("notes") String notes);
    
    @GET("conditions")
    Call<PrivacyAndConditionModel> getCondition(@Query("page") int page);
    
    @GET("faq/getFaq")
    Call<FAQModel> getFAQs();
    
    @FormUrlEncoded
    @POST("confirmPayment")
    Call<PaymentModel> orderCash(@Header("token") String token,
                                 @Field("payment_type") String paymentType,
                                 @Field("tracker_id") String trackerId);
    
    @GET("getOrders")
    Call<DoctorOrdersModel> getDrOrders(@Header("token") String token,
                                        @Query("type") String type);
    
    @GET("getOrders")
    Call<PatientOrderModel> getPatientOrders(@Header("token") String token,
                                             @Query("type") String orderType);
    
    @GET("getCalender")
    Call<GetCalenderTimesModel> getCalenderTimes(@Header("token") String token,
                                                 @Query("day") String day);
    
    @FormUrlEncoded
    @POST("forget")
    Call<ForgetPassModel> forget_pass(String token, String uPhone);
    
    
    @FormUrlEncoded
    @POST("update")
    Call<EditModel> editCenterNonImage(@Field("id") String id,
                                       @Field("name_ar") String name_ar,
                                       @Field("name_en") String name_en,
                                       @Field("type_id") String type_id,
                                       @Field("city_id") String city_id,
                                       @Field("address_ar") String address_ar,
                                       @Field("address_en") String address_en,
                                       @Field("web") String web,
                                       @Field("static_phone") String static_phone,
                                       @Field("phone") String phone,
                                       @Field("lat") String lat,
                                       @Field("long") String longX);
    
    @FormUrlEncoded
    @POST("update")
    Call<EditModel> editEduNonImage(@Field("id") String id,
                                    @Field("name_ar") String name_ar,
                                    @Field("name_en") String name_en,
                                    @Field("type_id") String type_id,
                                    @Field("city_id") String city_id,
                                    @Field("address_ar") String address_ar,
                                    @Field("address_en") String address_en,
                                    @Field("web") String web,
                                    @Field("lat") String lat,
                                    @Field("long") String longX);
    
    @Multipart
    @POST("update")
    Call<EditModel> editCenter(@Part("id") RequestBody id,
                               @Part MultipartBody.Part photo,
                               @Part("name_ar") RequestBody name_ar,
                               @Part("name_en") RequestBody name_en,
                               @Part("type_id") RequestBody type_id,
                               @Part("city_id") RequestBody city_id,
                               @Part("address_ar") RequestBody address_ar,
                               @Part("address_en") RequestBody address_en,
                               @Part("web") RequestBody web,
                               @Part("static_phone") RequestBody static_phone,
                               @Part("phone") RequestBody phone,
                               @Part("lat") RequestBody lat,
                               @Part("long") RequestBody longX);
    
    @Multipart
    @POST("update")
    Call<EditModel> editStoreAll(@Part("id") RequestBody id,
                                 @Part MultipartBody.Part photo1,
                                 @Part MultipartBody.Part photo2,
                                 @Part MultipartBody.Part photo3,
                                 @Part("name_ar") RequestBody name_ar,
                                 @Part("name_en") RequestBody name_en,
                                 @Part("type_id") RequestBody type_id,
                                 @Part("city_id") RequestBody city_id,
                                 @Part("currency_id") RequestBody currency_id,
                                 @Part("price") RequestBody price,
                                 @Part("size") RequestBody size,
                                 @Part("age_from") RequestBody age_from,
                                 @Part("age_to") RequestBody age_to,
                                 @Part("desc_ar") RequestBody desc_ar,
                                 @Part("desc_en") RequestBody desc_en);
    
    @Multipart
    @POST("update")
    Call<EditModel> editStore1(@Part("id") RequestBody id,
                               @Part MultipartBody.Part photo1,
                               @Part("name_ar") RequestBody name_ar,
                               @Part("name_en") RequestBody name_en,
                               @Part("type_id") RequestBody type_id,
                               @Part("city_id") RequestBody city_id,
                               @Part("currency_id") RequestBody currency_id,
                               @Part("price") RequestBody price,
                               @Part("size") RequestBody size,
                               @Part("age_from") RequestBody age_from,
                               @Part("age_to") RequestBody age_to,
                               @Part("desc_ar") RequestBody desc_ar,
                               @Part("desc_en") RequestBody desc_en);
    
    @Multipart
    @POST("update")
    Call<EditModel> editStore2(@Part("id") RequestBody id,
                               @Part MultipartBody.Part photo1,
                               @Part MultipartBody.Part photo2,
                               @Part("name_ar") RequestBody name_ar,
                               @Part("name_en") RequestBody name_en,
                               @Part("type_id") RequestBody type_id,
                               @Part("city_id") RequestBody city_id,
                               @Part("currency_id") RequestBody currency_id,
                               @Part("price") RequestBody price,
                               @Part("size") RequestBody size,
                               @Part("age_from") RequestBody age_from,
                               @Part("age_to") RequestBody age_to,
                               @Part("desc_ar") RequestBody desc_ar,
                               @Part("desc_en") RequestBody desc_en);
    
    @FormUrlEncoded
    @POST("update")
    Call<EditModel> editStoreNonImage(@Field("id") String id,
                                      @Field("name_ar") String name_ar,
                                      @Field("name_en") String name_en,
                                      @Field("type_id") String type_id,
                                      @Field("city_id") String city_id,
                                      @Field("currency_id") String currency_id,
                                      @Field("price") String price,
                                      @Field("size") String size,
                                      @Field("desc_ar") String desc_ar,
                                      @Field("desc_en") String desc_en,
                                      @Field("age_from") String age_from,
                                      @Field("age_to") String age_to);
    
    @Multipart
    @POST("update")
    Call<EditModel> editEdu(@Part("id") RequestBody id,
                            @Part MultipartBody.Part photo,
                            @Part("name_ar") RequestBody name_ar,
                            @Part("name_en") RequestBody name_en,
                            @Part("type_id") RequestBody type_id,
                            @Part("city_id") RequestBody city_id,
                            @Part("address_ar") RequestBody address_ar,
                            @Part("address_en") RequestBody address_en,
                            @Part("web") RequestBody web,
                            @Part("lat") RequestBody lat,
                            @Part("long") RequestBody longX);
    
    @GET("adds")
    Call<MyCentersModel> getMyAddsCenters(@Header("token") String token,
                                          @Query("type") String addsType,
                                          @Query("page") int currentPage);
    
    @GET("adds")
    Call<MyEducationModel> getMyAddsEdu(@Header("token") String token,
                                        @Query("type") String addsType,
                                        @Query("page") int currentPage);
    
    @GET("adds")
    Call<MyStoreModel> getMyAddsStore(@Header("token") String token,
                                      @Query("type") String addsType,
                                      @Query("page") int currentPage);
    
    @GET("adds")
    Call<MyActivitiesModel> getMyAddsActivity(@Header("token") String token,
                                              @Query("type") String addsType,
                                              @Query("page") int currentPage);
    
    @Multipart
    @POST("update")
    Call<EditModel> editActivity(@Part("id") RequestBody id,
                                 @Part MultipartBody.Part photo,
                                 @Part("name_ar") RequestBody name_ar,
                                 @Part("name_en") RequestBody name_en,
                                 @Part("type_id") RequestBody type_id,
                                 @Part("city_id") RequestBody city_id,
                                 @Part("address_ar") RequestBody address_ar,
                                 @Part("address_en") RequestBody address_en,
                                 @Part("link") RequestBody link,
                                 @Part("start_date") RequestBody start_date,
                                 @Part("duration") RequestBody duration,
                                 @Part("static_phone") RequestBody static_phone,
                                 @Part("phone") RequestBody phone,
                                 @Part("lat") RequestBody lat,
                                 @Part("long") RequestBody longX);
    
    @FormUrlEncoded
    @POST("update")
    Call<EditModel> editActivityNonImage(@Field("id") String id,
                                         @Field("name_ar") String name_ar,
                                         @Field("name_en") String name_en,
                                         @Field("type_id") String type_id,
                                         @Field("city_id") String city_id,
                                         @Field("address_ar") String address_ar,
                                         @Field("address_en") String address_en,
                                         @Field("link") String link,
                                         @Field("start_date") String start_date,
                                         @Field("duration") String duration,
                                         @Field("static_phone") String static_phone,
                                         @Field("phone") String phone,
                                         @Field("lat") String lat,
                                         @Field("long") String longX);
    
    @GET("specializations")
    Call<SpecializationsModel> getSpecialists();
    
    @GET("order")
    Call<JobOrdersModel> getJobOrders(@Query("page") int currentPage);
    
    
    @GET("fields")
    Call<FieldsJobModel> getJobFields();
    
    @GET("disabilities")
    Call<DisabilitiesModel> getJobDisability();
    
    @Multipart
    @POST("job/add")
    Call<DefaultResponse> addJob(@Part("name_ar") RequestBody name_ar,
                                 @Part("name_en") RequestBody name_en,
                                 @Part("city_id") RequestBody city_id,
                                 @Part("address_ar") RequestBody address_ar,
                                 @Part("address_en") RequestBody address_en,
                                 @Part("field_id") RequestBody field_id,
                                 @Part("email") RequestBody email,
                                 @Part("phone") RequestBody phone,
                                 @Part("desc_ar") RequestBody desc_ar,
                                 @Part("desc_en") RequestBody desc_en,
                                 @Part("feature1_ar") RequestBody feature1_ar,
                                 @Part("feature1_en") RequestBody feature1_en,
                                 @Part("feature2_ar") RequestBody feature2_ar,
                                 @Part("feature2_en") RequestBody feature2_en,
                                 @Part("feature3_ar") RequestBody feature3_ar,
                                 @Part("feature3_en") RequestBody feature3_en,
                                 @Part("feature4_ar") RequestBody feature4_ar,
                                 @Part("feature4_en") RequestBody feature4_en,
                                 @Part MultipartBody.Part photo);
    
    @FormUrlEncoded
    @POST("job/add")
    Call<DefaultResponse> addJobNonImage(@Field("name_ar") String name_ar,
                                         @Field("name_en") String name_en,
                                         @Field("city_id") String city_id,
                                         @Field("address_ar") String address_ar,
                                         @Field("address_en") String address_en,
                                         @Field("field_id") String field_id,
                                         @Field("email") String email,
                                         @Field("phone") String phone,
                                         @Field("desc_ar") String desc_ar,
                                         @Field("desc_en") String desc_en,
                                         @Field("feature1_ar") String feature1_ar,
                                         @Field("feature1_en") String feature1_en,
                                         @Field("feature2_ar") String feature2_ar,
                                         @Field("feature2_en") String feature2_en,
                                         @Field("feature3_ar") String feature3_ar,
                                         @Field("feature3_en") String feature3_en,
                                         @Field("feature4_ar") String feature4_ar,
                                         @Field("feature4_en") String feature4_en);
    
    @Multipart
    @POST("order/add")
    Call<DefaultResponse> addJobOrder(@Part("name") RequestBody name,
                                      @Part("city_id") RequestBody city_id,
                                      @Part("disability_id") RequestBody disability_id,
                                      @Part("field_id") RequestBody field_id,
                                      @Part("email") RequestBody email,
                                      @Part("phone") RequestBody phone,
                                      @Part("desc") RequestBody desc,
                                      @Part MultipartBody.Part cv,
                                      @Part MultipartBody.Part photo);
    
    @FormUrlEncoded
    @POST("order/add")
    Call<DefaultResponse> addJobOrderNonImage(@Field("name") String name,
                                              @Field("city_id") String city_id,
                                              @Field("disability_id") String disability_id,
                                              @Field("field_id") String field_id,
                                              @Field("email") String email,
                                              @Field("phone") String phone,
                                              @Field("desc") String desc);
    
    @GET("getCategory")
    Call<StoreModel> getStore(@Header("token") String token,
                              @Query("page") int page);
    
    @FormUrlEncoded
    @POST("wishlist")
    Call<ChangeFavModel> changeWichlist(@Header("token") String token,
                                        @Field("medical_id") Integer medical_id);
    
    
    @FormUrlEncoded
    @POST("cart-change")
    Call<ChangeCartModel> changeCart(@Header("token") String token, @Field("product_id") Long pId);
    
    @GET("get-cart")
    Call<CartModel> get_cart(@Header("token") String token);


//    @FormUrlEncoded
//    @POST("check-out")
//    Call<CheckOutModel> check_out(@Header("token") String token,
//                                  @Field("city_id") Integer city_id,
//                                  @Field("address") String address,
//                                  @Field("payment") String paymentMethod,
//                                  @Field("phone") String phone,
//                                  @FieldMap Map<String, String> quantity);

//    @GET("get-checkout")
//    Call<GetCheckOutIDModel> get_checkout_id_cart(@Header("token") String token,
//                                             @Query("country_id") Integer country_id,
//                                             @Query("city_id") Integer city_id,
//                                             @Query("address") String address,
//                                             @Query("phone") String phone,
//                                             @Query("postcode") String postcode,
//                                             @Query("transport_price") String transport_price,
//                                             @Query("amount") String amount,
//                                             @QueryMap Map<String, String> quantity,
//                                             @Query("currency") String currency,
//                                             @Query("paymentType") String paymentType);
    
    @GET("get-checkout")
    Call<GetCheckOutIDModel> get_checkout_id_cart(@Header("token") String token,
                                                  @Query("country_id") Integer country_id,
                                                  @Query("city_id") Integer city_id,
                                                  @Query("address") String address,
                                                  @Query("phone") String phone,
                                                  @Query("postcode") String postcode,
                                                  @Query("transport_price") String transport_price,
                                                  @Query("type") String type);
    
    @GET("get-checkout")
    Call<GetCheckOutIDModel> get_checkout_id_order(@Header("token") String token,
                                                   @Query("country_id") String country_id,
                                                   @Query("city_id") String city_id,
                                                   @Query("address") String address,
                                                   @Query("phone") String phone,
                                                   @Query("postcode") String postcode,
                                                   @Query("transport_price") String transport_price,
                                                   @Query("total") String total,
                                                   @Query("type") String type,
                                                   @Query("order_id") String order_id);
    
    
    @GET("get-invoices")
    Call<BillsModel> getBills(@Header("token") String token, @Query("page") int currentPage);
    
    @GET
    Call<ResponseBody> downloadCv(@Url String url);
    
    @GET("visa-checkout")
    Call<DeleteCartModel> deleteCart(@Header("token") String token,
                                     @Query("id") String checkOutId);
    
    
    @FormUrlEncoded
    @POST("check-out")
    Call<CashCartPayModel> cart_cash(@Header("token") String token,
                                     @Field("payment") String payment,
                                     @Field("country_id") Integer country_id,
                                     @Field("city_id") Integer cityCode,
                                     @Field("address") String address,
                                     @FieldMap Map<String, String> quantityListMap,
                                     @Field("phone") String phone);
}
