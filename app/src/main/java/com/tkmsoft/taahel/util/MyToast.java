package com.tkmsoft.taahel.util;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.R;

import androidx.fragment.app.FragmentActivity;

/**
 * Created by MahmoudAyman on 2/27/2019.
 **/
public class MyToast {
    private FragmentActivity mContext;

    public MyToast(FragmentActivity mContext) {
        this.mContext = mContext;
    }

    public void successToast(String msg, int duration) {
        LayoutInflater inflater = mContext.getLayoutInflater();
        View layout = inflater.inflate(R.layout.custome_toast,
                mContext.findViewById(R.id.custom_toast_container));
        LinearLayout linearLayout = layout.findViewById(R.id.custom_toast_container);


        linearLayout.setBackgroundResource(R.drawable.bg_round_toast);
        TextView toastTV = layout.findViewById(R.id.toastTV);
        toastTV.setText(msg);

        Toast toast = new Toast(mContext);
        toast.setView(layout);
        toast.setDuration(duration);
        toast.show();
    }



}
