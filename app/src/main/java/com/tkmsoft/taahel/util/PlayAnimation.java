package com.tkmsoft.taahel.util;

import com.forms.sti.progresslitieigb.ProgressLoadingJIGB;

import androidx.fragment.app.FragmentActivity;

/**
 * Created by MahmoudAyman on 20/02/2019.
 */
public class PlayAnimation {

    FragmentActivity mContext;
    public PlayAnimation(FragmentActivity mContext) {
        this.mContext = mContext;
    }

    public void showAnimationFullScreen(Integer src, String msg, Integer duration) {
        ProgressLoadingJIGB.startLoadingJIGB(mContext, src, msg, 0);

//        final Handler handler = new Handler();
//        handler.postDelayed(() ->
//                ProgressLoadingJIGB.finishLoadingJIGB(mContext),
// duration);
    }

    public void stopAnimation(){
        ProgressLoadingJIGB.finishLoadingJIGB(mContext);
    }

}
