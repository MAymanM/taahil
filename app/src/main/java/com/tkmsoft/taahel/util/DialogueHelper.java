package com.tkmsoft.taahel.util;

import android.app.AlertDialog;
import android.content.Intent;

import com.tkmsoft.taahel.R;

import androidx.fragment.app.FragmentActivity;

/**
 * Created by MahmoudAyman on 20/03/2019.
 */
public class DialogueHelper {

    private final AlertDialog.Builder alertDialogBuilder;
    FragmentActivity mContext;

    public DialogueHelper(FragmentActivity mContext) {
        this.mContext = mContext;
        alertDialogBuilder = new AlertDialog.Builder(mContext);
    }

    public void showGPSEnableDialogue() {
        alertDialogBuilder.setMessage(mContext.getString(R.string.enable_gps))
                .setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.go_to_settings_to_enable),
                        (dialog, id) -> {
                            Intent callGPSSettingIntent = new Intent(
                                    android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            mContext.startActivity(callGPSSettingIntent);
                        });
        alertDialogBuilder.setNegativeButton(mContext.getString(R.string.cancel),
                (dialog, id) -> dialog.cancel());
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
}
