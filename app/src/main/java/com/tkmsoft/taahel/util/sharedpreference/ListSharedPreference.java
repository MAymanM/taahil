package com.tkmsoft.taahel.util.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class ListSharedPreference {

    private Context context;

    public ListSharedPreference(Context context) {
        this.context = context;
    }

    public void setToken(String token) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("api_token", token).apply();
    }

    public void setIsFirstRun(boolean isFirstRun) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean("isFirstRun", isFirstRun).apply();
    }

    public void setUType(String type) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uType", type).apply();
    }

    public void setIsRibbleViewRun(boolean isRibbleViewRun) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean("isRibbleViewRun", isRibbleViewRun).apply();
    }

    public void setLanguage(String lang) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("lang", lang).apply();
    }

    public void setLoginStatus(boolean loginStatus) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean("loginStatus", loginStatus).apply();
    }

    public void setUId(int id) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putInt("uid", id).apply();
    }

    public void setUName(String name) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uname", name).apply();
    }

    public void setPhoneKey(String phoneKey) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("phoneKey", phoneKey).apply();
    }

    public void setPassword(String password) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("password", password).apply();
    }

    public void setDrGender(String drGender) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drGender", drGender).apply();
    }

    public void setDrName(String drName) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drName", drName).apply();
    }

    public void setDrEducation(String drEducation) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drEducation", drEducation).apply();
    }

    public void setDrSpecialization(List<String> specializations) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(specializations);
        prefEditor.putString("drSpecialization", json).apply();
    }


    public void setDrExp(String drExp) {

        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drExp", drExp).apply();
    }

    public void setDrPrice(String drPrice) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drPrice", drPrice).apply();
    }

    public void setDrCurrency(String drCurrency) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drCurrency", drCurrency).apply();
    }

    public void setDrCity(String drCity) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drCity", drCity).apply();
    }

    public void setDrDescription(String drDescription) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drDescription", drDescription).apply();
    }

    public boolean getIsFirstRun() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("isFirstRun", true);
    }

    public String getDrName() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drName", null);
    }

    public String getGender() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uGender", null);
    }

    public String getUType() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uType", "-1");
    }

    public boolean getisRibbleViewRun() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("isRibbleViewRun", false);
    }

    public String getToken() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("api_token", null);
    }

    public String getLanguage() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("lang", "ar");
    }

    public boolean getIsLogged() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("loginStatus", false);
    }

    public String getUId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uid", "id");
    }

    public String getUName() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uname", "agent");
    }

    public String getPhoneKey() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("phoneKey", null);
    }

    public String getPassword() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("password", null);
    }

    public List<String> getDrSpecialization() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        List<String> list = new ArrayList<>();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(prefs.getString("drSpecialization", "nothing"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    list.add(jsonArray.getString(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public void deleteIsFirstRun() {
        context.getSharedPreferences("isFirstRun", 0).edit().clear().apply();
    }

    public void deleteType() {
        context.getSharedPreferences("type", 0).edit().clear().apply();
    }

    public void deleteRibbleViewRun() {
        context.getSharedPreferences("isRibbleViewRun", 0).edit().clear().apply();
    }

    public String getDrDescription() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drDescription", null);
    }

    public String getDrExperience() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drExp", null);
    }

    public String getDrCity() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drCity", null);
    }

    public String getDrPrice() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drPrice", null);
    }

    public String getDrCurrency() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drCurrency", null);
    }

    public String getDrGender() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drGender", null);
    }

    public String getDrEducation() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drEducation", null);
    }

    public void setDrId(int id) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putInt("drId", id).apply();

    }

    public int getDrId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("drId", -1);
    }

    public void setOrderType(String orderType) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("orderType", orderType).apply();
    }

    public String getOrderType() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("orderType", "0");
    }

    public void deleteOrderType() {

    }

    public void setActivityTypeID(int activityTypeID) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putInt("activityTypeID", activityTypeID).apply();
    }

    public int getActivityTypeID() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("activityTypeID", 0);
    }

    public void setIsProfile(boolean isProfile) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean("isDrProfile", isProfile).apply();
    }

    public boolean isDrProfile() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return !prefs.getBoolean("isDrProfile", true);
    }


    public String getUAvatar() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uAvatar", "null");
    }

    public void setUAvatar(String avatar_path) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uAvatar", avatar_path).apply();
    }

    public void setDrTimesList(List<String> drTimesList) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(drTimesList);
        prefEditor.putString("drTimesList", json).apply();
    }

    public List<String> getDrTimesList() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        List<String> list = new ArrayList<>();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(prefs.getString("drTimesList", "nulllist"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    list.add(jsonArray.getString(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public void setDrJobDateId(int job_date_id) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putInt("job_date_id", job_date_id).apply();
    }

    public void setDrSelectedDate(String date) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("selectedDate", date).apply();
    }

    public int getDrJobDateId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("job_date_id", 0);
    }

    public String getDrSelectedDate() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("selectedDate", "SPnull");
    }

    public boolean getisRibbleViewRunForDr() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("ribbleViewRunForDr", false);
    }

    public void setIsRibbleViewRunForDr(boolean b) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean("ribbleViewRunForDr", b).apply();
    }

    public void setCheckedBox(String dayNum, String key, String val) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString(dayNum + "*" + key, val).apply();
    }

    public String getCheckedBox(String dayNum, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(dayNum + "*" + key, "null");
    }

    public void deleteCheckBox(String dayNum, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(dayNum + "*" + key).apply();
    }

    public void setRadioFree(String dayNum, String key, boolean val) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean(dayNum + key, val).apply();
    }

    public boolean getRadioFree(String dayNum, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(dayNum + key, false);
    }

    public void deleteAllSharedPref() {
        PreferenceManager.getDefaultSharedPreferences(context).
                edit().clear().apply();
    }

    public boolean getDeptCheck(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(key, false);
    }

    public void setDeptCheck(String key, boolean b) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean(key, b).apply();
    }

    public void setUEmail(String email) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uEmail", email).apply();
    }

    public String getUEmail() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uEmail", null);
    }

    public void setUBirthDate(String birthdate) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uBirthdate", birthdate).apply();
    }

    public String getUBirthDate() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uBirthdate", null);
    }

    public void setUGender(String gender) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uGender", gender).apply();
    }

    public String getUGender() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uGender", null);
    }

    public void setUCity(String city) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uCity", city).apply();
    }

    public String getUCity() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uCity", null);
    }

    public void setUCountry(String country) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uCountry", country).apply();
    }

    public String getUCountry() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uCountry", null);
    }

    public void setUPhone(String phone) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uPhone", phone).apply();
    }

    public String getUPhone() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uPhone", null);
    }

    public void setUPhoneKey(String phoneKey) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uPhoneKey", phoneKey).apply();
    }

    public String getUPhoneKey() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uPhoneKey", null);
    }

    public void setUReport(String report) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uReport", report).apply();
    }

    public String getUReport() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uReport", null);
    }

    public void isSelectedTimesScheduale(boolean b) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean("isSelectedTimesScheduale", b).apply();
    }

    public boolean getSelectedTimesScheduale() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("isSelectedTimesScheduale", false);
    }

    public void setUAbout(String about) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uAbout", about).apply();
    }

    public String getUAbout() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uAbout", null);
    }

    public void setUEducation(String education) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uEducation", education).apply();
    }

    public String getUEducation() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uEducation", null);
    }

    public void setUExperience(String experiences) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uExperience", experiences).apply();
    }

    public String getUExperience() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uExperience", null);
    }

    public void setULicense(String license) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uLicense", license).apply();
    }

    public void setUPrice(String price) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uPrice", price).apply();
    }

    public String getUPrice() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uPrice", null);
    }

    public void setUSpecialists(List<String> specializations) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(specializations);
        prefEditor.putString("uSpecialists", json).apply();
    }

    public List<String> getUSpecialists() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        List<String> list = new ArrayList<>();
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(prefs.getString("uSpecialists", null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    list.add(jsonArray.getString(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public String getURating() {
        return null;
    }

    public String getUCurrency() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uCurrency", null);
    }

    public void setUCurrency(String currency) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uCurrency", currency).apply();
    }

    public void setUCountryId(Integer countryId) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putInt("uCountryId", countryId).apply();
    }

    public int getUCountryId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("uCountryId", -1);
    }

    public void setUCityId(Integer cityId) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putInt("uCityId", cityId).apply();
    }

    public int getUCityId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("uCityId", -1);
    }

    public void setUCurrencyId(Integer currencyId) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putInt("uCurrencyId", currencyId).apply();
    }

    public int getUCurrencyId() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("uCurrencyId", -1);
    }

    public void setDrRate(float rate) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putFloat("drRate", rate).apply();
    }

    public float getDrRate() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getFloat("drRate", 0);
    }

    public void setULat(String lat) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uLat", lat).apply();
    }

    public String getULat() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uLat", null);
    }

    public void setUUserName(String userName) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("userName", userName).apply();
    }

    public String getUUserName() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("userName", null);
    }

    public void setActivityName(String activityName) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("activityName", activityName).apply();
    }

    public String getActivityName() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("activityName", "activity");
    }

    public void setDrAvatar(String avatar) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("drAvatar", avatar).apply();
    }

    public String getDrAvatar() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("drAvatar", "https://taahel.com/storage/uploads/members/avatars/default.jpg");
    }

    public void setUConfirm(String confirm) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("confirm", confirm).apply();
    }

    public String getUConfirm() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("confirm", "0");
    }

    public void setUApprove(String approve) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("approve", approve).apply();
    }

    public String getUApprove() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("approve", "0");
    }

    public void setULong(String aLong) {

        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("uLong", aLong).apply();
    }

    public String getULong() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("uLong", "0");
    }

    public void setIsFreeChoosen(boolean isFree) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean("isFreeChosen", isFree).apply();
    }

    public boolean getIsFreeChoosen() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return !prefs.getBoolean("isFreeChosen", false);
    }

    public void setIsTimeSelected(boolean isTimeSelected) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putBoolean("isTimeSelected", isTimeSelected).apply();
    }

    public boolean getIsTimeSelected() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("isTimeSelected", false);
    }

    public void setAddsType(String addsType) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("getAddsType", addsType).apply();
    }

    public String getAddsType() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("getAddsType", null);
    }

    public void setDrTimeCheckPos(int key) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putInt("setDrTimeCheckPos", key).apply();
    }


    public Integer getDrTimeCheckPos() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("setDrTimeCheckPos", -1);
    }

    public void deleteDrTimeCheckPos() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("setDrTimeCheckPos").apply();
    }

    public void setBackStack(String val) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("getBackStack", val).apply();
    }

    public String getBackStack() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("getBackStack", null);
    }


    public void setLat(Double lat) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putLong("lat", Double.doubleToLongBits(lat)).apply();
    }

    public Double getLat() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return Double.longBitsToDouble(prefs.getLong("lat", 0));
    }

    public void setLong(Double lat) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putLong("longX", Double.doubleToLongBits(lat)).apply();
    }

    public Double getLong() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return Double.longBitsToDouble(prefs.getLong("longX", 0));
    }

    public void setLocAddress(String address) {
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("locAddress", address).apply();
    }

    public String getLocAddress() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("locAddress", null);
    }

    public void deleteSharedPref(String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key).apply();
    }


}//end ListSharedPreference


