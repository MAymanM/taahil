package com.tkmsoft.taahel.util;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.tkmsoft.taahel.R;


/**
 * Created by MahmoudAyman on 10/27/2018.
 **/
public class MoveToFragment {

    private FragmentActivity fragmentActivity;

    public MoveToFragment(FragmentActivity fragmentActivity) {
        this.fragmentActivity = fragmentActivity;
    }

    public void moveInMain(Fragment destinationFragment) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.main_frameLayout, destinationFragment).commit();
    }

    public void moveInLogin(Fragment fragment) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.login_frame, fragment).commit();
    }
}
