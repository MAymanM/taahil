package com.tkmsoft.taahel.fragments.login;


import android.app.Activity;
import android.app.AlertDialog;

import androidx.fragment.app.Fragment;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.TherapistScheduleAdpt;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.api.auth.register.doctor.dates.DrTimesRegisterModel;
import com.tkmsoft.taahel.model.TherapistScheduleModel;
import com.tkmsoft.taahel.model.api.auth.register.doctor.dates.Status;
import com.tkmsoft.taahel.model.api.auth.register.doctor.dates.Title;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TherapistRegFragment3 extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.submitBtn)
    Button submitBtn;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private List<String> timesList = new ArrayList<>();
    private List<String> typeList = new ArrayList<>();
    private int dayNum;
    private ListSharedPreference listSharedPreference;
    private FragmentActivity mContext;

    private AlertDialog.Builder alertDialogBuilder;
    private String TAG = getClass().getSimpleName();
    private MoveToFragment moveToFragment;

    public TherapistRegFragment3() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_therapist_reg_fragment3, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);

        return rootView;
    }

    private void initUI(View rootView) {
        assert getArguments() != null;
        String dayName = getArguments().getString("day");
        dayNum = getArguments().getInt("dayNum");
        dayTV.setText(dayName);
        initRecyclerView();
        initAdapter();
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }
    }

    private void initAdapter() {
        ArrayList<TherapistScheduleModel> mDataList = new ArrayList<>();
        String[] time_am = getResources().getStringArray(R.array.dr_time_morning);
        String[] time_pm = getResources().getStringArray(R.array.dr_time_night);

        TherapistScheduleModel th;
        for (int i = 0; i < time_pm.length; i++) {
            th = new TherapistScheduleModel(time_pm[i], false, false, false);
            mDataList.add(i, th);
        }

        for (int i = 0; i < time_am.length; i++) {
            th = new TherapistScheduleModel(time_am[i], false, false, false);
            mDataList.add(i, th);
        }

        TherapistScheduleAdpt therapistScheduleAdpt = new TherapistScheduleAdpt(mContext,
                String.valueOf(dayNum), mDataList,
                new TherapistScheduleAdpt.ClickListListeners() {
                    @Override
                    public void onItemCheck(String checkBoxName, int position, boolean isFree) {
                        timesList.add(String.valueOf(position));
                        if (isFree)
                            typeList.add("1"); // set to free
                        else
                            typeList.add("0");
                        int pos = timesList.indexOf(String.valueOf(position));
                        Log.d("TAGYY", "add time: " + position + " in pos: " + pos);
                        Log.d("TAGYY", "add typeList def free in pos: " + pos);
                        Log.d("TAGYY", "all timesList: " + timesList);
                        Log.d("TAGYY", "all types: " + typeList);
                        Log.d("TAGYY", "------------------");
                    }

                    @Override
                    public void onItemUncheck(String checkBoxName, int position) {
                        int pos = timesList.indexOf(String.valueOf(position));
                        typeList.remove(pos);
                        timesList.remove(String.valueOf(position));
                        Log.d("TAGYY", "remove time: " + position + " in pos: " + pos);
                        Log.d("TAGYY", "remove typeList in pos: " + pos);
                        Log.d("TAGYY", "all timesList: " + timesList);
                        Log.d("TAGYY", "all types: " + typeList);
                        Log.d("TAGYY", "------------------");
                    }

                    @Override
                    public void onFreeCheck(String checkBoxName, String name, int position) {

                        int pos = timesList.indexOf(String.valueOf(position));
                        typeList.set(pos, "1");
                        Log.d("TAGYY", "set to free in pos: " + pos + " that has time: " + position);
                        Log.d("TAGYY", "all timesList: " + timesList);
                        Log.d("TAGYY", "all types: " + typeList);
                        Log.d("TAGYY", "------------------");
                    }

                    @Override
                    public void onPaidCheck(String checkBoxName, String name, int position) {
                        int pos = timesList.indexOf(String.valueOf(position));
                        typeList.set(pos, "0");
                        Log.d("TAGYY", "set to paid in pos: " + pos + " that has time: " + position);
                        Log.d("TAGYY", "all timesList: " + timesList);
                        Log.d("TAGYY", "all types: " + typeList);
                        Log.d("TAGYY", "------------------");
                    }

                });
        recyclerView.setAdapter(therapistScheduleAdpt);
        therapistScheduleAdpt.notifyDataSetChanged();
    }

    private void serverDoctorCompleteRegister() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            ApiLink retrofit = MyRetrofitClient.auth().create(ApiLink.class);
            LinkedHashMap<String, String> times = new LinkedHashMap<>();
            for (int i = 0; i < timesList.size(); i++) {
                times.put("times[" + i + "]", timesList.get(i));
            }

            LinkedHashMap<String, String> types = new LinkedHashMap<>();
            for (int i = 0; i < typeList.size(); i++) {
                times.put("type[" + i + "]", typeList.get(i));
            }

            Call<DrTimesRegisterModel> registerCall = retrofit.complete_doctor_dates_er(listSharedPreference.getToken(),
                    String.valueOf(dayNum), times, types);
            registerCall.enqueue(new Callback<DrTimesRegisterModel>() {
                @Override
                public void onResponse(@NonNull Call<DrTimesRegisterModel> call, @NonNull Response<DrTimesRegisterModel> response) {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("1")) {
                                //show msg
                                Title title = status.getTitle();
                                if (title != null) {
                                    setResponseMessage(title);
                                }
                                //do
                                moveToFragment.moveInLogin(new TherapistRegFragment2());
                                listSharedPreference.setIsTimeSelected(true);
                            } else {
                                Title title = status.getTitle();
                                if (title != null) {
                                    setResponseMessage(title);
                                }
                            }
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<DrTimesRegisterModel> call, @NonNull Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (
                Exception exc) {
            exc.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }

    }

    private void setResponseMessage(Title title) {
        if (getLanguage().equals("ar"))
            Toast.makeText(mContext, "" + title.getAr(), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + title.getEn(), Toast.LENGTH_SHORT).show();
    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }

    @OnClick(R.id.submitBtn)
    void onSubmitClick() {
//        if (timesList.size() == 0)
//            Toast.makeText(mContext, getString(R.string.plz_choose_time_first), Toast.LENGTH_SHORT).show();
//        else {
//        Toast.makeText(mContext, ""+listSharedPreference.getToken(), Toast.LENGTH_SHORT).show();
        serverDoctorCompleteRegister();
//        }
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }

        super.onAttach(activity);
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
    }

    private void fireBackButtonEvent() {
        ((LoginActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;
                moveToFragment.moveInLogin(new TherapistRegFragment2());
            }
        });
    }//end back pressed


}