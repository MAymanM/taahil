package com.tkmsoft.taahel.fragments.main.cart;


import androidx.fragment.app.Fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.helper.InitSpinner;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.cart.cash.CashCartPayModel;
import com.tkmsoft.taahel.model.api.checkout.checkoutId.Data;
import com.tkmsoft.taahel.model.api.checkout.checkoutId.GetCheckOutIDModel;
import com.tkmsoft.taahel.model.api.checkout.checkoutId.Status;
import com.tkmsoft.taahel.model.api.deletecart.DeleteCartModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckOutFragment extends Fragment {
    private FragmentActivity mContext;
    private ListSharedPreference listSharedPreference;
    private MainViewsCallBack mainViewsCallBack;
    private MoveToFragment moveToFragment;
    private ApiLink apiLinkPayment, apiLinkBase;
    
    @BindViews({R.id.spinner_country, R.id.spinner_city})
    List<Spinner> spinnerList;
    private Spinner spinner_country, spinner_city;
    
    @BindViews({R.id.addressET, R.id.phoneET, R.id.postalCodeET})
    List<EditText> editTextList;
    private EditText addressET, phoneET, postalCodeET;
    
    @BindViews({R.id.addressTV, R.id.deliveryTV, R.id.totalAllTV})
    List<TextView> textViewList;
    
    @BindViews({R.id.infoCV, R.id.paymentCV})
    List<CardView> cardViewList;
    private CardView infoCV, paymentCV;
    
    @BindViews({R.id.cashRB, R.id.visaRB})
    List<RadioButton> radioButtonList;
    private RadioButton cashRB, visaRB;
    
    @BindViews({R.id.progressBar, R.id.country_spinner_progressBar})
    List<ProgressBar> progressBarList;
    private ProgressBar progressBar, country_spinner_progressBar;
    
    @BindView(R.id.cityLinear)
    LinearLayout cityLinear;
    
    private TextView subTotalTV, deliveryTV, totalAllTV;
    @BindView(R.id.checkoutBtn)
    Button checkoutBtn;
    
    private String TAG = getClass().getSimpleName();
    private ArrayList<String> mCountriesArrayList;
    private ArrayList<String> mDeliveryCashList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList;
    private Integer countryCode, cityCode;
    private InitSpinner initSpinner;
    private List<String> quantitiesList;
    private int prevTotalAll;
    private String paymentMethod = "cash";
    private List<CountriesModel.DataBean.CountriesBean> dataCountryList;
    private String address, phone, postalCode;
    private String checkOutId;
    
    
    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        } else throw new ActivityNotFoundException();
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
        super.onAttach(context);
        try {
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        ConnectionDetector connectionDetector = new ConnectionDetector(context);
        listSharedPreference = new ListSharedPreference(mContext);
        initSpinner = new InitSpinner(mContext);
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            prevTotalAll = Integer.valueOf(Objects.requireNonNull(bundle.getString("totalAll")));
            quantitiesList = bundle.getStringArrayList("quntityArray");
        }
        apiLinkPayment = MyRetrofitClient.getPayment().create(ApiLink.class);
        apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);
    }
    
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_check_out, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }
    
    private void initUI() {
        setViews();
        
        subTotalTV.setText(String.valueOf(prevTotalAll));
        
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        
        initCountrySpinner();
        
        serverCountrySpinner();
    }
    
    private boolean isTxtNull(EditText editText) {
        return editText.getText().toString().trim().length() == 0;
    }
    
    private void setViews() {
        //spinners
        spinner_country = spinnerList.get(0);
        spinner_city = spinnerList.get(1);
        
        //progressbar
        progressBar = progressBarList.get(0);
        country_spinner_progressBar = progressBarList.get(1);
        progressBar.setVisibility(View.GONE);
        
        //EditText
        addressET = editTextList.get(0);
        phoneET = editTextList.get(1);
        postalCodeET = editTextList.get(2);
        
        //TextView
        subTotalTV = textViewList.get(0);
        deliveryTV = textViewList.get(1);
        totalAllTV = textViewList.get(2);
        
        subTotalTV.setText(String.valueOf(prevTotalAll));
        
        //cardView
        infoCV = cardViewList.get(0);
        paymentCV = cardViewList.get(1);
        
        //radiobutton
        cashRB = radioButtonList.get(0);
        visaRB = radioButtonList.get(1);
        
        
        cashRB.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                paymentMethod = "cash";
            }
        });
        
        visaRB.setOnCheckedChangeListener((buttonView, isChecked) ->
        {
            if (isChecked) {
                paymentMethod = "bank";
            }
        });
        
        
    }
    
    
    private Call<CountriesModel> callGetCountry() {
        return apiLinkBase.getCountries();
    }
    
    private void serverCountrySpinner() {
        country_spinner_progressBar.setVisibility(View.VISIBLE);
        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CountriesModel.StatusBean status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                CountriesModel.DataBean data = response.body().getData();
                                if (data != null) {
                                    dataCountryList = data.getCountries();
                                    if (dataCountryList != null && !dataCountryList.isEmpty()) {
                                        for (int i = 0; i < dataCountryList.size(); i++) {
                                            mCountriesArrayList.add(dataCountryList.get(i).getName_ar());
                                            mCountriesIdList.add(dataCountryList.get(i).getId());
                                        }//end for
                                        initCountrySpinner();
                                    }
                                }
                            }
                        }
                    }
                }
                country_spinner_progressBar.setVisibility(View.GONE);
            }
            
            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                country_spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end serverRegister()
    
    private void initCountrySpinner() {
        
        initSpinner.setSpinner(spinner_country, mCountriesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    countryCode = mCountriesIdList.get(position);
                    showCitySpinner(true);
                    initCitySpinner();
                } else {
                    countryCode = -1;
                    cityCode = -1;
                    showCitySpinner(false);
                }
            }
            
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            
            }
        });
    }
    
    private void showCitySpinner(boolean b) {
        if (b)
            cityLinear.setVisibility(View.VISIBLE);
        else
            cityLinear.setVisibility(View.GONE);
        
    }
    
    private void initCitySpinner() {
        ArrayList<String> mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);
        
        mDeliveryCashList = new ArrayList<>();
        mDeliveryCashList.add("0");
        
        
        for (int i = 0; i < dataCountryList.size(); i++) {
            if (countryCode == dataCountryList.get(i).getId()) {
                for (int k = 0; k < dataCountryList.get(i).getCities().size(); k++) {
                    if (getLanguage().equals("ar"))
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_ar());
                    else
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_en());
                    mCitiesIdList.add(dataCountryList.get(i).getCities().get(k).getId());
                    mDeliveryCashList.add(dataCountryList.get(i).getCities().get(k).getTransport_price());
                }
            }
        }
        
        initSpinner.setSpinner(spinner_city, mCitiesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    cityCode = mCitiesIdList.get(position);
                    deliveryTV.setText(mDeliveryCashList.get(position));
                    setResult();
                } else {
                    cityCode = -1;
                    deliveryTV.setText("");
                    totalAllTV.setText("");
                }
            }
            
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            
            }
        });
    }
    
    private void setResult() {
        String res = String.valueOf(prevTotalAll + Integer.valueOf(deliveryTV.getText().toString()));
        totalAllTV.setText(res);
    }
    
    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.serToolbarTitle(getString(R.string.complete_bill));
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.showAddFab(false);
    }
    
    
    private Call<GetCheckOutIDModel> callGetCheckoutId() {
//        LinkedHashMap<String, String> quantityListMap = new LinkedHashMap<>();
//        for (int i = 0; i < quantitiesList.size(); i++) {
//            quantityListMap.put("quantity[" + i + "]", quantitiesList.get(i));
//            Log.d(TAG, "qunt checkout: " + quantityListMap.get("quantity[" + i + "]"));
//        }
        String delivery = deliveryTV.getText().toString();
        String totalAll = totalAllTV.getText().toString();
        Timber.d("callGetCheckoutId: " + "\ncity:" + cityCode + "\naddress:" + address +
                "\nphone:" + phone + "\ndeliverycash:" + delivery + "\ntotalAmount" + totalAll);
        return apiLinkPayment.get_checkout_id_cart(listSharedPreference.getToken(),
                countryCode,
                cityCode,
                address,
                phone,
                postalCode,
                delivery,
                "cart");
    }
    
    private void serverGetCheckoutId() {
        progressBar.setVisibility(View.VISIBLE);
        checkoutBtn.setEnabled(false);
        callGetCheckoutId().enqueue(new Callback<GetCheckOutIDModel>() {
            @Override
            public void onResponse(@NonNull Call<GetCheckOutIDModel> call, @NonNull Response<GetCheckOutIDModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("1")) {
                                Data data = response.body().getData();
                                if (data != null) {
                                    goToCheckOut(data.getId());
                                    checkOutId = data.getId();
                                    Timber.d("onResponse: %s", data.getId());
                                }
                            } else {
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
                checkoutBtn.setEnabled(true);
            }
            
            @Override
            public void onFailure(@NonNull Call<GetCheckOutIDModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                checkoutBtn.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
    
    private Call<CashCartPayModel> callCartCash() {
        LinkedHashMap<String, String> quantityListMap = new LinkedHashMap<>();
        for (int i = 0; i < quantitiesList.size(); i++) {
            quantityListMap.put("quantity[" + i + "]", quantitiesList.get(i));
            Timber.d(TAG, "qunt checkout: %s", quantityListMap.get("quantity[" + i + "]"));
        }
        return apiLinkBase.cart_cash(listSharedPreference.getToken(),
                "cash",
                countryCode,
                cityCode,
                address,
                quantityListMap,
                phone);
    }
    
    private void goToCheckOut(String id) {
        Set<String> paymentBrands = new LinkedHashSet<>();
        
        paymentBrands.add("VISA");
        paymentBrands.add("MASTER");
        
        //change Connect.ProviderMode.TEST to Connect.ProviderMode.LIVE in production
        CheckoutSettings checkoutSettings = new CheckoutSettings(id, paymentBrands, Connect.ProviderMode.LIVE);
        checkoutSettings.setShopperResultUrl("https://taahel.com/result");
//        ComponentName receiverComponentName = new ComponentName("com.tkmsoft.taahel", Objects.requireNonNull(CheckoutBroadcastReceiver.class.getCanonicalName()));
//        Intent intent = checkoutSettings.createCheckoutActivityIntent(mContext, receiverComponentName);
        Intent intent = checkoutSettings.createCheckoutActivityIntent(mContext);
        checkoutSettings.setWebViewEnabledFor3DSecure(true);
        // Set shopper result URL
        
        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);
        
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case CheckoutActivity.RESULT_OK:
                /* transaction completed */
                deleteCart();
                break;
            case CheckoutActivity.RESULT_CANCELED:
                /* shopper canceled the checkout process */
                Timber.d("onActivityResult: canceled");
                Toast.makeText(mContext, "canceled", Toast.LENGTH_SHORT).show();
                break;
            case CheckoutActivity.RESULT_ERROR:
                /* error occurred */
                PaymentError error = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR);
                Timber.e("onActivityResult: " + "error" + error.getErrorInfo() + "\n" + error.getErrorMessage());
        }
    }
    
    private void deleteCart() {
        callDeleteCart().enqueue(new Callback<DeleteCartModel>() {
            @Override
            public void onResponse(@NotNull Call<DeleteCartModel> call, @NotNull Response<DeleteCartModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.deletecart.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("1")) {
                            Toast.makeText(mContext, "" + getString(R.string.payment_done), Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            
            @Override
            public void onFailure(@NotNull Call<DeleteCartModel> call, @NotNull Throwable t) {
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    
    private Call<DeleteCartModel> callDeleteCart() {
        Timber.d("check out id: %s", checkOutId);
        return apiLinkPayment.deleteCart(listSharedPreference.getToken(),
                checkOutId);
    }
    
    @OnClick(R.id.completePaymentBtn)
    void onCompletedBtn() {
        if (countryCode != -1) {
            if (cityCode != -1) {
                if (isEditTxtEmpty(addressET)) {
                    address = addressET.getText().toString();
                    if (isEditTxtEmpty(phoneET)) {
                        phone = phoneET.getText().toString();
                        if (isEditTxtEmpty(postalCodeET)) {
                            postalCode = postalCodeET.getText().toString();
                            showInfoLayout(false);
                            showPaymentLayout(true);
                        }
                    } else
                        Toast.makeText(mContext, "" + getString(R.string.enter_phone), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(mContext, "" + getString(R.string.enter_address), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();
        }
    }
    
    private boolean isEditTxtEmpty(EditText editText) {
        return editText.getText().toString().trim().length() != 0;
    }
    
    private void showInfoLayout(boolean b) {
        if (b)
            infoCV.setVisibility(View.VISIBLE);
        else
            infoCV.setVisibility(View.GONE);
    }
    
    private void showPaymentLayout(boolean b) {
        if (b)
            paymentCV.setVisibility(View.VISIBLE);
        else
            paymentCV.setVisibility(View.GONE);
    }
    
    @OnClick(R.id.checkoutBtn)
    void onProtectedBtn() {
        if (cashRB.isChecked()) {
            paymentMethod = "cash";
            serverCashCheckOut();
        } else if (visaRB.isChecked()) {
            paymentMethod = "bank";
            serverGetCheckoutId();
        } else
            Toast.makeText(mContext, "" + getString(R.string.enter_payment_method), Toast.LENGTH_SHORT).show();
    }
    
    private void serverCashCheckOut() {
        progressBar.setVisibility(View.VISIBLE);
        checkoutBtn.setEnabled(false);
        callCartCash().enqueue(new Callback<CashCartPayModel>() {
            @Override
            public void onResponse(@NonNull Call<CashCartPayModel> call, @NonNull Response<CashCartPayModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(mContext, "" + getString(R.string.payment_done), Toast.LENGTH_SHORT).show();
                        moveToFragment.moveInMain(new CartFragment());
                    }
                }
                progressBar.setVisibility(View.GONE);
                checkoutBtn.setEnabled(true);
            }
            
            @Override
            public void onFailure(@NonNull Call<CashCartPayModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                checkoutBtn.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
    
    
    @Override
    public void onResume() {
        super.onResume();
    }
    
    @Override
    public void onPause() {
        //callGetProducts().cancel();
        super.onPause();
    }
    
    @Override
    public void onDetach() {
        mainViewsCallBack = null;
        super.onDetach();
    }
    
    private void fireBackButtonEvent() {
        try {
            ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new CartFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
    //end back pressed
}
