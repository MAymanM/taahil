package com.tkmsoft.taahel.fragments.main.adds.activity;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.addings.activity.SubMyAddsActivityAdapter;
import com.tkmsoft.taahel.fragments.main.adds.MyAddsFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.adds.activity.Data;
import com.tkmsoft.taahel.model.api.adds.activity.Datum;
import com.tkmsoft.taahel.model.api.adds.activity.MyActivitiesModel;
import com.tkmsoft.taahel.model.api.adds.activity.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MahmoudAyman on 28/01/2019.
 */
public class SubMyAddsActivityFragment extends Fragment implements SubMyAddsActivityAdapter.ListAllClickListeners {
    private MainViewsCallBack mainViewsCallBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ListSharedPreference listSharedPreference;

    private int currentPage;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLink;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    SubMyAddsActivityAdapter subMyAddsEduAdapter;
    MoveToFragment moveToFragment;

    public SubMyAddsActivityFragment() {

    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        fireBackButtonEvent();
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.getProfile().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sub_my_adds_center, container, false);
        ButterKnife.bind(this, rootView);
        currentPage = 1;
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initPullRefreshLayout();
        initAdapter();
        initRecyclerView();
        loadFirstPage();
    }


    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(subMyAddsEduAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void initAdapter() {
        subMyAddsEduAdapter = new SubMyAddsActivityAdapter(mContext, new ArrayList<>(), this);
    }


    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);
        Log.d(TAG, "loadFirstPage: ");
        // To ensure list is visible when retry button in error view is clicked

        callGetMyActivity().enqueue(new Callback<MyActivitiesModel>() {
            @Override
            public void onResponse(@NonNull Call<MyActivitiesModel> call, @NonNull Response<MyActivitiesModel> response) {
                // Got data. Send it to adapter
                assert response.body() != null;
                Status status = response.body().getStatus();
                if (status != null) {
                    if (status.getType().equals("success")) {
                        Data data = response.body().getData();
                        if (data != null) {
                            List<Datum> datumList = data.getData();
                            if (datumList != null) {
                                if (!datumList.isEmpty()){
                                    List<Datum> result = new ArrayList<>();
                                    for (int i = 0; i < datumList.size(); i++) {
                                        if (datumList.get(i).getApprove().equals("1"))
                                            result.add(datumList.get(i));
                                    }
                                    subMyAddsEduAdapter.replaceData(result);
                                }
                            } else
                                isLastPage = true;
                        }else
                            isLastPage = true;
                    } else {
                        isLastPage = true;
                    }
                }
                progressBar.setVisibility(View.GONE);

                pullRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(@NonNull Call<MyActivitiesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);
        callGetMyActivity().enqueue(new Callback<MyActivitiesModel>() {
            @Override
            public void onResponse(@NonNull Call<MyActivitiesModel> call, @NonNull Response<MyActivitiesModel> response) {

                assert response.body() != null;
                Status status = response.body().getStatus();
                if (status != null) {
                    if (status.getType().equals("success")) {
                        Data data = response.body().getData();
                        if (data != null) {
                            List<Datum> datumList = data.getData();
                            if (datumList != null) {
                                if (!datumList.isEmpty()){
                                    List<Datum> result = new ArrayList<>();
                                    for (int i = 0; i < datumList.size(); i++) {
                                        if (datumList.get(i).getApprove().equals("1"))
                                            result.add(datumList.get(i));
                                    }
                                    subMyAddsEduAdapter.updateData(result);
                                }
                            } else
                                isLastPage = true;
                        }else
                            isLastPage = true;
                    } else {
                        isLastPage = true;
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<MyActivitiesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private String fetchErrorMessage(Throwable throwable) {

        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!connectionDetector.isConnectingToInternet()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Call<MyActivitiesModel> callGetMyActivity() {
        return apiLink.getMyAddsActivity(listSharedPreference.getToken(),
                listSharedPreference.getAddsType(),
                currentPage);
    }


    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefreshLayout.setOnRefreshListener(() -> moveToFragment.moveInMain(new SubMyAddsActivityFragment()));
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.serToolbarTitle(getString(R.string.activities));
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.showAddFab(false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }


    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new MyAddsFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

    @Override
    public void onItemClick(Datum data, int pos) {
        Fragment fragment = new EditActivityFragment();
        Bundle bundle = new Bundle();
        bundle.putString("pId", String.valueOf(data.getId()));
        bundle.putString("image", data.getPhoto());
        bundle.putString("nameAr", data.getNameAr());
        bundle.putString("nameEn", data.getNameEn());
        bundle.putInt("countryId", data.getCountry().getId());
        bundle.putInt("cityId", data.getCity().getId());
        bundle.putInt("typeId", data.getCategory().getId());
        bundle.putString("addressAr", data.getAddressAr());
        bundle.putString("addressEn", data.getAddressEn());
        bundle.putString("link", data.getLink());
        bundle.putString("start_date", data.getStartDate());
        bundle.putString("duration", data.getDuration());
        bundle.putString("static_phone", data.getStaticPhone());
        bundle.putString("phone", data.getPhone());
        bundle.putString("lat", data.getLat());
        bundle.putString("long", data.getLong());
        fragment.setArguments(bundle);

        moveToFragment.moveInMain(fragment);
    }
}