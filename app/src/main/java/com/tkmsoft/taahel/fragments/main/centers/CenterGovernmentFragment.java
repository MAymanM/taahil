package com.tkmsoft.taahel.fragments.main.centers;


import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.ads.AdsAdapter;
import com.tkmsoft.taahel.adapters.drugs.DrugsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.AdsModel;
import com.tkmsoft.taahel.model.api.drugs.view.Comment;
import com.tkmsoft.taahel.model.api.drugs.view.Data;
import com.tkmsoft.taahel.model.api.drugs.view.Datum;
import com.tkmsoft.taahel.model.api.drugs.view.DrugsModel;
import com.tkmsoft.taahel.model.api.drugs.view.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CenterGovernmentFragment extends Fragment implements DrugsAdapter.ListAllClickListeners {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.viewPager)
    ViewPager mPager;

    @BindView(R.id.indicator)
    CircleIndicator circleIndicator;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.ad_progressBar)
    ProgressBar ad_progressBar;

    @BindView(R.id.ads_cardView)
    CardView ads_cardView;

    private ListSharedPreference listSharedPreference;

    private int currentPage;
    private MainViewsCallBack mainViewsCallBack;
    private DrugsAdapter drugsAdapter;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    private ApiLink apiLinkCenters;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    private int currentPager = 0;

    private String cityCode = "null", countryCode = "null", rate = "null";
    private boolean isFilter;
    private MoveToFragment moveToFragment;

    public CenterGovernmentFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cityCode = getArguments().getString("city");
            countryCode = getArguments().getString("country");
            rate = getArguments().getString("rate");
            isFilter = getArguments().getBoolean("isFilter");
        } else
            isFilter = false;
        Log.d(TAG, "onCreate: " + "country: " + countryCode + "\ncity: " + cityCode + "\nrate: " + rate + "\ncategCode: " + "1" + "\nisFilter: " + isFilter);
        apiLinkCenters = MyRetrofitClient.getDrug().create(ApiLink.class);
        currentPage = 1;
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        try {
            context = getActivity();
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_center_government, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        drugsAdapter = new DrugsAdapter(new ArrayList<>(), this);
        initRecyclerView();
        setupFilterButton();
        setAddFab();
    }

    @OnClick(R.id.govBtn)
    protected void onGovBtn() {
        loadFirstPage();
    }

    @OnClick(R.id.privateBtn)
    protected void onPrivateBtn() {
        moveToFragment.moveInMain(new CenterPrivateFragment());
    }

    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(drugsAdapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void loadFirstPage() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            // To ensure list is visible when retry button in error view is clicked
            callGetDrugs().enqueue(new Callback<DrugsModel>() {
                @Override
                public void onResponse(@NonNull Call<DrugsModel> call, @NonNull Response<DrugsModel> response) {
                    // Got data. Send it to adapter
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            serverAds();
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> results = data.getProducts().getData();
                                if (isFilter) {
                                    attemptFilter(results, 1);
                                } else {
                                    drugsAdapter.replaceData(results);
                                }
                            }
                        } else {
                            isLastPage = true;
                        }
                    }

                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<DrugsModel> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            Toast.makeText(mContext, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void attemptFilter(List<Datum> results, int pos) {
        int rateInt;
        if (!rate.equals("null"))
            rateInt = Integer.valueOf(rate);
        else
            rateInt = 0;
        List<Datum> newArray = new ArrayList<>();
        for (int i = 0; i < results.size(); i++) {
            String resCountryCode = String.valueOf(results.get(i).getCountry().getId());
            String resCityCode = String.valueOf(results.get(i).getCity().getId());
            int resRate = results.get(i).getCountRate();

            if (!countryCode.equals("null")) {
                if (!cityCode.equals("null")) {
                    if (!rate.equals("null")) {
                        if (resCityCode.equals(cityCode) && resCountryCode.equals(countryCode) && resRate >= rateInt) {
                            newArray.add(results.get(i));
                        }
                    } else if (resCountryCode.equals(countryCode) && resCityCode.equals(cityCode)) {
                        newArray.add(results.get(i));
                    }
                }//end city
                else if (!rate.equals("null")) {
                    if (resCountryCode.equals(countryCode) && resRate >= rateInt) {
                        newArray.add(results.get(i));
                    }
                } else {
                    if (resCountryCode.equals(countryCode)) {
                        newArray.add(results.get(i));
                    }
                }
            }//end country
            else if (!rate.equals("null")) {
                if (resRate >= rateInt) {
                    newArray.add(results.get(i));
                }
            }
        }//end for
        if (pos == 1)
            drugsAdapter.replaceData(newArray);
        else
            drugsAdapter.updateData(newArray);
    }

    private void loadNextPage() {
        try {
            callGetDrugs().enqueue(new Callback<DrugsModel>() {
                @Override
                public void onResponse(@NonNull Call<DrugsModel> call, @NonNull Response<DrugsModel> response) {

                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> results = data.getProducts().getData();
                                if (isFilter) {
                                    attemptFilter(results, 2);
                                } else {
                                    drugsAdapter.updateData(results);
                                }
                            }
                        } else
                            isLastPage = true;
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DrugsModel> call, @NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Toast.makeText(mContext, getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }


    private Call<DrugsModel> callGetDrugs() {
        return apiLinkCenters.getDrugs("1", currentPage);
    }


    private void setupFilterButton() {
        ImageButton imageButton = mContext.findViewById(R.id.toolbar_filter_button);
        imageButton.setOnClickListener(view ->
                mainViewsCallBack.sendCategNum("1", "center"));
    }

    private void setAddFab() {
        FloatingActionButton fab = mContext.findViewById(R.id.main_fab);
        fab.setOnClickListener(view -> {
                    listSharedPreference.deleteSharedPref("lat");
                    listSharedPreference.deleteSharedPref("longX");
                    moveToFragment.moveInMain(new AddCentersFrag());
                }
        );
    }

    private void serverAds() {
        ad_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<AdsModel> sliderImages = retrofit.getAds("ads?type=1");
        sliderImages.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(@NonNull Call<AdsModel> call, @NonNull Response<AdsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            initImageSlider(response.body().getData().getAds_info());
                            ads_cardView.setVisibility(View.VISIBLE);
                        } else {
                            ads_cardView.setVisibility(View.GONE);
                        }
                    }
                }
                ad_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<AdsModel> call, @NonNull Throwable t) {
                ad_progressBar.setVisibility(View.GONE);
                ads_cardView.setVisibility(View.GONE);
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end server()

    private void initImageSlider(final List<AdsModel.DataBean.AdsInfoBean> adsInfo) {
        try {
            mPager.setAdapter(new AdsAdapter(getActivity(), adsInfo,
                    (adsInfoBeans, position) ->
                    {
                        //on ad click

                    }));
            circleIndicator.setViewPager(mPager);

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = () -> {
                if (currentPager == adsInfo.size()) {
                    currentPager = 0;
                }
                mPager.setCurrentItem(currentPager++, true);
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2500, 2500);
        } catch (Exception ignored) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadFirstPage();
        mainViewsCallBack.serToolbarTitle(getString(R.string.centers));
        mainViewsCallBack.showFilterBtn(true);
        mainViewsCallBack.showAddFab(true);
    }

    @Override
    public void onPause() {
        callGetDrugs().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        mainViewsCallBack = null;
        super.onDetach();

    }

    private ArrayList<String> getCommentsContent(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getContent());
        }
        return sList;
    }

    private ArrayList<String> getCommentsRate(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getRate());
        }
        return sList;
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
            }
        });
    }//end back pressed

    @Override
    public void onItemClick(Datum drugsBean, int pos) {
        Fragment fragment = new CenterDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("pId", String.valueOf(drugsBean.getId()));
        bundle.putString("image", drugsBean.getPhoto());
        bundle.putInt("typeId", drugsBean.getCategory().getId());

        if (getLanguage().equals("ar")) {
            bundle.putString("type", drugsBean.getCategory().getNameAr());
            bundle.putString("name", drugsBean.getNameAr());
            bundle.putString("country", drugsBean.getCountry().getNameAr());
            bundle.putString("city", drugsBean.getCity().getNameAr());
            bundle.putString("address", drugsBean.getAddressAr());
        } else {
            bundle.putString("type", drugsBean.getCategory().getNameEn());
            bundle.putString("name", drugsBean.getNameEn());
            bundle.putString("country", drugsBean.getCountry().getNameEn());
            bundle.putString("city", drugsBean.getCity().getNameEn());
            bundle.putString("address", drugsBean.getAddressEn());
        }

        bundle.putString("link", drugsBean.getWeb());
        bundle.putString("phone", drugsBean.getStaticPhone());
        bundle.putString("mobile", drugsBean.getPhone());
        bundle.putFloat("rate", drugsBean.getCountRate());
        bundle.putString("lat", drugsBean.getLat());
        bundle.putString("longX", drugsBean.getLong());
        bundle.putStringArrayList("commentsContent", getCommentsContent(drugsBean.getComments()));
        bundle.putStringArrayList("commentsRate", getCommentsRate(drugsBean.getComments()));
        bundle.putStringArrayList("cmntsUserNames", getCommentsUserNames(drugsBean.getComments()));
        bundle.putStringArrayList("cmntsUserImg", getCommentsUserImgs(drugsBean.getComments()));

        fragment.setArguments(bundle);
        moveToFragment.moveInMain(fragment);
    }

    private ArrayList<String> getCommentsUserImgs(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getAvatar());
        }
        return sList;
    }

    private ArrayList<String> getCommentsUserNames(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getName());
        }
        return sList;
    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }
}