package com.tkmsoft.taahel.fragments.login;


import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import androidx.fragment.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.api.auth.register.patient.Data;
import com.tkmsoft.taahel.model.api.auth.register.patient.Member;
import com.tkmsoft.taahel.model.api.auth.register.patient.Patient;
import com.tkmsoft.taahel.model.api.auth.register.patient.RegisterPatientModel;
import com.tkmsoft.taahel.model.api.auth.register.patient.Status;
import com.tkmsoft.taahel.model.api.auth.register.patient.Title;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class MostafedRegFragment extends Fragment implements PermissionCallback, ErrorCallback {

    private static final int PICK_FILE_REQUEST = 9966;

    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.radioGroup)
    RadioGroup radioGenderGroup;
    @BindView(R.id.emailET)
    EditText emailET;
    @BindView(R.id.birthDateET)
    EditText birthDateET;
    @BindView(R.id.reportET)
    EditText reportET;
    @BindView(R.id.save_button)
    Button btn_save;
    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private int mYear, mMonth, mDay;
    String dayNum, monthNum, yearNum;
    String countryCode, cityCode;
    ListSharedPreference listSharedPreference;
    private ArrayList<String> mCitiesArrayList, mCountriesArrayList;
    private View rootView;
    private String TAG = getClass().getSimpleName();

    // Storage Permissions
    String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };

    private FragmentActivity mContext;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList;
    private String image_path = "";
    private File file;

    public MostafedRegFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_mostafed_reg, container, false);
        ButterKnife.bind(this, rootView);
        listSharedPreference = new ListSharedPreference(MostafedRegFragment.this.getActivity().getApplicationContext());
//        Toast.makeText(mContext, ""+listSharedPreference.getToken(), Toast.LENGTH_SHORT).show();
        fireBackButtonEvent();
        initUI();
        return rootView;
    }

    private void initUI() {
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        serverCountrySpinner();
        initCountrySpinner();
        checkPermissions();
    }

    private void checkPermissions() {
        int REQUEST_PERMISSIONS = 347;
        new AskPermission.Builder(this).setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION)
                .setCallback(this)
                .setErrorCallback(this)
                .request(REQUEST_PERMISSIONS);
    }

    private void initCountrySpinner() {
        spinner_country.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCountriesArrayList);
        spinner_country.setAdapter(adapter);
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(i - 1));
                    cityRelative.setVisibility(View.VISIBLE);
                    serverCitiesSpinner(i);
                } else
                    cityRelative.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinner_country);
            }
        });
    }

    private void initCitiesSpinner() {
        mCitiesArrayList = new ArrayList<>();
        mCitiesIdList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList.add(0, 0);

        spinner_city.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCitiesArrayList);
        spinner_city.setAdapter(adapter);
        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    cityCode = String.valueOf(mCitiesIdList.get(i));
                else
                    cityCode = null;
//                Toast.makeText(mContext, ""+cityCode, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void serverCitiesSpinner(final int countryId) {
        initCitiesSpinner();
        spinner_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> citiesCall = retrofit.getCountries();
        citiesCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                spinner_progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                            }
                        }
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void serverCountrySpinner() {

        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> registerCall = retrofit.getCountries();
        registerCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                            mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                            mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                        }
                        initCountrySpinner();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverRegister()

    private void serverMosta() {
        progressBar.setVisibility(View.VISIBLE);

        RequestBody mFile2 = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part report = MultipartBody.Part.createFormData("report", file.getName(), mFile2);
        RequestBody country_id = RequestBody.create(MediaType.parse("text/plain"), countryCode);
        RequestBody city_id = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody user_email = RequestBody.create(MediaType.parse("text/plain"), emailET.getText().toString());
        RequestBody user_share = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody user_day = RequestBody.create(MediaType.parse("text/plain"), dayNum);
        RequestBody user_month = RequestBody.create(MediaType.parse("text/plain"), monthNum);
        RequestBody user_year = RequestBody.create(MediaType.parse("text/plain"), yearNum);

        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), getGender());

        ApiLink retrofit = MyRetrofitClient.auth().create(ApiLink.class);


        Call<RegisterPatientModel> registerCall = retrofit.complete_register_patient(listSharedPreference.getToken(),
                report, country_id, city_id, user_email, user_share, gender, user_day, user_month, user_year);

        registerCall.enqueue(new Callback<RegisterPatientModel>() {
            @Override
            public void onResponse(@NonNull Call<RegisterPatientModel> call, @NonNull Response<RegisterPatientModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("1")) {
                                //show msg
                                Title title = status.getTitle();
                                if (title != null) {
                                    setResponseMessage(title);
                                }
                                //save data
                                Data data = response.body().getData();
                                if (data != null) {
                                    Member member = data.getMember();
                                    if (member != null) {
                                        Patient patient = member.getPatient();
                                        if (patient != null) {
                                            saveUserInfo(member, patient);
                                            listSharedPreference.setLoginStatus(true);
                                            //move screen
                                            Intent myIntent = new Intent();
                                            myIntent.setClassName(MyApp.getContext().getPackageName(),
                                                    Objects.requireNonNull(MainActivity.class.getCanonicalName()));
                                            startActivity(myIntent);
                                        }
                                    }
                                }
                            } else {
                                Title title = status.getTitle();
                                if (title != null) {
                                    setResponseMessage(title);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RegisterPatientModel> call, @NonNull Throwable t) {
                try {
//                        Toast.makeText(getActivitiesCategs(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                    Log.d(TAG, "on fail: " + t.getMessage());
                } catch (Exception r) {
                    Log.d(TAG, "excepetion: " + r.getMessage());
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }//end server

    private void setResponseMessage(Title title) {
        if (getLang().equals("ar"))
            Toast.makeText(mContext, "" + title.getAr(), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + title.getEn(), Toast.LENGTH_SHORT).show();
    }

    private String getLang() {
        return listSharedPreference.getLanguage();
    }

    private void serverMostaNullReport() {
        try {
            progressBar.setVisibility(View.VISIBLE);

            String user_email = emailET.getText().toString();

            ApiLink retrofit = MyRetrofitClient.auth().create(ApiLink.class);
            Call<RegisterPatientModel> registerCall = retrofit.complete_register_patient_null_report(listSharedPreference.getToken(),
                    countryCode, cityCode, user_email, "1", getGender(),
                    dayNum, monthNum, yearNum, "213123", "12312321");

            registerCall.enqueue(new Callback<RegisterPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<RegisterPatientModel> call, @NonNull Response<RegisterPatientModel> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Status status = response.body().getStatus();
                            if (status != null) {
                                if (status.getType().equals("1")) {
                                    //show msg
                                    Title title = status.getTitle();
                                    if (title != null) {
                                        setResponseMessage(title);
                                    }
                                    //save data
                                    Data data = response.body().getData();
                                    if (data != null) {
                                        Member member = data.getMember();
                                        if (member != null) {
                                            Patient patient = member.getPatient();
                                            if (patient != null) {
                                                saveUserInfo(member, patient);
                                                listSharedPreference.setLoginStatus(true);
                                                //move screen
                                                Intent myIntent = new Intent();
                                                myIntent.setClassName(MyApp.getContext().getPackageName(),
                                                        Objects.requireNonNull(MainActivity.class.getCanonicalName()));
                                                startActivity(myIntent);
                                            }
                                        }
                                    }
                                }else {
                                    Title title = status.getTitle();
                                    if (title != null) {
                                        setResponseMessage(title);
                                    }
                                }
                            }
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<RegisterPatientModel> call, @NonNull Throwable t) {
                    try {
                        Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                        Log.d(TAG, "on fail: " + t.getMessage());
                    } catch (Exception r) {
                        Log.d(TAG, "excepetion: " + r.getMessage());
                    }
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception exc) {
            Toast.makeText(mContext, getString(R.string.plz_insert_all_fields), Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void saveUserInfo(Member member, Patient patient) {
        listSharedPreference.setUName(member.getName());
        listSharedPreference.setUEmail(member.getEmail());
        listSharedPreference.setUPhone(member.getPhone());
        listSharedPreference.setUPhoneKey(member.getPhoneKey());
        listSharedPreference.setUAvatar(member.getAvatar());
        listSharedPreference.setUBirthDate(patient.getBirthday());
        listSharedPreference.setUGender(member.getGender());
        if (getLang().equals("ar")) {
            listSharedPreference.setUCountry(member.getCountry().getNameAr());
            listSharedPreference.setUCity(member.getCity().getNameAr());
        } else {
            listSharedPreference.setUCountry(member.getCountry().getNameEn());
            listSharedPreference.setUCity(member.getCity().getNameEn());
        }
        listSharedPreference.setUCountryId(member.getCountry().getId());
        listSharedPreference.setUCityId(member.getCity().getId());
    }

    private String getGender() {
        int genderId = radioGenderGroup.getCheckedRadioButtonId();
        RadioButton radioButton = rootView.findViewById(genderId);
        switch (radioButton.getId()) {
            case R.id.maleRB:
                return "0";
            case R.id.femaleRB:
                return "1";
            default:
                return "-1";
        }
    }

    @OnClick(R.id.reportET)
    protected void onreportETClick() {
        showFileChooser();
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("image/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_PICK);
        //starts new activity to select file and return data
        startActivityForResult(intent, PICK_FILE_REQUEST);
    }

    @OnClick(R.id.save_button)
    protected void onSaveButtonClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                if (!image_path.equals(""))
                    serverMosta();
                else
                    serverMostaNullReport();
            }
        } else {
            if (!image_path.equals(""))
                serverMosta();
            else
                serverMostaNullReport();
        }
    }


    private void fireBackButtonEvent() {

        ((LoginActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction()
                        .replace(R.id.login_frame, new RegisterFragment(), "RegisterFragment")
                        .commit();
            }
        });
    }//end back pressed

    @OnClick(R.id.birthDateET)
    protected void onDateETClick() {
        final DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(), (datepicker, selectedyear, selectedmonth, selectedday) -> {

            Calendar myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, selectedyear);
            myCalendar.set(Calendar.MONTH, selectedmonth);
            myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);

            String myFormat = "dd/MM/yyyy"; //Change as you need
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
            birthDateET.setText(sdf.format(myCalendar.getTime()));

            dayNum = String.valueOf(selectedday);
            monthNum = String.valueOf(datepicker.getMonth() + 1);
            yearNum = String.valueOf(selectedyear);

            mDay = selectedday;
            mMonth = selectedmonth;
            mYear = selectedyear;

        }, 1975, 0, 1);
        //mDatePicker.setTitle("Select date");
        mDatePicker.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, MostafedRegFragment.this.getActivity());
            try {
                assert image_path != null;
                file = new File(image_path);
                reportET.setText(file.getName());
            } catch (Exception exc) {
                Toast.makeText(mContext, "" + exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }//end onActivityResult

    private String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }


    private String getFileName(String uriString, Uri uri, File myFile) {
        if (uriString.startsWith("content://")) {
            Cursor cursor = null;
            String name = "null";
            try {
                cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } catch (Exception exc) {
                exc.printStackTrace();
            } finally {
                assert cursor != null;
                cursor.close();
            }
            return name;
        } else if (uriString.startsWith("file://")) {
            return myFile.getName();
        } else
            return null;
    }

    @Override
    public void onPermissionsGranted(int requestCode) {

    }

    @Override
    public void onPermissionsDenied(int requestCode) {

    }

    @Override
    public void onShowRationalDialog(final PermissionInterface permissionInterface,
                                     int requestCode) {
        Log.d(TAG, "onShowRationalDialog: ");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("We need permissions for this app.");
        builder.setPositiveButton(R.string.ok, (dialog, which) -> permissionInterface.onDialogShown());
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    @Override
    public void onShowSettings(final PermissionInterface permissionInterface, int requestCode) {
        Log.d(TAG, "onShowSettings: ");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("We need permissions for this app. Open setting screen?");
        builder.setPositiveButton(R.string.ok, (dialog, which) -> permissionInterface.onSettingsShown());
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }


}
