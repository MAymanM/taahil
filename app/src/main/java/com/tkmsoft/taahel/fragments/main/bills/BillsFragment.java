package com.tkmsoft.taahel.fragments.main.bills;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.bills.BillsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.bill.BillsModel;
import com.tkmsoft.taahel.model.api.bill.Data;
import com.tkmsoft.taahel.model.api.bill.Datum;
import com.tkmsoft.taahel.model.api.bill.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MahmoudAyman on 2/21/2019.
 **/
public class BillsFragment extends Fragment implements BillsAdapter.ListAllClickListeners {

    private MainViewsCallBack mainViewsCallBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ListSharedPreference listSharedPreference;

    private int currentPage;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLinkBase;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    BillsAdapter billsAdapter;
    String cityCode, countryCode, rate;
    boolean isFilter;
    private MoveToFragment moveToFragment;

    public BillsFragment() {

    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString() + "error");
        }
        fireBackButtonEvent();
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);
        currentPage = 1;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_jobs, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initPullRefreshLayout();
        initRecyclerView();
    }

    private void initRecyclerView() {
        billsAdapter = new BillsAdapter(new ArrayList<>(), this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(billsAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);

        callGetBills().enqueue(new Callback<BillsModel>() {
            @Override
            public void onResponse(@NonNull Call<BillsModel> call, @NonNull Response<BillsModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("1")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> results = data.getData();
                                if (results != null) {
                                    if (!results.isEmpty())
                                        billsAdapter.replaceData(results);
                                    else
                                        isLastPage = true;
                                }
                            }
                        } else {
                            isLastPage = true;
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<BillsModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void loadNextPage() {
        callGetBills().enqueue(new Callback<BillsModel>() {
            @Override
            public void onResponse(@NonNull Call<BillsModel> call, @NonNull Response<BillsModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("1")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> results = data.getData();
                                if (results != null) {
                                    if (!results.isEmpty())
                                        billsAdapter.updateData(results);
                                    else
                                        isLastPage = true;
                                }
                            }
                        } else {
                            isLastPage = true;
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BillsModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private Call<BillsModel> callGetBills() {
        return apiLinkBase.getBills(listSharedPreference.getToken(), currentPage);
    }


    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefreshLayout.setOnRefreshListener(() ->
                moveToFragment.moveInMain(new BillsFragment())
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.my_bills));
        mainViewsCallBack.showAddFab(false);
        loadFirstPage();
    }

    @Override
    public void onPause() {
        callGetBills().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }


    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
            }
        });
    }//end back pressed

    @Override
    public void onItemClick(Datum data, int pos) {
        Fragment fragment = new BillDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", String.valueOf(data.getId()));
        bundle.putString("date", data.getDate());
        bundle.putString("address", data.getAddress());
        bundle.putString("delivery", data.getDelivery());
        bundle.putString("city", data.getCity());

        if (data.getPayment().equals("cash"))
            bundle.putString("payment", getString(R.string.cash));
        else
            bundle.putString("payment", getString(R.string.visa));

        bundle.putString("quantity", data.getProductsCount());


        if (!data.getDetails().isEmpty())
            bundle.putString("sumAll", data.getGrandTotal() + " " + data.getDetails().get(0).getCurrency());
        else
            bundle.putString("sumAll", data.getGrandTotal());

        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> quantity = new ArrayList<>();
        ArrayList<String> price = new ArrayList<>();
        ArrayList<String> total = new ArrayList<>();


        for (int i = 0; i < data.getDetails().size(); i++) {
            name.add(data.getDetails().get(i).getProductName());
            quantity.add(data.getDetails().get(i).getQuantity());
            price.add(data.getDetails().get(i).getPrice());
            total.add(data.getDetails().get(i).getTotal());
        }

        bundle.putStringArrayList("pNameArray", name);
        bundle.putStringArrayList("pQuantityArray", quantity);
        bundle.putStringArrayList("pPriceArray", price);
        bundle.putStringArrayList("pTotalArray", total);
        Log.d(TAG, "onItemClick: " + name);
        fragment.setArguments(bundle);
        moveToFragment.moveInMain(fragment);
    }

    private String getLang() {
        return listSharedPreference.getLanguage();
    }
}