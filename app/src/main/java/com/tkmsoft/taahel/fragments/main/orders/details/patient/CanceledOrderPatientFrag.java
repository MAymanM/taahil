package com.tkmsoft.taahel.fragments.main.orders.details.patient;

import androidx.fragment.app.Fragment;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.main.orders.SubRequestFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MahmoudAyman on 13/09/2018.
 */
public class CanceledOrderPatientFrag extends Fragment {
    @BindView(R.id.codeTV)
    TextView codeTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.timeTV)
    TextView timeTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;
    @BindView(R.id.reasonTV)
    TextView reasonTV;
    @BindView(R.id.paymentTV)
    TextView paymentTV;

    ListSharedPreference listSharedPreference;

    String code, date, name, country, city, address, day, time, orderId,description, paymentStatus;
    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private MainViewsCallBack mainViewsCallBack;
    private String reason;
    private String pId;

    MoveToFragment moveToFragment;

    public CanceledOrderPatientFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            code = arguments.getString("code");
            date = arguments.getString("date");
            name = arguments.getString("name");
            country = arguments.getString("country");
            description = arguments.getString("desc");
            city = arguments.getString("city");
            address = arguments.getString("address");
            day = arguments.getString("day");
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < arguments.getInt("timeSize"); i++) {
                stringBuilder.append(getArguments().getString("time" + i));
                if (i < arguments.getInt("timeSize") - 1) {
                    stringBuilder.append(" ,\n");
                }
            }
            time = stringBuilder.toString();
            orderId = arguments.getString("orderId");
            reason = arguments.getString("reason");
            pId = arguments.getString("pId");
            paymentStatus = arguments.getString("paymentStatus");
        }
    }

    @Override
    public void onAttach(Context activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        fireBackButtonEvent();
        try {
            mainViewsCallBack = (MainViewsCallBack) mContext;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_canceled_order_patient, container, false);
        listSharedPreference = new ListSharedPreference(CanceledOrderPatientFrag.this.getActivity().getApplicationContext());
        ButterKnife.bind(this, rootView);

        initUI(rootView);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.cancled_order));
    }

    private void initUI(View rootView) {
        nameTV.setPaintFlags(nameTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        nameTV.setText(name);
        codeTV.setText(code);
        dateTV.setText(date);
        countryTV.setText(country);
        cityTV.setText(city);
        addressTV.setText(address);
        dayTV.setText(day);
        timeTV.setText(time);
        reasonTV.setText(reason);
        descriptionTV.setText(description);
        codeTV.setText(pId);
        switch (paymentStatus) {
            case "undefined":
                paymentTV.setText(getString(R.string.not_paid_yet));
                break;
            case "cash":
                paymentTV.setText(getString(R.string.cash));
                break;
            case "visa":
                paymentTV.setText(getString(R.string.visa));
                break;
        }
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new SubRequestFragment());
            }
        });
    }//end back pressed
}