package com.tkmsoft.taahel.fragments.main.doctor;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.fragments.main.profile.doctor.DrProfileFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.order.requestADr.RequestADoctorModel;
import com.tkmsoft.taahel.model.api.order.requestADr.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RequestDrFragment2 extends Fragment {

    private FragmentActivity mContext;
    @BindView(R.id.aboutET)
    EditText descriptionET;
    @BindView(R.id.addressET)
    EditText addressET;
    private String TAG = getClass().getSimpleName();
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    ListSharedPreference listSharedPreference;
    private MainViewsCallBack mMainViewsCallBack;
    private MoveToFragment moveToFragment;
    private ConnectionDetector connectionDetector;
    private ApiLink apiLinkOrder;

    public RequestDrFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            mMainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        connectionDetector = new ConnectionDetector(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkOrder = MyRetrofitClient.getOrder().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_therapist_send_request, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {

    }

    @OnClick(R.id.sendReqNowBtn)
    protected void onSubmitClick() {
        if (connectionDetector.isConnectingToInternet())
            serverSendRequestOrder();
        else
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
    }


    private void serverSendRequestOrder() {
        progressBar.setVisibility(View.VISIBLE);

        callRequestADoctor().enqueue(new Callback<RequestADoctorModel>() {
            @Override
            public void onResponse(@NonNull Call<RequestADoctorModel> call, @NonNull Response<RequestADoctorModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                            moveToFragment.moveInMain(new HomeFragment());
                        } else {
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<RequestADoctorModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private Call<RequestADoctorModel> callRequestADoctor() {
        String token = listSharedPreference.getToken();
        String job_date_id = String.valueOf(listSharedPreference.getDrJobDateId());
        String date = listSharedPreference.getDrSelectedDate();
        String desc = descriptionET.getText().toString();
        String address = addressET.getText().toString();

        LinkedHashMap<String, String> timesMap = new LinkedHashMap<>();
        for (int i = 0; i < listSharedPreference.getDrTimesList().size(); i++) {
            timesMap.put("times[" + i + "]", listSharedPreference.getDrTimesList().get(i));
        }
        return apiLinkOrder.requestADoctor(token,
                job_date_id,
                date,
                desc,
                address,
                timesMap);
    }


    @Override
    public void onResume() {
        super.onResume();
        mMainViewsCallBack.showAddFab(false);
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.serToolbarTitle(getString(R.string.send_request));
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new DrProfileFragment());
            }
        });
    }//end back pressed

    @Override
    public void onDetach() {
        super.onDetach();
        mMainViewsCallBack = null;
    }
}