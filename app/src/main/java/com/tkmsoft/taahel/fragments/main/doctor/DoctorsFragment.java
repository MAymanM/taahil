package com.tkmsoft.taahel.fragments.main.doctor;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.DoctorsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.fragments.main.profile.doctor.DrProfileFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.filter.FilterDrsData;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.getDoctors.Data;
import com.tkmsoft.taahel.model.api.getDoctors.Datum;
import com.tkmsoft.taahel.model.api.getDoctors.Doctors;
import com.tkmsoft.taahel.model.api.getDoctors.DoctorsModel;
import com.tkmsoft.taahel.model.api.getDoctors.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class DoctorsFragment extends Fragment implements DoctorsAdapter.ListAllClickListeners {

    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.notFoundTV)
    TextView notFoundTV;
    DoctorsAdapter doctorsAdapter;
    ListSharedPreference listSharedPreference;
    ImageButton filterBtn;
    private MainViewsCallBack mMainViewsCallBack;
    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private String countryCode = "null", cityCode = "null";
    private String genderCode = "null";
    private String specialtyId = "null";
    private String rate = "null";
    private int currentPage;
    private boolean isFilter;
    private ApiLink apiLink;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    FilterDrsData filterDrsData;
    private MoveToFragment moveToFragment;

    public DoctorsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            mMainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        connectionDetector = new ConnectionDetector(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            countryCode = getArguments().getString("country");
            cityCode = getArguments().getString("city");
            genderCode = getArguments().getString("gender");
            specialtyId = getArguments().getString("speciality");
            rate = getArguments().getString("rate");
            isFilter = getArguments().getBoolean("isFilter");
        } else
            isFilter = false;
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_doctors, container, false);
        ButterKnife.bind(this, rootView);
        currentPage = 1;
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        doctorsAdapter = new DoctorsAdapter(new ArrayList<>(), this);
        filterDrsData = new FilterDrsData(countryCode, cityCode, rate, genderCode, specialtyId, doctorsAdapter);
        initRecyclerView();
        progressBar.setVisibility(View.VISIBLE);
        initPullRefreshLayout();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.doctors));
        mMainViewsCallBack.showFilterBtn(true);
        mMainViewsCallBack.showAddFab(false);
        setupFilterBtn();

        loadFirstPage();
    }

    private void setupFilterBtn() {
        filterBtn = getActivity().findViewById(R.id.toolbar_filter_button);
        filterBtn.setOnClickListener(view -> getFragmentManager().beginTransaction().replace(R.id.main_frameLayout, new DoctorsFilter()).commit());
    }


    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefreshLayout.setOnRefreshListener(() -> {
            doctorsAdapter = new DoctorsAdapter(new ArrayList<>(), DoctorsFragment.this);
            loadFirstPage();
        });
    }


    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(doctorsAdapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
//                Toast.makeText(mContext, "" + currentPage, Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);
        // To ensure list is visible when retry button in error view is clicked

        callGetDoctors().enqueue(new Callback<DoctorsModel>() {
            @Override
            public void onResponse(@NonNull Call<DoctorsModel> call, @NonNull Response<DoctorsModel> response) {
                // Got data. Send it to adapter
                try {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                Doctors doctors = data.getDoctors();
                                if (doctors != null) {
                                    List<Datum> results = doctors.getData();
                                    if (results != null) {
                                        if (isFilter) {
                                            filterDrsData.attemptFilter(results, 1);
                                        } else {
                                            doctorsAdapter.replaceData(results);
                                        }
                                    }
                                } else {
                                    isLastPage = true;
                                }
                            }
                        } else {
                            isLastPage = true;
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                    pullRefreshLayout.setRefreshing(false);
                }catch (Exception e){
                    Timber.e(e);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DoctorsModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void loadNextPage() {
        callGetDoctors().enqueue(new Callback<DoctorsModel>() {
            @Override
            public void onResponse(@NonNull Call<DoctorsModel> call, @NonNull Response<DoctorsModel> response) {
                assert response.body() != null;
                Status status = response.body().getStatus();
                if (status != null) {
                    if (status.getType().equals("success")) {
                        Data data = response.body().getData();
                        if (data != null) {
                            Doctors doctors = data.getDoctors();
                            if (doctors != null) {
                                List<Datum> results = doctors.getData();
                                if (results != null) {
                                    if (isFilter) {
                                        filterDrsData.attemptFilter(results, 2);
                                    } else {
                                        doctorsAdapter.updateData(results);
                                    }
                                }
                            }else {
                                isLastPage = true;
                            }
                        }
                    } else {
                        isLastPage = true;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<DoctorsModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private Call<DoctorsModel> callGetDoctors() {
        return apiLink.getDoctors(currentPage);
    }

    private void saveDoctorData(Datum data) {
        listSharedPreference.setDrId(data.getId());
        listSharedPreference.setDrName(data.getName());
        ArrayList<String> specialistsList = new ArrayList<>();
        for (int i = 0; i < data.getSpecializations().size(); i++) {
            specialistsList.add(data.getSpecializations().get(i).getNameAr());
        }
        listSharedPreference.setDrSpecialization(specialistsList);
//        Log.d(TAG, "" + listSharedPreference.getDrSpecialization());

        listSharedPreference.setDrEducation(data.getDoctor().getEducation());
        listSharedPreference.setDrExp(data.getDoctor().getExperiences());
        listSharedPreference.setDrAvatar("https://www.taahel.com/storage/uploads/members/avatars/" + data.getAvatar());
        listSharedPreference.setDrRate(data.getRate().floatValue());
        listSharedPreference.setDrPrice(data.getDoctor().getPrice());
        listSharedPreference.setDrCurrency(data.getCurrency().getNameAr());
        listSharedPreference.setDrCity(data.getCity().getNameAr());
        listSharedPreference.setDrDescription(data.getDoctor().getAbout());
        listSharedPreference.setDrGender(data.getGender());
    }


    @Override
    public void onPause() {
        callGetDoctors().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        mMainViewsCallBack.showFilterBtn(false);
        super.onDetach();
    }


    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new HomeFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

    @Override
    public void onItemClick(Datum datumList, int pos) {
        listSharedPreference.setIsProfile(false);
        saveDoctorData(datumList);
        getFragmentManager().beginTransaction()
                .replace(R.id.main_frameLayout, new DrProfileFragment())
                .commit();
    }
}
