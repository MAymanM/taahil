package com.tkmsoft.taahel.fragments.main.store;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Spinner;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.helper.InitSpinner;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.store.spinner.type.Category;
import com.tkmsoft.taahel.model.api.store.spinner.type.Data;
import com.tkmsoft.taahel.model.api.store.spinner.type.Status;
import com.tkmsoft.taahel.model.api.store.spinner.type.TypeStoreModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreFilterFragment extends Fragment {

    MainViewsCallBack mMainViewsCallBack;
    ListSharedPreference listSharedPreference;

    @BindViews({R.id.spinner_country, R.id.spinner_city, R.id.spinner_field})
    List<Spinner> spinnerList;
    Spinner spinner_country, spinner_city, spinner_field;
    @BindViews({R.id.country_spinner_progressBar, R.id.field_spinner_progressBar})
    List<ProgressBar> progressBarList;
    ProgressBar country_spinner_progressBar, field_spinner_progressBar;
    @BindView(R.id.cityLinearLayout)
    LinearLayout cityLinearLayout;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;

    private ArrayList<String> mCountriesArrayList;
    private ArrayList<String> mFieldsList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mFieldsIdList;
    private Integer countryCode, cityCode, fieldCode;
    private Float rate = 0f ;
    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private ApiLink apiLinkBase, apiLinkStore;
    private MoveToFragment moveToFragment;
    private List<CountriesModel.DataBean.CountriesBean> dataCountryList;
    private InitSpinner initSpinner;

    public StoreFilterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity)
            mContext = (FragmentActivity) activity;
        super.onAttach(activity);
        try {
            mMainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        fireBackButtonEvent();
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        initSpinner = new InitSpinner(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);
        apiLinkStore = MyRetrofitClient.getStore().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_filter_store, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View view) {
        setViews(view);
        setSpinners();

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser)
                ->
                rate = rating);
    }

    private void setViews(View view) {
        spinner_country = spinnerList.get(0);
        spinner_city = spinnerList.get(1);
        spinner_field = spinnerList.get(2);

        country_spinner_progressBar = progressBarList.get(0);
        field_spinner_progressBar = progressBarList.get(1);
    }

    @OnClick(R.id.updateBtn)
    protected void onUpdateClick() {
        if (countryCode == 0 && cityCode == -1 && fieldCode == -1 && rate == 0)
            mMainViewsCallBack.filterStore(countryCode, cityCode, rate, fieldCode, false);
        else
            mMainViewsCallBack.filterStore(countryCode, cityCode, rate, fieldCode, true);

        Log.d(TAG, "onUpdateClick: " + "country: " + countryCode + "\ncity: " + cityCode + "\nrate: " + rate + "\nfieldCode: " + fieldCode);
    }

    private void setSpinners() {
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);

        initCountrySpinner();

        mFieldsList = new ArrayList<>();
        mFieldsIdList = new ArrayList<>();
        mFieldsList.add(0, getString(R.string.field));
        mFieldsIdList.add(0, 0);

        initFieldSpinner();
        serverCountrySpinner();
        serverFieldSpinner();
    }

    private Call<CountriesModel> callGetCountry() {
        return apiLinkBase.getCountries();
    }

    private void serverCountrySpinner() {
        country_spinner_progressBar.setVisibility(View.VISIBLE);
        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CountriesModel.StatusBean status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                CountriesModel.DataBean data = response.body().getData();
                                if (data != null) {
                                    dataCountryList = data.getCountries();
                                    if (dataCountryList != null && !dataCountryList.isEmpty()) {
                                        for (int i = 0; i < dataCountryList.size(); i++) {
                                            mCountriesArrayList.add(dataCountryList.get(i).getName_ar());
                                            mCountriesIdList.add(dataCountryList.get(i).getId());
                                        }//end for
                                        initCountrySpinner();
                                    }
                                }
                            }
                        }
                    }
                }
                country_spinner_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                country_spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end serverRegister()

    private void initCountrySpinner() {

        initSpinner.setSpinner(spinner_country, mCountriesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    countryCode = mCountriesIdList.get(position);
                    showCitySpinner(true);
                    initCitySpinner();
                } else {
                    countryCode = -1;
                    cityCode = -1;
                    showCitySpinner(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCitySpinner() {
        ArrayList<String> mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);

        for (int i = 0; i < dataCountryList.size(); i++) {
            if (countryCode == dataCountryList.get(i).getId()) {
                for (int k = 0; k < dataCountryList.get(i).getCities().size(); k++) {
                    if (getLanguage().equals("ar"))
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_ar());
                    else
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_en());
                    mCitiesIdList.add(dataCountryList.get(i).getCities().get(k).getId());
                }
            }
        }

        initSpinner.setSpinner(spinner_city, mCitiesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    cityCode = mCitiesIdList.get(position);
                } else {
                    cityCode = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void initFieldSpinner() {

        initSpinner.setSpinner(spinner_field, mFieldsList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    fieldCode = mFieldsIdList.get(position);
                } else {
                    fieldCode = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void serverFieldSpinner() {
        field_spinner_progressBar.setVisibility(View.VISIBLE);
        callGetField().enqueue(new Callback<TypeStoreModel>() {
            @Override
            public void onResponse(@NonNull Call<TypeStoreModel> call, @NonNull Response<TypeStoreModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                Data data = response.body().getData();
                                if (data != null) {
                                    List<Category> categoryList = data.getCategories();
                                    if (categoryList != null) {
                                        for (int i = 0; i < categoryList.size(); i++) {
                                            if (getLanguage().equals("ar")) {
                                                mFieldsList.add(categoryList.get(i).getNameAr());
                                            } else {
                                                mFieldsList.add(categoryList.get(i).getNameEn());
                                            }
                                            mFieldsIdList.add(categoryList.get(i).getId());
                                        }//end for

                                        initFieldSpinner();
                                    }
                                }
                            }

                        }
                    }
                }
                field_spinner_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<TypeStoreModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                field_spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end()

    private Call<TypeStoreModel> callGetField() {
        return apiLinkStore.getStoreTypes();
    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }

    private void showCitySpinner(boolean b) {
        if (b)
            cityLinearLayout.setVisibility(View.VISIBLE);
        else
            cityLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.filter_store));
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.showAddFab(false);
    }

    @Override
    public void onDetach() {
        callGetCountry().cancel();
        super.onDetach();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new StoreFragment());
            }
        });

    }//end back pressed

}
