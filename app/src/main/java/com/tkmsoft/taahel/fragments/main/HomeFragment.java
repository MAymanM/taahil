package com.tkmsoft.taahel.fragments.main;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.ImageSliderAdapter;
import com.tkmsoft.taahel.fragments.main.doctor.DoctorsFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.SliderModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.MyBounceInterpolator;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    @BindView(R.id.drs_fab)
    ImageButton drs_fab;
    private MainViewsCallBack mMainViewsCallBack;
    //viewPager
    @BindView(R.id.viewPager)
    ViewPager mPager;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.indicator)
    CircleIndicator circleIndicator;
    @BindView(R.id.sendOpinionBtn)
    Button sendOpinionBtn;
    private static int currentPage = 0;
    private ListSharedPreference listSharedPreference;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    private ApiLink apiLink;
    private ConnectionDetector connectionDetector;
    private MoveToFragment moveToFragment;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
            fireBackButtonEvent();
        }
        try {
            mMainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        super.onAttach(activity);
        listSharedPreference = new ListSharedPreference(mContext);
        connectionDetector = new ConnectionDetector(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, rootView);
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);
        Log.d(TAG, "ؤ:" + "\n" +listSharedPreference.getToken() + "\n");

        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        if (connectionDetector.isConnectingToInternet()) {
            serverImageSlider();
        } else {
            Toast.makeText(mContext, ""+getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
        if (listSharedPreference.getUType().equals("0")) {
            drs_fab.setVisibility(View.GONE);
        } else {
            drs_fab.setVisibility(View.VISIBLE);
            launchTapTargetView(rootView);
        }
        animateShareButton();
    }

    private void animateShareButton() {
        final Animation myAnim = AnimationUtils.loadAnimation(mContext, R.anim.bounce);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);
        sendOpinionBtn.startAnimation(myAnim);
    }

    private void serverImageSlider() {
        progressBar.setVisibility(View.VISIBLE);
        try {
            callGetSlider().enqueue(new Callback<SliderModel>() {
                @Override
                public void onResponse(@NonNull Call<SliderModel> call, @NonNull Response<SliderModel> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            SliderModel.StatusBean statusBean = response.body().getStatus();
                            if (statusBean!= null) {
                                if (statusBean.getType().equals("success")) {
                                    SliderModel.DataBean dataBean = response.body().getData();
                                    if (dataBean!= null) {
                                        initAdapter(response.body().getData().getSliders());
                                    }
                                }
                            }
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }
                @Override
                public void onFailure(@NonNull Call<SliderModel> call, @NonNull Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    t.printStackTrace();
                }
            });
        } catch (Exception ignore) {
        }
    }//end serverRegister()

    private Call<SliderModel> callGetSlider() {
        return apiLink.getSliderImages();
    }

    private void initAdapter(final List<SliderModel.DataBean.SlidersBean> slidersBeans) {
        try {
            mPager.setAdapter(new ImageSliderAdapter(getActivity(), slidersBeans, (data, position) -> {
                //click image
            }));
            circleIndicator.setViewPager(mPager);
            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = () -> {
                if (currentPage == slidersBeans.size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2000, 3000);
        }catch (Exception ignore){

        }
    }

    @Override
    public void onStart() {
        mMainViewsCallBack.serToolbarTitle(getString(R.string.home));
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.showAddFab(false);
        super.onStart();
    }

    @Override
    public void onDetach() {
        callGetSlider().cancel();
        super.onDetach();
    }

    private void launchTapTargetView(View rootView) {
        if (!listSharedPreference.getisRibbleViewRun()) {
            TapTargetView.showFor(mContext,
                    TapTarget.forView(rootView.findViewById(R.id.drs_fab),
                            getString(R.string.doctors),
                            getString(R.string.ribble_description))
                            // All options below are optional
                            .outerCircleColor(R.color.colorAccent)      // Specify a color for the outer circle
                            .outerCircleAlpha(0.76f)            // Specify the alpha amount for the outer circle
                            .targetCircleColor(R.color.white)   // Specify a color for the target circle
                            .titleTextSize(30)                  // Specify the size (in sp) of the title text
                            .titleTextColor(R.color.white)      // Specify the color of the title text
                            .descriptionTextSize(20)            // Specify the size (in sp) of the description text
                            .descriptionTextColor(R.color.colorPrimaryDark)  // Specify the color of the description text
                            .textColor(R.color.white)            // Specify a color for both the title and description text
                            .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                            .dimColor(R.color.black)            // If set, will dim behind the view with 30% opacity of the given color
                            .drawShadow(true)                   // Whether to draw a drop shadow or not
                            .cancelable(true)                  // Whether tapping outside the outer circle dismisses the view
                            .tintTarget(true)                   // Whether to tint the target view's color
                            .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)
                            //.icon(droid)                     // Specify a custom drawable to draw as the target
                            .targetRadius(60),                  // Specify the target radius (in dp)
                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                        @Override
                        public void onTargetClick(TapTargetView view) {
                            super.onTargetClick(view);      // This call is optional
                            YoYo.with(Techniques.Landing).playOn(drs_fab);
                        }

                        @Override
                        public void onOuterCircleClick(TapTargetView view) {
                            super.onOuterCircleClick(view);
                            view.dismiss(true);
                        }
                    });
            listSharedPreference.setIsRibbleViewRun(true);
        }
    }

    @OnClick(R.id.sendOpinionBtn)
    void onBtnClick() {
        moveToFragment.moveInMain(new SentOpinionFragment());
    }

    @OnClick(R.id.drs_fab)
    void onFabClick() {
        moveToFragment.moveInMain(new DoctorsFragment());
    }

    private void fireBackButtonEvent() {
            ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
                @Override
                public void onBackPressed() {
                   mContext.finish();
                }
            });

    }//end back pressed

}