package com.tkmsoft.taahel.fragments.main.education;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.ReviewsAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.ReviewsModel;
import com.tkmsoft.taahel.model.api.comments.add.AddCommentModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class EducationDetailFragment extends Fragment implements OnMapReadyCallback {

    private MainViewsCallBack mainViewsCallBack;

    private String image, name, type, address, country, city, link, lat, longX;
    private float rate;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.typeTV)
    TextView typeTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.linkTV)
    TextView websiteTV;
    @BindView(R.id.imageView)
    ImageView imageView;

    private ListSharedPreference listSharedPreference;
    private FragmentActivity mContext;
    private ApiLink apiLink;
    private EditText commentET;
    private RatingBar ratingBarPop;
    private String pId;

    //Reviews
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private ArrayList<String> commentsContent = new ArrayList<>(), commentsRate = new ArrayList<>(),
            commentsUserNames = new ArrayList<>(), commentsUserImges = new ArrayList<>();
    private MoveToFragment moveToFragment;
    private int typeId;
    private Marker marker;
    private SupportMapFragment mapFragment;

    public EducationDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        try {
            context = getActivity();
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();

        if (bundle != null) {
            pId = bundle.getString("pId");
            image = bundle.getString("image");
            name = bundle.getString("name");
            type = bundle.getString("type");
            typeId = bundle.getInt("typeId");
            address = bundle.getString("address");
            country = bundle.getString("country");
            city = bundle.getString("city");
            link = bundle.getString("link");
            rate = bundle.getFloat("rate");
            lat = bundle.getString("lat");
            longX = bundle.getString("longX");

            commentsContent = bundle.getStringArrayList("commentsContent");
            commentsRate = bundle.getStringArrayList("commentsRate");
            commentsUserNames = bundle.getStringArrayList("cmntsUserNames");
            commentsUserImges = bundle.getStringArrayList("cmntsUserImg");
        }
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapFragment.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_education_detail, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initGMaps();
    }

    private void initGMaps() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            assert fragmentManager != null;
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
    }

    private void initUI(View rootView) {
        initRecyclerView();
        initAdapter();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1,
                GridLayoutManager.HORIZONTAL, false));
    }

    private void initAdapter() {
        ArrayList<ReviewsModel> mDataList = new ArrayList<>();
        for (int i = 0; i < commentsContent.size(); i++) {
            ReviewsModel th = new ReviewsModel(commentsUserNames.get(i), commentsRate.get(i),
                    commentsUserImges.get(i), commentsContent.get(i));
            mDataList.add(i, th);
        }

        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(getActivity(), mDataList,
                (reviewsModel, adapterPosition) -> {
                    //TODO click
                });
        recyclerView.setAdapter(reviewsAdapter);
        reviewsAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.cardView_writecmnt)
    void onCardViewWriteReviewClick() {

        LayoutInflater li = LayoutInflater.from(getActivity());
        final View popUpView = li.inflate(R.layout.activity_write_review, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        // create alert dialog

        alertDialogBuilder.setView(popUpView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        // set prompts.xml to alertdialog builder
        commentET = popUpView.findViewById(R.id.reviewET);
        final ImageButton imageButton = popUpView.findViewById(R.id.close_button);
        ratingBarPop = popUpView.findViewById(R.id.rateBar_review);
        final Button submitBtn = popUpView.findViewById(R.id.submitBtn);
        final ProgressBar progressBar = popUpView.findViewById(R.id.progressBar);
        imageButton.setOnClickListener
                (view -> alertDialog.dismiss());
        // show it
        alertDialog.show();

        submitBtn.setOnClickListener(v ->
                serverAddComment(alertDialog, progressBar));

    }

    private void serverAddComment(final AlertDialog alertDialog, final ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        postCommentModelCall().enqueue(new Callback<AddCommentModel>() {
            @Override
            public void onResponse(@NonNull Call<AddCommentModel> call, @NonNull Response<AddCommentModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().getType().equals("success")) {
                                Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();
                            } else
                                Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddCommentModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "" + R.string.error_msg_no_internet, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Call<AddCommentModel> postCommentModelCall() {
        return apiLink.addComment(listSharedPreference.getToken(),
                pId,
                commentET.getText().toString(),
                String.valueOf(ratingBarPop.getRating()),
                "1");
    }
    @OnClick(R.id.linkTV)
    void onLinkTVClick() {
        try {
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(webIntent);
        } catch (ActivityNotFoundException e) {
            Timber.e(e);
        }
    }

    @Override
    public void onStart() {
        nameTV.setText(name);
        typeTV.setText(type);
        addressTV.setText(address);
        countryTV.setText(country);
        cityTV.setText(city);
        ratingBar.setRating(rate);
        websiteTV.setText(link);
        websiteTV.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        Picasso.get().load(image).into(imageView);
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapFragment.onResume();
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(name);
        mainViewsCallBack.showAddFab(false);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        mapFragment.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapFragment.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                if (typeId == 1) {
//                    moveToFragment.moveInMain(new EduGovernmentFragment());
                    Toast.makeText(mContext, "2", Toast.LENGTH_SHORT).show();
                } else
                    moveToFragment.moveInMain(new EduPrivateFragment());
            }
        });
    }//end back pressed

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GoogleMap mMap = googleMap;
        LatLng latLng = new LatLng(Double.valueOf(lat), Double.valueOf(longX));
        mMap.addMarker(new MarkerOptions().position(latLng).title(name));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

    }
}
