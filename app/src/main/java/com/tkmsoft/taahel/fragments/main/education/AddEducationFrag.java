package com.tkmsoft.taahel.fragments.main.education;


import android.Manifest;
import android.app.Activity;

import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MapsActivity;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.education.AddEducationsModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class AddEducationFrag extends Fragment {

    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.addBtn)
    Button addBtn;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.name_arET)
    EditText name_arET;
    @BindView(R.id.name_enET)
    EditText name_enET;
    @BindView(R.id.address_ar_ET)
    EditText address_ar_ET;
    @BindView(R.id.address_en_ET)
    EditText address_en_ET;
    @BindView(R.id.linkET)
    EditText linkET;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    private View rootView;

    @BindView(R.id.locationTV)
    TextView locationTV;
    private MainViewsCallBack mainViewsCallBack;
    private ListSharedPreference listSharedPreference;
    private ArrayList<String> mCountriesArrayList, mCitiesArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList;
    private final int IMAGE_CODE = 2001;
    private String image_path = "";
    private String countryCode, cityCode;
    private FragmentActivity mContext;
    private MoveToFragment moveToFragment;

    public AddEducationFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        fireBackButtonEvent();
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "must implement OnFragmentInteractionListener");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_education, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        mCountriesArrayList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList = new ArrayList<>();
        mCountriesIdList.add(0, 0);
        serverCountrySpinner();
        initCountrySpinner();
    }

    private int getTypeId() {
        int typeId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = rootView.findViewById(typeId);
        if (radioButton.getText().toString().equals(getString(R.string.government))) {
            return 1;
        } else
            return 2;
    }
    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.add_activity));
        if (listSharedPreference.getLat() != 0.0) {
            if (listSharedPreference.getLanguage().equals("ar"))
                locationTV.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_location_on_green_700_24dp, 0);
            else
                locationTV.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_location_on_green_700_24dp, 0, 0, 0);
        }
    }
    @OnClick(R.id.locationTV)
    void onLocationTVClick() {
        startActivity(new Intent(MyApp.getContext().getApplicationContext(), MapsActivity.class));
    }
    ///////////////Spinners///////////////

    private void serverCountrySpinner() {
        initCountrySpinner();
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> registerCall = retrofit.getCountries();
        registerCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                            mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                            mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }//end serverCountry()

    private void serverCitiesSpinner(final int countryId) {
        initCitiesSpinner();
        spinner_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> citiesCall = retrofit.getCountries();
        citiesCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                spinner_progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                            }
                        }
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void initCountrySpinner() {
        spinner_country.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(mContext, mCountriesArrayList);
        spinner_country.setAdapter(adapter);
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    countryCode = String.valueOf(i);
                    cityRelative.setVisibility(View.VISIBLE);
                    serverCitiesSpinner(i);
                } else {
                    cityRelative.setVisibility(View.GONE);
                    countryCode = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinner_country);
            }
        });
    }

    private void initCitiesSpinner() {
        mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);

        spinner_city.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(mContext, mCitiesArrayList);
        spinner_city.setAdapter(adapter);

        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    cityCode = String.valueOf(mCitiesIdList.get(i));
                else
                    cityCode = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(cityRelative);
            }
        });
    }
    ///////////////////////////////////////////////////////

    private void serverAddEducation() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getEducation().create(ApiLink.class);
        File file1;
        file1 = new File(image_path);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);

        String api_tokenPart = listSharedPreference.getToken();
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", file1.getName(), mFile1);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getTypeId()));
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody address_arPart = RequestBody.create(MediaType.parse("text/plain"), address_ar_ET.getText().toString());
        RequestBody address_enPart = RequestBody.create(MediaType.parse("text/plain"), address_en_ET.getText().toString());
        RequestBody linkPart = RequestBody.create(MediaType.parse("text/plain"), linkET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(listSharedPreference.getLat()));
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(listSharedPreference.getLong()));

        Call<AddEducationsModel> registerCall = retrofit.addEducation(api_tokenPart, photoPart, name_arPart, name_enPart, type_idPart,
                city_idPart, address_arPart, address_enPart, linkPart, latPart, longXPart);
        registerCall.enqueue(new Callback<AddEducationsModel>() {
            @Override
            public void onResponse(@NonNull Call<AddEducationsModel> call, @NonNull Response<AddEducationsModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            moveToFragment.moveInMain(new EduGovernmentFragment());
                        } else
                            Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddEducationsModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    ///////////////On Click///////////////
    @OnClick(R.id.imageView)
    protected void imageOnClick() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_CODE);
    }

    @OnClick(R.id.addBtn)
    protected void onNextBtnClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(mContext,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                addOnPermissionGranted();
            }
        } else {
            //phone under API 21
            addOnPermissionGranted();
        }
    }

    private void addOnPermissionGranted() {
        if (!image_path.equals(""))
            if (countryCode != null)
                if (cityCode != null)
                    serverAddEducation();
                else
                    Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + getString(R.string.plz_choose_pic), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, getActivity());
            showImageInView(uri);
        }//end if check for data

    }//end onActivityResult

    private void showImageInView(Uri image_path) {
        Picasso.get().load(image_path).into(imageView);
    }

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }


    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new EduGovernmentFragment());
            }
        });
    }//end back pressed
}//end class name
