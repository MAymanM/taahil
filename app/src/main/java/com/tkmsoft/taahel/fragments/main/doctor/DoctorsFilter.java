package com.tkmsoft.taahel.fragments.main.doctor;


import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.specialization.Data;
import com.tkmsoft.taahel.model.api.specialization.Specialization;
import com.tkmsoft.taahel.model.api.specialization.SpecializationsModel;
import com.tkmsoft.taahel.model.api.specialization.Status;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DoctorsFilter extends Fragment {

    MainViewsCallBack mMainViewsCallBack;
    ListSharedPreference listSharedPreference;

    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindViews({R.id.maleCB, R.id.femaleCB})
    List<CheckBox> checkBoxesList;
    @BindViews({R.id.spinner_country, R.id.spinner_city, R.id.spinner_department_popup})
    List<Spinner> spinnersList;
    @BindView(R.id.updateBtn)
    Button updateBtn;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;

    CheckBox maleCB, femaleCB;

    private ArrayList<String> mCountriesArrayList, mCitiesArrayList, mDeptArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mDeptIdList;
    private String countryCode = "null", cityCode = "null";
    private String specialtyId = "null";
    private String TAG = getClass().getSimpleName();
    private String rate = "null";
    private boolean isFilter = false;
    ApiLink apiLink;

    public DoctorsFilter() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        FragmentActivity mContext;
        if (activity instanceof FragmentActivity)
            mContext = (FragmentActivity) activity;
        super.onAttach(activity);
        try {
            mMainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        fireBackButtonEvent();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.doctors));
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.showAddFab(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_doctors_filter, container, false);
        listSharedPreference = new ListSharedPreference(DoctorsFilter.this.getActivity().getApplicationContext());
        ButterKnife.bind(this, view);
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);
        maleCB = checkBoxesList.get(0);
        femaleCB = checkBoxesList.get(1);
        initUI(view);
        return view;
    }

    private void initUI(View view) {
        setSpinners();
        maleCB.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                femaleCB.setChecked(false);
            }
        });

        femaleCB.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                maleCB.setChecked(false);
            }
        });

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            rate = String.valueOf(Math.round(rating));
            if (rate.equals("0"))
                rate = "null";
        });

    }

    @OnClick(R.id.updateBtn)
    protected void onUpdateClick() {
        String genderCode = "null";
        if (maleCB.isChecked())
            genderCode = "0";
        else if (femaleCB.isChecked())
            genderCode = "1";
        else
            genderCode = "null";

        if (countryCode.equals("null") && cityCode.equals("null") && specialtyId.equals("null") && rate.equals("null") && genderCode.equals("null"))
            mMainViewsCallBack.filterDoctor(countryCode, cityCode, genderCode, specialtyId, rate, false);
        else
            mMainViewsCallBack.filterDoctor(countryCode, cityCode, genderCode, specialtyId, rate, true);

        Log.d(TAG, "onUpdateClick: " + "\ncountry: " + countryCode + "\ncity: " + cityCode + "\nspecialist: " +
                specialtyId + "\nrate: " + rate + "\ngender: " + genderCode);

    }

    private void setSpinners() {


        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        serverCountrySpinner();
        initCountrySpinner();

        mDeptArrayList = new ArrayList<>();
        mDeptArrayList.add(0, getString(R.string.department_));
        mDeptIdList = new ArrayList<>();
        mDeptIdList.add(0, 0);
        serverSpecialization();
        initDeptSpinner();
    }

    private Call<CountriesModel> getCountriesCall() {
        return apiLink.getCountries();
    }

    private void serverCountrySpinner() {
        try {
            getCountriesCall().enqueue(new Callback<CountriesModel>() {
                @Override
                public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                                mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                                mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                            }
                            initCountrySpinner();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                    Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            });
        } catch (Exception ignore) {

        }
    }

    private void initCountrySpinner() {
        try {

            spinnersList.get(0).getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                    PorterDuff.Mode.SRC_ATOP);
            DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCountriesArrayList);
            spinnersList.get(0).setAdapter(adapter);
            spinnersList.get(0).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i > 0) {
                        countryCode = String.valueOf(mCountriesIdList.get(i));
                        cityRelative.setVisibility(View.VISIBLE);
                        serverCitiesSpinner(i);
                    } else {
                        cityRelative.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    YoYo.with(Techniques.Shake).playOn(spinnersList.get(0));
                }
            });
        } catch (Exception ignore) {

        }
    }

    private void serverCitiesSpinner(final int countryId) {
        try {
            initCitiesSpinner();
            spinner_progressBar.setVisibility(View.VISIBLE);
            getCountriesCall().enqueue(new Callback<CountriesModel>() {
                @Override
                public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                    spinner_progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        try {
                            if (response.body() != null) {
                                for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                    mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                    mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                                }
                            }
                        } catch (Exception exc) {
//                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                    spinner_progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            });
        } catch (Exception ignore) {

        }
    }//end serverCitiesSpinner()

    private void initCitiesSpinner() {
        mCitiesArrayList = new ArrayList<>();
        mCitiesIdList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList.add(0, 0);

        spinnersList.get(1).getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCitiesArrayList);
        spinnersList.get(1).setAdapter(adapter);
        spinnersList.get(1).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    cityCode = String.valueOf(mCitiesIdList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private Call<SpecializationsModel> callGetSpecialists() {
        return apiLink.getSpecialists();
    }

    private void serverSpecialization() {

        callGetSpecialists().enqueue(new Callback<SpecializationsModel>() {
            @Override
            public void onResponse(@NonNull Call<SpecializationsModel> call, @NonNull Response<SpecializationsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                Data data = response.body().getData();
                                if (data != null) {
                                    List<Specialization> specializationList = data.getSpecializations();
                                    if (specializationList != null && !specializationList.isEmpty()) {
                                        for (int i = 0; i < specializationList.size(); i++) {
                                            if (listSharedPreference.getLanguage().equals("ar"))
                                                mDeptArrayList.add(specializationList.get(i).getNameAr());
                                            else
                                                mDeptArrayList.add(specializationList.get(i).getNameEn());
                                            mDeptIdList.add(specializationList.get(i).getId());
                                        }
                                        initDeptSpinner();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SpecializationsModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void initDeptSpinner() {
        spinnersList.get(2).getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mDeptArrayList);
        spinnersList.get(2).setAdapter(adapter);
        spinnersList.get(2).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    specialtyId = String.valueOf(mDeptIdList.get(i));
                } else
                    specialtyId = "null";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onPause() {
        getCountriesCall().cancel();
        callGetSpecialists().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.main_frameLayout, new DoctorsFragment())
                            .commit();
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed
}