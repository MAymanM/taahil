package com.tkmsoft.taahel.fragments.main.reports;


import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.reports.ReportsPermissionAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.reports.patient.changepermission.RepChangePermissionModel;
import com.tkmsoft.taahel.model.api.reports.patient.permission.Datum;
import com.tkmsoft.taahel.model.api.reports.patient.permission.ReportsPermissionModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportsPermissionFragment extends Fragment {

    ListSharedPreference listSharedPreference;
    private FragmentActivity mContext;
    private MainViewsCallBack mMainViewsCallBack;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    private String TAG = getClass().getSimpleName();
    private List<Datum> mDataList;
    private MoveToFragment moveToFragment;

    public ReportsPermissionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
            fireBackButtonEvent();
        }
        try {
            mMainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        super.onAttach(activity);
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reports_permission, container, false);
        ButterKnife.bind(this, rootView);
        initPullRefreshLayout();

        initRecyclerView();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serverGetPermissionReport();
    }


    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
    }

    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        pullRefreshLayout.setOnRefreshListener(() -> moveToFragment.moveInMain(new ReportsPermissionFragment()));
    }

    private void serverGetPermissionReport() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<ReportsPermissionModel> citiesCall = retrofit.getPermissionReports(listSharedPreference.getToken());
        citiesCall.enqueue(new Callback<ReportsPermissionModel>() {
            @Override
            public void onResponse(@NonNull Call<ReportsPermissionModel> call, @NonNull Response<ReportsPermissionModel> response) {
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getStatus().getType().equals("success")) {
                        mDataList = new ArrayList<>();
                        mDataList = response.body().getData().getData();
                        initAdapter();
                    }
//                    else
//                        Toast.makeText(mContext, "" + response.body().getStatus().getStatus(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReportsPermissionModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end server

    private void initAdapter() {

        ReportsPermissionAdapter reportsAdapter = new ReportsPermissionAdapter(getActivity().getApplicationContext(), mDataList,
                (data, adapterPosition, isChecked) -> serverChangePermission(String.valueOf(data.getId()), isChecked));

        recyclerView.setAdapter(reportsAdapter);
        reportsAdapter.notifyDataSetChanged();
    }

    private void serverChangePermission(String reportId, final boolean isChecked) {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<RepChangePermissionModel> citiesCall = retrofit.changePermission(reportId);
        citiesCall.enqueue(new Callback<RepChangePermissionModel>() {
            @Override
            public void onResponse(@NonNull Call<RepChangePermissionModel> call, @NonNull Response<RepChangePermissionModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getStatus().getType().equals("success")) {
                        if (isChecked)
                            Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                        if (!isChecked)
                            moveToFragment.moveInMain(new ReportsPermissionFragment());
                    } else
                        Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(@NonNull Call<RepChangePermissionModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.reports_permission));
        mMainViewsCallBack.showAddFab(false);
        mMainViewsCallBack.showFilterBtn(false);
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.main_frameLayout, new HomeFragment())
                            .commit();
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

}
