package com.tkmsoft.taahel.fragments.main.activities;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MapsActivity;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.activities.AddActivityModel;
import com.tkmsoft.taahel.model.api.activities.view.category.ActivityCategModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class AddActivitiesFrag extends Fragment {

    @BindView(R.id.spinner_activityKind)
    Spinner spinner_activityKind;
    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.addBtn)
    Button addBtn;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.name_arET)
    EditText name_arET;
    @BindView(R.id.name_enET)
    EditText name_enET;
    @BindView(R.id.durationET)
    EditText durationET;
    @BindView(R.id.address_ar_ET)
    EditText address_ar_ET;
    @BindView(R.id.address_en_ET)
    EditText address_en_ET;
    @BindView(R.id.staticPhone_ET)
    EditText staticPhone_ET;
    @BindView(R.id.phoneET)
    EditText phoneET;
    @BindView(R.id.linkET)
    EditText linkET;
    @BindView(R.id.locationTV)
    TextView locationTV;
    private String countryCode, cityCode, typeCode;
    private MainViewsCallBack mainViewsCallBack;
    private ArrayList<String> mCurrencyArrayList, mCountriesArrayList, mCitiesList, mActivitiesList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mActivitiesIdList;
    private ListSharedPreference listSharedPreference;
    private final int IMAGE_CODE = 2001;
    private String image_path = "";
    private int mDay, mMonth, mYear;
    private FragmentActivity mContext;
    private String dayNum, yearNum, monthNum;
    private ApiLink apiLink;
    private MoveToFragment moveToFragment;

    public AddActivitiesFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.getBaseActivity().create(ApiLink.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_activities, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        mCountriesArrayList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList = new ArrayList<>();
        mCountriesIdList.add(0, 0);
        serverCountrySpinner();
        initCountrySpinner();
        mActivitiesList = new ArrayList<>();
        mActivitiesList.add(0, getString(R.string.activity_type));
        mActivitiesIdList = new ArrayList<>();
        mActivitiesIdList.add(0, 0);

        serverActivitiesSpinner();
        initActivitiesSpinner();
    }

    ///////////////Spinners///////////////

    private void serverActivitiesSpinner() {

        callGetActivityCategs().enqueue(new Callback<ActivityCategModel>() {
            @Override
            public void onResponse(@NonNull Call<ActivityCategModel> call, @NonNull Response<ActivityCategModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getData() != null) {
                        for (int i = 0; i < response.body().getData().getCategories().size(); i++) {
                            mActivitiesList.add(response.body().getData().getCategories().get(i).getName_ar());
                            mActivitiesIdList.add(response.body().getData().getCategories().get(i).getId());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ActivityCategModel> call, @NonNull Throwable t) {
//                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                serverActivitiesSpinner();
            }
        });
    }//end server

    private Call<ActivityCategModel> callGetActivityCategs() {
        return apiLink.getActivitiesCategs();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.add_activity));
        if (listSharedPreference.getLat() != 0.0) {
            if (listSharedPreference.getLanguage().equals("ar"))
                locationTV.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_location_on_green_700_24dp, 0);
            else
                locationTV.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_location_on_green_700_24dp, 0, 0, 0);
        }
    }
    @OnClick(R.id.locationTV)
    void onLocationTVClick() {
        startActivity(new Intent(MyApp.getContext().getApplicationContext(), MapsActivity.class));
    }
    private void serverCountrySpinner() {
        initCountrySpinner();
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> registerCall = retrofit.getCountries();
        registerCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getData() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                            mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                            mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCountry()

    private void serverCitiesSpinner(final int countryId) {
        initCitiesSpinner();
        spinner_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> citiesCall = retrofit.getCountries();
        citiesCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                spinner_progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        assert response.body() != null;
                        if (response.body().getData() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                mCitiesList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                            }
                        }
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void initCountrySpinner() {
        spinner_country.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(mContext, mCountriesArrayList);
        spinner_country.setAdapter(adapter);
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    countryCode = String.valueOf(i);
                    cityRelative.setVisibility(View.VISIBLE);
                    serverCitiesSpinner(i);
                } else
                    cityRelative.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinner_country);
            }
        });
    }

    private void initCitiesSpinner() {
        mCitiesList = new ArrayList<>();
        mCitiesList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);

        spinner_city.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(mContext.getApplicationContext(), mCitiesList);
        spinner_city.setAdapter(adapter);
        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    cityCode = String.valueOf(mCitiesIdList.get(i));
                else
                    cityCode = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(cityRelative);
            }
        });
    }

    private void initActivitiesSpinner() {

        spinner_activityKind.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(mContext, mActivitiesList);
        spinner_activityKind.setAdapter(adapter);
        spinner_activityKind.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    typeCode = String.valueOf(mActivitiesIdList.get(i));
                else
                    typeCode = null;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    ///////////////////////////////////////////////////////

    private void serverAddAvtivity() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBaseActivity().create(ApiLink.class);
        File file1;
        file1 = new File(image_path);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);

        String api_tokenPart = listSharedPreference.getToken();
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", file1.getName(), mFile1);
        //RequestBody country_idPart = RequestBody.create(MediaType.parse("text/plain"), countryCode);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody start_datePart = RequestBody.create(MediaType.parse("text/plain"), dateTV.getText().toString());
        RequestBody durationPart = RequestBody.create(MediaType.parse("text/plain"), durationET.getText().toString());
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody address_arPart = RequestBody.create(MediaType.parse("text/plain"), address_ar_ET.getText().toString());
        RequestBody address_enPart = RequestBody.create(MediaType.parse("text/plain"), address_en_ET.getText().toString());
        RequestBody linkPart = RequestBody.create(MediaType.parse("text/plain"), linkET.getText().toString());
        RequestBody static_phonePart = RequestBody.create(MediaType.parse("text/plain"), staticPhone_ET.getText().toString());
        RequestBody phonePart = RequestBody.create(MediaType.parse("text/plain"), phoneET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(listSharedPreference.getLat()));
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(listSharedPreference.getLong()));

        Call<AddActivityModel> registerCall = retrofit.addActivity(api_tokenPart, photoPart, name_arPart, name_enPart, type_idPart,
                start_datePart, durationPart, city_idPart, address_arPart, address_enPart,
                linkPart, static_phonePart, phonePart, latPart, longXPart);
        registerCall.enqueue(new Callback<AddActivityModel>() {
            @Override
            public void onResponse(@NonNull Call<AddActivityModel> call, @NonNull Response<AddActivityModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.main_frameLayout, new HomeFragment())
                                    .commit();
                        } else
                            Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddActivityModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    ///////////////On Click///////////////
    @OnClick(R.id.imageView)
    protected void imageOnClick() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_CODE);
    }

    @OnClick(R.id.addBtn)
    protected void onNextBtnClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                addOnPermissionGranted();
            }
        } else {
            addOnPermissionGranted();
        }
    }

    private void addOnPermissionGranted() {
        if (!image_path.equals(""))
            if (countryCode != null)
                if (cityCode != null)
                    if (typeCode != null)
                        serverAddAvtivity();
                    else
                        Toast.makeText(mContext, "" + getString(R.string.enter_type), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + getString(R.string.plz_choose_pic), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.dateTV)
    void onDateTVBtnClick() {
        DatePickerDialog mDatePicker = new DatePickerDialog(
                mContext, (datepicker, selectedyear, selectedmonth, selectedday) ->
        {
            Calendar myCalendar = Calendar.getInstance();
            myCalendar.set(Calendar.YEAR, selectedyear);
            myCalendar.set(Calendar.MONTH, selectedmonth);
            myCalendar.set(Calendar.DAY_OF_MONTH, selectedday);
            String myFormat = "yyyy-MM-dd"; //Change as you need

            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);
            dateTV.setText(sdf.format(myCalendar.getTime()));

            dayNum = String.valueOf(selectedday);
            monthNum = String.valueOf(datepicker.getMonth() + 1);
            yearNum = String.valueOf(selectedyear);

            mDay = selectedday;
            mMonth = selectedmonth;
            mYear = selectedyear;
        }, 1975, 0, 1);
        //mDatePicker.setTitle("Select date");
        mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePicker.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, mContext);
            showImageInView(uri);
        }//end if check for data

    }//end onActivityResult

    private void showImageInView(Uri uri) {
        Picasso.get().load(uri).into(imageView);
    }

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDetach() {
        callGetActivityCategs().cancel();
        super.onDetach();
        mainViewsCallBack = null;
    }



    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new ActivitiesFragment());
            }
        });
    }//end back pressed

}
