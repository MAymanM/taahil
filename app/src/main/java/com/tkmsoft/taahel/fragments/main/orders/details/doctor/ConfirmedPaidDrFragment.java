package com.tkmsoft.taahel.fragments.main.orders.details.doctor;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.main.orders.RequestsFragment;
import com.tkmsoft.taahel.fragments.main.orders.SubRequestFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.interfaces.SendPatientProfData;
import com.tkmsoft.taahel.model.api.order.orderDetails.dr.accept.AcceptDrModel;
import com.tkmsoft.taahel.model.api.order.orderDetails.dr.accept.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmedPaidDrFragment extends Fragment {

    private static final int LICENCE_CODE = 789654;
    @BindView(R.id.codeTV)
    TextView codeTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.timeTV)
    TextView timeTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;
    @BindView(R.id.paymentTV)
    TextView paymentTV;
    TextView uploadReport;

    String code, date, name, country, city, address, day, time, orderId, paymentStatus;
    String pName, pGender, pBirthDate, pAvatar, pEmail, pPhone, pCountry, pCity;

    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private MainViewsCallBack mainViewsCallBack;

    ListSharedPreference listSharedPreference;
    private String image_path = "";
    private File file;
    private String desc;
    private AlertDialog alertDialog;
    private SendPatientProfData sendPatientProfData;
    private String pId;
    private MoveToFragment moveToFragment;

    public ConfirmedPaidDrFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        fireBackButtonEvent();
        try {
            activity = getActivity();
            mainViewsCallBack = (MainViewsCallBack) activity;
            sendPatientProfData = (SendPatientProfData) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        code = arguments.getString("code");
        date = arguments.getString("date");
        name = arguments.getString("name");
        country = arguments.getString("country");
        city = arguments.getString("city");
        address = arguments.getString("address");
        day = arguments.getString("day");
        desc = arguments.getString("desc");

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < arguments.getInt("timeSize"); i++) {
            stringBuilder.append(getArguments().getString("time" + i));
            if (i < arguments.getInt("timeSize") - 1) {
                stringBuilder.append(" ,\n");
            }
        }

        time = stringBuilder.toString();
        orderId = arguments.getString("orderId");

        pGender = arguments.getString("pGender");
        pAvatar = arguments.getString("pAvatar");
        pBirthDate = arguments.getString("pBirthDate");
        pName = arguments.getString("pName");
        pEmail = arguments.getString("pEmail");
        pPhone = arguments.getString("pPhone");
        pCountry = arguments.getString("pCountry");
        pCity = arguments.getString("pCity");
        pId = arguments.getString("pId");
        paymentStatus = arguments.getString("paymentStatus");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_confirmed_paid_dr, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        nameTV.setPaintFlags(nameTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        nameTV.setText(name);
        codeTV.setText(code);
        dateTV.setText(date);
        countryTV.setText(country);
        cityTV.setText(city);
        addressTV.setText(address);
        dayTV.setText(day);
        timeTV.setText(time);
        descriptionTV.setText(desc);

        switch (paymentStatus) {
            case "undefined":
                paymentTV.setText(getString(R.string.not_paid_yet));
                break;
            case "cash":
                paymentTV.setText(getString(R.string.cash));
                break;
            case "visa":
                paymentTV.setText(getString(R.string.visa));
                break;
        }

    }

    @OnClick(R.id.completeBtn)
    protected void onCompleteClick() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View popUpView = li.inflate(R.layout.complete_order_layout, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        // create alert dialog

        alertDialogBuilder.setView(popUpView);
        alertDialog = alertDialogBuilder.create();
        // set prompts.xml to alertdialog builder
        final EditText reasonET = popUpView.findViewById(R.id.noteET);
        uploadReport = popUpView.findViewById(R.id.medical_licenseTV);
        final ImageButton imageButton = popUpView.findViewById(R.id.close_button);
        final Button sendBtn = popUpView.findViewById(R.id.sendBtn);
        final ProgressBar progressBar = popUpView.findViewById(R.id.progressBar);

        imageButton.setOnClickListener
                (view -> alertDialog.dismiss());

        sendBtn.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Permission is not granted
                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.
                    } else {
                        // No explanation needed; request the permission
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                112);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {
                    // Permission has already been granted
                    if (!image_path.equals(""))
                        serverComplete(reasonET.getText().toString(), progressBar);
                    else
                        serverCompleteNonUpload(reasonET.getText().toString(), progressBar);
                }
            } else {

                if (!image_path.equals(""))
                    serverComplete(reasonET.getText().toString(), progressBar);
                else
                    serverCompleteNonUpload(reasonET.getText().toString(), progressBar);
            }
        });

        uploadReport.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, LICENCE_CODE);
        });

        // show it
        alertDialog.show();
    }

    private void serverCompleteNonUpload(String note, final ProgressBar progressBar) {
        try {
            progressBar.setVisibility(View.VISIBLE);

            ApiLink retrofit = MyRetrofitClient.getOrder().create(ApiLink.class);

            Call<AcceptDrModel> registerCall = retrofit.complete_dr_order(orderId, note);
            registerCall.enqueue(new Callback<AcceptDrModel>() {
                @Override
                public void onResponse(@NonNull Call<AcceptDrModel> call, @NonNull Response<AcceptDrModel> response) {
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Status status = response.body().getStatus();
                                if  (status != null) {
                                    if (status.getType().equals("success")) {
                                        Toast.makeText(getActivity(), status.getTitle(), Toast.LENGTH_SHORT).show();
                                        alertDialog.dismiss();
                                        moveToFragment.moveInMain(new RequestsFragment());
                                    } else
                                        Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc.getMessage(), Toast.LENGTH_SHORT).show();
                        exc.printStackTrace();
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<AcceptDrModel> call, @NonNull Throwable t) {
                    try {
                        t.printStackTrace();
                        Log.d(TAG, "on fail: " + t.getMessage());
                    } catch (Exception r) {
                        Log.d(TAG, "excepetion: " + r.getMessage());
                    }
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception exc) {
            Toast.makeText(mContext, "" + exc, Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.nameTV)
    protected void onNameTVClick() {
        sendPatientProfData.sendData(pId, pGender, pAvatar, pBirthDate, pName, pEmail, pPhone, pCountry, pCity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LICENCE_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, ConfirmedPaidDrFragment.this.getActivity());
            assert image_path != null;
            file = new File(image_path);
            uploadReport.setText(file.getName());
        }//end if check for data
        else {
            uploadReport.setText(getString(R.string.medical_license));
        }
    }//end onActivityResult

    private String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    private void serverComplete(String note, final ProgressBar progressBar) {
        try {
            progressBar.setVisibility(View.VISIBLE);

            RequestBody mFile2 = RequestBody.create(MediaType.parse("image/*"), file);
            MultipartBody.Part report = MultipartBody.Part.createFormData("report", file.getName(), mFile2);
            RequestBody notesRB = RequestBody.create(MediaType.parse("text/plain"), note);

            ApiLink retrofit = MyRetrofitClient.getOrder().create(ApiLink.class);

            Call<AcceptDrModel> registerCall = retrofit.complete_dr_order(orderId, report, notesRB);
            Log.d(TAG, "serverComplete: " + "orderId: " + orderId + "\n notes: " + notesRB);
            registerCall.enqueue(new Callback<AcceptDrModel>() {
                @Override
                public void onResponse(@NonNull Call<AcceptDrModel> call, @NonNull Response<AcceptDrModel> response) {
                    try {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().getStatus().getType().equals("success")) {
                                    Toast.makeText(getActivity(), response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                    alertDialog.dismiss();
                                    getFragmentManager()
                                            .beginTransaction()
                                            .replace(R.id.main_frameLayout, new RequestsFragment())
                                            .commit();
                                } else
                                    Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "else on sucees", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc.getMessage(), Toast.LENGTH_SHORT).show();
                        exc.printStackTrace();
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<AcceptDrModel> call, @NonNull Throwable t) {
                    try {
                        t.printStackTrace();
                        Log.d(TAG, "on fail: " + t.getMessage());
                    } catch (Exception r) {
                        Log.d(TAG, "excepetion: " + r.getMessage());
                    }
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception exc) {
            Toast.makeText(mContext, "" + exc, Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.confirmed_order_paid));
    }


    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, new SubRequestFragment())
                        .commit();
            }
        });
    }//end back pressed

}