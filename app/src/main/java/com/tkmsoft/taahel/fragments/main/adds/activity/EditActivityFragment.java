package com.tkmsoft.taahel.fragments.main.adds.activity;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.helper.InitSpinner;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.activities.view.category.ActivityCategModel;
import com.tkmsoft.taahel.model.api.adds.edit.EditModel;
import com.tkmsoft.taahel.model.api.adds.edit.Status;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditActivityFragment extends Fragment {

    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.spinner_type)
    Spinner spinner_type;
    @BindView(R.id.cityRelLayout)
    LinearLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.spinner_type_progressBar)
    ProgressBar spinner_type_progressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.name_arET)
    EditText name_arET;
    @BindView(R.id.name_enET)
    EditText name_enET;
    @BindView(R.id.address_ar_ET)
    EditText address_ar_ET;
    @BindView(R.id.address_en_ET)
    EditText address_en_ET;
    @BindView(R.id.staticPhone_ET)
    EditText static_phoneET;
    @BindView(R.id.phoneET)
    EditText phoneET;
    @BindView(R.id.linkET)
    EditText linkET;
    @BindView(R.id.durationET)
    EditText durationET;
    @BindView(R.id.dateTV)
    TextView dateTV;

    MainViewsCallBack mainViewsCallBack;
    ListSharedPreference listSharedPreference;
    private ArrayList<String> mCountriesArrayList;
    private ArrayList<String> mTypeArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList,mTypeIdList;
    private final int IMAGE_CODE = 2001;
    private String image_path = "";
    private String countryCode, cityCode, typeCode;
    private FragmentActivity mContext;
    MoveToFragment moveToFragment;

    String pId;
    String image;
    String nameAr, nameEn;
    String addressAr, addressEn;
    String lat;
    String longX;
    Integer countryId, cityId, typeId;
    private InitSpinner initSpinner;
    private ConnectionDetector connectionManager;
    private ApiLink apiBase,apiActivity;
    private List<CountriesModel.DataBean.CountriesBean> dataCountryList;
    private String TAG = getClass().getSimpleName();
    private int day, month, year;
    boolean isInserted = false;
    private String link;
    private String start_date, duration, static_phone, phone;

    public EditActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        initSpinner = new InitSpinner(mContext);
        connectionManager = new ConnectionDetector(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            pId = bundle.getString("pId");
            image = bundle.getString("image");
            nameAr = bundle.getString("nameAr");
            nameEn = bundle.getString("nameEn");
            countryId = bundle.getInt("countryId");
            cityId = bundle.getInt("cityId");
            typeId = bundle.getInt("typeId");
            addressAr = bundle.getString("addressAr");
            addressEn = bundle.getString("addressEn");
            link = bundle.getString("link");
            start_date = bundle.getString("start_date");
            duration = bundle.getString("duration");
            static_phone = bundle.getString("static_phone");
            phone = bundle.getString("phone");

            lat = bundle.getString("lat");
            longX = bundle.getString("longX");
        }
        apiBase = MyRetrofitClient.getBase().create(ApiLink.class);
        apiActivity = MyRetrofitClient.getBaseActivity().create(ApiLink.class);

    }//end onCreate

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_edu, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        setData();
        initCountrySpinner();
        initTypeSpinner();
    }
    private void setData() {
        Picasso.get().load(image).error(R.drawable.place_holder).into(imageView);
        name_arET.setText(nameAr);
        name_enET.setText(nameEn);
        address_ar_ET.setText(addressAr);
        address_en_ET.setText(addressEn);
        linkET.setText(link);
        dateTV.setText(start_date);
        durationET.setText(duration);
        static_phoneET.setText(static_phone);
        phoneET.setText(phone);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    private boolean isConnected() {
        return connectionManager.isConnectingToInternet();
    }

    ///////////////Spinners///////////////

    private Call<ActivityCategModel> callGetType() {
        return apiActivity.getActivitiesCategs();
    }

    private void initTypeSpinner() {
        mTypeArrayList = new ArrayList<>();
        mTypeIdList = new ArrayList<>();
        mTypeArrayList.add(0, getString(R.string.type));
        mTypeIdList.add(0, 0);

        initSpinner.setSpinner(spinner_type, mTypeArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    typeCode = String.valueOf(mTypeIdList.get(position));
                } else {
                    typeCode = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void serverTypeSpinner() {
        spinner_type_progressBar.setVisibility(View.VISIBLE);
        callGetType().enqueue(new Callback<ActivityCategModel>() {
            @Override
            public void onResponse(@NonNull Call<ActivityCategModel> call, @NonNull Response<ActivityCategModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        ActivityCategModel.StatusBean status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                ActivityCategModel.DataBean data = response.body().getData();
                                if (data != null) {
                                    List<ActivityCategModel.DataBean.CategoriesBean> categoryList = data.getCategories();
                                    if (categoryList!= null) {
                                        for (int i = 0; i < categoryList.size(); i++) {
                                            if (getLanguage().equals("ar")) {
                                                mTypeArrayList.add(categoryList.get(i).getName_ar());
                                            } else {
                                                mTypeArrayList.add(categoryList.get(i).getName_en());
                                            }
                                            mTypeIdList.add(categoryList.get(i).getId());
                                        }//end for
                                        setSpinnerValue(mTypeIdList, typeId, spinner_type);
                                    }
                                }
                            }
                        }
                    }
                }
                spinner_type_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ActivityCategModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                spinner_type_progressBar.setVisibility(View.GONE);
            }
        });
    }//end()

    private void setSpinnerValue(ArrayList<Integer> integerArrayList, Integer id, Spinner spinner) {
        int j = 0;
        while (j < integerArrayList.size()) {
            if (integerArrayList.get(j).equals(id)) {
                spinner.setSelection(j);
                break;
            } else
                j++;
        }
    }

    private Call<CountriesModel> callGetCountry() {
        return apiBase.getCountries();
    }

    private void serverCountrySpinner() {

        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CountriesModel.StatusBean status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                CountriesModel.DataBean data = response.body().getData();
                                if (data != null) {
                                    dataCountryList = data.getCountries();
                                    if (dataCountryList != null && !dataCountryList.isEmpty()) {
                                        for (int i = 0; i < dataCountryList.size(); i++) {
                                            mCountriesArrayList.add(dataCountryList.get(i).getName_ar());
                                            mCountriesIdList.add(dataCountryList.get(i).getId());
                                        }//end for

                                        int j = 0;
                                        while (j < mCountriesIdList.size()) {
                                            if (mCountriesIdList.get(j).equals(countryId)) {
                                                spinner_country.setSelection(j);
                                                Log.d(TAG, "onResponse: " + j);
                                                break;
                                            } else
                                                j++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                spinner_progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end serverRegister()

    private void initCountrySpinner() {
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);

        initSpinner.setSpinner(spinner_country, mCountriesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(position));
                    showCitySpinner();
                    initCitySpinner();
                } else {
                    countryCode = null;
                    hideCitySpinner();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCitySpinner() {
        ArrayList<String> mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);


        for (int i = 0; i < dataCountryList.size(); i++) {
            if (countryCode.equals(String.valueOf(dataCountryList.get(i).getId()))) {
                for (int k = 0; k < dataCountryList.get(i).getCities().size(); k++) {
                    if (getLanguage().equals("ar"))
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_ar());
                    else
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_en());
                    mCitiesIdList.add(dataCountryList.get(i).getCities().get(k).getId());
                }
            }
        }

        initSpinner.setSpinner(spinner_city, mCitiesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    cityCode = String.valueOf(mCitiesIdList.get(position));
                } else {
                    cityCode = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (!isInserted) {
            int j = 0;
            while (j < mCitiesIdList.size()) {
                if (mCitiesIdList.get(j).equals(cityId)) {
                    spinner_city.setSelection(j);
                    isInserted = true;
                    Log.d(TAG, "onCitySelect: " + j);
                    break;
                } else
                    j++;
            }

        }

    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }

    private void hideCitySpinner() {
        cityRelative.setVisibility(View.GONE);
    }

    private void showCitySpinner() {
        cityRelative.setVisibility(View.VISIBLE);
    }

    /////////////////////end spinners////////////////

    private void serverEditActivity() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBaseActivity().create(ApiLink.class);
        File file1;
        file1 = new File(image_path);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", file1.getName(), mFile1);
        RequestBody idPart = RequestBody.create(MediaType.parse("text/plain"), pId);

        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody start_datePart = RequestBody.create(MediaType.parse("text/plain"), dateTV.getText().toString());
        RequestBody durationPart = RequestBody.create(MediaType.parse("text/plain"), durationET.getText().toString());
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody address_arPart = RequestBody.create(MediaType.parse("text/plain"), address_ar_ET.getText().toString());
        RequestBody address_enPart = RequestBody.create(MediaType.parse("text/plain"), address_en_ET.getText().toString());
        RequestBody linkPart = RequestBody.create(MediaType.parse("text/plain"), linkET.getText().toString());
        RequestBody static_phonePart = RequestBody.create(MediaType.parse("text/plain"), static_phoneET.getText().toString());
        RequestBody phonePart = RequestBody.create(MediaType.parse("text/plain"), phoneET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), "132155");
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), "1236985");

        Call<EditModel> registerCall = retrofit.editActivity(idPart, photoPart, name_arPart, name_enPart,type_idPart,city_idPart,address_arPart,address_enPart,
                linkPart,start_datePart,durationPart,
                static_phonePart,phonePart,latPart,longXPart);
        registerCall.enqueue(new Callback<EditModel>() {
            @Override
            public void onResponse(@NonNull Call<EditModel> call, @NonNull Response<EditModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                                moveToFragment.moveInMain(new SubMyAddsActivityFragment());
                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<EditModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
//                Toast.makeText(mContext, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void serverEditActivityNonImage() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getEducation().create(ApiLink.class);

        String name_arPart = name_arET.getText().toString();
        String name_enPart = name_enET.getText().toString();
        String city_idPart = cityCode;
        String type_idPart = typeCode;
        String address_arPart = address_ar_ET.getText().toString();
        String address_enPart = address_en_ET.getText().toString();
        String linkPart = linkET.getText().toString();
        String startDatePart = dateTV.getText().toString();
        String durationPart = durationET.getText().toString();
        String staticPhonePart = static_phoneET.getText().toString();
        String phonePart = phoneET.getText().toString();
        String latPart = "132155";
        String longXPart = "1236985";

        Call<EditModel> registerCall = retrofit.editActivityNonImage(pId,
                name_arPart, name_enPart,type_idPart,city_idPart,address_arPart,address_enPart,
                linkPart,startDatePart,durationPart,
                staticPhonePart,
                phonePart,latPart,longXPart);

        registerCall.enqueue(new Callback<EditModel>() {
            @Override
            public void onResponse(@NonNull Call<EditModel> call, @NonNull Response<EditModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                                moveToFragment.moveInMain(new SubMyAddsActivityFragment());
                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<EditModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    ///////////////On Click///////////////
    @OnClick(R.id.imageView)
    protected void imageOnClick() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, IMAGE_CODE);
    }

    @OnClick(R.id.addBtn)
    protected void onNextBtnClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                addActivityNow();
            }
        } else {
            addActivityNow();
        }
    }

    private void addActivityNow() {
        if (countryCode != null) {
            if (cityCode != null) {
                if (typeCode!= null)
                if (!image_path.equals(""))
                    serverEditActivity();
                else
                    serverEditActivityNonImage();
                else
                    Toast.makeText(mContext, ""+getString(R.string.enter_type), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, getActivity());
            showImageInView(uri);
        }//end if check for data

    }//end onActivityResult

    private void showImageInView(Uri image_path) {
        Picasso.get().load(image_path).into(imageView);
    }

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public void onStart() {
        if (isConnected()) {
           serverCountrySpinner();
           serverTypeSpinner();
        }
        else
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        super.onStart();
    }

    @OnClick(R.id.dateTV)
    protected void onSendOrderClick() {
        if (listSharedPreference.getIsLogged()) {
            DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(),
                    (datepicker, selectedYear, selectedMonth, selectedDay) -> {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedYear);
                        myCalendar.set(Calendar.MONTH, selectedMonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                        String dateFormat = "yyyy-MM-dd"; //Change as you need
                        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
                        dateTV.setText(sdf.format(myCalendar.getTime()));
                        day = selectedDay;
                        month = datepicker.getMonth() + 1;
                        year = selectedYear;
                    }, 1975, 0, 1);
            mDatePicker.setTitle(getString(R.string.select_day));
            TextView tv = new TextView(getActivity());
            tv.setText(getString(R.string.select_day));
            tv.setTextSize(26);
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            tv.setTextColor(getResources().getColor(R.color.black));
            tv.setPadding(2, 8, 0, 8);
            mDatePicker.setCustomTitle(tv);
            mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
            mDatePicker.show();
        } else {
            Toast.makeText(mContext, R.string.please_login_first, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        callGetCountry().cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.edit));
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new SubMyAddsActivityFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

}