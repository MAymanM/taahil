package com.tkmsoft.taahel.fragments.main.profile.doctor;

import android.app.Activity;
import android.app.DatePickerDialog;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.bumptech.glide.Glide;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.TherapistDetailDeptAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.fragments.main.doctor.DoctorsFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.interfaces.SendDateCallBack;
import com.tkmsoft.taahel.model.TherapistDetailDeptsModel;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class DrProfileFragment extends Fragment {

    MainViewsCallBack mMainViewsCallBack;
    @BindView(R.id.dr_imgView)
    CircleImageView imageView;
    @BindView(R.id.dr_ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.educationTV)
    TextView educationTV;
    @BindView(R.id.deptTV)
    TextView specialistTV;
    @BindView(R.id.yearsOfExpTV)
    TextView expTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.priceTV)
    TextView priceTV;
    @BindView(R.id.genderTV)
    TextView genderTV;
    @BindView(R.id.aboutTV)
    TextView aboutTV;
    @BindView(R.id.refreshBtn)
    ImageButton refreshBtn;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.orderNowBtn)
    Button orderNowBtn;

    Calendar myCalendar;
    private int mDay, mMonth, mYear;

    SendDateCallBack sendDateCallBack;
    ListSharedPreference listSharedPreference;
    private String drName, drGender, drExperience, drEducation, drCity, drPrice, drCurrency, drAbout, drAvatar;
    private List<String> drSpecialist;
    int apiDayNumber;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    private View rootView;
    private int day, month, year;
    private float drRating;

    public DrProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            mMainViewsCallBack = (MainViewsCallBack) context;
            sendDateCallBack = (SendDateCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void getUserInfo() {
        //progressBar.setVisibility(View.VISIBLE);
        String uAvatar = listSharedPreference.getUAvatar();
        String uName = listSharedPreference.getUName();
        String uEducation = listSharedPreference.getUEducation();
        List<String> uSpecializationsList = listSharedPreference.getUSpecialists();
        String uExp = listSharedPreference.getUExperience();
        String city = listSharedPreference.getUCity();
        String price = listSharedPreference.getUPrice();
        String gender = listSharedPreference.getUGender();
        String about = listSharedPreference.getUAbout();
        String currency = listSharedPreference.getUCurrency();
//        Toast.makeText(mContext, "" + city, Toast.LENGTH_SHORT).show();
        try {
            if (URLUtil.isValidUrl(uAvatar))
                Glide.with(rootView).load(uAvatar).into(imageView);
            else
                imageView.setImageResource(R.drawable.place_holder);
        } catch (Exception e) {
            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        nameTV.setText(uName);
        educationTV.setText(uEducation);
        specialistTV.setText(uSpecializationsList.get(0));
        StringBuilder strBuilderExp = new StringBuilder(getString(R.string.exp_years_));
        strBuilderExp.append(" ");
        strBuilderExp.append(uExp);
        expTV.setText(strBuilderExp);

        cityTV.setText(city);
        priceTV.setText(price + " " + currency);
        setGender(gender);
        aboutTV.setText(about);
        initAdapter(uSpecializationsList, 1);
        //progressBar.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_dr_profile, container, false);
        listSharedPreference = new ListSharedPreference(DrProfileFragment.this.getActivity().getApplicationContext());
        ButterKnife.bind(this, rootView);
        myCalendar = Calendar.getInstance();
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
        if (listSharedPreference.isDrProfile()) {
            drName = listSharedPreference.getDrName();
            drGender = listSharedPreference.getDrGender();
            drEducation = listSharedPreference.getDrEducation();
            drAbout = listSharedPreference.getDrDescription();
            drExperience = listSharedPreference.getDrExperience();
            drRating = listSharedPreference.getDrRate();
            drCity = listSharedPreference.getDrCity();
            drPrice = listSharedPreference.getDrPrice();
            drCurrency = listSharedPreference.getDrCurrency();
            drSpecialist = listSharedPreference.getDrSpecialization();
            drAvatar = listSharedPreference.getDrAvatar();
            ratingBar.setVisibility(View.VISIBLE);
            setData();
            orderNowBtn.setVisibility(View.VISIBLE);
        } else {
            getUserInfo();
            ratingBar.setVisibility(View.GONE);
            orderNowBtn.setVisibility(View.GONE);
        }
    }

    private void setData() {
        setGender(drGender);
        nameTV.setText(drName);
        educationTV.setText(drEducation);
        specialistTV.setText(drSpecialist.get(0));
        StringBuilder strBuilderExp = new StringBuilder(getString(R.string.exp_years_));
        strBuilderExp.append(" ");
        strBuilderExp.append(drExperience);
        expTV.setText(strBuilderExp);
        ratingBar.setRating(drRating);
        cityTV.setText(drCity);
        priceTV.setText(drPrice);
        priceTV.append(drCurrency);
        priceTV.append(getString(R.string.per_hr));
        aboutTV.setText(drAbout);
        initAdapter(drSpecialist);

        try {
            if (URLUtil.isValidUrl(drAvatar))
                Glide.with(rootView).load(drAvatar).into(imageView);
            else
                imageView.setImageResource(R.drawable.place_holder);
        } catch (Exception e) {
            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.orderNowBtn)
    protected void onSendOrderClick() {
        if (listSharedPreference.getIsLogged()) {
            DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(),
                    (datepicker, selectedYear, selectedMonth, selectedDay) -> {
                        Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, selectedYear);
                        myCalendar.set(Calendar.MONTH, selectedMonth);
                        myCalendar.set(Calendar.DAY_OF_MONTH, selectedDay);

                        String dateFormat = "yyyy-MM-dd"; //Change as you need
                        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
                        String dayName = getDayName(selectedYear, selectedMonth, selectedDay);
                        String fullDate = dayName + ", " + sdf.format(myCalendar.getTime());
                        String date = sdf.format(myCalendar.getTime());
                        //Toast.makeText(getActivitiesCategs(), "api: "+apiDayNumber, Toast.LENGTH_SHORT).show();
                        sendDateCallBack.setDate(fullDate, date, apiDayNumber);
                        day = selectedDay;
                        month = datepicker.getMonth() + 1;
                        year = selectedYear;
                        Log.d("madopop", "day: " + day + "mounth: " + month + "year: " + year);
                    }, 1975, 0, 1);
            mDatePicker.setTitle(getString(R.string.select_day));
            TextView tv = new TextView(getActivity());
            tv.setText(getString(R.string.select_day));
            tv.setTextSize(26);
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            tv.setTextColor(getResources().getColor(R.color.black));
            tv.setPadding(2, 8, 0, 8);
            mDatePicker.setCustomTitle(tv);
            mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
            mDatePicker.show();
        } else {
            Toast.makeText(mContext, R.string.please_login_first, Toast.LENGTH_SHORT).show();
        }
    }

    private String getDayName(int year, int month, int day) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(year, month, day - 1);
        int dayNum = gregorianCalendar.get(GregorianCalendar.DAY_OF_WEEK);
        Log.d(TAG, "daynum: " + dayNum);
        switch (dayNum) {
            case 1:
                apiDayNumber = 1;
                return getString(R.string.monday);
            case 2:
                apiDayNumber = 2;
                return getString(R.string.tuesday);
            case 3:
                apiDayNumber = 3;
                return getString(R.string.wednesday);
            case 4:
                apiDayNumber = 4;
                return getString(R.string.thursday);
            case 5:
                apiDayNumber = 5;
                return getString(R.string.friday);
            case 6:
                apiDayNumber = 6;
                return getString(R.string.saturday);
            case 7:
                apiDayNumber = 0;
                return getString(R.string.sunday);
            default:
                apiDayNumber = -1;
                return getString(R.string.nothing);
        }
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
    }

    private void initAdapter(List<String> specializations) {
        ArrayList<TherapistDetailDeptsModel> mDataList = new ArrayList<>();
        for (int i = 0; i < specializations.size(); i++) {
            TherapistDetailDeptsModel th = new TherapistDetailDeptsModel(specializations.get(i));
            mDataList.add(i, th);
        }
        TherapistDetailDeptAdapter therapistAdapter = new TherapistDetailDeptAdapter(getActivity().getApplicationContext(), mDataList);
        recyclerView.setAdapter(therapistAdapter);
        therapistAdapter.notifyDataSetChanged();
    }

    private void initAdapter(List<String> specializations, int v) {
        ArrayList<TherapistDetailDeptsModel> mDataList = new ArrayList<>();
        for (int i = 0; i < specializations.size(); i++) {
            TherapistDetailDeptsModel th = new TherapistDetailDeptsModel(specializations.get(i));
            mDataList.add(i, th);
        }
        TherapistDetailDeptAdapter therapistAdapter = new TherapistDetailDeptAdapter(getActivity().getApplicationContext(), mDataList);
        recyclerView.setAdapter(therapistAdapter);
        therapistAdapter.notifyDataSetChanged();
    }

    private void setGender(String gender) {

        if (gender.equals("0")) {
            genderTV.setText(getString(R.string.male));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                imageView.setBorderColor(getActivity().getColor(R.color.blue_male));
            } else
                imageView.setBorderColor(getActivity().getResources().getColor(R.color.blue_male));

        } else {
            genderTV.setText(getString(R.string.female));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                imageView.setBorderColor(getActivity().getColor(R.color.pink_female));
            } else {
                imageView.setBorderColor(getActivity().getResources().getColor(R.color.pink_female));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.dr_details));
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.showAddFab(false);
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    if (listSharedPreference.isDrProfile())
                        getFragmentManager().beginTransaction()
                                .replace(R.id.main_frameLayout, new DoctorsFragment())
                                .commit();
                    else
                        getFragmentManager().beginTransaction()
                                .replace(R.id.main_frameLayout, new HomeFragment())
                                .commit();
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

    @Override
    public void onDetach() {
        AndroidNetworking.cancelAll();
        super.onDetach();
    }
}