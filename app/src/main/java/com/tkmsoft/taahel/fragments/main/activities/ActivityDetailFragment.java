package com.tkmsoft.taahel.fragments.main.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.ReviewsAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.ReviewsModel;
import com.tkmsoft.taahel.model.api.comments.add.AddCommentModel;
import com.tkmsoft.taahel.model.api.comments.add.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class ActivityDetailFragment extends Fragment implements OnMapReadyCallback {

    private MainViewsCallBack mainViewsCallBack;
    private ListSharedPreference listSharedPreference;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.typeTV)
    TextView typeTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.durationTV)
    TextView durationTV;
    @BindView(R.id.phoneTV)
    TextView phoneTV;
    @BindView(R.id.mobileTV)
    TextView mobileTV;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.linkTV)
    TextView websiteTV;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private FragmentActivity mContext;
    private String pId;
    private String name;
    private String type;
    private String address;
    private String country;
    private String city;
    private String link;
    private String date;
    private String duration;
    private String lat;
    private String longX;
    private String phone;
    private String mobile;
    private String rate;
    private String comments;
    private ArrayList<String> commentsContent = new ArrayList<>(), commentsRate = new ArrayList<>(),
            commentsUserNames = new ArrayList<>(), commentsUserImges = new ArrayList<>();
    private ApiLink apiLinkBase;
    private EditText commentET;
    private RatingBar ratingBarPop;
    private MoveToFragment moveToFragment;
    private SupportMapFragment mapFragment;

    public ActivityDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            pId = bundle.getString("pId");
            String image = bundle.getString("image");
            name = bundle.getString("name");
            type = bundle.getString("type");
            address = bundle.getString("address");
            country = bundle.getString("country");
            city = bundle.getString("city");
            link = bundle.getString("link");
            date = bundle.getString("date");
            phone = bundle.getString("phone");
            mobile = bundle.getString("mobile");
            duration = bundle.getString("duration");
            lat = bundle.getString("lat");
            rate = bundle.getString("rate");
            longX = bundle.getString("longX");
            commentsContent = bundle.getStringArrayList("commentsContent");
            commentsRate = bundle.getStringArrayList("commentsRate");
            commentsUserNames = bundle.getStringArrayList("cmntsUserNames");
            commentsUserImges = bundle.getStringArrayList("cmntsUserImg");
        }
        apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_activity_detail, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initGMaps();
    }

    private void initGMaps() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            assert fragmentManager != null;
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
    }
    @OnClick(R.id.linkTV)
    void onLinkTVClick() {
        try {
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(webIntent);
        } catch (ActivityNotFoundException e) {
            Timber.e(e);
        }
    }
    private void initUI(View rootView) {
        nameTV.setText(name);
        typeTV.setText(type);
        addressTV.setText(address);
        countryTV.setText(country);
        cityTV.setText(city);
        dateTV.setText(date);
        durationTV.setText(duration);
        ratingBar.setRating(Float.parseFloat(rate));
        phoneTV.setText(phone);
        mobileTV.setText(mobile);
        websiteTV.setText(link);
        websiteTV.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        initRecyclerView();
        initAdapter();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),
                1, GridLayoutManager.HORIZONTAL, false));
    }

    private void initAdapter() {
        ArrayList<ReviewsModel> mDataList = new ArrayList<>();
        for (int i = 0; i < commentsContent.size(); i++) {
            ReviewsModel th = new ReviewsModel(commentsUserNames.get(i), commentsRate.get(i),
                    commentsUserImges.get(i), commentsContent.get(i));
            mDataList.add(i, th);
        }

        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(getActivity(), mDataList,
                (reviewsModel, adapterPosition) -> {
                    //do click
                });
        recyclerView.setAdapter(reviewsAdapter);
        reviewsAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.cardView_writecmnt)
    protected void onCarvViewWriteReviewClick() {
        if (listSharedPreference.getIsLogged()) {
            LayoutInflater li = LayoutInflater.from(getActivity());
            final View popUpView = li.inflate(R.layout.activity_write_review, null);

            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            // create alert dialog

            alertDialogBuilder.setView(popUpView);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            // set prompts.xml to alertdialog builder
            commentET = popUpView.findViewById(R.id.reviewET);
            final ImageButton imageButton = popUpView.findViewById(R.id.close_button);
            ratingBarPop = popUpView.findViewById(R.id.rateBar_review);
            final Button submitBtn = popUpView.findViewById(R.id.submitBtn);
            final ProgressBar progressBar = popUpView.findViewById(R.id.progressBar);
            imageButton.setOnClickListener(view ->
                    alertDialog.dismiss()
            );
            // show it
            alertDialog.show();

            submitBtn.setOnClickListener(v
                            -> {
                        if (listSharedPreference.getIsLogged())
                            serverAddComment(alertDialog, progressBar);
                        else
                            Toast.makeText(mContext, "" + getString(R.string.please_login_first), Toast.LENGTH_SHORT).show();
                    }
            );
        } else {
            Intent myIntent = new Intent(MyApp.getContext().getApplicationContext(), LoginActivity.class);
            startActivity(myIntent);
        }
    }

    private void serverAddComment(final AlertDialog alertDialog, final ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        postCommentModelCall().enqueue(new Callback<AddCommentModel>() {
            @Override
            public void onResponse(@NonNull Call<AddCommentModel> call, @NonNull Response<AddCommentModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();
                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddCommentModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private Call<AddCommentModel> postCommentModelCall() {
        return apiLinkBase.addComment(listSharedPreference.getToken(),
                pId,
                commentET.getText().toString(),
                String.valueOf(ratingBarPop.getRating()),
                "1");
    }


    @Override
    public void onStart() {
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(name);
        mainViewsCallBack.showAddFab(false);
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapFragment.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        mapFragment.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapFragment.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new SubActivityFragment());
            }
        });
    }//end back pressed

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GoogleMap mMap = googleMap;
        LatLng latLng = new LatLng(Double.valueOf(lat), Double.valueOf(longX));
        mMap.addMarker(new MarkerOptions().position(latLng).title(name));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }
}