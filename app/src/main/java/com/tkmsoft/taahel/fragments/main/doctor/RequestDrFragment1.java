package com.tkmsoft.taahel.fragments.main.doctor;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.DrDayTimesAdapter;
import com.tkmsoft.taahel.fragments.main.profile.doctor.DrProfileFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.order.drTimes.Data;
import com.tkmsoft.taahel.model.api.order.drTimes.DayTime;
import com.tkmsoft.taahel.model.api.order.drTimes.DoctorTimesModel;
import com.tkmsoft.taahel.model.api.order.drTimes.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestDrFragment1 extends Fragment implements DrDayTimesAdapter.ListAllListeners {

    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.sendBtn)
    Button button;

    private MainViewsCallBack mainViewsCallBack;
    List<String> selectedList;

    private String fullDate, date;
    int dayNum;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.notFoundTV)
    TextView notFoundTV;
    ListSharedPreference listSharedPreference;
    int job_date_id = 0;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLinkOrder;
    MoveToFragment moveToFragment;
    private DrDayTimesAdapter drDayTimesAdapter;
    public RequestDrFragment1() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            context = getActivity();
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            date = bundle.getString("date");
            fullDate = bundle.getString("fullDate");
            dayNum = bundle.getInt("dayNum");
        }
        Log.d(TAG, "date: " + date + "\nfullDate: " + fullDate + "\ndayNum: " +
                dayNum + "\ndrId: " + listSharedPreference.getDrId());
        apiLinkOrder = MyRetrofitClient.getOrder().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_therapist_day_picker, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        dateTV.setText(fullDate);
        selectedList = new ArrayList<>();
        initRecyclerView();
        serverGetDoctorDayTime();
    }

    private void initRecyclerView() {
         drDayTimesAdapter = new DrDayTimesAdapter(mContext, new ArrayList<>(), this);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }
        recyclerView.setAdapter(drDayTimesAdapter);
    }

    private void serverGetDoctorDayTime() {
        progressBar.setVisibility(View.VISIBLE);
        callGetDrTimes().enqueue(new Callback<DoctorTimesModel>() {
            @Override
            public void onResponse(@NonNull Call<DoctorTimesModel> call, @NonNull Response<DoctorTimesModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                date = data.getDate();
                                job_date_id = data.getJobDateId();
                                List<DayTime> dayTimeList = data.getDayTimes();
                                if (dayTimeList != null) {
                                    drDayTimesAdapter.replaceData(dayTimeList);
                                }
                                Log.d(TAG, "onResponse: " + "date: " + data.getDate()+"\nDayTimes: "+dayTimeList);
                            }
                        } else {
                            moveToFragment.moveInMain(new DrProfileFragment());
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DoctorTimesModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                serverGetDoctorDayTime();
            }
        });
    }

    private Call<DoctorTimesModel> callGetDrTimes() {
        return apiLinkOrder.getDoctorDayTimes(String.valueOf(listSharedPreference.getDrId()),
                                              String.valueOf(dayNum),
                                              date);
    }

    @OnClick(R.id.sendBtn)
    protected void onSendClick() {
        try {
            if (selectedList.size() > 0) {
                listSharedPreference.setDrTimesList(selectedList);
                listSharedPreference.setDrJobDateId(job_date_id);
                listSharedPreference.setDrSelectedDate(date);
                Log.d("madopop", "job_date_id: " + job_date_id + " " + "date: " + date + " " + "timeList: " + selectedList);
                moveToFragment.moveInMain(new RequestDrFragment2());
            } else {
                Toast.makeText(getActivity(), R.string.choose_times_first, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ecr) {
//            Toast.makeText(mContext, "rt" + ecr.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.doctors));
    }


    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain( new DrProfileFragment());
            }
        });
    }//end back pressed

    @Override
    public void onDetach() {
        mainViewsCallBack = null;
        super.onDetach();
    }

    @Override
    public void onItemChecked(DayTime dayTime, int adapterPosition, boolean b) {
        selectedList.clear();
        selectedList.add(String.valueOf(dayTime.getId()));
    }

}
