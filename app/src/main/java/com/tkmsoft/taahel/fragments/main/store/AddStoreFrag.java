package com.tkmsoft.taahel.fragments.main.store;

import android.Manifest;
import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.store.add.StoreAddModel;
import com.tkmsoft.taahel.model.api.store.spinner.type.TypeStoreModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.model.spinners.CurrencyModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class AddStoreFrag extends Fragment {
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_city_progressBar;
    @BindView(R.id.addBtn)
    Button addBtn;
    @BindView(R.id.imageView1)
    ImageView img1_item;
    @BindView(R.id.imageView2)
    ImageView img2_item;
    @BindView(R.id.imageView3)
    ImageView img3_item;
    //Spinner
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindView(R.id.spinner_currency)
    Spinner spinner_currency;
    @BindView(R.id.spinner_type)
    Spinner spinner_type;

    @BindView(R.id.name_arET)
    EditText name_arET;
    @BindView(R.id.name_enET)
    EditText name_enET;
    @BindView(R.id.priceET)
    EditText priceET;
    @BindView(R.id.description_arET)
    EditText description_arEt;
    @BindView(R.id.description_enET)
    EditText description_enEt;
    @BindView(R.id.sizeET)
    EditText sizeET;
    @BindView(R.id.ageFromET)
    EditText ageFromET;
    @BindView(R.id.ageToET)
    EditText ageToET;
    @BindViews({R.id.imageView1, R.id.imageView2, R.id.imageView3})
    List<ImageView> imageViewList;
    ImageView imageView1, imageView2, imageView3;

    MainViewsCallBack mMainViewsCallBack;
    ListSharedPreference listSharedPreference;
    private ArrayList<String> mCountriesArrayList, mCitiesArrayList, mCurrencyArrayList, mTypeArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mCurrencyIdList, mTypeIdArrayList;
    private final int IMAGE_CODE1 = 123;
    private final int IMAGE_CODE2 = 456;
    private final int IMAGE_CODE3 = 789;
    private String image_path1 = "", image_path2 = "", image_path3 = "";
    private String currencyCode, countryCode, cityCode, typeCode;
    File file1, file2, file3;
    FragmentActivity mContext;

    public AddStoreFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            mMainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_store, container, false);
        ButterKnife.bind(this, rootView);
        listSharedPreference = new ListSharedPreference(AddStoreFrag.this.getActivity().getApplicationContext());
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {

        imageView1 = imageViewList.get(0);
        imageView2 = imageViewList.get(1);
        imageView3 = imageViewList.get(2);


        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        initCountrySpinner();
        serverCountrySpinner();
        mCurrencyArrayList = new ArrayList<>();
        mCurrencyIdList = new ArrayList<>();
        mCurrencyArrayList.add(0, getString(R.string.currency));
        mCurrencyIdList.add(0, 0);
        serverCurrencySpinner();
        initCurrencySpinner();

        mTypeArrayList = new ArrayList<>();
        mTypeIdArrayList = new ArrayList<>();
        mTypeArrayList.add(0, getString(R.string.type));
        mTypeIdArrayList.add(0, 0);
        serverTypeSpinner();
        initTypeSpinner();
    }

    private void serverAddCenter1() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getStore().create(ApiLink.class);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);

        String api_tokenPart = listSharedPreference.getToken();
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("main_photo", file1.getName(), mFile1);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody pricePart = RequestBody.create(MediaType.parse("text/plain"), priceET.getText().toString());
        RequestBody currency_idPart = RequestBody.create(MediaType.parse("text/plain"), currencyCode);
        RequestBody description_arPart = RequestBody.create(MediaType.parse("text/plain"), description_arEt.getText().toString());
        RequestBody description_enPart = RequestBody.create(MediaType.parse("text/plain"), description_enEt.getText().toString());
        RequestBody sizePart = RequestBody.create(MediaType.parse("text/plain"), sizeET.getText().toString());
        RequestBody from_agePart = RequestBody.create(MediaType.parse("text/plain"), ageFromET.getText().toString());
        RequestBody to_agePart = RequestBody.create(MediaType.parse("text/plain"), ageToET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), "132155");
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), "1236985");

        Call<StoreAddModel> registerCall = retrofit.addMedical(api_tokenPart, photoPart, name_arPart, name_enPart,
                type_idPart, city_idPart, pricePart, currency_idPart, sizePart, from_agePart, to_agePart,
                description_arPart, description_enPart, latPart, longXPart);
        registerCall.enqueue(new Callback<StoreAddModel>() {
            @Override
            public void onResponse(@NonNull Call<StoreAddModel> call, @NonNull Response<StoreAddModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.main_frameLayout, new StoreFragment())
                                    .commit();
                        } else
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<StoreAddModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void serverCountrySpinner() {
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> registerCall = retrofit.getCountries();
        registerCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                            mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                            mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                        }
                        initCountrySpinner();
                    }
                } else {
                    Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCountry()

    private void serverCurrencySpinner() {
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CurrencyModel> currencyCall = retrofit.getCurrency();
        currencyCall.enqueue(new Callback<CurrencyModel>() {
            @Override
            public void onResponse(@NonNull Call<CurrencyModel> call, @NonNull Response<CurrencyModel> response) {
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCurrencies().size(); i++) {
                                mCurrencyArrayList.add(response.body().getData().getCurrencies().get(i).getName_ar());
                                mCurrencyIdList.add(response.body().getData().getCurrencies().get(i).getId());
                            }
                        } else {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                        }//end else
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CurrencyModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCurrencySpinner()

    private void serverTypeSpinner() {
        ApiLink retrofit = MyRetrofitClient.getStore().create(ApiLink.class);
        Call<TypeStoreModel> typeModelCall = retrofit.getStoreTypes();
        typeModelCall.enqueue(new Callback<TypeStoreModel>() {
            @Override
            public void onResponse(@NonNull Call<TypeStoreModel> call, @NonNull Response<TypeStoreModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            if (isArabic()) {
                                for (int i = 0; i < response.body().getData().getCategories().size(); i++) {
                                    mTypeArrayList.add(response.body().getData().getCategories().get(i).getNameAr());
                                    mTypeIdArrayList.add(response.body().getData().getCategories().get(i).getId());
                                }
                            } else {
                                for (int i = 0; i < response.body().getData().getCategories().size(); i++) {
                                    mTypeArrayList.add(response.body().getData().getCategories().get(i).getNameEn());
                                }
                            }
                        } else
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivitiesCategs(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(@NonNull Call<TypeStoreModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverType()

    private boolean isArabic() {
        return listSharedPreference.getLanguage().equals("ar");
    }


    private void serverCitiesSpinner(final int countryId) {
        initCitiesSpinner();
        spinner_city_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> citiesCall = retrofit.getCountries();
        citiesCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                spinner_city_progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());

                            }
                        } else {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();

                        }//end else
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_city_progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void initCountrySpinner() {
        spinner_country.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCountriesArrayList);
        spinner_country.setAdapter(adapter);
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(i));
                    cityRelative.setVisibility(View.VISIBLE);
                    serverCitiesSpinner(i);
                } else
                    cityRelative.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinner_country);
            }
        });
    }

    private void initCurrencySpinner() {

        spinner_currency.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCurrencyArrayList);
        spinner_currency.setAdapter(adapter);
        spinner_currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    currencyCode = String.valueOf(mCurrencyIdList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initCitiesSpinner() {
        mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);

        spinner_city.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCitiesArrayList);
        spinner_city.setAdapter(adapter);

        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    cityCode = String.valueOf(mCitiesIdList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(cityRelative);
            }
        });
    }

    private void initTypeSpinner() {
        spinner_type.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mTypeArrayList);
        spinner_type.setAdapter(adapter);
        spinner_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    typeCode = String.valueOf(i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinner_type);
            }
        });
    }
    ///////////////////////////////////////////////////////

    @Override
    public void onResume() {
        super.onResume();
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.serToolbarTitle(getString(R.string.add_item));
        mMainViewsCallBack.showAddFab(false);
    }


    @OnClick(R.id.imageView1)
    protected void image1OnClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, IMAGE_CODE1);
            }
        }
    }

    @OnClick({R.id.closeIB1, R.id.closeIB2, R.id.closeIB3})
    protected void imageOnClick(ImageButton imageButton) {
        int id = imageButton.getId();
        switch (id) {
            case R.id.closeIB1:
                Picasso.get().load(R.drawable.place_holder).into(imageView1);
                image_path1 = "";
                break;
            case R.id.closeIB2:
                Picasso.get().load(R.drawable.place_holder).into(imageView2);
                image_path2 = "";
                break;
            case R.id.closeIB3:
                Picasso.get().load(R.drawable.place_holder).into(imageView3);
                image_path3 = "";
                break;
        }
    }

    @OnClick(R.id.imageView2)
    protected void image2OnClick() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_CODE2);
    }

    @OnClick(R.id.imageView3)
    protected void image3OnClick() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_CODE3);
    }

    @OnClick(R.id.addBtn)
    protected void onAddClick() {
        if (countryCode != null) {
            if (cityCode != null) {
                if (typeCode != null) {
                    if (currencyCode!= null) {
                        servers();
                    }else
                        Toast.makeText(mContext, ""+getString(R.string.enter_currency), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(mContext, "" + getString(R.string.enter_type_store), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();

        } else
            Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();
    }

    private void servers() {
        if (!image_path1.equals("")) {
            if (!image_path2.equals("")) {
                if (!image_path3.equals("")) {
                    serverAddCenterAll();
                }//end 3
                else {
                    serverAddCenter2();
                }//end 1 & 2
            }//end 2
            else if (!image_path3.equals("")) {
                serverAddCenter3();

            }//end 1 & 3
            else {
                serverAddCenter1();
            }//end add 1
        } else
            Toast.makeText(mContext, "" + getString(R.string.plz_choose_main_pic), Toast.LENGTH_SHORT).show();
    }

    private void serverAddCenterAll() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getStore().create(ApiLink.class);
        File file1, file2, file3;
        file1 = new File(image_path1);
        file2 = new File(image_path2);
        file3 = new File(image_path3);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);
        RequestBody mFile2 = RequestBody.create(MediaType.parse("image/*"), file2);
        RequestBody mFile3 = RequestBody.create(MediaType.parse("image/*"), file3);

        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("main_photo", file1.getName(), mFile1);
        MultipartBody.Part photoPart2 = MultipartBody.Part.createFormData("second_photo", file2.getName(), mFile2);
        MultipartBody.Part photoPart3 = MultipartBody.Part.createFormData("third_photo", file3.getName(), mFile3);

        String api_tokenPart = listSharedPreference.getToken();

        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());

        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);

        RequestBody pricePart = RequestBody.create(MediaType.parse("text/plain"), priceET.getText().toString());
        RequestBody currency_idPart = RequestBody.create(MediaType.parse("text/plain"), currencyCode);

        RequestBody description_arPart = RequestBody.create(MediaType.parse("text/plain"), description_arEt.getText().toString());
        RequestBody description_enPart = RequestBody.create(MediaType.parse("text/plain"), description_enEt.getText().toString());

        RequestBody sizePart = RequestBody.create(MediaType.parse("text/plain"), sizeET.getText().toString());
        RequestBody from_agePart = RequestBody.create(MediaType.parse("text/plain"), ageFromET.getText().toString());

        RequestBody to_agePart = RequestBody.create(MediaType.parse("text/plain"), ageToET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), "132155");
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), "1236985");

        Call<StoreAddModel> registerCall = retrofit.addMedicalAll(api_tokenPart, photoPart, photoPart2, photoPart3,
                name_arPart, name_enPart, type_idPart, city_idPart, pricePart, currency_idPart, sizePart, from_agePart, to_agePart,
                description_arPart, description_enPart, latPart, longXPart);
        registerCall.enqueue(new Callback<StoreAddModel>() {
            @Override
            public void onResponse(@NonNull Call<StoreAddModel> call, @NonNull Response<StoreAddModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();

                            getFragmentManager().beginTransaction()
                                    .replace(R.id.main_frameLayout, new StoreFragment())
                                    .commit();
                        } else
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<StoreAddModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void serverAddCenter3() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getStore().create(ApiLink.class);
        File file1, file3;
        file1 = new File(image_path1);
        file3 = new File(image_path3);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);
        RequestBody mFile3 = RequestBody.create(MediaType.parse("image/*"), file3);
        String api_tokenPart = listSharedPreference.getToken();
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("main_photo", file1.getName(), mFile1);
        MultipartBody.Part photoPart3 = MultipartBody.Part.createFormData("third_photo", file3.getName(), mFile3);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody pricePart = RequestBody.create(MediaType.parse("text/plain"), priceET.getText().toString());
        RequestBody currency_idPart = RequestBody.create(MediaType.parse("text/plain"), currencyCode);
        RequestBody description_arPart = RequestBody.create(MediaType.parse("text/plain"), description_arEt.getText().toString());
        RequestBody description_enPart = RequestBody.create(MediaType.parse("text/plain"), description_enEt.getText().toString());
        RequestBody sizePart = RequestBody.create(MediaType.parse("text/plain"), sizeET.getText().toString());
        RequestBody from_agePart = RequestBody.create(MediaType.parse("text/plain"), ageFromET.getText().toString());
        RequestBody to_agePart = RequestBody.create(MediaType.parse("text/plain"), ageToET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), "132155");
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), "1236985");

        Call<StoreAddModel> registerCall = retrofit.addMedical3(api_tokenPart, photoPart, photoPart3, name_arPart, name_enPart,
                type_idPart, city_idPart, pricePart, currency_idPart, sizePart, from_agePart, to_agePart,
                description_arPart, description_enPart, latPart, longXPart);
        registerCall.enqueue(new Callback<StoreAddModel>() {
            @Override
            public void onResponse(@NonNull Call<StoreAddModel> call, @NonNull Response<StoreAddModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.main_frameLayout, new StoreFragment())
                                    .commit();
                        } else
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<StoreAddModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void serverAddCenter2() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getStore().create(ApiLink.class);
        File file1, file2;
        file1 = new File(image_path1);
        file2 = new File(image_path2);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);
        RequestBody mFile2 = RequestBody.create(MediaType.parse("image/*"), file2);
        String api_tokenPart = listSharedPreference.getToken();
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("main_photo", file1.getName(), mFile1);
        MultipartBody.Part photoPart2 = MultipartBody.Part.createFormData("second_photo", file2.getName(), mFile2);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody pricePart = RequestBody.create(MediaType.parse("text/plain"), priceET.getText().toString());
        RequestBody currency_idPart = RequestBody.create(MediaType.parse("text/plain"), currencyCode);
        RequestBody description_arPart = RequestBody.create(MediaType.parse("text/plain"), description_arEt.getText().toString());
        RequestBody description_enPart = RequestBody.create(MediaType.parse("text/plain"), description_enEt.getText().toString());
        RequestBody sizePart = RequestBody.create(MediaType.parse("text/plain"), sizeET.getText().toString());
        RequestBody from_agePart = RequestBody.create(MediaType.parse("text/plain"), ageFromET.getText().toString());
        RequestBody to_agePart = RequestBody.create(MediaType.parse("text/plain"), ageToET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), "132155");
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), "1236985");

        Call<StoreAddModel> registerCall = retrofit.addMedical2(api_tokenPart, photoPart, photoPart2, name_arPart, name_enPart,
                type_idPart, city_idPart, pricePart, currency_idPart, sizePart, from_agePart, to_agePart,
                description_arPart, description_enPart, latPart, longXPart);
        registerCall.enqueue(new Callback<StoreAddModel>() {
            @Override
            public void onResponse(@NonNull Call<StoreAddModel> call, @NonNull Response<StoreAddModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.main_frameLayout, new StoreFragment())
                                    .commit();
                        } else
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<StoreAddModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_CODE1 && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path1 = getImagePathFromUri(uri, getActivity());
            assert image_path1 != null;
            file1 = new File(image_path1);
            Glide.with(getActivity()).load(uri).into(img1_item);
        }//end if check for data
        else if (requestCode == IMAGE_CODE2 && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path2 = getImagePathFromUri(uri, getActivity());
            assert image_path2 != null;
            file2 = new File(image_path2);
            Glide.with(getActivity()).load(uri).into(img2_item);
        }//end if check for data
        else if (requestCode == IMAGE_CODE3 && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path3 = getImagePathFromUri(uri, getActivity());
            assert image_path3 != null;
            file3 = new File(image_path3);
            Glide.with(getActivity()).load(uri).into(img3_item);
        }//end if check for data

    }//end onActivityResult

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMainViewsCallBack = null;
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.main_frameLayout, new StoreFragment())
                            .commit();
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

}
