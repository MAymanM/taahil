package com.tkmsoft.taahel.fragments.main.activities;


import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterActivityFrag extends Fragment {


    MainViewsCallBack mMainViewsCallBack;
    ListSharedPreference listSharedPreference;

    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindViews({R.id.spinner_country, R.id.spinner_city})
    List<Spinner> spinnersList;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;


    private ArrayList<String> mCountriesArrayList, mCitiesArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList;
    private String countryCode = "null", cityCode = "null";

    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private String rate = "null";
    private ApiLink apiLink;
    private MoveToFragment moveToFragment;

    public FilterActivityFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity)
            mContext = (FragmentActivity) activity;
        super.onAttach(activity);
        try {
            mMainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_filter_activity, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View view) {
        setSpinners();

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) ->
        {
            rate = String.valueOf(Math.round(rating));
            if (rate.equals("0"))
                rate = "null";
        });
    }

    @OnClick(R.id.updateBtn)
    protected void onUpdateClick() {

        if (countryCode.equals("null") && cityCode.equals("null") && rate.equals("null"))
            mMainViewsCallBack.filterActivity(countryCode, cityCode, rate, false);
        else
            mMainViewsCallBack.filterActivity(countryCode, cityCode, rate, true);

        Log.d(TAG, "onUpdateClick: " + "country: " + countryCode + "city: " + cityCode + "rate: " + rate);
    }

    private void setSpinners() {
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        serverCountrySpinner();
        initCountrySpinner();
    }

    private void serverCountrySpinner() {
        registerCall().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                            mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                            mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                        }
                        try {
                            initCountrySpinner();
                        } catch (Exception ignore) {

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.filter_centers));
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.showAddFab(false);
    }

    private void initCountrySpinner() {
        spinnersList.get(0).getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCountriesArrayList);
        spinnersList.get(0).setAdapter(adapter);
        spinnersList.get(0).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(i));
                    cityRelative.setVisibility(View.VISIBLE);
                    serverCitiesSpinner(i);
                } else
                    cityRelative.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinnersList.get(0));
            }
        });
    }

    private void serverCitiesSpinner(final int countryId) {
        initCitiesSpinner();
        spinner_progressBar.setVisibility(View.VISIBLE);
        registerCall().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                spinner_progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                            }
                        }
                    } catch (Exception exc) {
//                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void initCitiesSpinner() {
        try {
            mCitiesArrayList = new ArrayList<>();
            mCitiesIdList = new ArrayList<>();
            mCitiesArrayList.add(0, getString(R.string.city));
            mCitiesIdList.add(0, 0);

            spinnersList.get(1).getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCitiesArrayList);
            spinnersList.get(1).setAdapter(adapter);
            spinnersList.get(1).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i > 0)
                        cityCode = String.valueOf(mCitiesIdList.get(i));
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        } catch (Exception ignore) {

        }
    }

    private Call<CountriesModel> registerCall() {
        return apiLink.getCountries();
    }

    @Override
    public void onDetach() {
        AndroidNetworking.cancelAll();
        registerCall().cancel();
        super.onDetach();
    }

    private void fireBackButtonEvent() {

        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new SubActivityFragment());
            }
        });
    }//end back pressed

}
