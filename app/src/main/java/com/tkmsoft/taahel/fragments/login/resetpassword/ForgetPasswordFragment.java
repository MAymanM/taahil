package com.tkmsoft.taahel.fragments.login.resetpassword;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.google.gson.reflect.TypeToken;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.login.CodeVerifyFragment;
import com.tkmsoft.taahel.fragments.login.LoginFragment;
import com.tkmsoft.taahel.fragments.login.MostafedRegFragment;
import com.tkmsoft.taahel.fragments.login.TherapistRegFragment;
import com.tkmsoft.taahel.fragments.login.TherapistRegFragment2;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.api.auth.confirm.ConfirmModel;
import com.tkmsoft.taahel.model.api.auth.confirm.Status;
import com.tkmsoft.taahel.model.api.auth.forgetpass.ForgetPassModel;
import com.tkmsoft.taahel.model.api.auth.forgetpass.Title;
import com.tkmsoft.taahel.model.api.auth.login.Doctor;
import com.tkmsoft.taahel.model.api.auth.login.LoginModel;
import com.tkmsoft.taahel.model.api.auth.login.Member;
import com.tkmsoft.taahel.model.api.auth.login.Patient;
import com.tkmsoft.taahel.model.api.auth.login.Specialization;
import com.tkmsoft.taahel.model.api.auth.updatePlayerId.UpdatePlayerIDModel;
import com.tkmsoft.taahel.model.api.resetpass.Data;
import com.tkmsoft.taahel.model.api.resetpass.ResetPassModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.Links;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cdflynn.android.library.checkview.CheckView;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForgetPasswordFragment extends Fragment {

    private FragmentActivity mContext;
    private ListSharedPreference listSharedPreference;
    ///PhoneCardView
    @BindView(R.id.phoneCV)
    CardView phoneCV;
    @BindView(R.id.sendPhoneBtn)
    Button sendPhoneBtn;
    @BindView(R.id.phoneET)
    EditText phoneET;
    ///CodeCardView
    @BindView(R.id.verifyCV)
    CardView verifyCV;
    @BindView(R.id.codeET)
    EditText codeET;
    @BindView(R.id.resendTV)
    TextView resendTV;
    @BindView(R.id.sendCodeBtn)
    Button sendCodeBtn;
    ///PassCardView
    @BindView(R.id.passCV)
    CardView passCV;
    @BindView(R.id.new_passET)
    EditText new_passET;
    @BindView(R.id.new_pass_confirmET)
    EditText new_pass_confirmET;
    @BindView(R.id.sendPassBtn)
    Button sendPassBtn;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private String TAG = getClass().getSimpleName();
    private MoveToFragment moveToFragment;
    private ApiLink apiLinkAuth;
    private OSPermissionSubscriptionState status;

    public ForgetPasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        moveToFragment = new MoveToFragment(mContext);
        super.onAttach(context);
        ConnectionDetector connectionDetector = new ConnectionDetector(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate( @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkAuth = MyRetrofitClient.auth().create(ApiLink.class);
        status = OneSignal.getPermissionSubscriptionState();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_forget_password, container, false);
        ButterKnife.bind(this, rootView);
        verifyCV.setVisibility(View.GONE);
        passCV.setVisibility(View.GONE);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {

    }

    ////////////// 1- Step //////////////////
    @OnClick(R.id.sendPhoneBtn)
    void onSendPhoneBtnClick() {
        serverForget();
    }

    private void serverForget() {
        String phoneNum = phoneET.getText().toString();
        progressBar.setVisibility(View.VISIBLE);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        AndroidNetworking.post(Links.Auth.forget())
                .addBodyParameter("phone", phoneNum) // posting java object
                .setTag("forgetPass")
                .setOkHttpClient(client)
                .build()
                .getAsParsed(new TypeToken<ForgetPassModel>() {
                }, new ParsedRequestListener<ForgetPassModel>() {
                    @Override
                    public void onResponse(ForgetPassModel response) {
                        progressBar.setVisibility(View.GONE);
                        if (response.getStatus().getType().equals("1")) {

                            Title title = response.getStatus().getTitle();
                            if (title != null) {
                                setResponseMessage(title);
                            }
                            phoneCV.setVisibility(View.GONE);
                            verifyCV.setVisibility(View.VISIBLE);
                            assert response.getData() != null;
                            if (response.getData().getMember() != null)
                                listSharedPreference.setToken(response.getData().getMember().getApiToken());
                        } else {
                            Title title = response.getStatus().getTitle();
                            if (title != null) {
                                setResponseMessage(title);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressBar.setVisibility(View.GONE);
                        if (error.getErrorCode() != 0) {
                            Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                            Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                        {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                });
    }//end server

    ///////////////////// 2- Step //////////////////
    private void setResponseMessage(Title title) {
        if (getLang().equals("ar"))
            Toast.makeText(mContext, "" + title.getAr(), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + title.getEn(), Toast.LENGTH_SHORT).show();
    }
    private void setResponseMessage(String  ar, String en) {
        if (getLang().equals("ar"))
            Toast.makeText(mContext, "" + ar, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + en, Toast.LENGTH_SHORT).show();
    }

    private String getLang() {
        return listSharedPreference.getLanguage();
    }

    @OnClick(R.id.resendTV)
    void onResendTVClcik() {
        serverResendCode();
    }

    private void serverResendCode() {
        String phoneNum = listSharedPreference.getUPhone();
        progressBar.setVisibility(View.VISIBLE);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        AndroidNetworking.post(Links.Auth.forget())
                .addBodyParameter("phone", phoneNum) // posting java object
                .setTag("forgetPass")
                .setOkHttpClient(client)
                .build()
                .getAsParsed(new TypeToken<ForgetPassModel>() {
                }, new ParsedRequestListener<ForgetPassModel>() {
                    @Override
                    public void onResponse(ForgetPassModel response) {
                        progressBar.setVisibility(View.GONE);
                        if (response.getStatus().getType().equals("1")) {
                            Title title = response.getStatus().getTitle();
                            if (title != null) {
                                setResponseMessage(title);
                            }
                            listSharedPreference.setToken(response.getData().getMember().getApiToken());
                        } else {
                            Title title = response.getStatus().getTitle();
                            if (title != null) {
                                setResponseMessage(title);
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressBar.setVisibility(View.GONE);
                        if (error.getErrorCode() != 0) {
                            Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                            Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                        {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                    }
                });
    }

    @OnClick(R.id.sendCodeBtn)
    void onSendCodeBtnClick() {
        serverConfirm();
    }

    private void serverConfirm() {
        progressBar.setVisibility(View.VISIBLE);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        AndroidNetworking.post(Links.Auth.confirm())
                .addHeaders("token", listSharedPreference.getToken())
                .addBodyParameter("code", codeET.getText().toString())
                .setTag("confirmCode")
                .setOkHttpClient(client)
                .build()
                .getAsParsed(new TypeToken<ConfirmModel>() {
                }, new ParsedRequestListener<ConfirmModel>() {
                    @Override
                    public void onResponse(ConfirmModel response) {
                        progressBar.setVisibility(View.GONE);
                        if (response != null) {
                            Status status = response.getStatus();
                            if (status != null) {
                                if (status.getType().equals("1")) {
                                    //show msg
                                    com.tkmsoft.taahel.model.api.auth.confirm.Title title = status.getTitle();
                                    if (getLang().equals("ar"))
                                        Toast.makeText(mContext, "" + title.getAr(), Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(mContext, "" + title.getEn(), Toast.LENGTH_SHORT).show();

                                    verifyCV.setVisibility(View.GONE);
                                    passCV.setVisibility(View.VISIBLE);


                                } else {
                                    com.tkmsoft.taahel.model.api.auth.confirm.Title title = status.getTitle();
                                    if (getLang().equals("ar"))
                                        Toast.makeText(mContext, "" + title.getAr(), Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(mContext, "" + title.getEn(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressBar.setVisibility(View.GONE);
                        if (error.getErrorCode() != 0) {
                            Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                            Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                        Toast.makeText(mContext, "" + R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }//end serverConfirm()
/////////////// 3- Step ////////////////

    @OnClick(R.id.sendPassBtn)
    void onSendPassBtnClick() {
        serverResetPass();
    }

    private void serverResetPass() {
        progressBar.setVisibility(View.VISIBLE);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        AndroidNetworking.post(Links.Auth.reset())
                .addHeaders("token", listSharedPreference.getToken())
                .addBodyParameter("password", new_passET.getText().toString())
                .addBodyParameter("password_confirmation", new_pass_confirmET.getText().toString())
                .setTag("reset")
                .setOkHttpClient(client)
                .build()
                .getAsParsed(new TypeToken<ResetPassModel>() {
                }, new ParsedRequestListener<ResetPassModel>() {
                    @Override
                    public void onResponse(ResetPassModel response) {
                        progressBar.setVisibility(View.GONE);
                        com.tkmsoft.taahel.model.api.resetpass.Status status = response.getStatus();
                        if (status.getType().equals("1")) {
                            //show msg
                            com.tkmsoft.taahel.model.api.resetpass.Title title = response.getStatus().getTitle();
                            if (title != null) {
                                if (getLang().equals("ar"))
                                    Toast.makeText(mContext, "" + title.getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + title.getEn(), Toast.LENGTH_SHORT).show();
                            }
                            //save data
                            Data data = response.getData();
                            if (data != null) {
                                com.tkmsoft.taahel.model.api.resetpass.Member member = data.getMember();
                                if (member != null) {
                                    //TDO returned data from reset pass
                                    serverLogin(member);
                                }
                            }
                            showDialogue();
                        } else {
                            com.tkmsoft.taahel.model.api.resetpass.Title title = response.getStatus().getTitle();
                            if (title != null) {
                                if (getLang().equals("ar"))
                                    Toast.makeText(mContext, "" + title.getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + title.getEn(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressBar.setVisibility(View.GONE);
                        if (error.getErrorCode() != 0) {
                            Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                            Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        } else {
                            // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                            Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                        }
                        Toast.makeText(mContext, "" + R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                });
    }//end serverConfirm()
    private void serverUpdatePlayerId() {
        callUpdatePlayerId().enqueue(new Callback<UpdatePlayerIDModel>() {
            @Override
            public void onResponse(@NonNull Call<UpdatePlayerIDModel> call, @NonNull Response<UpdatePlayerIDModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        com.tkmsoft.taahel.model.api.auth.updatePlayerId.Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {

                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdatePlayerIDModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private Call<UpdatePlayerIDModel> callUpdatePlayerId() {
        return apiLinkAuth.updatePlayerId(
                listSharedPreference.getToken(),//token
                status.getSubscriptionStatus().getUserId()//player_id
        );
    }
    private void serverLogin(com.tkmsoft.taahel.model.api.resetpass.Member member) {
        progressBar.setVisibility(View.VISIBLE);
        String phone = member.getPhone();
        String pass = new_passET.getText().toString();
        callLogin(phone, pass).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.auth.login.Status status = response.body().getStatus();
                    if (status != null) {
                        switch (status.getType()) {
                            case "1": {

                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());

                                com.tkmsoft.taahel.model.api.auth.login.Data data = response.body().getData();
                                if (data != null) {
                                    com.tkmsoft.taahel.model.api.auth.login.Member member = data.getMember();
                                    if (member != null) {
                                        String memberType = member.getType();
                                        saveUserData(member);

                                        if (memberType.equals("0")) {
                                            Doctor doctor = member.getDoctor();
                                            List<Specialization> specialization = member.getSpecializations();
                                            if (doctor != null && specialization != null)
                                                saveDrData(member, doctor, specialization);
                                        } else {
                                            Patient patient = member.getPatient();
                                            if (patient != null)
                                                savePatientData(patient);
                                        }

                                        listSharedPreference.setLoginStatus(true);
                                        Intent myIntent = new Intent();
                                        myIntent.setClassName(MyApp.getContext().getPackageName(), Objects.requireNonNull(MainActivity.class.getCanonicalName()));
                                        startActivity(myIntent);
                                        mContext.finish();
                                    }
                                }
                                break;
                            }
                            case "01": {
                                //not confirmed

                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());

                                com.tkmsoft.taahel.model.api.auth.login.Data data = response.body().getData();
                                if (data != null) {
                                    com.tkmsoft.taahel.model.api.auth.login.Member member = data.getMember();
                                    if (member != null) {
                                        goToConfirmPage(member);
                                    }
                                }
                                break;
                            }
                            case "02": {
                                //not completed register

                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());

                                com.tkmsoft.taahel.model.api.auth.login.Data data = response.body().getData();
                                if (data != null) {
                                    com.tkmsoft.taahel.model.api.auth.login.Member member = data.getMember();
                                    if (member != null) {
                                        listSharedPreference.setToken(member.getApiToken()); // important to set token
                                        if (member.getType().equals("0"))
                                            moveToFragment.moveInLogin(new TherapistRegFragment());
                                        else
                                            moveToFragment.moveInLogin(new MostafedRegFragment());
                                    }
                                }
                                break;
                            }
                            case "03": {
                                //not completed schedule

                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());

                                com.tkmsoft.taahel.model.api.auth.login.Data data = response.body().getData();
                                if (data != null) {
                                    com.tkmsoft.taahel.model.api.auth.login.Member member = data.getMember();
                                    if (member != null) {
                                        listSharedPreference.deleteAllSharedPref();
                                        listSharedPreference.setToken(member.getApiToken()); // important to set token
                                        moveToFragment.moveInLogin(new TherapistRegFragment2());
                                    }
                                }
                                break;
                            }
                            default:
                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());
                                break;
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<LoginModel> call, @NonNull Throwable t) {
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
    private Call<LoginModel> callLogin(String phone, String pass) {
        return apiLinkAuth.login(phone, pass);
    }
    private void saveUserData(Member member) {
        listSharedPreference.setToken(member.getApiToken());
        listSharedPreference.setUType(member.getType());
        listSharedPreference.setUName(member.getName());
        listSharedPreference.setUUserName(member.getUsername());
        listSharedPreference.setUEmail(member.getEmail());
        listSharedPreference.setPassword(new_passET.getText().toString());
        listSharedPreference.setUAvatar(member.getAvatar());
        if (getLang().equals("ar")) {
            listSharedPreference.setUCity(member.getCity().getNameAr());
            listSharedPreference.setUCountry(member.getCountryAr());
        } else {
            listSharedPreference.setUCity(member.getCity().getNameEn());
            listSharedPreference.setUCountry(member.getCountryEn());
        }
        listSharedPreference.setUGender(member.getGender());
        listSharedPreference.setUId(member.getId());
        listSharedPreference.setULat(member.getLat());
        listSharedPreference.setULong(member.getLong());
        listSharedPreference.setUPhone(member.getPhone());
        listSharedPreference.setUPhoneKey(member.getPhoneKey());
        listSharedPreference.setUCountryId(member.getCountry().getId());
        listSharedPreference.setUCityId(member.getCity().getId());
    }

    private void goToConfirmPage(Member data) {
        listSharedPreference.setToken(data.getApiToken());
        listSharedPreference.setUType(data.getType());
        listSharedPreference.setUName(data.getName());
        listSharedPreference.setUUserName(data.getUsername());
        listSharedPreference.setUAvatar(data.getAvatar());
        listSharedPreference.setUId(data.getId());
        listSharedPreference.setUPhone(data.getPhone());
        listSharedPreference.setUPhoneKey(data.getPhoneKey());
        moveToFragment.moveInLogin(new CodeVerifyFragment());
    }

    private void saveDrData(Member member, Doctor doctor, List<Specialization> specializations) {
        listSharedPreference.setUAbout(doctor.getAbout());
        listSharedPreference.setUEducation(doctor.getEducation());
        listSharedPreference.setUExperience(doctor.getExperiences());
        listSharedPreference.setULicense(doctor.getLicense());
        listSharedPreference.setUPrice(doctor.getPrice());
        ArrayList<String> specialistsStringList = new ArrayList<>();
        for (int i = 0; i < specializations.size(); i++) {
            specialistsStringList.add(specializations.get(i).getNameAr());
        }
        listSharedPreference.setUSpecialists(specialistsStringList);

        if (member.getCurrency() != null) {
            if (getLang().equals("ar"))
                listSharedPreference.setUCurrency(member.getCurrency().getNameAr());
            else
                listSharedPreference.setUCurrency(member.getCurrency().getNameEn());
            listSharedPreference.setUCurrencyId(member.getCurrency().getId());
        }
    }

    private void savePatientData(Patient patient) {
        listSharedPreference.setUBirthDate(patient.getBirthday());
        listSharedPreference.setUReport(patient.getReport());
    }

    private void showDialogue() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams") final View popUpView = li.inflate(R.layout.pass_changed_successfuly, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // create alert dialog

        alertDialogBuilder.setView(popUpView);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        final Button button = popUpView.findViewById(R.id.loginNowBtn);
        final CheckView checkView = popUpView.findViewById(R.id.checkView);
        checkView.check();

        button.setOnClickListener(view -> {
            alertDialog.dismiss();
            listSharedPreference.setLoginStatus(true);
            Intent myIntent = new Intent();
            myIntent.setClassName(MyApp.getContext().getPackageName(), Objects.requireNonNull(MainActivity.class.getCanonicalName()));
            startActivity(myIntent);
            getActivity().finish();
        });
        // show it
        alertDialog.show();
    }

    private void fireBackButtonEvent() {
        ((LoginActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;
                moveToFragment.moveInLogin(new LoginFragment());
            }
        });
    }//end back pressed

    @Override
    public void onDetach() {
        AndroidNetworking.cancelAll();
        super.onDetach();
    }
}
