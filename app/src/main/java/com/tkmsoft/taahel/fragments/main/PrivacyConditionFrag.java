package com.tkmsoft.taahel.fragments.main;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.PrivacyAndConditionAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.PrivacyAndConditionModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyConditionFrag extends Fragment implements PrivacyAndConditionAdapter.ListAllClickListeners {


    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private PrivacyAndConditionAdapter privacyAndConditionAdapter;
    private MainViewsCallBack mMainViewsCallBack;
    private String TAG = getClass().getSimpleName();

    private FragmentActivity mContext;

    private int currentPage;
    private ApiLink apiLink;
    private ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    private List<PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean> dataBeanList;

    public PrivacyConditionFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            mMainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_privacy_condition, container, false);
        ButterKnife.bind(this, rootView);
        ListSharedPreference listSharedPreference = new ListSharedPreference(PrivacyConditionFrag.this.getActivity().getApplicationContext());
        dataBeanList = new ArrayList<>();
        currentPage = 1;
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        privacyAndConditionAdapter = new PrivacyAndConditionAdapter(PrivacyConditionFrag.this.getActivity().getApplicationContext(), dataBeanList, this);
        initRecyclerView();
        progressBar.setVisibility(View.VISIBLE);
        initPullRefreshLayout();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.privacy_policy));
        loadFirstPage();
    }


    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        privacyAndConditionAdapter.clearData();
        pullRefreshLayout.setOnRefreshListener(() -> {
            privacyAndConditionAdapter = new PrivacyAndConditionAdapter(PrivacyConditionFrag.this.getActivity().getApplicationContext(),
                    dataBeanList, PrivacyConditionFrag.this);
            loadFirstPage();
        });
    }


    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(privacyAndConditionAdapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void loadFirstPage() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            Log.d(TAG, "loadFirstPage: ");
            // To ensure list is visible when retry button in error view is clicked

            callGetPrivacyCondition().enqueue(new Callback<PrivacyAndConditionModel>() {
                @Override
                public void onResponse(@NonNull Call<PrivacyAndConditionModel> call, @NonNull Response<PrivacyAndConditionModel> response) {
                    // Got data. Send it to adapter
                    pullRefreshLayout.setRefreshing(false);
                    assert response.body() != null;
                    if (response.body().getStatus().getType().equals("success")) {
                        if (response.body().getData() != null) {
                            List<PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean> results = response.body().getData().getConditions().getData();
                            privacyAndConditionAdapter.replaceData(results);
                        }
                    } else {
                        isLastPage = true;
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<PrivacyAndConditionModel> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    pullRefreshLayout.setRefreshing(false);
                }
            });
        }catch (Exception ignore){}
    }

    private void loadNextPage() {
        try{
        Log.d(TAG, "loadNextPage: " + currentPage);
        callGetPrivacyCondition().enqueue(new Callback<PrivacyAndConditionModel>() {
            @Override
            public void onResponse(@NonNull Call<PrivacyAndConditionModel> call, @NonNull Response<PrivacyAndConditionModel> response) {
                assert response.body() != null;
                if (response.body().getStatus().getType().equals("success")) {

                    if (response.body().getData() != null) {
                        if (response.body().getData().getConditions().getData().size() != 0) {
                            List<PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean> results = response.body().getData().getConditions().getData();
                            privacyAndConditionAdapter.updateData(results);
                        }

                    }
                } else
                    isLastPage = true;
            }

            @Override
            public void onFailure(@NonNull Call<PrivacyAndConditionModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(mContext, "" + fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
            }
        });
        }catch (Exception ignore){}
    }

    private String fetchErrorMessage(Throwable throwable) {

        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!connectionDetector.isConnectingToInternet()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Call<PrivacyAndConditionModel> callGetPrivacyCondition() {
        return apiLink.getCondition(currentPage);
    }


    @Override
    public void onDetach() {
        callGetPrivacyCondition().cancel();
        super.onDetach();
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.main_frameLayout, new HomeFragment())
                            .commit();
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

    @Override
    public void onItemClick(PrivacyAndConditionModel.DataBeanX.ConditionsBean.DataBean datumList, int pos) {

    }
}
