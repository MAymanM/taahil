package com.tkmsoft.taahel.fragments.main.orders.details.patient;

import android.app.AlertDialog;

import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.main.orders.SubRequestFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.checkout.checkoutId.Data;
import com.tkmsoft.taahel.model.api.checkout.checkoutId.GetCheckOutIDModel;
import com.tkmsoft.taahel.model.api.deletecart.DeleteCartModel;
import com.tkmsoft.taahel.model.api.order.payment.PaymentModel;
import com.tkmsoft.taahel.model.api.order.payment.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by MahmoudAyman on 13/09/2018.
 */
public class ConfirmedNonPaidPatientFragment extends Fragment {
    
    @BindView(R.id.codeTV)
    TextView codeTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.timeTV)
    TextView timeTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;
    @BindView(R.id.paymentTV)
    TextView paymentTV;
    @BindView(R.id.payBtn)
    Button payBtn;
    
    String code, date, name, country, city, address, day, time, orderId, description, paymentStatus;
    
    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private MainViewsCallBack mainViewsCallBack;
    
    private ListSharedPreference listSharedPreference;
    private ApiLink apiOrderLink, apiLinkPayment;
    private String paymentType, trackerId;
    private MoveToFragment moveToFragment;
    private String checkOutId;
    private String countryId, cityId;
    private String total;
    
    @Override
    public void onAttach(Context activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        fireBackButtonEvent();
        try {
            activity = getActivity();
            mainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            code = arguments.getString("code");
            date = arguments.getString("date");
            name = arguments.getString("name");
            total = arguments.getString("total");
            country = arguments.getString("country");
            countryId = String.valueOf(listSharedPreference.getUCountryId());
            city = arguments.getString("city");
            cityId = String.valueOf(listSharedPreference.getUCityId());
            address = arguments.getString("address");
            day = arguments.getString("day");
            description = arguments.getString("desc");
            paymentStatus = arguments.getString("paymentStatus");
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < arguments.getInt("timeSize"); i++) {
                stringBuilder.append(getArguments().getString("time" + i));
                if (i < arguments.getInt("timeSize") - 1) {
                    stringBuilder.append(" ,\n");
                }
            }
            time = stringBuilder.toString();
            orderId = arguments.getString("orderId");
            trackerId = arguments.getString("trackerId");
        }
        apiOrderLink = MyRetrofitClient.getOrder().create(ApiLink.class);
        apiLinkPayment = MyRetrofitClient.getPayment().create(ApiLink.class);
    }
    
    public ConfirmedNonPaidPatientFragment() {
        // Required empty public constructor
    }
    
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_confirmed_non_paid_patient, container, false);
        ButterKnife.bind(this, rootView);
        
        initUI(rootView);
        return rootView;
    }
    
    private void initUI(View rootView) {
        nameTV.setText(name);
        codeTV.setText(code);
        dateTV.setText(date);
        countryTV.setText(country);
        cityTV.setText(city);
        addressTV.setText(address);
        dayTV.setText(day);
        timeTV.setText(time);
        descriptionTV.setText(description);
        switch (paymentStatus) {
            case "undefined":
                paymentTV.setText(getString(R.string.not_paid_yet));
                break;
            case "cash":
                paymentTV.setText(getString(R.string.cash));
                break;
            case "visa":
                paymentTV.setText(getString(R.string.visa));
                break;
        }
    }
    
    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.confirmed_order));
    }
    
    @OnClick(R.id.payBtn)
    void onPayBtnClick() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View popUpView = li.inflate(R.layout.complete_order_payment_layout, null);
        
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // create alert dialog
        
        alertDialogBuilder.setView(popUpView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        // set prompts.xml to alertDialog builder
        final RadioButton cashRadio = popUpView.findViewById(R.id.cashRB);
        final RadioButton visaRadio = popUpView.findViewById(R.id.visaRB);
        cashRadio.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                paymentType = "cash";
                visaRadio.setChecked(false);
            }
        });
        
        visaRadio.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                paymentType = "visa";
                cashRadio.setChecked(false);
            }
        });
        
        final Button sendBtn = popUpView.findViewById(R.id.sendBtn);
        final Button cancelBtn = popUpView.findViewById(R.id.cancelBtn);
        
        final ProgressBar progressBar = popUpView.findViewById(R.id.pay_progress);
        
        cancelBtn.setOnClickListener
                (view -> alertDialog.dismiss());
        
        sendBtn.setOnClickListener(view -> {
            if (paymentType.equals("cash"))
                serverPayCash(progressBar, alertDialog);
            else
                serverVisa(progressBar, alertDialog, sendBtn);
        });
        // show it
        alertDialog.show();
    }
    
    private Call<GetCheckOutIDModel> callCheckOut() {
//        LinkedHashMap<String, String> quantityListMap = new LinkedHashMap<>();
//        for (int i = 0; i < quantitiesList.size(); i++) {
//            quantityListMap.put("quantity[" + i + "]", quantitiesList.get(i));
//            Log.d(TAG, "qunt checkout: " + quantityListMap.get("quantity[" + i + "]"));
//        }
//        delivery = deliveryTV.getText().toString();
//        totalAll = totalAllTV.getText().toString();
//        Timber.d("callCheckOut: " + "\ncity:" + cityCode + "\naddress:" + address +
//                "\nphone:" + phone + "\ndeliverycash:" + delivery + "\ntotalAmount" + totalAll);
        return apiLinkPayment.get_checkout_id_order(listSharedPreference.getToken(),
                countryId,
                cityId,
                address,
                listSharedPreference.getUPhone(),
                "11511",
                "15",
                total,
                "order"
                , orderId);
    }
    
    private void goToCheckOut(String id) {
        Set<String> paymentBrands = new LinkedHashSet<>();
        
        paymentBrands.add("VISA");
        paymentBrands.add("MASTER");
        
        //change Connect.ProviderMode.TEST to Connect.ProviderMode.LIVE in production
        CheckoutSettings checkoutSettings = new CheckoutSettings(id, paymentBrands, Connect.ProviderMode.LIVE);
        checkoutSettings.setShopperResultUrl("https://taahel.com/result");
//        ComponentName receiverComponentName = new ComponentName("com.tkmsoft.taahel", Objects.requireNonNull(CheckoutBroadcastReceiver.class.getCanonicalName()));
//        Intent intent = checkoutSettings.createCheckoutActivityIntent(mContext, receiverComponentName);
        Intent intent = checkoutSettings.createCheckoutActivityIntent(mContext);
        checkoutSettings.setWebViewEnabledFor3DSecure(true);
        // Set shopper result URL
        
        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);
        
    }
    
    private void serverVisa(ProgressBar progressBar, AlertDialog alertDialog, Button checkoutBtn) {
        progressBar.setVisibility(View.VISIBLE);
        checkoutBtn.setEnabled(false);
        callCheckOut().enqueue(new Callback<GetCheckOutIDModel>() {
            @Override
            public void onResponse(@NonNull Call<GetCheckOutIDModel> call, @NonNull Response<GetCheckOutIDModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        com.tkmsoft.taahel.model.api.checkout.checkoutId.Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("1")) {
                                Data data = response.body().getData();
                                if (data != null) {
                                    goToCheckOut(data.getId());
                                    checkOutId = data.getId();
                                    Timber.d("onResponse: %s", data.getId());
                                }
                            } else {
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
                alertDialog.dismiss();
                checkoutBtn.setEnabled(true);
            }
            
            @Override
            public void onFailure(@NonNull Call<GetCheckOutIDModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                checkoutBtn.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case CheckoutActivity.RESULT_OK:
                /* transaction completed */
                serverDeleteOrder();
                break;
            case CheckoutActivity.RESULT_CANCELED:
                /* shopper canceled the checkout process */
                Timber.d("onActivityResult: canceled");
                Toast.makeText(mContext, "canceled", Toast.LENGTH_SHORT).show();
                break;
            case CheckoutActivity.RESULT_ERROR:
                /* error occurred */
                PaymentError error = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR);
                Timber.e("onActivityResult: " + "error" + error.getErrorInfo() + "\n" + error.getErrorMessage());
        }
    }
    
    private void serverDeleteOrder() {
        callDeleteCart().enqueue(new Callback<DeleteCartModel>() {
            @Override
            public void onResponse(@NotNull Call<DeleteCartModel> call, @NotNull Response<DeleteCartModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.deletecart.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("1")) {
                            Toast.makeText(mContext, "" + getString(R.string.payment_done), Toast.LENGTH_SHORT).show();
                            moveToFragment.moveInMain(new SubRequestFragment());
                        } else
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(@NotNull Call<DeleteCartModel> call, @NotNull Throwable t) {
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    
    private Call<DeleteCartModel> callDeleteCart() {
        Timber.d("check out id: %s", checkOutId);
        return apiLinkPayment.deleteCart(listSharedPreference.getToken(),
                checkOutId);
    }
    
    //cash
    private void serverPayCash(final ProgressBar progressBar, final AlertDialog alertDialog) {
        progressBar.setVisibility(View.VISIBLE);
        confirmCashPaymentCall().enqueue(new Callback<PaymentModel>() {
            @Override
            public void onResponse(@NonNull Call<PaymentModel> call, @NonNull Response<PaymentModel> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (response.body().getStatus().getType().equals("1")) {
                                Toast.makeText(mContext, R.string.payment_sent_ok, Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();
                                moveToFragment.moveInMain(new SubRequestFragment());
                            } else
                                Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception exc) {
                    Timber.d("onResponse: %s", exc.getMessage());
                }
                progressBar.setVisibility(View.GONE);
            }
            
            @Override
            public void onFailure(@NonNull Call<PaymentModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    
    private Call<PaymentModel> confirmCashPaymentCall() {
        return apiOrderLink.orderCash(listSharedPreference.getToken(),
                "cash",
                trackerId);
    }
    
    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new SubRequestFragment());
            }
        });
    }//end back pressed
}
