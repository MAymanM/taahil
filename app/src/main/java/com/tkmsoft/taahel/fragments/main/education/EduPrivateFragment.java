package com.tkmsoft.taahel.fragments.main.education;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.ads.AdsAdapter;
import com.tkmsoft.taahel.adapters.education.EducationAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.AdsModel;
import com.tkmsoft.taahel.model.api.education.view.Comment;
import com.tkmsoft.taahel.model.api.education.view.Datum;
import com.tkmsoft.taahel.model.api.education.view.EducationModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EduPrivateFragment extends Fragment implements EducationAdapter.ListAllListeners {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.viewPager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CircleIndicator circleIndicator;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.notFoundTV)
    TextView notFoundTV;
    @BindView(R.id.ads_cardView)
    CardView ads_cardView;
    private int currentPage;

    ListSharedPreference listSharedPreference;

    private MainViewsCallBack mainViewsCallBack;
    EducationAdapter educationNewAdapt;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLink;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;

    private int currentPager = 0;
    private String cityCode = "null", countryCode = "null", rate = "null";
    boolean isFilter;
    MoveToFragment moveToFragment;

    public EduPrivateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        fireBackButtonEvent();
        super.onAttach(context);
        try {
            context = getActivity();
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cityCode = getArguments().getString("city");
            countryCode = getArguments().getString("country");
            rate = getArguments().getString("rate");
            isFilter = getArguments().getBoolean("isFilter");
        } else
            isFilter = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_education_private, container, false);
        ButterKnife.bind(this, rootView);
        listSharedPreference = new ListSharedPreference(EduPrivateFragment.this.getActivity().getApplicationContext());
        currentPage = 1;
        apiLink = MyRetrofitClient.getEducation().create(ApiLink.class);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        educationNewAdapt = new EducationAdapter(new ArrayList<>(), this);
        initRecyclerView();
        setupFilterButton();
        setAddFab();
        loadFirstPage();
    }

    @OnClick(R.id.govBtn)
    protected void onGovBtn() {
        Toast.makeText(mContext, "xcv", Toast.LENGTH_SHORT).show();
//        moveToFragment.moveInMain(new EduGovernmentFragment());
    }

    @OnClick(R.id.privateBtn)
    protected void onPrivateBtn() {
        loadFirstPage();
    }

    private void setupFilterButton() {
        ImageButton imageButton = getActivity().findViewById(R.id.toolbar_filter_button);
        imageButton.setOnClickListener(view -> mainViewsCallBack.sendCategNum("2", "edu"));
    }

    private void setAddFab() {
        FloatingActionButton fab = mContext.findViewById(R.id.main_fab);
        fab.setOnClickListener(view -> {
            listSharedPreference.deleteSharedPref("lat");
            listSharedPreference.deleteSharedPref("longX");
            getFragmentManager().beginTransaction()
                    .replace(R.id.main_frameLayout, new AddEducationFrag())
                    .commit();
        });
    }

    private void serverAds() {
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<AdsModel> sliderImages = retrofit.getAds("ads?type=2");
        sliderImages.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(@NonNull Call<AdsModel> call, @NonNull Response<AdsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            initImageSlider(response.body().getData().getAds_info());
                            ads_cardView.setVisibility(View.VISIBLE);
                        } else
                            ads_cardView.setVisibility(View.GONE);
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<AdsModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }//end serverRegister()

    private void initImageSlider(final List<AdsModel.DataBean.AdsInfoBean> adsInfo) {
        try {
            mPager.setAdapter(new AdsAdapter(getActivity(), adsInfo,
                    (images, position) -> {
                        //on ad click
                    }));
            circleIndicator.setViewPager(mPager);

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = () -> {
                if (currentPager == adsInfo.size()) {
                    currentPager = 0;
                }
                mPager.setCurrentItem(currentPager++, true);
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2500, 2500);
        } catch (Exception ignored) {

        }
    }

    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(educationNewAdapt);
        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);
        Log.d(TAG, "loadFirstPage: ");
        // To ensure list is visible when retry button in error view is clicked

        callGetEducation().enqueue(new Callback<EducationModel>() {
            @Override
            public void onResponse(@NonNull Call<EducationModel> call, @NonNull Response<EducationModel> response) {
                // Got data. Send it to adapter
                assert response.body() != null;
                if (response.body().getStatus().getType().equals("success")) {
                    List<Datum> results = response.body().getData().getProducts().getData();
                    if (isFilter) {
                        attemptFilter(results, 1);
                    } else {
                        educationNewAdapt.replaceData(results);
                    }
                } else {
                    isLastPage = true;
                    Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<EducationModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void attemptFilter(List<Datum> results, int pos) {
        int rateInt;
        if (!rate.equals("null"))
            rateInt = Integer.valueOf(rate);
        else
            rateInt = 0;
        List<Datum> newArray = new ArrayList<>();
        for (int i = 0; i < results.size(); i++) {
            String resCountryCode = String.valueOf(results.get(i).getCountry().getId());
            String resCityCode = String.valueOf(results.get(i).getCity().getId());
            int resRate = results.get(i).getCountRate();

            if (!countryCode.equals("null")) {
                if (!cityCode.equals("null")) {
                    if (!rate.equals("null")) {
                        if (resCityCode.equals(cityCode) && resCountryCode.equals(countryCode) && resRate >= rateInt) {
                            newArray.add(results.get(i));
                        }
                    } else if (resCountryCode.equals(countryCode) && resCityCode.equals(cityCode)) {
                        newArray.add(results.get(i));
                    }
                }//end city
                else if (!rate.equals("null")) {
                    if (resCountryCode.equals(countryCode) && resRate >= rateInt) {
                        newArray.add(results.get(i));
                    }
                } else {
                    if (resCountryCode.equals(countryCode)) {
                        newArray.add(results.get(i));
                    }
                }
            }//end country
            else if (!rate.equals("null")) {
                if (resRate >= rateInt) {
                    newArray.add(results.get(i));
                }
            }
        }//end for
        if (pos == 1)
            educationNewAdapt.replaceData(newArray);
        else
            educationNewAdapt.updateData(newArray);
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);
        callGetEducation().enqueue(new Callback<EducationModel>() {
            @Override
            public void onResponse(@NonNull Call<EducationModel> call, @NonNull Response<EducationModel> response) {

                assert response.body() != null;
                if (response.body().getStatus().getType().equals("success")) {
                    List<Datum> results = response.body().getData().getProducts().getData();
                    if (isFilter) {
                        attemptFilter(results, 2);
                    } else {
                        educationNewAdapt.updateData(results);
                    }
                } else
                    isLastPage = true;
            }

            @Override
            public void onFailure(@NonNull Call<EducationModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                //      educationNewAdapt.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    private String fetchErrorMessage(Throwable throwable) {

        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!connectionDetector.isConnectingToInternet()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Call<EducationModel> callGetEducation() {
        return apiLink.getEducations("2", currentPage);
    }

    @Override
    public void onStart() {
        super.onStart();
        serverAds();
    }


    @Override
    public void onDetach() {
        callGetEducation().cancel();
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(true);
        mainViewsCallBack.showFilterBtn(true);
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.main_frameLayout, new HomeFragment())
                            .commit();
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

    private ArrayList<String> getCommentsContent(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getContent());
        }
        return sList;
    }

    private ArrayList<String> getCommentsRate(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getRate());
        }
        return sList;
    }

    @Override
    public void onItemClick(Datum eduData, int pos) {
//        Fragment fragment = new EducationDetailFragment();
        Toast.makeText(mContext, "4", Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        bundle.putString("pId", String.valueOf(eduData.getId()));
        bundle.putString("image", eduData.getPhoto());
        bundle.putInt("typeId", eduData.getCategory().getId());
        if (getLanguage().equals("ar")) {
            bundle.putString("type", eduData.getCategory().getNameAr());
            bundle.putString("name", eduData.getNameAr());
            bundle.putString("country", eduData.getCountry().getNameAr());
            bundle.putString("city", eduData.getCity().getNameAr());
            bundle.putString("address", eduData.getAddressAr());
        } else {
            bundle.putString("type", eduData.getCategory().getNameEn());
            bundle.putString("name", eduData.getNameEn());
            bundle.putString("country", eduData.getCountry().getNameEn());
            bundle.putString("city", eduData.getCity().getNameEn());
            bundle.putString("address", eduData.getAddressEn());
        }
        bundle.putString("link", eduData.getWeb());
        bundle.putFloat("rate", eduData.getCountRate());
        bundle.putString("lat", eduData.getLat());
        bundle.putString("longX", eduData.getLong());

        bundle.putStringArrayList("commentsContent", getCommentsContent(eduData.getComments()));
        bundle.putStringArrayList("commentsRate", getCommentsRate(eduData.getComments()));
        bundle.putStringArrayList("cmntsUserNames", getCommentsUserNames(eduData.getComments()));
        bundle.putStringArrayList("cmntsUserImg", getCommentsUserImgs(eduData.getComments()));

//        fragment.setArguments(bundle);
//        moveToFragment.moveInMain(fragment);
    }

    private ArrayList<String> getCommentsUserImgs(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getAvatar());
        }
        return sList;
    }

    private ArrayList<String> getCommentsUserNames(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getName());
        }
        return sList;
    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }
}
