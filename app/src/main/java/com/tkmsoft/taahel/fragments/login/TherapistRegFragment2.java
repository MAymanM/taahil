package com.tkmsoft.taahel.fragments.login;


import android.app.Activity;

import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.DaysListAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.DaysModel;
import com.tkmsoft.taahel.model.api.auth.login.Data;
import com.tkmsoft.taahel.model.api.auth.login.Doctor;
import com.tkmsoft.taahel.model.api.auth.login.LoginModel;
import com.tkmsoft.taahel.model.api.auth.login.Member;
import com.tkmsoft.taahel.model.api.auth.login.Specialization;
import com.tkmsoft.taahel.model.api.auth.login.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TherapistRegFragment2 extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    ListSharedPreference listSharedPreference;
    @BindView(R.id.register_done_Btn)
    Button register_done_Btn;
    private FragmentActivity mContext;
    private MoveToFragment moveToFragment;
    private ApiLink apiLink;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    public TherapistRegFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        fireBackButtonEvent();
        super.onAttach(activity);
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext.getApplicationContext());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.auth().create(ApiLink.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_therapist_reg_2, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
        initAdapter();
        launchTapTargetView(rootView);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        }
    }

    private void initAdapter() {
        ArrayList<DaysModel> mDataList = new ArrayList<>();
        String[] daysList = {getString(R.string.saturday), getString(R.string.sunday), getString(R.string.monday),
                getString(R.string.tuesday), getString(R.string.wednesday),
                getString(R.string.thursday), getString(R.string.friday)};

        int[] daysNum = {6, 0, 1, 2, 3, 4, 5};

        for (int i = 0; i < daysList.length; i++) {
            DaysModel th = new DaysModel(daysList[i], daysNum[i]);
            mDataList.add(i, th);
        }

        DaysListAdapter daysListAdapter = new DaysListAdapter(mContext, mDataList, (daysModel, adapterPosition) ->
        {
            listSharedPreference.setIsFreeChoosen(false);
            Fragment fr = new TherapistRegFragment3();
            Bundle args = new Bundle();
            args.putString("day", daysModel.getDay());
            args.putInt("dayNum", daysModel.getDayNum());
            fr.setArguments(args);
            moveToFragment.moveInLogin(fr);
        });

        recyclerView.setAdapter(daysListAdapter);
        daysListAdapter.notifyDataSetChanged();
    }

    private void launchTapTargetView(View rootView) {
        if (!listSharedPreference.getisRibbleViewRunForDr()) {
            TapTargetView.showFor(mContext,
                    TapTarget.forView(rootView.findViewById(R.id.register_done_Btn),
                            getString(R.string.warning),
                            getString(R.string.ribble_description_for_dr))
                            // All options below are optional
                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                            .outerCircleAlpha(0.76f)            // Specify the alpha amount for the outer circle
                            .targetCircleColor(R.color.white)   // Specify a color for the target circle
                            .titleTextSize(30)                  // Specify the size (in sp) of the title text
                            .titleTextColor(R.color.colorAccent)      // Specify the color of the title text
                            .descriptionTextSize(20)            // Specify the size (in sp) of the description text
                            .descriptionTextColor(R.color.colorAccent)  // Specify the color of the description text
                            .textColor(R.color.colorAccent)            // Specify a color for both the title and description text
                            .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                            .dimColor(R.color.black)            // If set, will dim behind the view with 30% opacity of the given color
                            .drawShadow(true)                   // Whether to draw a drop shadow or not
                            .cancelable(true)                  // Whether tapping outside the outer circle dismisses the view
                            .tintTarget(true)                   // Whether to tint the target view's color
                            .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)
                            //.icon(droid)                     // Specify a custom drawable to draw as the target
                            .targetRadius(100),                  // Specify the target radius (in dp)
                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                        @Override
                        public void onTargetClick(TapTargetView view) {
                            super.onTargetClick(view);      // This call is optional
                            view.dismiss(true);
                        }

                        @Override
                        public void onOuterCircleClick(TapTargetView view) {
                            super.onOuterCircleClick(view);
                            view.dismiss(true);
                        }
                    });
            listSharedPreference.setIsRibbleViewRunForDr(true);
        }
    }

    private void fireBackButtonEvent() {

        ((LoginActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInLogin(new TherapistRegFragment());
            }
        });
    }//end back pressed

    @OnClick(R.id.register_done_Btn)
    void onRegister_done_Btn() {
        if (listSharedPreference.getIsTimeSelected()) {
            serverLogin();
        } else
            Toast.makeText(mContext, "" + getString(R.string.time_not_selected_yet), Toast.LENGTH_SHORT).show();
    }

    private Call<LoginModel> callLogin() {

        return apiLink.login(listSharedPreference.getUPhone(),
                listSharedPreference.getPassword());
    }

    private void serverLogin() {
        progressBar.setVisibility(View.VISIBLE);
        callLogin().enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        switch (status.getType()) {
                            case "1": {
                                listSharedPreference.deleteAllSharedPref();
                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());

                                Data data = response.body().getData();
                                if (data != null) {
                                    Member member = data.getMember();
                                    if (member != null) {
                                        String memberType = member.getType();
                                        saveUserData(member);

                                        if (memberType.equals("0")) {
                                            Doctor doctor = member.getDoctor();
                                            List<Specialization> specialization = member.getSpecializations();
                                            if (doctor != null && specialization != null)
                                                saveDrData(member, doctor, specialization);
                                        }

                                        listSharedPreference.setLoginStatus(true);
                                        Intent myIntent = new Intent();
                                        myIntent.setClassName(MyApp.getContext().getPackageName(), Objects.requireNonNull(MainActivity.class.getCanonicalName()));
                                        startActivity(myIntent);
                                        mContext.finish();
                                    }
                                }
                                break;
                            }
                            default:
                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());
                                break;
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<LoginModel> call, @NonNull Throwable t) {
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void setResponseMessage(String ar, String en) {
        if (listSharedPreference.getLanguage().equals("ar"))
            Toast.makeText(mContext, "" + ar, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + en, Toast.LENGTH_SHORT).show();
    }

    private void saveDrData(Member member, Doctor doctor, List<Specialization> specializations) {
        listSharedPreference.setUAbout(doctor.getAbout());
        listSharedPreference.setUEducation(doctor.getEducation());
        listSharedPreference.setUExperience(doctor.getExperiences());
        listSharedPreference.setULicense(doctor.getLicense());
        listSharedPreference.setUPrice(doctor.getPrice());
        ArrayList<String> specialistsStringList = new ArrayList<>();
        for (int i = 0; i < specializations.size(); i++) {
            specialistsStringList.add(specializations.get(i).getNameAr());
        }
        listSharedPreference.setUSpecialists(specialistsStringList);

        if (member.getCurrency() != null) {
            if (listSharedPreference.getLanguage().equals("ar"))
                listSharedPreference.setUCurrency(member.getCurrency().getNameAr());
            else
                listSharedPreference.setUCurrency(member.getCurrency().getNameEn());
            listSharedPreference.setUCurrencyId(member.getCurrency().getId());
        }
    }

    private void saveUserData(Member member) {
        listSharedPreference.setToken(member.getApiToken());
        listSharedPreference.setUType(member.getType());
        listSharedPreference.setUName(member.getName());
        listSharedPreference.setUUserName(member.getUsername());
        listSharedPreference.setUEmail(member.getEmail());
//        listSharedPreference.setUAddress(address);
        listSharedPreference.setUAvatar(member.getAvatar());
        if (listSharedPreference.getLanguage().equals("ar")) {
            listSharedPreference.setUCity(member.getCity().getNameAr());
            listSharedPreference.setUCountry(member.getCountryAr());
        } else {
            listSharedPreference.setUCity(member.getCity().getNameEn());
            listSharedPreference.setUCountry(member.getCountryEn());
        }
        listSharedPreference.setUGender(member.getGender());
        listSharedPreference.setUId(member.getId());
        listSharedPreference.setULat(member.getLat());
        listSharedPreference.setULong(member.getLong());
        listSharedPreference.setUPhone(member.getPhone());
        listSharedPreference.setUPhoneKey(member.getPhoneKey());
        listSharedPreference.setUCountryId(member.getCountry().getId());
        listSharedPreference.setUCityId(member.getCity().getId());
    }

}
