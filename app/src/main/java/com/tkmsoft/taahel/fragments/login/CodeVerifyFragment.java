package com.tkmsoft.taahel.fragments.login;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.api.auth.confirm.ConfirmModel;
import com.tkmsoft.taahel.model.api.auth.confirm.Status;
import com.tkmsoft.taahel.model.api.auth.forgetpass.ForgetPassModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CodeVerifyFragment extends Fragment {

    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.codeET)
    EditText codeET;
    @BindView(R.id.resendTV)
    TextView resendTV;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ListSharedPreference listSharedPreference;
    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private MoveToFragment moveToFragment;
    ApiLink apiLink;

    public CodeVerifyFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
            fireBackButtonEvent();
        }
        super.onAttach(activity);
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext.getApplicationContext());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.auth().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_code, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {

    }

    @OnClick(R.id.resendTV)
    protected void onResendTVClick() {
        serverResendCode();
    }

    private void serverResendCode() {
        progressBar.setVisibility(View.VISIBLE);
        callForget().enqueue(new Callback<ForgetPassModel>() {
            @Override
            public void onResponse(@NonNull Call<ForgetPassModel> call, @NonNull Response<ForgetPassModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        com.tkmsoft.taahel.model.api.auth.forgetpass.Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("1")) {
                                //show msg
                                if (status.getTitle() != null)
                                    setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());
                            } else {
                                if (status.getTitle() != null)
                                    setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ForgetPassModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });


    }

    private Call<ForgetPassModel> callForget() {
        return apiLink.forget_pass(listSharedPreference.getToken(), listSharedPreference.getUPhone());
    }

    private Call<ConfirmModel> callConfirm() {
        return apiLink.confirm(listSharedPreference.getToken(), codeET.getText().toString());
    }

    private void setResponseMessage(String ar, String en) {
        if (getLang().equals("ar"))
            Toast.makeText(mContext, "" + ar, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + en, Toast.LENGTH_SHORT).show();
    }

    private String getLang() {
        return listSharedPreference.getLanguage();
    }

    @OnClick(R.id.btn_submit)
    protected void ocSubmitClcik() {
        if (codeET.getText().length() > 2) {
            try {
                serverConfirm();
            } catch (Exception exc) {
                Toast.makeText(mContext, "" + exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mContext, R.string.enter_code_1st, Toast.LENGTH_SHORT).show();
        }
    }

    private void serverConfirm() {
        progressBar.setVisibility(View.VISIBLE);
        callConfirm().enqueue(new Callback<ConfirmModel>() {
            @Override
            public void onResponse(@NonNull Call<ConfirmModel> call, @NonNull Response<ConfirmModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("1")) {
                                //show msg
                                if (status.getTitle() != null)
                                    setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());
                                if (listSharedPreference.getUType().equals("0"))
                                    moveToFragment.moveInLogin(new TherapistRegFragment());
                                else
                                    moveToFragment.moveInLogin(new MostafedRegFragment());
                            } else {
                                if (status.getTitle() != null)
                                    setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ConfirmModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }//end serverConfirm()


    private void fireBackButtonEvent() {
        ((LoginActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction()
                        .replace(R.id.login_frame, new RegisterFragment(), "RegisterFragment")
                        .commit();
            }
        });
    }//end back pressed
}