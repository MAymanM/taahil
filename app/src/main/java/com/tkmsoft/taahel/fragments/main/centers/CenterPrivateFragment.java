package com.tkmsoft.taahel.fragments.main.centers;


import android.app.Activity;

import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.ads.AdsAdapter;
import com.tkmsoft.taahel.adapters.drugs.DrugsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.AdsModel;
import com.tkmsoft.taahel.model.api.drugs.view.Comment;
import com.tkmsoft.taahel.model.api.drugs.view.Datum;
import com.tkmsoft.taahel.model.api.drugs.view.DrugsModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class CenterPrivateFragment extends Fragment implements DrugsAdapter.ListAllClickListeners {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.viewPager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CircleIndicator circleIndicator;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.notFoundTV)
    TextView notFoundTV;

    @BindView(R.id.ads_cardView)
    CardView ads_cardView;

    @BindView(R.id.ad_progressBar)
    ProgressBar ad_progressBar;

    ListSharedPreference listSharedPreference;

    private MainViewsCallBack mainViewsCallBack;
    private int currentPage;
    DrugsAdapter drugsAdapter;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLink;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    private int currentPager = 0;

    String cityCode = "null", countryCode = "null", rate = "null";
    boolean isFilter;
    MoveToFragment moveToFragment;

    public CenterPrivateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cityCode = getArguments().getString("city");
            countryCode = getArguments().getString("country");
            rate = getArguments().getString("rate");
            isFilter = getArguments().getBoolean("isFilter");
        } else
            isFilter = false;
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        try {
            context = getActivity();
            mainViewsCallBack = (MainViewsCallBack) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_center_private, container, false);
        ButterKnife.bind(this, rootView);
        listSharedPreference = new ListSharedPreference(CenterPrivateFragment.this.getActivity().getApplicationContext());
        currentPage = 1;
        apiLink = MyRetrofitClient.getDrug().create(ApiLink.class);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        drugsAdapter = new DrugsAdapter(new ArrayList<>(), this);
        initRecyclerView();
        setupFilterButton();
        setAddFab();
        loadFirstPage();
        mainViewsCallBack.showAddFab(true);
        mainViewsCallBack.showFilterBtn(true);
    }

    @OnClick(R.id.govBtn)
    protected void onGovBtn() {
        moveToFragment.moveInMain(new CenterGovernmentFragment());
    }

    @OnClick(R.id.privateBtn)
    protected void onPrivateBtn() {
        loadFirstPage();
    }

    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(drugsAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);
        Log.d(TAG, "loadFirstPage: ");
        // To ensure list is visible when retry button in error view is clicked

        callGetDrugs().enqueue(new Callback<DrugsModel>() {
            @Override
            public void onResponse(@NonNull Call<DrugsModel> call, @NonNull Response<DrugsModel> response) {
                // Got data. Send it to adapter
                assert response.body() != null;
                if (response.body().getStatus().getType().equals("success")) {
                    if (response.body().getData() != null) {
                        List<Datum> results = response.body().getData().getProducts().getData();
                        if (isFilter) {
                            attemptFilter(results, 1);
                        } else {
                            drugsAdapter.replaceData(results);
                        }
                        serverAds();
                    }
                } else {
                    isLastPage = true;
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DrugsModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void attemptFilter(List<Datum> results, int pos) {
        int rateInt;
        if (!rate.equals("null"))
            rateInt = Integer.valueOf(rate);
        else
            rateInt = 0;
        List<Datum> newArray = new ArrayList<>();
        for (int i = 0; i < results.size(); i++) {
            String resCountryCode = String.valueOf(results.get(i).getCountry().getId());
            String resCityCode = String.valueOf(results.get(i).getCity().getId());
            int resRate = results.get(i).getCountRate();

            if (!countryCode.equals("null")) {
                if (!cityCode.equals("null")) {
                    if (!rate.equals("null")) {
                        if (resCityCode.equals(cityCode) && resCountryCode.equals(countryCode) && resRate >= rateInt) {
                            newArray.add(results.get(i));
                        }
                    } else if (resCountryCode.equals(countryCode) && resCityCode.equals(cityCode)) {
                        newArray.add(results.get(i));
                    }
                }//end city
                else if (!rate.equals("null")) {
                    if (resCountryCode.equals(countryCode) && resRate >= rateInt) {
                        newArray.add(results.get(i));
                    }
                } else {
                    if (resCountryCode.equals(countryCode)) {
                        newArray.add(results.get(i));
                    }
                }
            }//end country
            else if (!rate.equals("null")) {
                if (resRate >= rateInt) {
                    newArray.add(results.get(i));
                }
            }
        }//end for
        if (pos == 1)
            drugsAdapter.replaceData(newArray);
        else
            drugsAdapter.updateData(newArray);
    }


    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);
        callGetDrugs().enqueue(new Callback<DrugsModel>() {
            @Override
            public void onResponse(@NonNull Call<DrugsModel> call, @NonNull Response<DrugsModel> response) {

                assert response.body() != null;
                if (response.body().getStatus().getType().equals("success")) {
                    if (response.body().getData() != null) {
                        List<Datum> results = response.body().getData().getProducts().getData();
                        if (isFilter) {
                            attemptFilter(results, 2);
                        } else {
                            drugsAdapter.updateData(results);
                        }
                    }
                } else
                    isLastPage = true;
            }

            @Override
            public void onFailure(@NonNull Call<DrugsModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private Call<DrugsModel> callGetDrugs() {
        return apiLink.getDrugs("2", currentPage);
    }

    private void setupFilterButton() {
        ImageButton imageButton = mContext.findViewById(R.id.toolbar_filter_button);
        imageButton.setOnClickListener(view -> mainViewsCallBack.sendCategNum("2", "center"));
    }

    private void setAddFab() {
        FloatingActionButton fab = mContext.findViewById(R.id.main_fab);
        fab.setOnClickListener(view -> {
            listSharedPreference.deleteSharedPref("lat");
            listSharedPreference.deleteSharedPref("longX");
            moveToFragment.moveInMain(new AddCentersFrag());
        });
    }


    private void serverAds() {
        ad_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<AdsModel> sliderImages = retrofit.getAds("ads?type=1");
        sliderImages.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(@NonNull Call<AdsModel> call, @NonNull Response<AdsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            initImageSlider(response.body().getData().getAds_info());
                            ads_cardView.setVisibility(View.VISIBLE);
                        } else {
                            ads_cardView.setVisibility(View.GONE);
                        }
                    }
                }
                ad_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<AdsModel> call, @NonNull Throwable t) {
                ad_progressBar.setVisibility(View.GONE);
                ads_cardView.setVisibility(View.GONE);
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end server()

    private void initImageSlider(final List<AdsModel.DataBean.AdsInfoBean> adsInfo) {
        try {
            mPager.setAdapter(new AdsAdapter(getActivity(), adsInfo,
                    (adsInfoBeans, position) -> {
                        //on ad click
                    }));
            circleIndicator.setViewPager(mPager);

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = () -> {
                if (currentPager == adsInfo.size()) {
                    currentPager = 0;
                }
                mPager.setCurrentItem(currentPager++, true);
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2500, 2500);
        } catch (Exception ignored) {

        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDetach() {
        callGetDrugs().cancel();
        super.onDetach();
    }


    private ArrayList<String> getCommentsContent(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getContent());
        }
        return sList;
    }

    private ArrayList<String> getCommentsRate(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getRate());
        }
        return sList;
    }

    private void fireBackButtonEvent() {

        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
            }
        });

    }//end back pressed

    @Override
    public void onItemClick(Datum drugsBean, int pos) {
        Fragment fragment = new CenterDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("pId", String.valueOf(drugsBean.getId()));
        bundle.putString("image", drugsBean.getPhoto());
        bundle.putInt("typeId", drugsBean.getCategory().getId());

        if (getLanguage().equals("ar")) {
            bundle.putString("type", drugsBean.getCategory().getNameAr());
            bundle.putString("name", drugsBean.getNameAr());
            bundle.putString("country", drugsBean.getCountry().getNameAr());
            bundle.putString("city", drugsBean.getCity().getNameAr());
            bundle.putString("address", drugsBean.getAddressAr());
        } else {
            bundle.putString("type", drugsBean.getCategory().getNameEn());
            bundle.putString("name", drugsBean.getNameEn());
            bundle.putString("country", drugsBean.getCountry().getNameEn());
            bundle.putString("city", drugsBean.getCity().getNameEn());
            bundle.putString("address", drugsBean.getAddressEn());
        }
        bundle.putString("link", drugsBean.getWeb());
        bundle.putString("phone", drugsBean.getStaticPhone());
        bundle.putString("mobile", drugsBean.getPhone());
        bundle.putFloat("rate", drugsBean.getCountRate());
        bundle.putString("lat", drugsBean.getLat());
        bundle.putString("longX", drugsBean.getLong());

        bundle.putStringArrayList("commentsContent", getCommentsContent(drugsBean.getComments()));
        bundle.putStringArrayList("commentsRate", getCommentsRate(drugsBean.getComments()));
        bundle.putStringArrayList("cmntsUserNames", getCommentsUserNames(drugsBean.getComments()));
        bundle.putStringArrayList("cmntsUserImg", getCommentsUserImgs(drugsBean.getComments()));

        fragment.setArguments(bundle);
        moveToFragment.moveInMain(fragment);
    }

    private ArrayList<String> getCommentsUserImgs(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getAvatar());
        }
        return sList;
    }

    private ArrayList<String> getCommentsUserNames(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getName());
        }
        return sList;
    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }


}
