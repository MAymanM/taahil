package com.tkmsoft.taahel.fragments.main.activities;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.SubActivityAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.activities.view.products.ActivityProductsModel;
import com.tkmsoft.taahel.model.api.activities.view.products.Comment;
import com.tkmsoft.taahel.model.api.activities.view.products.Datum;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */

public class SubActivityFragment extends Fragment implements SubActivityAdapter.ListAllListeners {
    private MainViewsCallBack mainViewsCallBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.notFoundTV)
    TextView notFoundTV;

    ListSharedPreference listSharedPreference;

    private int currentPage;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLink;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    SubActivityAdapter subActivityAdapter;
    String cityCode = "null", countryCode = "null", rate = "null";
    boolean isFilter;
    MoveToFragment moveToFragment;

    public SubActivityFragment() {

    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        fireBackButtonEvent();
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cityCode = getArguments().getString("city");
            countryCode = getArguments().getString("country");
            rate = getArguments().getString("rate");
            isFilter = getArguments().getBoolean("isFilter");
        } else
            isFilter = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sub_activity, container, false);
        ButterKnife.bind(this, rootView);
        currentPage = 1;
        apiLink = MyRetrofitClient.getBaseActivity().create(ApiLink.class);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initPullRefreshLayout();
        initAdapter();
        initRecyclerView();
        setupFilterButton();
        loadFirstPage();
    }


    private void setupFilterButton() {
        ImageButton filterBtn = getActivity().findViewById(R.id.toolbar_filter_button);
        filterBtn.setOnClickListener(view -> getFragmentManager().beginTransaction()
                .replace(R.id.main_frameLayout, new FilterActivityFrag()).commit());
    }

    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(subActivityAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void initAdapter() {
        subActivityAdapter = new SubActivityAdapter(new ArrayList<>(), this);
    }

    private ArrayList<String> getCommentsContentList(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getContent());
        }
        return sList;
    }

    private ArrayList<String> getCommentsRateList(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getRate());
        }
        return sList;
    }

    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);
        Log.d(TAG, "loadFirstPage: ");
        // To ensure list is visible when retry button in error view is clicked

        callGetActivityProducts().enqueue(new Callback<ActivityProductsModel>() {
            @Override
            public void onResponse(@NonNull Call<ActivityProductsModel> call, @NonNull Response<ActivityProductsModel> response) {
                // Got data. Send it to adapter
                assert response.body() != null;
                if (response.body().getStatus() != null) {
                    if (response.body().getStatus().getType().equals("success")) {
                        if (response.body().getData() != null) {
                            List<Datum> results = response.body().getData().getProducts().getData();
                            if (isFilter) {
                                attemptFilter(results, 1);
                            } else {
                                subActivityAdapter.replaceData(results);
                            }
                        }
                    } else {
                        isLastPage = true;
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ActivityProductsModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void attemptFilter(List<Datum> results, int pos) {
        int rateInt;
        if (!rate.equals("null"))
            rateInt = Integer.valueOf(rate);
        else
            rateInt = 0;
        List<Datum> newArray = new ArrayList<>();
        for (int i = 0; i < results.size(); i++) {
            String resCountryCode = String.valueOf(results.get(i).getCountry().getId());
            String resCityCode = String.valueOf(results.get(i).getCity().getId());
            int resRate = results.get(i).getCountRate();

            if (!countryCode.equals("null")) {
                if (!cityCode.equals("null")) {
                    if (!rate.equals("null")) {
                        if (resCityCode.equals(cityCode) && resCountryCode.equals(countryCode) && resRate >= rateInt) {
                            newArray.add(results.get(i));
                        }
                    } else if (resCountryCode.equals(countryCode) && resCityCode.equals(cityCode)) {
                        newArray.add(results.get(i));
                    }
                }//end city
                else if (!rate.equals("null")) {
                    if (resCountryCode.equals(countryCode) && resRate >= rateInt) {
                        newArray.add(results.get(i));
                    }
                } else {
                    if (resCountryCode.equals(countryCode)) {
                        newArray.add(results.get(i));
                    }
                }
            }//end country
            else if (!rate.equals("null")) {
                if (resRate >= rateInt) {
                    newArray.add(results.get(i));
                }
            }
        }//end for
        if (pos == 1)
            subActivityAdapter.replaceData(newArray);
        else
            subActivityAdapter.updateData(newArray);
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);
        callGetActivityProducts().enqueue(new Callback<ActivityProductsModel>() {
            @Override
            public void onResponse(@NonNull Call<ActivityProductsModel> call, @NonNull Response<ActivityProductsModel> response) {

                assert response.body() != null;
                if (response.body().getStatus().getType().equals("success")) {
                    if (response.body().getData() != null) {
                        List<Datum> results = response.body().getData().getProducts().getData();
                        if (isFilter) {
                            attemptFilter(results, 2);
                        } else {
                            subActivityAdapter.updateData(results);
                        }
                    }
                } else
                    isLastPage = true;
            }

            @Override
            public void onFailure(@NonNull Call<ActivityProductsModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(mContext, "" + fetchErrorMessage(t), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String fetchErrorMessage(Throwable throwable) {

        String errorMsg = getResources().getString(R.string.error_msg_unknown);
        if (!connectionDetector.isConnectingToInternet()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    private Call<ActivityProductsModel> callGetActivityProducts() {
        return apiLink.getActivityProducts(String.valueOf(listSharedPreference.getActivityTypeID()), currentPage);
    }


    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefreshLayout.setOnRefreshListener(() -> loadFirstPage());
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showFilterBtn(true);
        mainViewsCallBack.serToolbarTitle(listSharedPreference.getActivityName());
        mainViewsCallBack.showAddFab(false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }


    private void fireBackButtonEvent() {

        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {

                moveToFragment.moveInMain(new ActivitiesFragment());

            }
        });
    }//end back pressed

    @Override
    public void onItemClick(Datum data, int pos) {
        Fragment fragment = new ActivityDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("pId", String.valueOf(data.getId()));
        bundle.putString("image", data.getPhoto());
        bundle.putString("name", data.getNameAr());
        bundle.putString("type", data.getCategory().getNameAr());
        bundle.putString("address", data.getAddressAr());
        bundle.putString("country", data.getCity().getNameAr());
        bundle.putString("city", data.getCity().getNameAr());
        bundle.putString("link", data.getLink());
        bundle.putString("phone", data.getStaticPhone());
        bundle.putString("mobile", data.getPhone());
        bundle.putString("date", data.getStartDate());
        bundle.putString("rate", String.valueOf(data.getCountRate()));
        bundle.putString("duration", data.getDuration());
        bundle.putString("lat", data.getLat());
        bundle.putString("longX", data.getLong());
        bundle.putStringArrayList("commentsContent", getCommentsContentList(data.getComments()));
        bundle.putStringArrayList("commentsRate", getCommentsRateList(data.getComments()));
        bundle.putStringArrayList("cmntsUserNames", getCommentsUserNames(data.getComments()));
        bundle.putStringArrayList("cmntsUserImg", getCommentsUserImgs(data.getComments()));

        fragment.setArguments(bundle);
        moveToFragment.moveInMain(fragment);
    }

    private ArrayList<String> getCommentsUserImgs(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getAvatar());
        }
        return sList;
    }

    private ArrayList<String> getCommentsUserNames(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getName());
        }
        return sList;
    }
}
