package com.tkmsoft.taahel.fragments.main.education;


import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FilterEducationFragment extends Fragment {

    MainViewsCallBack mMainViewsCallBack;
    ListSharedPreference listSharedPreference;

    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindViews({R.id.spinner_country, R.id.spinner_city})
    List<Spinner> spinnersList;
    Spinner spinner_country, spinner_city;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;

    private ArrayList<String> mCountriesArrayList, mCitiesArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList;
    private String countryCode = "null", cityCode = "null";

    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private String rate = "null";
    private String categCode = "null";
    private MoveToFragment moveToFragment;
    private ApiLink apiBase;

    public FilterEducationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity)
            mContext = (FragmentActivity) activity;
        super.onAttach(activity);
        try {
            mMainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categCode = getArguments().getString("numb");
        }

        apiBase = MyRetrofitClient.getBase().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_filter_centers, container, false);
        listSharedPreference = new ListSharedPreference(FilterEducationFragment.this.getActivity().getApplicationContext());
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View view) {
        spinner_country = spinnersList.get(0);
        spinner_city = spinnersList.get(1);
        setSpinners();

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            rate = String.valueOf(Math.round(rating));
            if (rate.equals("0"))
                rate = "null";
        });
    }

    @OnClick(R.id.updateBtn)
    protected void onUpdateClick() {

        if (countryCode.equals("null") && cityCode.equals("null") && rate.equals("null"))
            mMainViewsCallBack.filterEdu(countryCode, cityCode, rate, categCode, false);
        else
            mMainViewsCallBack.filterEdu(countryCode, cityCode, rate, categCode, true);

        Log.d(TAG, "onUpdateClick: " + "country: " + countryCode + "city: " + cityCode + "rate: " + rate + "categCode: " + categCode);
    }

    private void setSpinners() {
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        initCountrySpinner();

        serverCountrySpinner();
    }

    private void serverCountrySpinner() {

        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                            mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                            mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                        }
                        initCountrySpinner();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private Call<CountriesModel> callGetCountry() {
        return apiBase.getCountries();
    }

    private void initCountrySpinner() {
        try {
            spinner_country.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                    PorterDuff.Mode.SRC_ATOP);
            DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCountriesArrayList);
            spinner_country.setAdapter(adapter);
            spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (i > 0) {
                        countryCode = String.valueOf(mCountriesIdList.get(i));
                        cityRelative.setVisibility(View.VISIBLE);
                        serverCitiesSpinner(i);
                    } else {
                        countryCode = "null";
                        cityRelative.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }catch (Exception ignore){}
    }

    private void serverCitiesSpinner(final int countryId) {
        initCitiesSpinner();
        spinner_progressBar.setVisibility(View.VISIBLE);
        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                spinner_progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                            }
                        }
                    } catch (Exception exc) {
//                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void initCitiesSpinner() {
        mCitiesArrayList = new ArrayList<>();
        mCitiesIdList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList.add(0, 0);

        spinner_city.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCitiesArrayList);
        spinner_city.setAdapter(adapter);
        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    cityCode = String.valueOf(mCitiesIdList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onDetach() {
        AndroidNetworking.cancelAll();
        super.onDetach();
    }

    @Override
    public void onPause() {
        callGetCountry().cancel();
        super.onPause();
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    if (categCode.equals("1")) {
//                        moveToFragment.moveInMain(new EduGovernmentFragment());
                        Toast.makeText(mContext, "xc", Toast.LENGTH_SHORT).show();
                    } else
                        moveToFragment.moveInMain(new EduPrivateFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

}
