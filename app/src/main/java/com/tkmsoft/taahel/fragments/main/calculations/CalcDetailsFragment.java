package com.tkmsoft.taahel.fragments.main.calculations;


import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindViews;
import butterknife.ButterKnife;
/**
 * A simple {@link Fragment} subclass.
 */
public class CalcDetailsFragment extends Fragment {

    private FragmentActivity mContext;
    private MainViewsCallBack mainViewsCallBack;

    private MoveToFragment moveToFragment;
    private String code;
    private String pName;
    private String date;
    private String day;
    private String time;
    private String status;
    private String address;
    private String paymentStatus;
    private String drCalc;
    private String taahelCalc;

    @BindViews({R.id.codeTV, R.id.dateTV, R.id.nameTV, R.id.addressTV,R.id.dayTV,R.id.timeTV,R.id.descriptionTV,R.id.paymentTV,
            R.id.drCalcsTV,R.id.taahelCalcsTV})
    List<TextView> textViewList;
    TextView codeTV,dateTV,nameTV, addressTV,dayTV,timeTV, paymentTV, descriptionTV,drCalcsTV,taahelCalcsTV;


    public CalcDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
        super.onAttach(context);
        try {
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        ConnectionDetector connectionDetector = new ConnectionDetector(context);
        ListSharedPreference listSharedPreference = new ListSharedPreference(context.getApplicationContext());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            code = bundle.getString("code");
            pName = bundle.getString("pName");
            date = bundle.getString("date");

            day = bundle.getString("day");
            time = bundle.getString("time");
            status = bundle.getString("status");

            address = bundle.getString("address");
            paymentStatus = bundle.getString("paymentStatus");
            String payPrice = bundle.getString("payPrice");
            drCalc = bundle.getString("drCalc");
            taahelCalc = bundle.getString("taahelCalc");
        }
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_calc_details, container, false);
        ButterKnife.bind(this,rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        codeTV = textViewList.get(0);
        dateTV = textViewList.get(1);
        nameTV = textViewList.get(2);
        addressTV = textViewList.get(3);
        dayTV = textViewList.get(4);
        timeTV = textViewList.get(5);
        descriptionTV = textViewList.get(6);
        paymentTV = textViewList.get(7);
        drCalcsTV = textViewList.get(8);
        taahelCalcsTV = textViewList.get(9);

    }

    @Override
    public void onStart() {
        codeTV.setText(code);
        dateTV.setText(date);
        nameTV.setText(pName);
        addressTV.setText(address);
        dayTV.setText(day);
        timeTV.setText(time);
        descriptionTV.setText(status);
        paymentTV.setText(paymentStatus);
        drCalcsTV.setText(drCalc);
        taahelCalcsTV.setText(taahelCalc);

        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach()
    {
        mainViewsCallBack = null;
        super.onDetach();
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) Objects.requireNonNull(getActivity())).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new HomeFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

}
