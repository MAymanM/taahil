package com.tkmsoft.taahel.fragments.main.jobOrders;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.jobs.JobOrdersAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.joborders.Data;
import com.tkmsoft.taahel.model.api.joborders.Datum;
import com.tkmsoft.taahel.model.api.joborders.JobOrdersModel;
import com.tkmsoft.taahel.model.api.joborders.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobOrdersFragment extends Fragment implements JobOrdersAdapter.ListAllClickListeners {
    private MainViewsCallBack mainViewsCallBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ListSharedPreference listSharedPreference;

    private int currentPage;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLink;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    JobOrdersAdapter subActivityAdapter;
    String cityCode = "null", countryCode = "null", rate = "null";
    boolean isFilter;
    private MoveToFragment moveToFragment;

    public JobOrdersFragment() {

    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        fireBackButtonEvent();
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            cityCode = getArguments().getString("city");
//            countryCode = getArguments().getString("country");
//            rate = getArguments().getString("rate");
//            isFilter = getArguments().getBoolean("isFilter");
//        } else
//            isFilter = false;
        apiLink = MyRetrofitClient.getJobs().create(ApiLink.class);
        currentPage = 1;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_job_orders, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initPullRefreshLayout();
        initRecyclerView();
        setupAddButton();
    }


    private void initRecyclerView() {
        subActivityAdapter = new JobOrdersAdapter(new ArrayList<>(), this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(subActivityAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);

        callJobOrdersModel().enqueue(new Callback<JobOrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<JobOrdersModel> call, @NonNull Response<JobOrdersModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> results = data.getData();
                                if (results != null) {
                                    subActivityAdapter.replaceData(results);
                                }
                            }
                        } else {
                            isLastPage = true;
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<JobOrdersModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void loadNextPage() {
        callJobOrdersModel().enqueue(new Callback<JobOrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<JobOrdersModel> call, @NonNull Response<JobOrdersModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> results = data.getData();
                                if (results != null) {
                                    subActivityAdapter.updateData(results);
                                }
                            }
                        } else {
                            isLastPage = true;
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JobOrdersModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private Call<JobOrdersModel> callJobOrdersModel() {
        return apiLink.getJobOrders(currentPage);
    }


    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefreshLayout.setOnRefreshListener(() -> moveToFragment.moveInMain(new JobOrdersFragment()));
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.job_order));
        mainViewsCallBack.showAddFab(true);
        loadFirstPage();
    }

    private void setupAddButton() {
        FloatingActionButton fab = getActivity().findViewById(R.id.main_fab);
        fab.setOnClickListener(view -> moveToFragment.moveInMain(new AddJobOrderFragment()));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
            }
        });
    }//end back pressed


    private String getLang() {
        return listSharedPreference.getLanguage();
    }

    @Override
    public void onItemClick(Datum data, int pos) {
        Fragment fragment = new JobOrderDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("id", String.valueOf(data.getId()));
        bundle.putString("image", data.getPhoto());
        bundle.putString("name", data.getName());
        bundle.putString("cv", data.getCv());
        bundle.putString("email", data.getEmail());
        bundle.putString("phone", data.getPhone());
        bundle.putString("desc", data.getDesc());

        if (getLang().equals("ar")) {
            bundle.putString("country", data.getCountry().getNameAr());
            bundle.putString("city", data.getCity().getNameAr());
            bundle.putString("disability", data.getDisability().getNameAr());
            bundle.putString("field", data.getField().getNameAr());
        }

        else {
            bundle.putString("country", data.getCountry().getNameEn());
            bundle.putString("city", data.getCity().getNameEn());
            bundle.putString("disability", data.getDisability().getNameEn());
            bundle.putString("field", data.getField().getNameEn());
        }

        fragment.setArguments(bundle);
        moveToFragment.moveInMain(fragment);
    }
}