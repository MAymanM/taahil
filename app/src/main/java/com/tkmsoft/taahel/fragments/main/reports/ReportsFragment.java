package com.tkmsoft.taahel.fragments.main.reports;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.reports.ReportsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.reports.Datum;
import com.tkmsoft.taahel.model.api.reports.ReportsModel;
import com.tkmsoft.taahel.model.api.reports.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportsFragment extends Fragment implements ReportsAdapter.ListAllListeners {
    ListSharedPreference listSharedPreference;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    private MainViewsCallBack mMainViewsCallBack;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    List<Datum> mDataList;
    private ApiLink retrofit;
    MoveToFragment moveToFragment;

    public ReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
            fireBackButtonEvent();
        }
        super.onAttach(activity);
        try {
            mMainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        retrofit = MyRetrofitClient.getProfile().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reprorts, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);

        return rootView;
    }

    private void initUI(View rootView) {
        progressBar.setVisibility(View.VISIBLE);
        initPullRefreshLayout();
        initRecyclerView();
        serverReport();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (listSharedPreference.getUType().equals("0"))
            mMainViewsCallBack.serToolbarTitle(getString(R.string.p_reports));
        else
            mMainViewsCallBack.serToolbarTitle(getString(R.string.my_reports));
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
    }

    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefreshLayout.setOnRefreshListener(() -> moveToFragment.moveInMain(new ReportsFragment()));
    }

    private void serverReport() {
        progressBar.setVisibility(View.VISIBLE);


        callGetReports().enqueue(new Callback<ReportsModel>() {
            @Override
            public void onResponse(@NonNull Call<ReportsModel> call, @NonNull Response<ReportsModel> response) {
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            List<Datum> datum = response.body().getData();
                            if (datum != null) {
                                mDataList = new ArrayList<>();
                                mDataList = response.body().getData();
                                initAdapter();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReportsModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
                // Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end server

    private Call<ReportsModel> callGetReports() {
        return retrofit.getReports(listSharedPreference.getToken());
    }

    private void initAdapter() {
        ReportsAdapter reportsAdapter = new ReportsAdapter(getActivity().getApplicationContext(), mDataList, this);

        recyclerView.setAdapter(reportsAdapter);
        reportsAdapter.notifyDataSetChanged();
    }


    @Override
    public void onPause() {
        callGetReports().cancel();
        super.onPause();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
            }
        });
    }//end back pressed

    @Override
    public void onItemViewClick(Datum reportsModel, int adapterPosition) {

    }

    @Override
    public void onDownloadClick(String repLink, int adapterPosition) {

    }


}
