package com.tkmsoft.taahel.fragments.main.orders.details.patient;

import android.app.Activity;
import android.app.AlertDialog;
import androidx.fragment.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.google.gson.reflect.TypeToken;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.main.orders.RequestsFragment;
import com.tkmsoft.taahel.fragments.main.orders.SubRequestFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.order.orderDetails.patient.PatientDeleteOrderModel;
import com.tkmsoft.taahel.network.Links;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;


/**
 * Created by MahmoudAyman on 13/09/2018.
 */
public class PinnedOrderPatientFragment extends Fragment {

    private MainViewsCallBack mainViewsCallBack;
    @BindView(R.id.codeTV)
    TextView codeTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.timeTV)
    TextView timeTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.paymentTV)
    TextView paymentTV;

    private String code, date, name, country, city, address, day, time, orderId, description,paymentStatus;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();

    public PinnedOrderPatientFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_pinned_order_patient, container, false);
        ButterKnife.bind(this, rootView);
        Bundle arguments = getArguments();

        code = arguments.getString("code");
        date = arguments.getString("date");
        name = arguments.getString("name");
        country = arguments.getString("country");
        city = arguments.getString("city");
        address = arguments.getString("address");
        day = arguments.getString("day");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < arguments.getInt("timeSize"); i++) {
            stringBuilder.append(getArguments().getString("time" + i));
            if (i < arguments.getInt("timeSize") - 1) {
                stringBuilder.append(" ,\n");
            }
        }
        time = stringBuilder.toString();
        orderId = arguments.getString("orderId");
        description = arguments.getString("desc");
        paymentStatus = arguments.getString("paymentStatus");
//        Toast.makeText(mContext, "" + name, Toast.LENGTH_SHORT).show();
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
//        nameTV.setPaintFlags(nameTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        nameTV.setText(name);
        codeTV.setText(code);
        dateTV.setText(date);
        countryTV.setText(country);
        cityTV.setText(city);
        addressTV.setText(address);
        dayTV.setText(day);
        timeTV.setText(time);
        descriptionTV.setText(description);
        switch (paymentStatus) {
            case "undefined":
                paymentTV.setText(getString(R.string.not_paid_yet));
                break;
            case "cash":
                paymentTV.setText(getString(R.string.cash));
                break;
            case "visa":
                paymentTV.setText(getString(R.string.visa));
                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.pinned_order));
    }

    @OnClick(R.id.deleteBtn)
    protected void onDeleteClick() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setMessage(getString(R.string.are_you_sure_delete));
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), (arg0, arg1) -> serverPatientDeleteOrder());

        alertDialogBuilder.setNegativeButton(getString(R.string.no), (dialog, which) -> dialog.dismiss());
        alertDialogBuilder.show();
    }

    private void serverPatientDeleteOrder() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            AndroidNetworking.get(Links.Order.deleteOrderPatient())
                    .addPathParameter("id", orderId) // posting java object
                    .setOkHttpClient(client)
                    .setTag("acceptOrderDr")
                    .build()
                    .getAsParsed(new TypeToken<PatientDeleteOrderModel>() {
                    }, new ParsedRequestListener<PatientDeleteOrderModel>() {
                        @Override
                        public void onResponse(PatientDeleteOrderModel response) {
                            try {
                                if (response.getStatus().getType().equals("success")) {

                                    Toast.makeText(mContext, "" + response.getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                    getFragmentManager().beginTransaction()
                                            .replace(R.id.main_frameLayout, new RequestsFragment())
                                            .commit();

                                } else
                                    Toast.makeText(mContext, "" + response.getStatus().getTitle(), Toast.LENGTH_SHORT).show();

                            } catch (Exception exc) {
                                Log.d(TAG, "onResponse: " + exc.getMessage());
                            }
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(ANError error) {
                            if (error.getErrorCode() != 0) {
                                Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                                Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            } else {
                                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            }
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception exc) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        fireBackButtonEvent();
        super.onAttach(context);
        try {
            context = getActivity();
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {

                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, new SubRequestFragment())
                        .commit();
            }
        });
    }//end back pressed

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }
}
