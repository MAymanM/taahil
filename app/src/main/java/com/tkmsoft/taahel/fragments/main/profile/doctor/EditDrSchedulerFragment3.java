package com.tkmsoft.taahel.fragments.main.profile.doctor;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.doctor.UpdateCalenderAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.TherapistScheduleModel;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.getCalender.Data;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.getCalender.GetCalenderTimesModel;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.getCalender.JobTime;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.getCalender.Status;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.updateCalender.UpdateCalenderModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditDrSchedulerFragment3 extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.submitBtn)
    Button submitBtn;
    ArrayList<TherapistScheduleModel> mDataList;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    List<String> timesList = new ArrayList<>();
    List<String> typeFreeList = new ArrayList<>();
    private int dayNum;
    ListSharedPreference listSharedPreference;
    private FragmentActivity mContext;

    private AlertDialog.Builder alertDialogBuilder;
    private String TAG = getClass().getSimpleName();
    private ApiLink apiLink;
    private ConnectionDetector connectionDetector;
    private MoveToFragment moveToFragment;

    public EditDrSchedulerFragment3() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        listSharedPreference = new ListSharedPreference(mContext);
        super.onAttach(activity);
        fireBackButtonEvent();
        connectionDetector = new ConnectionDetector(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.getProfile().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_dr_schedular3, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        listSharedPreference.setIsFreeChoosen(false);
        return rootView;
    }

    private void initUI(View rootView) {
        String dayName = getArguments().getString("day");
        dayNum = getArguments().getInt("dayNum");
        dayTV.setText(dayName);
        initRecyclerView();
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }
    }

    private Call<GetCalenderTimesModel> callGetCalender() {
        return apiLink.getCalenderTimes(getToken(), String.valueOf(dayNum));
    }

    private String getToken() {
        return listSharedPreference.getToken();
    }

    @Override
    public void onStart() {
        if (connectionDetector.isConnectingToInternet()) {
            deleteCheckBoxes();
            serverGetCalenderInfo();
            initAdapter();
        } else
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        super.onStart();
    }

    private void serverGetCalenderInfo() {

        progressBar.setVisibility(View.VISIBLE);

        callGetCalender().enqueue(new Callback<GetCalenderTimesModel>() {
            @Override
            public void onResponse(@NonNull Call<GetCalenderTimesModel> call, @NonNull Response<GetCalenderTimesModel> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<JobTime> jobTimeList = data.getJobTimes();
                                if (jobTimeList != null) {
                                    for (int i = 0; i < jobTimeList.size(); i++) {
                                        listSharedPreference.setCheckedBox(String.valueOf(dayNum),
                                                getCheckBoxName(data.getJobTimes().get(i).getTime()),
                                                getCheckBoxName(data.getJobTimes().get(i).getTime()));

                                        if (jobTimeList.get(i).getFree().equals("1")) {
                                            listSharedPreference.setRadioFree(String.valueOf(dayNum),
                                                    getCheckBoxName(jobTimeList.get(i).getTime()),
                                                    true);
                                        } else
                                            listSharedPreference.setRadioFree(String.valueOf(dayNum),
                                                    getCheckBoxName(jobTimeList.get(i).getTime()),
                                                    false);

                                        timesList.add(jobTimeList.get(i).getTime());
                                        typeFreeList.add(jobTimeList.get(i).getFree());

                                    }//end for

                                    initAdapter();

                                }
                                Log.d(TAG, "onResponse: time: " + timesList + "\n types: " + typeFreeList);
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<GetCalenderTimesModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    private String getCheckBoxName(String time) {
        switch (time) {
            case "0":
                return getString(R.string.am12_1);
            case "1":
                return getString(R.string.am1_2);
            case "2":
                return getString(R.string.am2_3);
            case "3":
                return getString(R.string.am3_4);
            case "4":
                return getString(R.string.am4_5);
            case "5":
                return getString(R.string.am5_6);
            case "6":
                return getString(R.string.am6_7);
            case "7":
                return getString(R.string.am7_8);
            case "8":
                return getString(R.string.am8_9);
            case "9":
                return getString(R.string.am9_10);
            case "10":
                return getString(R.string.am10_11);
            case "11":
                return getString(R.string.am11_12);
            case "12":
                return getString(R.string.pm12_1);
            case "13":
                return getString(R.string.pm1_2);
            case "14":
                return getString(R.string.pm2_3);
            case "15":
                return getString(R.string.pm3_4);
            case "16":
                return getString(R.string.pm4_5);
            case "17":
                return getString(R.string.pm5_6);
            case "18":
                return getString(R.string.pm6_7);
            case "19":
                return getString(R.string.pm7_8);
            case "20":
                return getString(R.string.pm8_9);
            case "21":
                return getString(R.string.pm9_10);
            case "22":
                return getString(R.string.pm10_11);
            case "23":
                return getString(R.string.pm11_12);
            default:
                return null;
        }
    }

    private void initAdapter() {
        mDataList = new ArrayList<>();
        final String[] time_am = getResources().getStringArray(R.array.dr_time_morning);
        String[] time_pm = getResources().getStringArray(R.array.dr_time_night);

        TherapistScheduleModel th;
        for (int i = 0; i < time_pm.length; i++) {
            th = new TherapistScheduleModel(time_pm[i], false, false, false);
            mDataList.add(i, th);
        }

        for (int i = 0; i < time_am.length; i++) {
            th = new TherapistScheduleModel(time_am[i], false, false, false);
            mDataList.add(i, th);
        }

        //                        if (!listSharedPreference.getCheckedBox(String.valueOf(dayNum),checkBoxName).equals(checkBoxName)) {
        //                            Toast.makeText(mContext, "" + timesList, Toast.LENGTH_SHORT).show();
        //                        }else
        //                            Toast.makeText(mContext, "else2", Toast.LENGTH_SHORT).show();
        //                        if (!listSharedPreference.getCheckedBox(String.valueOf(dayNum),checkBoxName).equals(checkBoxName)) {
        //                            Toast.makeText(mContext, "" + typeFreeList, Toast.LENGTH_SHORT).show();
        //                        }else
        //                            Toast.makeText(mContext, "else3", Toast.LENGTH_SHORT).show();
        //                        if (!listSharedPreference.getCheckedBox(String.valueOf(dayNum),checkBoxName).equals(checkBoxName)) {
        //                            Toast.makeText(mContext, "" + typeFreeList, Toast.LENGTH_SHORT).show();
        //                        }else
        //                            Toast.makeText(mContext, "else4", Toast.LENGTH_SHORT).show();
        UpdateCalenderAdapter updateCalenderAdapter = new UpdateCalenderAdapter(mContext,
                String.valueOf(dayNum), mDataList,
                new UpdateCalenderAdapter.ClickListListeners() {
                    @Override
                    public void onItemCheck(String name, int position) {
                        timesList.add(String.valueOf(position));

                        int pos = timesList.indexOf(String.valueOf(position));
                        Log.d(TAG, "onItemCheck: " + listSharedPreference.getCheckedBox(String.valueOf(dayNum), name));
                        Log.d(TAG, "add time: " + position + "\nin pos: " + pos);
                        Log.d(TAG, "all timesList: " + timesList);
                        Log.d(TAG, "all types: " + typeFreeList);
                        Log.d(TAG, "------------------");
                    }

                    @Override
                    public void onItemUncheck(String checkBoxName, int position) {
//                        if (!listSharedPreference.getCheckedBox(String.valueOf(dayNum),checkBoxName).equals(checkBoxName)) {
                        int pos = timesList.indexOf(String.valueOf(position));
                        if (typeFreeList.size() == timesList.size())
                            typeFreeList.remove(pos);
                        timesList.remove(String.valueOf(position));
                        Log.d(TAG, "remove time: " + position + " in pos: " + pos);
                        Log.d(TAG, "remove typeFreeList in pos: " + pos);
                        Log.d(TAG, "all timesList: " + timesList);
                        Log.d(TAG, "all types: " + typeFreeList);
                        Log.d(TAG, "------------------");

//                            Toast.makeText(mContext, "" + timesList, Toast.LENGTH_SHORT).show();
//                        }else
//                            Toast.makeText(mContext, "else2", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onFreeCheck(String checkBoxName, String name, int position) {
//                        if (!listSharedPreference.getCheckedBox(String.valueOf(dayNum),checkBoxName).equals(checkBoxName)) {

                        int pos = timesList.indexOf(String.valueOf(position));
                        if (typeFreeList.size() < timesList.size()) {
                            typeFreeList.add(pos, "1");
                        } else if ((typeFreeList.size() == timesList.size()))
                            typeFreeList.set(pos, "1");
                        else
                            Toast.makeText(mContext, "index error", Toast.LENGTH_SHORT).show();

                        Log.d(TAG, "set to free in pos: " + position + " that has time: " + position);
                        Log.d(TAG, "all timesList: " + timesList);
                        Log.d(TAG, "all types: " + typeFreeList);
                        Log.d(TAG, "------------------");

//                            Toast.makeText(mContext, "" + typeFreeList, Toast.LENGTH_SHORT).show();
//                        }else
//                            Toast.makeText(mContext, "else3", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onPaidCheck(String checkBoxName, String name, int position) {
//                        if (!listSharedPreference.getCheckedBox(String.valueOf(dayNum),checkBoxName).equals(checkBoxName)) {
                        int pos = timesList.indexOf(String.valueOf(position));
                        if (typeFreeList.size() < timesList.size())
                            typeFreeList.add(pos, "0");
                        else if (typeFreeList.size() == timesList.size())
                            typeFreeList.set(pos, "0");
                        else
                            Toast.makeText(mContext, "index error", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "set to paid in pos: " + position + " that has time: " + position);
                        Log.d(TAG, "all timesList: " + timesList);
                        Log.d(TAG, "all types: " + typeFreeList);
                        Log.d(TAG, "------------------");

//                            Toast.makeText(mContext, "" + typeFreeList, Toast.LENGTH_SHORT).show();
//                        }else
//                            Toast.makeText(mContext, "else4", Toast.LENGTH_SHORT).show();

                    }

                });
        recyclerView.setAdapter(updateCalenderAdapter);
        updateCalenderAdapter.notifyDataSetChanged();
    }


    private void deleteCheckBoxes() {
        for (int i = 0; i < 24; i++) {
            listSharedPreference.deleteCheckBox(
                    String.valueOf(dayNum),
                    getCheckBoxName(String.valueOf(i))
            );
        }
    }

    private void serverUpdateCalender() {

        progressBar.setVisibility(View.VISIBLE);

        ApiLink retrofit = MyRetrofitClient.getProfile().create(ApiLink.class);

        LinkedHashMap<String, String> times = new LinkedHashMap<>();
        for (int i = 0; i < timesList.size(); i++) {
            times.put("times[" + i + "]", timesList.get(i));
        }

        LinkedHashMap<String, String> types = new LinkedHashMap<>();
        for (int i = 0; i < typeFreeList.size(); i++) {
            times.put("type[" + i + "]", typeFreeList.get(i));
        }

        Call<UpdateCalenderModel> registerCall = retrofit.updateCalender(listSharedPreference.getToken(),
                String.valueOf(dayNum), times, types);
        registerCall.enqueue(new Callback<UpdateCalenderModel>() {
            @Override
            public void onResponse(@NonNull Call<UpdateCalenderModel> call, @NonNull Response<UpdateCalenderModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.profile.updatedoctor.updateCalender.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();

                            moveToFragment.moveInMain(new EditDrSchedulerFragment2());
                        } else {
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateCalenderModel> call, @NonNull Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    @OnClick(R.id.submitBtn)
    protected void onSubmitClick() {
        if (timesList.size() == 0)
            Toast.makeText(mContext, getString(R.string.plz_choose_time_first), Toast.LENGTH_SHORT).show();
        else {
            if (!hasManyFree())
                if (typeFreeList.size() == timesList.size())
                    serverUpdateCalender();
                else
                    Toast.makeText(mContext, R.string.complete_ur_dates, Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(mContext, "" + getString(R.string.cant_choose_more_than_one_free), Toast.LENGTH_SHORT).show();
            Log.d(TAG, "onSubmitClick:\n" + "isFree? " + hasManyFree() + "\ntype: " + typeFreeList + "\ntimes: " + timesList);
        }
    }


    private boolean hasManyFree() {
        int counter = 0;
        for (int i = 0; i < typeFreeList.size(); i++) {
            if (typeFreeList.get(i).equals("1")) {
                counter++;
            }
        }
        return counter > 1;
    }


    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;

                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, new EditDrSchedulerFragment2())
                        .commit();
            }
        });
    }//end back pressed

}