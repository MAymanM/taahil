package com.tkmsoft.taahel.fragments.main.adds.center;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.helper.InitSpinner;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.adds.edit.EditModel;
import com.tkmsoft.taahel.model.api.adds.edit.Status;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditCenterFragment extends Fragment {

    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.cityRelLayout)
    LinearLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.addBtn)
    Button addBtn;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.name_arET)
    EditText name_arET;
    @BindView(R.id.name_enET)
    EditText name_enET;
    @BindView(R.id.address_ar_ET)
    EditText address_ar_ET;
    @BindView(R.id.address_en_ET)
    EditText address_en_ET;
    @BindView(R.id.staticPhone_ET)
    EditText staticPhone_ET;
    @BindView(R.id.phoneET)
    EditText phoneET;
    @BindView(R.id.webET)
    EditText webET;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.governmentalRB)
    RadioButton governmentalRB;
    @BindView(R.id.privateRB)
    RadioButton privateRB;
    private View rootView;

    MainViewsCallBack mainViewsCallBack;
    ListSharedPreference listSharedPreference;
    private ArrayList<String> mCountriesArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList;
    private final int IMAGE_CODE = 2001;
    private String image_path = "";
    private String countryCode, cityCode;
    private FragmentActivity mContext;
    MoveToFragment moveToFragment;

    String pId;
    String image;
    String nameAr, nameEn;
    String addressAr, addressEn;
    String web;
    String lat;
    String longX;
    String phone;
    String staticPhone;
    Integer countryId, cityId, typeId;
    private InitSpinner initSpinner;
    private ConnectionDetector connectionManager;
    private ApiLink apiBase;
    private List<CountriesModel.DataBean.CountriesBean> dataCountryList;
    private String TAG = getClass().getSimpleName();

    boolean isInserted = false;

    public EditCenterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        initSpinner = new InitSpinner(mContext);
        connectionManager = new ConnectionDetector(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            pId = bundle.getString("pId");
            image = bundle.getString("image");
            nameAr = bundle.getString("nameAr");
            nameEn = bundle.getString("nameEn");
            typeId = bundle.getInt("typeId");
            addressAr = bundle.getString("addressAr");
            addressEn = bundle.getString("addressEn");
            countryId = bundle.getInt("countryId");
            cityId = bundle.getInt("cityId");
            web = bundle.getString("web");
            phone = bundle.getString("phone");
            staticPhone = bundle.getString("staticPhone");
            lat = bundle.getString("lat");
            longX = bundle.getString("longX");
        }
        apiBase = MyRetrofitClient.getBase().create(ApiLink.class);
    }//end onCreate

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_edit_center, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        setData();
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        initCountrySpinner();
    }

    private void setData() {
        Picasso.get().load(image).error(R.drawable.place_holder).into(imageView);
        name_arET.setText(nameAr);
        name_enET.setText(nameEn);
        address_ar_ET.setText(addressAr);
        address_en_ET.setText(addressEn);
        webET.setText(web);
        phoneET.setText(phone);
        staticPhone_ET.setText(staticPhone);

        if (typeId == 2)
            privateRB.setChecked(true);
        else
            governmentalRB.setChecked(true);

    }

    private Integer getTypeId() {
        int typeId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = rootView.findViewById(typeId);
        switch (radioButton.getId()) {
            case R.id.governmentalRB:
                return 1;
            case R.id.privateRB:
                return 2;
            default:
                return null;
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    private boolean isConnected() {
        return connectionManager.isConnectingToInternet();
    }

    ///////////////Spinners///////////////

    private Call<CountriesModel> callGetCountry() {
        return apiBase.getCountries();
    }

    private void serverCountrySpinner() {
//        spinner_progressBar.setVisibility(View.VISIBLE);

        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CountriesModel.StatusBean status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                CountriesModel.DataBean data = response.body().getData();
                                if (data != null) {
                                    dataCountryList = data.getCountries();
                                    if (dataCountryList != null && !dataCountryList.isEmpty()) {
                                        for (int i = 0; i < dataCountryList.size(); i++) {
                                            mCountriesArrayList.add(dataCountryList.get(i).getName_ar());
                                            mCountriesIdList.add(dataCountryList.get(i).getId());
                                        }//end for
                                        initCountrySpinner();
                                        setSpinnerValue(mCountriesIdList,countryId, spinner_country);
                                    }
                                }
                            }
                        }
                    }
                }
                spinner_progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end serverRegister()
    private void setSpinnerValue(ArrayList<Integer> integerArrayList, Integer id, Spinner spinner) {
        int j = 0;
        while (j < integerArrayList.size()) {
            if (integerArrayList.get(j).equals(id)) {
                spinner.setSelection(j);
                break;
            } else
                j++;
        }
    }
    private void initCountrySpinner() {


        initSpinner.setSpinner(spinner_country, mCountriesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(position));
                    showCitySpinner();
                    initCitySpinner();
                } else {
                    countryCode = null;
                    hideCitySpinner();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCitySpinner() {
        ArrayList<String> mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);


        for (int i = 0; i < dataCountryList.size(); i++) {
            if (countryCode.equals(String.valueOf(dataCountryList.get(i).getId()))) {
                for (int k = 0; k < dataCountryList.get(i).getCities().size(); k++) {
                    if (getLanguage().equals("ar"))
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_ar());
                    else
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_en());
                    mCitiesIdList.add(dataCountryList.get(i).getCities().get(k).getId());
                }
            }
        }

        initSpinner.setSpinner(spinner_city, mCitiesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    cityCode = String.valueOf(mCitiesIdList.get(position));
                } else {
                    cityCode = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (!isInserted) {
            int j = 0;
            while (j < mCitiesIdList.size()) {
                if (mCitiesIdList.get(j).equals(cityId)) {
                    spinner_city.setSelection(j);
                    isInserted = true;
                    Log.d(TAG, "onCitySelect: " + j);
                    break;
                } else
                    j++;
            }
        }

    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }

    private void hideCitySpinner() {
        cityRelative.setVisibility(View.GONE);
    }

    private void showCitySpinner() {
        cityRelative.setVisibility(View.VISIBLE);
    }

    /////////////////////end spinners////////////////

    private void serverEditCenter() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getDrug().create(ApiLink.class);
        File file1;
        file1 = new File(image_path);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);

        RequestBody idPart = RequestBody.create(MediaType.parse("text/plain"), pId);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", file1.getName(), mFile1);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getTypeId()));
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody address_arPart = RequestBody.create(MediaType.parse("text/plain"), address_ar_ET.getText().toString());
        RequestBody address_enPart = RequestBody.create(MediaType.parse("text/plain"), address_en_ET.getText().toString());
        RequestBody linkPart = RequestBody.create(MediaType.parse("text/plain"), webET.getText().toString());
        RequestBody static_phonePart = RequestBody.create(MediaType.parse("text/plain"), staticPhone_ET.getText().toString());
        RequestBody phonePart = RequestBody.create(MediaType.parse("text/plain"), phoneET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), "132155");
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), "1236985");

        final Call<EditModel> registerCall = retrofit.editCenter(idPart, photoPart, name_arPart, name_enPart, type_idPart,
                city_idPart, address_arPart, address_enPart,
                linkPart, static_phonePart, phonePart, latPart, longXPart);
        registerCall.enqueue(new Callback<EditModel>() {
            @Override
            public void onResponse(@NonNull Call<EditModel> call, @NonNull Response<EditModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            moveToFragment.moveInMain(new SubMyAddsCenterFragment());
                        } else
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<EditModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
//                Toast.makeText(mContext, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void serverEditCenterNonImage() {
        progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getDrug().create(ApiLink.class);

        String name_arPart = name_arET.getText().toString();
        String name_enPart = name_enET.getText().toString();
        String type_idPart = String.valueOf(getTypeId());
        String city_idPart = cityCode;
        String address_arPart = address_ar_ET.getText().toString();
        String address_enPart = address_en_ET.getText().toString();
        String linkPart = webET.getText().toString();
        String static_phonePart = staticPhone_ET.getText().toString();
        String phonePart = phoneET.getText().toString();
        String latPart = "132155";
        String longXPart = "1236985";

        Call<EditModel> callEdit = retrofit.editCenterNonImage(pId, name_arPart, name_enPart, type_idPart,
                city_idPart, address_arPart, address_enPart,
                linkPart, static_phonePart, phonePart, latPart, longXPart);
        callEdit.enqueue(new Callback<EditModel>() {
            @Override
            public void onResponse(@NonNull Call<EditModel> call, @NonNull Response<EditModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                                moveToFragment.moveInMain(new SubMyAddsCenterFragment());
                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<EditModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    ///////////////On Click///////////////
    @OnClick(R.id.imageView)
    protected void imageOnClick() {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("image/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_PICK);
        //starts new activity to select file and return data
        startActivityForResult(intent, IMAGE_CODE);
    }

    @OnClick(R.id.addBtn)
    protected void onNextBtnClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                addCenterNow();
            }
        } else {
            addCenterNow();
        }
    }

    private void addCenterNow() {
        if (countryCode != null) {
            if (cityCode != null) {
                if (!image_path.equals(""))
                    serverEditCenter();
                else
                    serverEditCenterNonImage();
            } else
                Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, getActivity());
            showImageInView(uri);
        }//end if check for data

    }//end onActivityResult

    private void showImageInView(Uri image_path) {
        Picasso.get().load(image_path).into(imageView);
    }

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public void onStart() {
        if (isConnected())
            serverCountrySpinner();
        else
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        callGetCountry().cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.edit));
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new SubMyAddsCenterFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

}
