package com.tkmsoft.taahel.fragments.main.store;

import android.os.Bundle;

import com.tkmsoft.taahel.adapters.store.StoreAdapter;
import com.tkmsoft.taahel.model.api.store.view.Datum;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MahmoudAyman on 23/02/2019.
 */
class AttemptStoreFilter {

    private Integer countryCode, cityCode, fieldCode;
    private Float rate;
    private StoreAdapter storeAdapter;

    AttemptStoreFilter(Bundle bundle) {
        countryCode = bundle.getInt("country");
        cityCode = bundle.getInt("city");
        fieldCode = bundle.getInt("field");
        rate = bundle.getFloat("rate");
    }

    void filter(List<Datum> results, int pos) {
        List<Datum> newArray = new ArrayList<>();

        for (int i = 0; i < results.size(); i++) {
            Integer resCountryCode = results.get(i).getCountry().getId();
            Integer resCityCode = results.get(i).getCity().getId();
            Integer resFieldCode = results.get(i).getCategory().getId();
            float resRate = Float.parseFloat(results.get(i).getCountRate());
            if (countryCode != -1) {
                if (cityCode != -1) {
                    if (fieldCode != -1) {
                        if (rate != 0f) {
                            if (resCountryCode.equals(countryCode) && resCityCode.equals(cityCode) &&
                                    resFieldCode.equals(fieldCode) && resRate >= rate) {
                                newArray.add(results.get(i));
                            }//end all
                        } else if (resCountryCode.equals(countryCode) && resCityCode.equals(cityCode) &&
                                resFieldCode.equals(fieldCode)) {
                            newArray.add(results.get(i));
                        }// C & c & f
                    } else if (resCountryCode.equals(countryCode) && resCityCode.equals(cityCode)) {
                        newArray.add(results.get(i));
                    }//C & c
                } else if (fieldCode != -1) {
                    if (rate != 0f) {
                        if (resCountryCode.equals(countryCode) && resFieldCode.equals(fieldCode) && resRate >= rate) {
                            newArray.add(results.get(i));
                        }// C & f & rate
                    } else if (resCountryCode.equals(countryCode) && resFieldCode.equals(fieldCode)) {
                        newArray.add(results.get(i));
                    }//C & f
                } else if (rate != 0f) {
                    if (resCountryCode.equals(countryCode) && resRate >= rate) {
                        newArray.add(results.get(i));
                    }//C && r
                } else if (resCountryCode.equals(countryCode)) {
                    newArray.add(results.get(i));
                }//end C
            } else if (fieldCode != -1) {
                if (rate != 0f) {
                    if (resFieldCode.equals(fieldCode) && resRate >= rate) {
                        newArray.add(results.get(i));
                    }//f & r
                } else if (resFieldCode.equals(fieldCode)) {
                    newArray.add(results.get(i));
                }//f
            } else if (rate != 0f) {
                if (resRate >= rate) {
                    newArray.add(results.get(i));
                }//r
            }
        }//end for
        if (pos == 1)
            storeAdapter.replaceData(newArray);
        else
            storeAdapter.updateData(newArray);
    }//end for

    public void setAdapter(StoreAdapter storeAdapter) {
        this.storeAdapter = storeAdapter;
    }
}
