package com.tkmsoft.taahel.fragments.main.calculations;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.calculations.CalcsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.order.drs.Data;
import com.tkmsoft.taahel.model.api.order.drs.DoctorOrdersModel;
import com.tkmsoft.taahel.model.api.order.drs.Order;
import com.tkmsoft.taahel.model.api.order.drs.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CalculationsFragment extends Fragment implements CalcsAdapter.ListAllListeners {


    private FragmentActivity mContext;
    private ListSharedPreference listSharedPreference;
    private MainViewsCallBack mainViewsCallBack;
    private MoveToFragment moveToFragment;
    private ApiLink apiLink;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private CalcsAdapter calcsAdapter;
    private String TAG = getClass().getSimpleName();
    private Integer totalDrCalc, totalTaahelCalc;
    @BindView(R.id.totalTaahilTV)
    TextView totalTaahilTV;
    @BindView(R.id.totalDrTV)
    TextView totalDrTV;


    public CalculationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
        super.onAttach(context);
        try {
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        ConnectionDetector connectionDetector = new ConnectionDetector(context);
        listSharedPreference = new ListSharedPreference(context.getApplicationContext());
        mainViewsCallBack.serToolbarTitle(getString(R.string.my_calculations));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.getOrder().create(ApiLink.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_calculations, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        ArrayList<Order> mDataList = new ArrayList<>();
        calcsAdapter = new CalcsAdapter(mDataList, this);
        initRecyclerView();
    }

    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(calcsAdapter);
    }

    private void serverGetOrders() {
        progressBar.setVisibility(View.VISIBLE);
        callGetOrders().enqueue(new Callback<DoctorOrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<DoctorOrdersModel> call, @NonNull Response<DoctorOrdersModel> response) {
                // Got data. Send it to adapter
                if (response.body() != null) {
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Order> orderList = data.getOrders();
                                if (orderList != null) {
                                    calcsAdapter.updateData(orderList);
                                    totalDrCalc = data.getCaculations().getDoctorFinance();
                                    totalTaahelCalc = data.getCaculations().getTaaheelFinance();

                                    totalTaahilTV.setText(String.valueOf(totalTaahelCalc));
                                    totalDrTV.setText(String.valueOf(totalDrCalc));
                                }
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DoctorOrdersModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private Call<DoctorOrdersModel> callGetOrders() {
        return apiLink.getDrOrders(listSharedPreference.getToken(),
                "3");
    }

    @Override
    public void onStart() {
        serverGetOrders();
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        callGetOrders().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        mainViewsCallBack = null;
        super.onDetach();
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) Objects.requireNonNull(getActivity())).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new HomeFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed


    @Override
    public void onItemViewClick(Order order, int adapterPosition) {
        Fragment fragment = new CalcDetailsFragment();
        Bundle bundle = new Bundle();

        bundle.putString("code", String.valueOf(order.getId()));
        bundle.putString("pName", order.getMember().getName());
        bundle.putString("date", order.getDate());
        switch (order.getOrderDay().getDay()) {
            case "0":
                bundle.putString("day", getString(R.string.sunday));
                break;
            case "1":
                bundle.putString("day", getString(R.string.monday));
                break;
            case "2":
                bundle.putString("day", getString(R.string.tuesday));
                break;
            case "3":
                bundle.putString("day", getString(R.string.wednesday));
                break;
            case "4":
                bundle.putString("day", getString(R.string.thursday));
                break;
            case "5":
                bundle.putString("day", getString(R.string.friday));
                break;
            case "6":
                bundle.putString("day", getString(R.string.saturday));
                break;
            default:
                bundle.putString("day", "null");
                break;
        }
        if (Integer.valueOf(order.getTimeSelected().get(0).getTime()) <= 12)
            bundle.putString("time", order.getTimeSelected().get(0).getTime() + getString(R.string.am));
        else
            bundle.putString("time", order.getTimeSelected().get(0).getTime() + getString(R.string.pm));
        bundle.putString("status", order.getDesc());
        bundle.putString("address", order.getAddress());
        String pay = "null";
        switch (order.getPaymentStatus()) {
            case "undefined":
                pay = getString(R.string.not_paid_yet);
                break;
            case "cash":
                pay = getString(R.string.cash);
                break;
            case "visa":
                pay = getString(R.string.visa);
                break;
        }
        bundle.putString("paymentStatus", pay);
        bundle.putString("payPrice", order.getPaymentTracker().getPrice());

        bundle.putString("drCalc", "0");
        bundle.putString("taahelCalc", String.valueOf((Integer.valueOf(order.getPaymentTracker().getPrice()) * 5) / 100));

        fragment.setArguments(bundle);
        moveToFragment.moveInMain(fragment);

    }
}