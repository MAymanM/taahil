package com.tkmsoft.taahel.fragments.main.orders.details.doctor;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.main.orders.SubRequestFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.interfaces.SendPatientProfData;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmedNonPaidDrFragment extends Fragment {

    private static final int LICENCE_CODE = 789654;
    @BindView(R.id.codeTV)
    TextView codeTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.timeTV)
    TextView timeTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;
    @BindView(R.id.paymentTV)
    TextView paymentTV;

    String code, date, name, country, city, address, day, time, orderId, paymentStatus;
    String pName, pGender, pBirthDate, pAvatar, pEmail, pPhone, pCountry, pCity;

    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private MainViewsCallBack mainViewsCallBack;

    ListSharedPreference listSharedPreference;
    private String desc;
    private SendPatientProfData sendPatientProfData;
    private String pId;
    private MoveToFragment moveToFragment;

    public ConfirmedNonPaidDrFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        fireBackButtonEvent();
        try {
            activity = getActivity();
            mainViewsCallBack = (MainViewsCallBack) activity;
            sendPatientProfData = (SendPatientProfData) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        code = arguments.getString("code");
        date = arguments.getString("date");
        name = arguments.getString("name");
        country = arguments.getString("country");
        city = arguments.getString("city");
        address = arguments.getString("address");
        day = arguments.getString("day");
        desc = arguments.getString("desc");

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < arguments.getInt("timeSize"); i++) {
            stringBuilder.append(getArguments().getString("time" + i));
            if (i < arguments.getInt("timeSize") - 1) {
                stringBuilder.append(" ,\n");
            }
        }

        time = stringBuilder.toString();
        orderId = arguments.getString("orderId");

        pGender = arguments.getString("pGender");
        pAvatar = arguments.getString("pAvatar");
        pBirthDate = arguments.getString("pBirthDate");
        pName = arguments.getString("pName");
        pEmail = arguments.getString("pEmail");
        pPhone = arguments.getString("pPhone");
        pCountry = arguments.getString("pCountry");
        pCity = arguments.getString("pCity");
        pId = arguments.getString("pId");
        paymentStatus = arguments.getString("paymentStatus");

    }

    @OnClick(R.id.nameTV)
    protected void onNameTVClick() {
        sendPatientProfData.sendData(pId, pGender, pAvatar, pBirthDate, pName, pEmail, pPhone, pCountry, pCity);
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.confirmed_order));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_confirmed_non_paid_dr, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        nameTV.setPaintFlags(nameTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        nameTV.setText(name);
        codeTV.setText(code);
        dateTV.setText(date);
        countryTV.setText(country);
        cityTV.setText(city);
        addressTV.setText(address);
        dayTV.setText(day);
        timeTV.setText(time);
        descriptionTV.setText(desc);

        switch (paymentStatus) {
            case "undefined":
                paymentTV.setText(getString(R.string.not_paid_yet));
                break;
            case "cash":
                paymentTV.setText(getString(R.string.cash));
                break;
            case "visa":
                paymentTV.setText(getString(R.string.visa));
                break;
        }

    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new SubRequestFragment());
            }
        });
    }//end back pressed

}
