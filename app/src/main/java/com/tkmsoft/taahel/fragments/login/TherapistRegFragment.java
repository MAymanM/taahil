package com.tkmsoft.taahel.fragments.login;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import androidx.fragment.app.Fragment;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.reflect.TypeToken;
import com.kishan.askpermission.AskPermission;
import com.kishan.askpermission.ErrorCallback;
import com.kishan.askpermission.PermissionCallback;
import com.kishan.askpermission.PermissionInterface;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.StringAdapter;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.api.auth.register.doctor.CompleteRegDoc1;
import com.tkmsoft.taahel.model.api.auth.register.doctor.Data;
import com.tkmsoft.taahel.model.api.auth.register.doctor.Doctor;
import com.tkmsoft.taahel.model.api.auth.register.doctor.Status;
import com.tkmsoft.taahel.model.api.auth.register.doctor.Title;
import com.tkmsoft.taahel.model.api.specialization.Specialization;
import com.tkmsoft.taahel.model.api.specialization.SpecializationsModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.model.spinners.CurrencyModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.Links;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class TherapistRegFragment extends Fragment implements PermissionCallback, ErrorCallback {

    private static final int LICENCE_CODE = 777;

    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.radioGroup)
    RadioGroup radioGender;
    @BindView(R.id.emailET)
    EditText emailET;
    @BindView(R.id.priceET)
    EditText priceET;
    @BindView(R.id.spinner_currency)
    Spinner spinner_currency;
    @BindView(R.id.expYearsET)
    EditText expYearsET;
    @BindView(R.id.educationET)
    EditText educationET;
    @BindView(R.id.aboutET)
    EditText aboutET;
    @BindView(R.id.medical_licenseTV)
    TextView medical_licenseET;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.deptTV)
    TextView deptTv;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.next_btn)
    Button next_btn;
    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;

    private ListSharedPreference listSharedPreference;
    private String image_path = "";
    private String countryCode, cityCode, currencyCode;
    private ArrayList<String> mCitiesArrayList, mCountriesArrayList, mCurrencyArrayList;
    List<String> specialities;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    private View rootView;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mCurrencyIdList;
    private File file;

    GridLayoutManager gridLayoutManager;
    MoveToFragment moveToFragment;

    public TherapistRegFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        moveToFragment = new MoveToFragment(mContext);

        listSharedPreference = new ListSharedPreference(mContext.getApplicationContext());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_therapist_register, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        fireBackButtonEvent();
        checkPermissions();
        return rootView;
    }

    private void checkPermissions() {
        int REQUEST_PERMISSIONS = 344;
        new AskPermission.Builder(this).setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                .setCallback(this)
                .setErrorCallback(this)
                .request(REQUEST_PERMISSIONS);
    }

    private void initUI(View rootView) {
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        serverCountrySpinner();
        mCurrencyArrayList = new ArrayList<>();
        mCurrencyIdList = new ArrayList<>();
        mCurrencyArrayList.add(0, getString(R.string.currency));
        mCurrencyIdList.add(0, 0);
        serverCurrencySpinner();
        initCurrencySpinner();
        initRecyclerView();
    }


    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        serverSpecialization();
    }

    private void serverSpecialization() {
        try {
            progressBar.setVisibility(View.VISIBLE);

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            int pageNum = 1;
            AndroidNetworking.get(Links.Base.getSpecialization())
                    .addPathParameter("page", String.valueOf(pageNum))
                    .setTag("getSpecialization")
                    .setOkHttpClient(client)
                    .build()
                    .getAsParsed(new TypeToken<SpecializationsModel>() {
                    }, new ParsedRequestListener<SpecializationsModel>() {
                        @Override
                        public void onResponse(SpecializationsModel response) {
                            try {
                                //Toast.makeText(mContext, "" + response.getData().getSpecializations().size(), Toast.LENGTH_SHORT).show();

                                initAdapter(response.getData().getSpecializations());
                            } catch (Exception exc) {
                                Toast.makeText(mContext, "" + exc, Toast.LENGTH_SHORT).show();
                            }
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(ANError error) {
                            if (error.getErrorCode() != 0) {
                                Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                                Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            } else {
                                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            }
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception exc) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(mContext, "failed, try to use another number", Toast.LENGTH_SHORT).show();
        }
    }

    private void initAdapter(final List<Specialization> specializations) {
        specialities = new ArrayList<>();
        StringAdapter stringAdapter = new StringAdapter(getActivity().getApplicationContext(), specializations,
                new StringAdapter.ListAllListeners() {
                    @Override
                    public void onCheck(Specialization stringModel, int position) {
                        if (!specialities.contains(String.valueOf(stringModel.getId())))
                            specialities.add(String.valueOf(stringModel.getId()));
                        Log.d(TAG, " " + specialities);
                    }

                    @Override
                    public void onUnCheck(Specialization stringModel, int position) {
                        if (specialities.contains(String.valueOf(stringModel.getId())))
                            specialities.remove(String.valueOf(stringModel.getId()));
                        Log.d(TAG, " " + specialities);
                    }
                });
//        stringAdapter.updateData(specializations);
        recyclerView.setAdapter(stringAdapter);
        stringAdapter.notifyDataSetChanged();
    }

    private void initCountrySpinner() {
        spinner_country.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCountriesArrayList);
        spinner_country.setAdapter(adapter);
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(i));
                    cityRelative.setVisibility(View.VISIBLE);
                    serverCitiesSpinner(i);
                } else
                    cityRelative.setVisibility(View.GONE);
                //Toast.makeText(mContext, "" + countryCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinner_country);
            }
        });
    }

    private void initCitiesSpinner() {
        mCitiesArrayList = new ArrayList<>();
        mCitiesIdList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList.add(0, 0);

        spinner_city.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCitiesArrayList);
        spinner_city.setAdapter(adapter);
        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cityCode = String.valueOf(mCitiesIdList.get(i));
                //Toast.makeText(mContext, "" + cityCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initCurrencySpinner() {

        spinner_currency.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCurrencyArrayList);
        spinner_currency.setAdapter(adapter);
        spinner_currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    currencyCode = String.valueOf(mCurrencyIdList.get(i));
                else
                    currencyCode = null;
                // Toast.makeText(mContext, "" + currencyCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void serverCurrencySpinner() {
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CurrencyModel> currencyCall = retrofit.getCurrency();
        currencyCall.enqueue(new Callback<CurrencyModel>() {
            @Override
            public void onResponse(@NonNull Call<CurrencyModel> call, @NonNull Response<CurrencyModel> response) {
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCurrencies().size(); i++) {
                                mCurrencyArrayList.add(response.body().getData().getCurrencies().get(i).getName_ar());
                                mCurrencyIdList.add(response.body().getData().getCurrencies().get(i).getId());
                            }
                        }
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CurrencyModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCurrencySpinner()

    private void serverCitiesSpinner(final int countryId) {
        initCitiesSpinner();
        spinner_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> citiesCall = retrofit.getCountries();
        citiesCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                spinner_progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                            mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                            mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void serverCountrySpinner() {

        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> registerCall = retrofit.getCountries();
        registerCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                            mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                            mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                        }
                        initCountrySpinner();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverRegister()

    @OnClick(R.id.next_btn)
    protected void onNextBtnClick() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                if (!image_path.equals("")) {
                    if (specialities.size() > 0) {
                        if (isEmptyEditTxt(priceET)) {
                            if (currencyCode!= null) {
                                if (isEmptyEditTxt(educationET)) {
                                    if (isEmptyEditTxt(expYearsET)) {
                                        if (isEmptyEditTxt(aboutET)) {
                                            serverTherapist();
                                        }else
                                            Toast.makeText(mContext, ""+getString(R.string.empty_about), Toast.LENGTH_SHORT).show();
                                    }else
                                        Toast.makeText(mContext, ""+getString(R.string.empty_years), Toast.LENGTH_SHORT).show();
                                }else
                                    Toast.makeText(mContext, ""+getString(R.string.empty_education), Toast.LENGTH_SHORT).show();
                            }else
                                Toast.makeText(mContext, ""+getString(R.string.empty_currency), Toast.LENGTH_SHORT).show();

                        } else
                            Toast.makeText(mContext, "" + getString(R.string.empty_price), Toast.LENGTH_SHORT).show();

                    } else
                        Toast.makeText(mContext, "" + getString(R.string.empty_specialist), Toast.LENGTH_SHORT).show();
                } else
                    serverTherapistNullLicense();
            }
        } else {
            if (!image_path.equals(""))
                if (specialities.size() > 0) {
                    if (isEmptyEditTxt(priceET)) {
                        if (currencyCode!= null) {
                            if (isEmptyEditTxt(educationET)) {
                                if (isEmptyEditTxt(expYearsET)) {
                                    if (isEmptyEditTxt(aboutET)) {
                                        serverTherapist();
                                    }else
                                        Toast.makeText(mContext, ""+getString(R.string.empty_about), Toast.LENGTH_SHORT).show();
                                }else
                                    Toast.makeText(mContext, ""+getString(R.string.empty_years), Toast.LENGTH_SHORT).show();
                            }else
                                Toast.makeText(mContext, ""+getString(R.string.empty_education), Toast.LENGTH_SHORT).show();
                        }else
                            Toast.makeText(mContext, ""+getString(R.string.empty_currency), Toast.LENGTH_SHORT).show();

                    } else
                        Toast.makeText(mContext, "" + getString(R.string.empty_price), Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(mContext, "" + getString(R.string.empty_specialist), Toast.LENGTH_SHORT).show();
            else
                serverTherapistNullLicense();
        }
    }

    private boolean isEmptyEditTxt(EditText editText) {
        return editText.getText().toString().trim().length() != 0;
    }

    private void serverTherapistNullLicense() {
        try {
            progressBar.setVisibility(View.VISIBLE);

            final String email = emailET.getText().toString();
            final String price = priceET.getText().toString();
            final String education = educationET.getText().toString();
            final String about = aboutET.getText().toString();
            final String exp = expYearsET.getText().toString();

            final LinkedHashMap<String, String> specialistsMap = new LinkedHashMap<>();
            for (int i = 0; i < specialities.size(); i++) {
                specialistsMap.put("specialties[" + i + "]", specialities.get(i));
                Log.d(TAG, "serverTherapistNullLicense: " + specialities.get(i));
            }


            ApiLink retrofit = MyRetrofitClient.auth().create(ApiLink.class);

            Call<CompleteRegDoc1> registerCall = retrofit.complete_register_doctor_null_license(listSharedPreference.getToken(),
                    specialistsMap, countryCode, cityCode, email, price, currencyCode,
                    exp, education, about, getGender());

            registerCall.enqueue(new Callback<CompleteRegDoc1>() {
                @Override
                public void onResponse(@NonNull Call<CompleteRegDoc1> call, @NonNull Response<CompleteRegDoc1> response) {
                    listSharedPreference.setIsRibbleViewRunForDr(false);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Status status = response.body().getStatus();
                            if (status != null) {
                                if (status.getType().equals("1")) {
                                    Title title = status.getTitle();
                                    if (title != null)
                                        setResponseMessage(title);

                                    Data data = response.body().getData();
                                    if (data != null) {
                                        saveUserInfo(data);
                                        Doctor doctor = data.getDoctor();
                                        List<com.tkmsoft.taahel.model.api.auth.register.doctor.Specialization> specialization = data.getSpecializations();
                                        if (doctor != null && specialization != null)
                                            saveDrData(data, doctor, specialization);
                                    }
                                    getFragmentManager().beginTransaction()
                                            .replace(R.id.login_frame, new TherapistRegFragment2()).commit();
                                } else {
                                    Title title = status.getTitle();
                                    if (title != null)
                                        setResponseMessage(title);
                                }
                            }
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(@NonNull Call<CompleteRegDoc1> call, @NonNull Throwable t) {
                    try {
//                        Toast.makeText(getActivitiesCategs(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                        Log.d(TAG, "on fail: " + t.getMessage());
                    } catch (Exception r) {
                        Log.d(TAG, "excepetion: " + r.getMessage());
                    }
                    progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception exc) {
            Toast.makeText(mContext, getString(R.string.plz_insert_all_fields), Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    }

    private void setResponseMessage(Title title) {
        if (getLang().equals("ar"))
            Toast.makeText(mContext, "" + title.getAr(), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + title.getEn(), Toast.LENGTH_SHORT).show();
    }

    private String getLang() {
        return listSharedPreference.getLanguage();
    }

    private void serverTherapist() {
        progressBar.setVisibility(View.VISIBLE);

        final String email = emailET.getText().toString();
        final String price = priceET.getText().toString();
        final String education = educationET.getText().toString();
        final String about = aboutET.getText().toString();
        final String exp = expYearsET.getText().toString();

        RequestBody mFile2 = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part licence = MultipartBody.Part.createFormData("license", file.getName(), mFile2);
        RequestBody country_id = RequestBody.create(MediaType.parse("text/plain"), countryCode);
        RequestBody city_id = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody user_email = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody user_price = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody user_currencyId = RequestBody.create(MediaType.parse("text/plain"), currencyCode);
        RequestBody user_education = RequestBody.create(MediaType.parse("text/plain"), education);
        RequestBody user_about = RequestBody.create(MediaType.parse("text/plain"), about);
        RequestBody user_exp = RequestBody.create(MediaType.parse("text/plain"), exp);

        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), getGender());

        RequestBody rb;
        LinkedHashMap<String, RequestBody> specialistsMap = new LinkedHashMap<>();
        for (int i = 0; i < specialities.size(); i++) {
            rb = RequestBody.create(MediaType.parse("text/plain"), specialities.get(i));
            specialistsMap.put("specialties[" + i + "]", rb);
            Log.d(TAG, "serverTherapistNullLicense: " + specialistsMap.get("specialties[" + i + "]"));
        }

        ApiLink retrofit = MyRetrofitClient.auth().create(ApiLink.class);

        Call<CompleteRegDoc1> registerCall = retrofit.complete_register_doctor_Qe
                (listSharedPreference.getToken(), licence, specialistsMap, country_id, city_id, user_email,
                        user_price, user_currencyId,
                        user_exp, user_education, user_about, gender);

        registerCall.enqueue(new Callback<CompleteRegDoc1>() {
            @Override
            public void onResponse(@NonNull Call<CompleteRegDoc1> call, @NonNull Response<CompleteRegDoc1> response) {
                listSharedPreference.setIsRibbleViewRunForDr(false);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("1")) {
                                Title title = status.getTitle();
                                if (title != null)
                                    setResponseMessage(title);

                                Data data = response.body().getData();
                                if (data != null) {
                                    saveUserInfo(data);
                                    Doctor doctor = data.getDoctor();
                                    List<com.tkmsoft.taahel.model.api.auth.register.doctor.Specialization> specialization = data.getSpecializations();
                                    if (doctor != null && specialization != null)
                                        saveDrData(data, doctor, specialization);
                                }
                                getFragmentManager().beginTransaction()
                                        .replace(R.id.login_frame, new TherapistRegFragment2()).commit();
                            } else {
                                Title title = status.getTitle();
                                if (title != null)
                                    setResponseMessage(title);
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<CompleteRegDoc1> call, @NonNull Throwable t) {
                try {
                    Toast.makeText(getActivity(), "" + getString(R.string.error_msg_timeout), Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                    Log.d(TAG, "on fail: " + t.getMessage());
                } catch (Exception r) {
                    Log.d(TAG, "excepetion: " + r.getMessage());
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void saveUserInfo(Data data) {
        listSharedPreference.setUName(data.getName());
        listSharedPreference.setUEmail(data.getEmail());
        listSharedPreference.setUPhone(data.getPhone());
        listSharedPreference.setUPhoneKey(data.getPhoneKey());
        listSharedPreference.setUAvatar(data.getAvatar());
        listSharedPreference.setUGender(data.getGender());
        if (getLang().equals("ar")) {
            listSharedPreference.setUCountry(data.getCountry().getNameAr());
            listSharedPreference.setUCity(data.getCity().getNameAr());
        } else {
            listSharedPreference.setUCountry(data.getCountry().getNameEn());

            listSharedPreference.setUCity(data.getCity().getNameEn());
        }
        listSharedPreference.setUCountryId(data.getCountry().getId());
        listSharedPreference.setUCityId(data.getCity().getId());
    }

    private void saveDrData(Data data, Doctor doctor, List<com.tkmsoft.taahel.model.api.auth.register.doctor.Specialization> specialization) {
        listSharedPreference.setUAbout(doctor.getAbout());
        listSharedPreference.setUEducation(doctor.getEducation());
        listSharedPreference.setUExperience(doctor.getExperiences());
        listSharedPreference.setULicense(doctor.getLicense());
        listSharedPreference.setUPrice(doctor.getPrice());
        ArrayList<String> specialistsStringList = new ArrayList<>();
        for (int i = 0; i < specialization.size(); i++) {
            if (getLang().equals("ar"))
                specialistsStringList.add(specialization.get(i).getNameAr());
            else
                specialistsStringList.add(specialization.get(i).getNameEn());
        }
        listSharedPreference.setUSpecialists(specialistsStringList);
        if (getLang().equals("ar"))
            listSharedPreference.setUCurrency(data.getCurrency().getNameAr());
        else
            listSharedPreference.setUCurrency(data.getCurrency().getNameEn());

        listSharedPreference.setUCurrencyId(data.getCurrency().getId());
    }

    private String getGender() {
        int genderId = radioGender.getCheckedRadioButtonId();
        RadioButton radioButton = rootView.findViewById(genderId);
        switch (radioButton.getId()) {
            case R.id.maleRB:
                return "0";
            case R.id.femaleRB:
                return "1";
            default:
                return "-1";
        }
    }

    @OnClick(R.id.deptTV)
    protected void onDeptClick() {
        if (recyclerView.getVisibility() == View.GONE) {
            recyclerView.setVisibility(View.VISIBLE);
            if (listSharedPreference.getLanguage().equals("en"))
                deptTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_accent_up_24dp_wrapper, 0);
            else
                deptTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_accent_up_24dp_wrapper, 0, 0, 0);
        } else {
            recyclerView.setVisibility(View.GONE);
            if (listSharedPreference.getLanguage().equals("en"))
                deptTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_accent_arrow_down_24dp_wrapper, 0);
            else
                deptTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_accent_arrow_down_24dp_wrapper, 0, 0, 0);
        }
    }


    @OnClick(R.id.medical_licenseTV)
    protected void imageOnClick() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, LICENCE_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == LICENCE_CODE && resultCode == RESULT_OK && data != null) {
//            Uri uri = data.getData();
//            assert uri != null;
//            String uriString = uri.toString();
//            File myFile = new File(uriString);
//            image_path = myFile.getAbsolutePath();
//            String displayName = getFileName(uriString, uri, myFile);
//            medical_licenseET.setText(displayName);
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, TherapistRegFragment.this.getActivity());
            assert image_path != null;
            file = new File(image_path);
            medical_licenseET.setText(file.getName());
        }//end if check for data
        else {
            medical_licenseET.setText(getString(R.string.medical_license));
        }


    }//end onActivityResult


    private String getFileName(String uriString, Uri uri, File myFile) {
        if (uriString.startsWith("content://")) {
            Cursor cursor = null;
            String name = "null";
            try {
                cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } catch (Exception exc) {
                exc.printStackTrace();
            } finally {
                assert cursor != null;
                cursor.close();
            }
            return name;
        } else if (uriString.startsWith("file://")) {
            return myFile.getName();
        } else
            return null;
    }

    private void fireBackButtonEvent() {

        ((LoginActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction()
                        .replace(R.id.login_frame, new RegisterFragment(), "RegisterFragment")
                        .commit();
            }
        });
    }//end back pressed

    private String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] data = {MediaStore.Images.Media.DATA};
            CursorLoader loader = new CursorLoader(context, contentURI, data, null, null, null);
            Cursor cursor = loader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public void onShowRationalDialog(final PermissionInterface permissionInterface, int requestCode) {
        Log.d(TAG, "onShowRationalDialog: ");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("We need permissions for this app.");
        builder.setPositiveButton(R.string.ok, (dialog, which) -> permissionInterface.onDialogShown());
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    @Override
    public void onShowSettings(final PermissionInterface permissionInterface, int requestCode) {
        Log.d(TAG, "onShowSettings: ");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("We need permissions for this app. Open setting screen?");
        builder.setPositiveButton(R.string.ok, (dialog, which) -> permissionInterface.onSettingsShown());
        builder.setNegativeButton(R.string.cancel, null);
        builder.show();
    }

    @Override
    public void onPermissionsGranted(int requestCode) {

    }

    @Override
    public void onPermissionsDenied(int requestCode) {

    }
}