package com.tkmsoft.taahel.fragments.main;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.api.OpinionModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SentOpinionFragment extends Fragment {

    ListSharedPreference listSharedPreference;

    @BindView(R.id.opinionET)
    EditText opinionET;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.sendBtn)
    Button sendBtn;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLink;

    public SentOpinionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sent_opinion, container, false);
        ButterKnife.bind(this, rootView);
        listSharedPreference = new ListSharedPreference(SentOpinionFragment.this.getActivity().getApplicationContext());
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
    }

    @OnClick(R.id.sendBtn)
    protected void onClickk() {
        serverSendOpinion();
    }

    private void serverSendOpinion() {
        progressBar.setVisibility(View.VISIBLE);
        try {
            callSendOpinion().enqueue(new Callback<OpinionModel>() {
                @Override
                public void onResponse(@NonNull Call<OpinionModel> call, @NonNull Response<OpinionModel> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().getType().equals("success")) {
                                Toast.makeText(mContext, ""+response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                getFragmentManager().beginTransaction().replace(R.id.main_frameLayout, new HomeFragment()).commit();
                            }
                            else
                                Toast.makeText(mContext, ""+response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<OpinionModel> call, @NonNull Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(mContext, "" + R.string.network_error, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception exc) {
            progressBar.setVisibility(View.GONE);
            exc.printStackTrace();
        }
    }

    private Call<OpinionModel> callSendOpinion() {
        return apiLink.postOpinion(opinionET.getText().toString());
    }

    @Override
    public void onAttach(Activity activity) {
        fireBackButtonEvent();

        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        callSendOpinion().cancel();
        super.onDetach();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, new HomeFragment())
                        .commit();
            }
        });
    }//end back pressed
}
