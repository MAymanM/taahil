package com.tkmsoft.taahel.fragments.main;


import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.FAQsAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.FAQModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FAQFragment extends Fragment implements FAQsAdapter.ListAllClickListeners {

    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private FragmentActivity mContext;
    private MainViewsCallBack mMainViewsCallBack;
    private ApiLink apiLink;
    private FAQsAdapter faqsAdapter;
    MoveToFragment moveToFragment;

    public FAQFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            mMainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        ListSharedPreference listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        ConnectionDetector connectionDetector = new ConnectionDetector(mContext);

        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_faq, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        faqsAdapter = new FAQsAdapter(new ArrayList<>(), this);
        initPullRefreshLayout();
        initRecyclerView();
        serverGetFAQs();
    }

    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }
        recyclerView.setAdapter(faqsAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_WATER_DROP);
        pullRefreshLayout.setOnRefreshListener(() -> moveToFragment.moveInMain(new FAQFragment()));
    }

    private void serverGetFAQs() {
        progressBar.setVisibility(View.VISIBLE);
        callGetFAQ().enqueue(new Callback<FAQModel>() {
            @Override
            public void onResponse(@NonNull Call<FAQModel> call, @NonNull Response<FAQModel> response) {
                if (response.body() != null) {
                    FAQModel.StatusBean status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            FAQModel.DataBean datum = response.body().getData();
                            if (datum!= null) {
                                List<FAQModel.DataBean.FaqsBean> faqsList = datum.getFaqs();
                                if (faqsList != null) {
                                    faqsAdapter.replaceData(faqsList);
                                }
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(@NonNull Call<FAQModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
                serverGetFAQs();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.faq));

    }

    @Override
    public void onDetach() {
        callGetFAQ().cancel();
        super.onDetach();
    }

    private Call<FAQModel> callGetFAQ() {
        return apiLink.getFAQs();
    }

    private void fireBackButtonEvent() {

        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
            }
        });

    }//end back pressed

    @Override
    public void onItemClick(FAQModel.DataBean.FaqsBean datumList, int pos) {

    }
}

