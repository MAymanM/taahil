package com.tkmsoft.taahel.fragments.main.store;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.ads.AdsAdapter;
import com.tkmsoft.taahel.adapters.store.StoreAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.AdsModel;
import com.tkmsoft.taahel.model.api.favourite.change.ChangeFavModel;
import com.tkmsoft.taahel.model.api.store.view.Comment;
import com.tkmsoft.taahel.model.api.store.view.Data;
import com.tkmsoft.taahel.model.api.store.view.Datum;
import com.tkmsoft.taahel.model.api.store.view.Status;
import com.tkmsoft.taahel.model.api.store.view.StoreModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreFragment extends Fragment implements StoreAdapter.ListAllClickListeners {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private MainViewsCallBack mMainViewsCallBack;

    @BindView(R.id.viewPager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CircleIndicator circleIndicator;

    @BindView(R.id.ads_cardView)
    CardView ads_cardView;
    @BindView(R.id.ad_progressBar)
    ProgressBar ad_progressBar;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    private MoveToFragment moveToFragment;
    private ApiLink apiLinkStore;
    private Integer currentPagerAds, currentPage;
    private ListSharedPreference listSharedPreference;
    private StoreAdapter storeAdapter;
    private boolean isLastPage = false;
    private String mLanguage = "ar";
    private boolean isFilter = false;
    private AttemptStoreFilter attemptStoreFilter;

    public StoreFragment() {
        // Required empty public constructor
    }



    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        try {
            mMainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        ConnectionDetector connectionDetector = new ConnectionDetector(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null){
            isFilter = bundle.getBoolean("isFilter");
            attemptStoreFilter = new AttemptStoreFilter(bundle);
        }

        ApiLink apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);
        apiLinkStore = MyRetrofitClient.getStore().create(ApiLink.class);
        currentPagerAds = 0;
        currentPage = 1;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_store, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }


    private void initUI(View rootView) {
        initRecyclerView();
        loadFirstPAge();
        mLanguage = listSharedPreference.getLanguage();
    }

    private void setupAddButton() {
        FloatingActionButton fab = mContext.findViewById(R.id.main_fab);
        fab.setOnClickListener(view ->
                moveToFragment.moveInMain(new AddStoreFrag())
        );
    }


    private void initRecyclerView() {
        storeAdapter = new StoreAdapter(new ArrayList<>(), this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(storeAdapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }
    private void loadFirstPAge() {
        progressBar.setVisibility(View.VISIBLE);

        callGetStore().enqueue(new Callback<StoreModel>() {
            @Override
            public void onResponse(@NonNull Call<StoreModel> call, @NonNull Response<StoreModel> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            serverAds();
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> datumList = data.getData();
                                if (datumList != null) {
                                    if (isFilter) {
                                        attemptStoreFilter.setAdapter(storeAdapter);
                                        attemptStoreFilter.filter(datumList, 1);
                                    } else {
                                        storeAdapter.replaceData(datumList);
                                    }
                                }
                            }
                        } else
                            isLastPage = true;
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<StoreModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }//end server

    private void loadNextPage() {
        callGetStore().enqueue(new Callback<StoreModel>() {
            @Override
            public void onResponse(@NonNull Call<StoreModel> call, @NonNull Response<StoreModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> datumList = data.getData();
                                if (datumList != null) {
                                    if (isFilter) {
                                        attemptStoreFilter.filter(datumList, 2);
                                    } else {
                                        storeAdapter.updateData(datumList);
                                    }
                                }
                            }
                        } else
                            isLastPage = true;
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<StoreModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }



    private void setupFilterButton() {
        ImageButton imageButton =  mContext.findViewById(R.id.toolbar_filter_button);
        imageButton.setOnClickListener(view ->
                moveToFragment.moveInMain(new StoreFilterFragment())
        );
    }


    private Call<StoreModel> callGetStore() {
        return apiLinkStore.getStore(getToken(),
                currentPage);
    }

    private ArrayList<String> getCommentsContent(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getContent());
        }
        return sList;
    }

    private ArrayList<String> getCommentsRate(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getRate());
        }
        return sList;
    }

    private void serverAds() {
        ad_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<AdsModel> sliderImages = retrofit.getAds("ads?type=0");
        sliderImages.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(@NonNull Call<AdsModel> call, @NonNull Response<AdsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        AdsModel.StatusBean statusBean = response.body().getStatus();
                        if (statusBean != null) {
                            if (statusBean.getType().equals("success")) {
                                AdsModel.DataBean dataBean = response.body().getData();
                                if (dataBean != null) {
                                    List<AdsModel.DataBean.AdsInfoBean> adsInfoBeans = dataBean.getAds_info();
                                    if (adsInfoBeans != null) {
                                        initImageSlider(adsInfoBeans);
                                    }
                                    ads_cardView.setVisibility(View.VISIBLE);
                                } else {
                                    ads_cardView.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
                ad_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<AdsModel> call, @NonNull Throwable t) {
                ad_progressBar.setVisibility(View.GONE);
                ads_cardView.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }//end server()

    private void initImageSlider(final List<AdsModel.DataBean.AdsInfoBean> adsInfo) {
        try {
            mPager.setAdapter(new AdsAdapter(getActivity(), adsInfo,
                    (adsInfoBeans, position) -> {
                        //on ad click

                    }));
            circleIndicator.setViewPager(mPager);

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = () -> {
                if (currentPagerAds == adsInfo.size()) {
                    currentPagerAds = 0;
                }
                mPager.setCurrentItem(currentPagerAds++, true);
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 2500, 2500);
        } catch (Exception ignored) {

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setupFilterButton();
        setupAddButton();
        mMainViewsCallBack.showFilterBtn(true);
        mMainViewsCallBack.serToolbarTitle(getString(R.string.store));
        mMainViewsCallBack.showAddFab(true);
    }

    @Override
    public void onDetach() {
        mMainViewsCallBack = null;
        super.onDetach();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
            }
        });
    }//end back pressed


    @Override
    public void onItemClick(Datum datumList, int pos) {
        listSharedPreference.setBackStack("store");
        Fragment fragment = new StoreDetail();
        Bundle bundle = new Bundle();
        bundle.putInt("pId", datumList.getId());
        bundle.putString("image", datumList.getMainPhoto());
        bundle.putString("price", datumList.getPrice());
        if (mLanguage.equals("ar")) {
            bundle.putString("name", datumList.getNameAr());
            bundle.putString("currency", datumList.getCurrency().getNameAr());
            bundle.putString("country", datumList.getCountry().getNameAr());
            bundle.putString("city", datumList.getCity().getNameAr());
            bundle.putString("dept", datumList.getCategory().getNameAr());
            bundle.putString("desc", datumList.getDescAr());
        } else {
            bundle.putString("name", datumList.getNameEn());
            bundle.putString("currency", datumList.getCurrency().getNameEn());
            bundle.putString("country", datumList.getCountry().getNameEn());
            bundle.putString("city", datumList.getCity().getNameEn());
            bundle.putString("dept", datumList.getCategory().getNameEn());
            bundle.putString("desc", datumList.getDescEn());
        }
        bundle.putString("views", datumList.getViews());
        bundle.putString("favs", datumList.getCountWishlists());
        bundle.putString("code", String.valueOf(datumList.getId()));
        bundle.putFloat("rate", Float.parseFloat(datumList.getCountRate()));
        bundle.putString("size", datumList.getSize());
        bundle.putString("ageFrom", datumList.getAgeFrom());
        bundle.putString("ageTo", datumList.getAgeTo());
        bundle.putStringArrayList("commentsContent", getCommentsContent(datumList.getComments()));
        bundle.putStringArrayList("commentsRate", getCommentsRate(datumList.getComments()));
        bundle.putStringArrayList("cmntsUserNames", getCommentsUserNames(datumList.getComments()));
        bundle.putStringArrayList("cmntsUserImg", getCommentsUserImgs(datumList.getComments()));

        bundle.putBoolean("isFav", datumList.getFav());
        bundle.putBoolean("isCart", datumList.getCart());


        fragment.setArguments(bundle);
        moveToFragment.moveInMain(fragment);
    }
    private ArrayList<String> getCommentsUserImgs(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getAvatar());
        }
        return sList;
    }

    private ArrayList<String> getCommentsUserNames(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getMember().getName());
        }
        return sList;
    }

    @Override
    public void onFavBtnClick(ImageButton favBtn, Datum data, int pos) {
        if (listSharedPreference.getIsLogged())
            serverChangeFav(data.getId(), favBtn);
        else {
            goToLogin();
        }
    }

    private void goToLogin() {
        Toast.makeText(mContext, R.string.please_login_first, Toast.LENGTH_SHORT).show();
        Intent myIntent = new Intent(MyApp.getContext().getApplicationContext(), LoginActivity.class);
        startActivity(myIntent);
        mContext.finish();
    }

    private void serverChangeFav(Integer id, final ImageButton favBtn) {
        progressBar.setVisibility(View.VISIBLE);
        callGetChangeFav(id).enqueue(new Callback<ChangeFavModel>() {
            @Override
            public void onResponse(@NonNull Call<ChangeFavModel> call, @NonNull Response<ChangeFavModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.favourite.change.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            boolean isFav = status.getIsFav();
                            if (isFav) {
                                favBtn.setImageResource(R.drawable.ic_favorite_full_24dp);
                            } else
                                favBtn.setImageResource(R.drawable.ic_favorite_empty_24dp);
                        } else
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<ChangeFavModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private Call<ChangeFavModel> callGetChangeFav(Integer id) {
        return apiLinkStore.changeWichlist(getToken(), id);
    }

    private String getToken() {
        return listSharedPreference.getToken();
    }


}
