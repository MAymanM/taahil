package com.tkmsoft.taahel.fragments.main.jobOrders;


import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.downloader.Status;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.FileUtils;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.MyToast;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobOrderDetailFragment extends Fragment {

    @BindViews({R.id.nameTV, R.id.fieldTV, R.id.countryTV, R.id.cityTV, R.id.emailTV, R.id.descTV,
            R.id.phoneTV, R.id.disabilityTV, R.id.cvTV})
    List<TextView> textViewList;
    @BindView(R.id.imageView)
    ImageView imageView;
    TextView nameTV, fieldTV, countryTV, cityTV, emailTV, descTV, phoneTV, disabilityTV, cvTV;
    private FragmentActivity mContext;
    private MainViewsCallBack mainViewsCallBack;
    private MoveToFragment moveToFragment;

    private String name, field, country, city, email, desc, phone;
    private String disability, cv;
    private String image;
    @BindView(R.id.cv_rel)
    RelativeLayout cv_rel;
    private String TAG = getClass().getSimpleName();
    @BindView(R.id.loading_bar_linearLay)
    LinearLayout loading_bar_linearLay;
    @BindView(R.id.loadingBar)
    ProgressBar loadingBar;
    @BindView(R.id.loadingTV)
    TextView loadingTV;
    MyToast myToast;
    public JobOrderDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        fireBackButtonEvent();
        ListSharedPreference listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        myToast = new MyToast(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString("name");
            field = getArguments().getString("field");
            country = getArguments().getString("country");
            city = getArguments().getString("city");
            email = getArguments().getString("email");
            desc = getArguments().getString("desc");
            phone = getArguments().getString("phone");
            disability = getArguments().getString("disability");
            cv = getArguments().getString("cv");
            image = getArguments().getString("image");
        }
        ApiLink apiLinkBase = MyRetrofitClient.downloadJobOrder().create(ApiLink.class);
        PRDownloader.initialize(mContext.getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_job_order_detail, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        nameTV = textViewList.get(0);
        fieldTV = textViewList.get(1);
        countryTV = textViewList.get(2);
        cityTV = textViewList.get(3);
        emailTV = textViewList.get(4);
        descTV = textViewList.get(5);
        phoneTV = textViewList.get(6);
        disabilityTV = textViewList.get(7);
        cvTV = textViewList.get(8);

        nameTV.setText(name);
        fieldTV.setText(field);
        countryTV.setText(country);
        cityTV.setText(city);
        emailTV.setText(email);
        descTV.setText(desc);
        phoneTV.setText(phone);
        disabilityTV.setText(disability);
        cvTV.setText(getString(R.string.download_cv));

        Picasso.get().load(image).error(R.drawable.place_holder).into(imageView);
    }

    @OnClick(R.id.cv_rel)
    protected void onCvClick() {
        serverDownloadCV();
    }

    private void serverDownloadCV() {
        showLoadingBar(true);
        loadingBar.setIndeterminate(true);
        String[] parts = cv.split(Pattern.quote("/")); // Split on period.
        String fileName = parts[7];

        String dirPath = FileUtils.getRootDirPath(mContext.getApplicationContext());

        Log.d(TAG, "file url: "+cv + "file path: "+ dirPath);
        int downloadId = PRDownloader.download(cv, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(() -> {
                    Log.d(TAG, "serverDownloadCV: " + "start downloading");
                    showLoadingBar(true);
                    loadingBar.setIndeterminate(false);

                }).setOnPauseListener(() -> Log.d(TAG, "serverDownloadCV: "+ "pause"))
                .setOnCancelListener(() -> Log.d(TAG, "serverDownloadCV: "+ "cancel"))
                .setOnProgressListener(progress -> {
                    long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
                    loadingBar.setProgress((int) progressPercent);
                    loadingTV.setText(FileUtils.getProgressDisplayLine(progress.currentBytes, progress.totalBytes));
                    loadingBar.setIndeterminate(false);
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d(TAG, "onDownloadComplete: "+"downloaded success");
                        showLoadingBar(false);
                        myToast.successToast(getString(R.string.successfully_downloaded), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d(TAG, "onError: "+error.toString());
                        loadingBar.setProgress(0);
                        loadingTV.setText("");
                        showLoadingBar(false);
                    }
                });

        Status status = PRDownloader.getStatus(downloadId);
        Log.d(TAG, "serverDownloadCV status: "+status);
    }

    private void showLoadingBar(boolean b) {
        if (b)
            loading_bar_linearLay.setVisibility(View.VISIBLE);
        else
            loading_bar_linearLay.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(name);
        mainViewsCallBack.showAddFab(false);
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new JobOrdersFragment());
            }
        });
    }//end back pressed
}