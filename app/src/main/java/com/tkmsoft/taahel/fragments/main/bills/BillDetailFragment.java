package com.tkmsoft.taahel.fragments.main.bills;


import androidx.fragment.app.Fragment;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.bills.BillsDetailsAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class BillDetailFragment extends Fragment {


    private FragmentActivity mContext;
    ListSharedPreference listSharedPreference;
    private MainViewsCallBack mainViewsCallBack;
    private MoveToFragment moveToFragment;

    @BindViews({R.id.billNumTV, R.id.dateTV, R.id.addressTV, R.id.deliveryTV,
            R.id.cityTV, R.id.paymentTV, R.id.quantityTV, R.id.totalTV})
    List<TextView> textViewList;

    TextView billNumTV, dateTV, addressTV, deliveryTV, cityTV, paymentTV, quantityTV, totalTV;

    String billNum, date, address, delivery, city, payment, quantity, sumAll;

    ArrayList<String> pNameArray = new ArrayList<>(), pQuantityArray= new ArrayList<>(), pPriceArray= new ArrayList<>(), pTotalArray = new ArrayList<>();

    BillsDetailsAdapter billsDetailsAdapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public BillDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {

        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }

        super.onAttach(context);
        try {
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }

        ConnectionDetector connectionDetector = new ConnectionDetector(context);
        listSharedPreference = new ListSharedPreference(context.getApplicationContext());
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            billNum = bundle.getString("id");
            date = bundle.getString("date");
            address = bundle.getString("address");
            delivery = bundle.getString("delivery");
            city = bundle.getString("city");
            payment = bundle.getString("payment");
            quantity = bundle.getString("quantity");
            sumAll = bundle.getString("sumAll");
            pNameArray = bundle.getStringArrayList("pNameArray");
            pQuantityArray = bundle.getStringArrayList("pQuantityArray");
            pPriceArray = bundle.getStringArrayList("pPriceArray");
            pTotalArray = bundle.getStringArrayList("pTotalArray");
        }else {
            pNameArray = new ArrayList<>();
            pQuantityArray = new ArrayList<>();
            pPriceArray = new ArrayList<>();
            pTotalArray = new ArrayList<>();
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bill_detail, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        setTextViews();
        initRecyclerView();
    }

    private void setTextViews() {
        billNumTV = textViewList.get(0);
        billNumTV.setText(billNum);

        dateTV = textViewList.get(1);
        dateTV.setText(date);

        addressTV = textViewList.get(2);
        addressTV.setText(address);

        deliveryTV = textViewList.get(3);
        deliveryTV.setText(delivery);

        cityTV = textViewList.get(4);
        cityTV.setText(city);

        paymentTV = textViewList.get(5);
        paymentTV.setText(payment);

        quantityTV = textViewList.get(6);
        quantityTV.setText(quantity);

        totalTV = textViewList.get(7);
        totalTV.setText(sumAll);
    }

    private void initRecyclerView() {
        billsDetailsAdapter = new BillsDetailsAdapter(pNameArray, pQuantityArray, pPriceArray, pTotalArray);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(billsDetailsAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.serToolbarTitle(getString(R.string.bill_details));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        mainViewsCallBack = null;
        super.onDetach();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new BillsFragment());
            }
        });
    }//end back pressed
}
