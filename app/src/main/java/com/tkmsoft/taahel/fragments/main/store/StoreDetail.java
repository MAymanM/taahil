package com.tkmsoft.taahel.fragments.main.store;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import androidx.fragment.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.ImageSliderAdapter;
import com.tkmsoft.taahel.adapters.ReviewsAdapter;
import com.tkmsoft.taahel.fragments.main.favourite.FavouritesFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.ReviewsModel;
import com.tkmsoft.taahel.model.api.SliderModel;
import com.tkmsoft.taahel.model.api.cart.change.ChangeCartModel;
import com.tkmsoft.taahel.model.api.comments.add.AddCommentModel;
import com.tkmsoft.taahel.model.api.comments.add.Status;
import com.tkmsoft.taahel.model.api.favourite.change.ChangeFavModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreDetail extends Fragment {

    @BindView(R.id.viewPager)
    ViewPager mPager;
    @BindView(R.id.indicator)
    CircleIndicator circleIndicator;

    //Reviews
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    //Write Review
    @BindView(R.id.cardView_writecmnt)
    CardView cardView_writecmnt;
    @BindView(R.id.img_cmnt_write)
    ImageView img_cmnt_write;

    @BindViews({R.id.nameTV, R.id.priceTV, R.id.favesTV, R.id.viewsTV, R.id.descriptionTV,
            R.id.codeTV, R.id.deptTV, R.id.sizeTV, R.id.ageTV, R.id.cityTV, R.id.countryTV,
            R.id.static_descTV, R.id.static_specsTV, R.id.textView_cmnt_write})
    List<TextView> textViewList;
    private TextView nameTV;
    private TextView priceTV;
    private TextView favesTV;
    private TextView viewsTV;
    private TextView descTV;
    private TextView codeTV;
    private TextView deptTV;
    private TextView sizeTV;
    private TextView ageTV;
    private TextView cityTV;
    private TextView countryTV;
    private TextView static_descTV;
    private TextView static_specsTV;
    @BindViews({R.id.rel_specs_details, R.id.rel_desc_txt})
    List<RelativeLayout> relativeLayoutList;
    RelativeLayout rel_specs_details, rel_desc_txt;

    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindViews({R.id.cartLottieView, R.id.favLottieView})
    List<LottieAnimationView> lottieViewList;
    private LottieAnimationView cartLottieView, favLottieView;
    private static int currentPage = 0;

    private ArrayList<SliderModel.DataBean.SlidersBean> sliderArray = new ArrayList<>();

    private ListSharedPreference listSharedPreference;

    private MainViewsCallBack mMainViewsCallBack;
    private ArrayList<ReviewsModel> mDataList;
    private ReviewsAdapter reviewsAdapter;
    private String price;
    private String views;
    private String favs;
    private String code;
    private String size;
    private String ageFrom;
    private String ageTo;
    private String desc;
    private String image;
    private String name;
    private String country;
    private String city;
    private float rate;

    private ArrayList<String> commentsContent = new ArrayList<>(), commentsRate = new ArrayList<>(),
            commentsUserNames = new ArrayList<>(), commentsUserImges = new ArrayList<>();    private FragmentActivity mContext;
    private String dept;
    private ApiLink apiLinkBase;
    private Integer pId;
    private MoveToFragment moveToFragment;
    private boolean isFav = false;
    private boolean isCart = false;
    private ApiLink apiLinkStore;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private boolean isLogged;

    public StoreDetail() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        try {
            context = getActivity();
            mMainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        ConnectionDetector connectionDetector = new ConnectionDetector(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            pId = bundle.getInt("pId");
            image = bundle.getString("image");
            name = bundle.getString("name");
            price = bundle.getString("price");
            country = bundle.getString("country");
            city = bundle.getString("city");
            views = bundle.getString("views");
            favs = bundle.getString("favs");
            code = bundle.getString("code");
            String color = bundle.getString("color");
            rate = bundle.getFloat("rate");
            size = bundle.getString("size");
            ageFrom = bundle.getString("ageFrom");
            ageTo = bundle.getString("ageTo");
            desc = bundle.getString("desc");
            String lat = bundle.getString("lat");
            String longX = bundle.getString("longX");
            dept = bundle.getString("dept");
            isFav = bundle.getBoolean("isFav");
            isCart = bundle.getBoolean("isCart");
            isLogged = listSharedPreference.getIsLogged();
            commentsContent = bundle.getStringArrayList("commentsContent");
            commentsRate = bundle.getStringArrayList("commentsRate");
            commentsUserNames = bundle.getStringArrayList("cmntsUserNames");
            commentsUserImges = bundle.getStringArrayList("cmntsUserImg");
        }
        apiLinkStore = MyRetrofitClient.getStore().create(ApiLink.class);
        apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);
    }//end onCreate

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_store_detail, container, false);
        ButterKnife.bind(this, rootView);

        initUI(rootView);

        return rootView;
    }

    private void initUI(View rootView) {
        initImageSlider();
        initRecyclerView();
        initViews();
        setData();
        initAdapter();
    }

    private void initViews() {

        nameTV = textViewList.get(0);
        priceTV = textViewList.get(1);
        favesTV = textViewList.get(2);
        viewsTV = textViewList.get(3);
        descTV = textViewList.get(4);
        codeTV = textViewList.get(5);
        deptTV = textViewList.get(6);
        sizeTV = textViewList.get(7);
        ageTV = textViewList.get(8);
        cityTV = textViewList.get(9);
        countryTV = textViewList.get(10);
        static_descTV = textViewList.get(11);
        static_specsTV = textViewList.get(12);
        TextView textView_cmnt_write = textViewList.get(13);

        rel_specs_details = relativeLayoutList.get(0);
        rel_desc_txt = relativeLayoutList.get(1);

        cartLottieView = lottieViewList.get(0);
        favLottieView = lottieViewList.get(1);

    }

    @SuppressLint("SetTextI18n")
    private void setData() {

        nameTV.setText(name);
        priceTV.setText(price);
        favesTV.setText(favs);
        viewsTV.setText(views);
        descTV.setText(desc);
        codeTV.setText(code);
        deptTV.setText(dept);
        sizeTV.setText(size);
        ageTV.setText(ageFrom + " : " + ageTo);
        cityTV.setText(city);
        countryTV.setText(country);

        if (isFav)
            favLottieView.setImageResource(R.drawable.ic_favorite_full_24dp);
        else
            favLottieView.setImageResource(R.drawable.ic_favorite_empty_24dp);

        if (isCart)
            cartLottieView.setImageResource(R.drawable.ic_add_to_cart_full_24dp);
        else
            cartLottieView.setImageResource(R.drawable.ic_add_to_cart_empty_24dp);


        ratingBar.setRating(rate);

    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 1,
                GridLayoutManager.HORIZONTAL, false));
    }

    private void initAdapter() {
        ArrayList<ReviewsModel> mDataList = new ArrayList<>();
        for (int i = 0; i < commentsContent.size(); i++) {
            ReviewsModel th = new ReviewsModel(commentsUserNames.get(i), commentsRate.get(i),
                    commentsUserImges.get(i), commentsContent.get(i));
            mDataList.add(i, th);
        }

        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(getActivity(), mDataList,
                (reviewsModel, adapterPosition) -> {

                });
        recyclerView.setAdapter(reviewsAdapter);
        reviewsAdapter.notifyDataSetChanged();
    }

    private EditText commentET;
    private RatingBar ratingBarPop;

    @OnClick(R.id.cardView_writecmnt)
    void onCarvViewWriteReviewClick() {

        LayoutInflater li = LayoutInflater.from(getActivity());
        final View popUpView = li.inflate(R.layout.activity_write_review, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        // create alert dialog

        alertDialogBuilder.setView(popUpView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        // set prompts.xml to alertdialog builder
        commentET = popUpView.findViewById(R.id.reviewET);
        final ImageButton imageButton = popUpView.findViewById(R.id.close_button);
        ratingBarPop = popUpView.findViewById(R.id.rateBar_review);
        final Button submitBtn = popUpView.findViewById(R.id.submitBtn);
        final ProgressBar progressBar = popUpView.findViewById(R.id.progressBar);
        imageButton.setOnClickListener
                (view ->
                        alertDialog.dismiss()
                );
        // show it
        alertDialog.show();

        submitBtn.setOnClickListener(v -> serverAddComment(alertDialog, progressBar));

    }

    private void serverAddComment(final AlertDialog alertDialog, final ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        postCommentModelCall().enqueue(new Callback<AddCommentModel>() {
            @Override
            public void onResponse(@NonNull Call<AddCommentModel> call, @NonNull Response<AddCommentModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();
                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddCommentModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private Call<AddCommentModel> postCommentModelCall() {
        return apiLinkBase.addComment(listSharedPreference.getToken(),
                String.valueOf(pId),
                commentET.getText().toString(),
                String.valueOf(ratingBarPop.getRating()),
                "0");
    }

    @OnClick(R.id.static_descTV)
    void onDescriptionClick() {
        hideShowDetails(static_descTV, rel_desc_txt);
    }

    @OnClick(R.id.static_specsTV)
    void onSpecsClick() {
        hideShowDetails(static_specsTV, rel_specs_details);
    }


    @OnClick({R.id.favLottieView, R.id.cartLottieView})
    void onImageButtonClick(LottieAnimationView lottieAnimationView) {
        int id = lottieAnimationView.getId();
        if (isLogged) {
            switch (id) {
                case R.id.favLottieView:
                    serverChangeFav();
                    break;
                case R.id.cartLottieView:
                    serverChangeCart();
                    break;
            }
        } else
            goToLogin();
    }

    private void serverChangeCart() {
        progressBar.setVisibility(View.VISIBLE);
        callChangeCart().enqueue(new Callback<ChangeCartModel>() {
            @Override
            public void onResponse(@NonNull Call<ChangeCartModel> call, @NonNull Response<ChangeCartModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.cart.change.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("1")) {
                            boolean isCart = status.getIsCart();
                            if (isCart) {
                                doCartAnimation();
                            } else
                                cartLottieView.setImageResource(R.drawable.ic_add_to_cart_empty_24dp);

                        } else
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChangeCartModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private void doCartAnimation() {
        cartLottieView.setAnimation(R.raw.cart_anim);
        cartLottieView.playAnimation();
        cartLottieView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                cartLottieView.setImageResource(R.drawable.ic_add_to_cart_full_24dp);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
    private void doFavAnimation() {
        favLottieView.setAnimation(R.raw.fav_anim);
        favLottieView.playAnimation();
        favLottieView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                favLottieView.setImageResource(R.drawable.ic_favorite_full_24dp);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private Call<ChangeCartModel> callChangeCart() {
        return apiLinkBase.changeCart(getToken(), Long.valueOf(pId));
    }


    private void goToLogin() {
        Toast.makeText(mContext, R.string.please_login_first, Toast.LENGTH_SHORT).show();
        Intent myIntent = new Intent(MyApp.getContext().getApplicationContext(), LoginActivity.class);
        startActivity(myIntent);
        mContext.finish();
    }

    private void serverChangeFav() {
        progressBar.setVisibility(View.VISIBLE);
        callGetChangeFav().enqueue(new Callback<ChangeFavModel>() {
            @Override
            public void onResponse(@NonNull Call<ChangeFavModel> call, @NonNull Response<ChangeFavModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.favourite.change.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            boolean isFav = status.getIsFav();
                            if (isFav) {
                                doFavAnimation();
                            } else
                                favLottieView.setImageResource(R.drawable.ic_favorite_empty_24dp);
                        } else
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChangeFavModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private Call<ChangeFavModel> callGetChangeFav() {
        return apiLinkStore.changeWichlist(getToken(), pId);
    }

    private String getToken() {
        return listSharedPreference.getToken();
    }


    private void hideShowDetails(TextView textView, RelativeLayout contentRelativeLayout) {

        if (contentRelativeLayout.getVisibility() == View.GONE) {
            contentRelativeLayout.setVisibility(View.VISIBLE);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_up_24dp, 0, 0, 0);
        } else {
            contentRelativeLayout.setVisibility(View.GONE);
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_down_24dp, 0, 0, 0);
        }
    }

    private void initImageSlider() {
        sliderArray = new ArrayList<>();
        SliderModel.DataBean.SlidersBean slidersBean = new SliderModel.DataBean.SlidersBean();
        slidersBean.setPhoto(image);
        sliderArray.add(slidersBean);

        mPager.setAdapter(new ImageSliderAdapter(getActivity(), sliderArray, (images, position) -> {

        }));
        circleIndicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = () -> {
            if (currentPage == sliderArray.size()) {
                currentPage = 0;
            }
            mPager.setCurrentItem(currentPage++, true);
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2000, 2500);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMainViewsCallBack = null;
    }

    @Override
    public void onStart() {
        mMainViewsCallBack.showAddFab(false);
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.serToolbarTitle(name);
        super.onStart();
    }


    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                if (listSharedPreference.getBackStack().equals("store"))
                    moveToFragment.moveInMain(new StoreFragment());
                else
                    moveToFragment.moveInMain(new FavouritesFragment());
            }
        });
    }//end back pressed

}
