package com.tkmsoft.taahel.fragments.login;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegSelectorFragment extends Fragment {

    @BindView(R.id.imageView_therapist)
    AppCompatImageView image_therapist;
    @BindView(R.id.imageView_mostfed)
    AppCompatImageView image_mostfed;
    @BindView(R.id.cardView2_mostafed)
    CardView card_mostfed;
    @BindView(R.id.cardView1_doctor)
    CardView card_mo3aleg;

    ListSharedPreference listSharedPreference;

    public RegSelectorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reg_selector, container, false);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        ButterKnife.bind(this, rootView);
        listSharedPreference = new ListSharedPreference(RegSelectorFragment.this.getActivity().getApplicationContext());

        image_therapist.setImageResource(R.drawable.ic_therapist_big);
        image_mostfed.setImageResource(R.drawable.ic_mostfed_big);

        return rootView;
    }

    @OnClick(R.id.cardView1_doctor)
    protected void onCardTherapist_Click() {
        listSharedPreference.setUType("0");
        getFragmentManager().beginTransaction()
                .replace(R.id.login_frame, new RegisterFragment(),"RegisterFragment")
                .commit();
    }

    @OnClick(R.id.cardView2_mostafed)
    protected void onCardMostfed_Click() {
        listSharedPreference.setUType("1");
        getFragmentManager().beginTransaction()
                .replace(R.id.login_frame, new RegisterFragment())
                .commit();
    }

    private void fireBackButtonEvent() {
        ((LoginActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction()
                        .replace(R.id.login_frame, new LoginFragment())
                        .commit();
            }
        });
    }//end back pressed

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        fireBackButtonEvent();
    }
}//end RegSelectorFragment