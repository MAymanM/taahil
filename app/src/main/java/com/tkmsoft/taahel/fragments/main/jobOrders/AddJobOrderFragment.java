package com.tkmsoft.taahel.fragments.main.jobOrders;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.helper.FilePath;
import com.tkmsoft.taahel.helper.FileTypes;
import com.tkmsoft.taahel.helper.InitSpinner;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.defaultRes.DefaultResponse;
import com.tkmsoft.taahel.model.api.defaultRes.Title;
import com.tkmsoft.taahel.model.api.jobs.disabilities.DisabilitiesModel;
import com.tkmsoft.taahel.model.api.jobs.fields.Datum;
import com.tkmsoft.taahel.model.api.jobs.fields.FieldsJobModel;
import com.tkmsoft.taahel.model.api.jobs.fields.Status;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddJobOrderFragment extends Fragment {
    private static final int IMAGE_CODE = 1002, CV_CODE = 1003;
    private MainViewsCallBack mainViewsCallBack;
    @BindViews({R.id.progressBar, R.id.country_spinner_progressBar, R.id.field_spinner_progressBar, R.id.disability_spinner_progressBar})
    List<ProgressBar> progressBarList;
    ProgressBar progressBar, country_spinner_progressBar, field_spinner_progressBar, disability_spinner_progressBar;

    @BindViews({R.id.nameET, R.id.emailET, R.id.phoneET, R.id.descET})
    List<EditText> editTextList;
    EditText nameET, emailET, phoneET, descET;
    @BindView(R.id.cvTV)
    TextView cvTV;

    @BindViews({R.id.spinner_country, R.id.spinner_city, R.id.spinner_field, R.id.spinner_disability})
    List<Spinner> spinnerList;
    Spinner spinner_country, spinner_city, spinner_field, spinner_disability;

    @BindView(R.id.cityLinearLayout)
    LinearLayout cityLinearLayout;

    ListSharedPreference listSharedPreference;
    @BindView(R.id.imageView)
    ImageView imageView;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLinkJobs, apiLinkBase;
    ConnectionDetector connectionDetector;
    private MoveToFragment moveToFragment;
    private InitSpinner initSpinner;

    private ArrayList<String> mCountriesArrayList;
    private ArrayList<String> mFieldArrayList;
    private ArrayList<String> mDisabilityArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mFieldIdList, mDisabilityIdList;
    private List<CountriesModel.DataBean.CountriesBean> dataCountryList;

    private String countryCode, cityCode, fieldCode, disabilityCode;
    private String image_path = "", cv_path = "";
    private FileTypes fileTypes;
    private String photoMimeType, cvMimeType;
    private File file1, file2;

    public AddJobOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }

        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        initSpinner = new InitSpinner(mContext);
        fileTypes = new FileTypes(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiLinkJobs = MyRetrofitClient.getJobs().create(ApiLink.class);
        apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_job_order, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        setViewData();
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);

        initCountrySpinner();

        mFieldArrayList = new ArrayList<>();
        mFieldIdList = new ArrayList<>();
        mFieldArrayList.add(0, getString(R.string.field));
        mFieldIdList.add(0, 0);

        initFieldSpinner();

        mDisabilityArrayList = new ArrayList<>();
        mDisabilityIdList = new ArrayList<>();
        mDisabilityArrayList.add(0, getString(R.string.disability));
        mDisabilityIdList.add(0, 0);

        initDisabilitySpinner();

        serverCountrySpinner();
        serverFieldSpinner();
        serverDisabilitySpinner();

    }

    @OnClick(R.id.addBtn)
    protected void onAddClick() {
        try {
            attemptToAdd();
        } catch (Exception e) {
            Toast.makeText(mContext, "" + getString(R.string.plz_choose_pic), Toast.LENGTH_SHORT).show();
            YoYo.with(Techniques.Shake).playOn(cvTV);
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.imageView)
    protected void onImageClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(mContext,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                openImageIntent();
            }
        } else {
            openImageIntent();
        }
    }

    private void openImageIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, IMAGE_CODE);
    }

    private void openFileIntent() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, CV_CODE);
    }

    @OnClick(R.id.cvTV)
    protected void onFileClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(mContext,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                openFileIntent();
            }
        } else {
            openFileIntent();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                switch (requestCode) {
                    case IMAGE_CODE:
                        image_path = getImagePathFromUri(uri, mContext);
                        file1 = new File(image_path);
                        photoMimeType = fileTypes.getFileType(file1.getAbsolutePath());
                        showImageInView(uri);
                        break;
                    case CV_CODE:
                        cv_path = FilePath.getPath(mContext, uri);
                        file2 = new File(cv_path);
                        cvMimeType = fileTypes.getFileType(file2.getAbsolutePath());
                        cvTV.setText(file2.getName());
                        break;
                }
            }
        }

    }//end onActivityResult

    private void showImageInView(Uri image_path) {
        Picasso.get().load(image_path).resize(500, 300).into(imageView);
    }

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    private void attemptToAdd() {

        if (!countryCode.equals("")) {
            if (!cityCode.equals("")) {
                if (!fieldCode.equals("")) {
                    if (!cv_path.equals("")) {
                        if (!image_path.equals("")) {
                            serverAddJob();
                        } else {
                            serverAddJobNonImage();
                        }
                    } else
                        Toast.makeText(mContext, "" + getString(R.string.cv_required), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(mContext, "" + getString(R.string.enter_field), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();

    }


    private void serverAddJob() {
        progressBar.setVisibility(View.VISIBLE);
        callAddJobOrder().enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(@NonNull Call<DefaultResponse> call, @NonNull Response<DefaultResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.defaultRes.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Title title = status.getTitle();
                            if (title != null) {
                                if (getLanguage().equals("ar"))
                                    Toast.makeText(mContext, "" + status.getTitle().getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + status.getTitle().getEn(), Toast.LENGTH_SHORT).show();

                                moveToFragment.moveInMain(new JobOrdersFragment());
                            }
                        } else {
                            Title title = status.getTitle();
                            if (title != null) {
                                if (getLanguage().equals("ar"))
                                    Toast.makeText(mContext, "" + status.getTitle().getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + status.getTitle().getEn(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DefaultResponse> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void serverAddJobNonImage() {
        progressBar.setVisibility(View.VISIBLE);
        callAddJobOrderNonImage().enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(@NonNull Call<DefaultResponse> call, @NonNull Response<DefaultResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.defaultRes.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Title title = status.getTitle();
                            if (title != null) {
                                if (getLanguage().equals("ar"))
                                    Toast.makeText(mContext, "" + status.getTitle().getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + status.getTitle().getEn(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Title title = status.getTitle();
                            if (title != null) {
                                if (getLanguage().equals("ar"))
                                    Toast.makeText(mContext, "" + status.getTitle().getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + status.getTitle().getEn(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DefaultResponse> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private Call<FieldsJobModel> callGetField() {
        return apiLinkBase.getJobFields();
    }

    private void initFieldSpinner() {

        initSpinner.setSpinner(spinner_field, mFieldArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    fieldCode = String.valueOf(mFieldIdList.get(position));
                } else {
                    fieldCode = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void serverFieldSpinner() {
        field_spinner_progressBar.setVisibility(View.VISIBLE);
        callGetField().enqueue(new Callback<FieldsJobModel>() {
            @Override
            public void onResponse(@NonNull Call<FieldsJobModel> call, @NonNull Response<FieldsJobModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                List<Datum> dataList = response.body().getData();
                                if (dataList != null) {
                                    for (int i = 0; i < dataList.size(); i++) {
                                        if (getLanguage().equals("ar")) {
                                            mFieldArrayList.add(dataList.get(i).getNameAr());
                                        } else {
                                            mFieldArrayList.add(dataList.get(i).getNameEn());
                                        }
                                        mFieldIdList.add(dataList.get(i).getId());
                                    }//end for

                                    initFieldSpinner();
                                }
                            }
                        }
                    }
                }
                field_spinner_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<FieldsJobModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                field_spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end()

    private Call<DisabilitiesModel> callGetDisability() {
        return apiLinkBase.getJobDisability();
    }

    private void initDisabilitySpinner() {

        initSpinner.setSpinner(spinner_disability, mDisabilityArrayList).setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position > 0) {
                            disabilityCode = String.valueOf(mDisabilityIdList.get(position));
                        } else {
                            disabilityCode = "";
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
    }

    private void serverDisabilitySpinner() {
        disability_spinner_progressBar.setVisibility(View.VISIBLE);
        callGetDisability().enqueue(new Callback<DisabilitiesModel>() {
            @Override
            public void onResponse(@NonNull Call<DisabilitiesModel> call, @NonNull Response<DisabilitiesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        com.tkmsoft.taahel.model.api.jobs.disabilities.Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                List<com.tkmsoft.taahel.model.api.jobs.disabilities.Datum> dataList = response.body().getData();
                                if (dataList != null) {
                                    for (int i = 0; i < dataList.size(); i++) {
                                        if (getLanguage().equals("ar")) {
                                            mDisabilityArrayList.add(dataList.get(i).getNameAr());
                                        } else {
                                            mDisabilityArrayList.add(dataList.get(i).getNameEn());
                                        }
                                        mDisabilityIdList.add(dataList.get(i).getId());
                                    }//end for

                                    initDisabilitySpinner();
                                }
                            }
                        }
                    }
                }
                disability_spinner_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DisabilitiesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                disability_spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end()

    private Call<CountriesModel> callGetCountry() {
        return apiLinkBase.getCountries();
    }

    private void serverCountrySpinner() {
        country_spinner_progressBar.setVisibility(View.VISIBLE);
        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CountriesModel.StatusBean status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                CountriesModel.DataBean data = response.body().getData();
                                if (data != null) {
                                    dataCountryList = data.getCountries();
                                    if (dataCountryList != null && !dataCountryList.isEmpty()) {
                                        for (int i = 0; i < dataCountryList.size(); i++) {
                                            mCountriesArrayList.add(dataCountryList.get(i).getName_ar());
                                            mCountriesIdList.add(dataCountryList.get(i).getId());
                                        }//end for

                                        initCountrySpinner();
                                    }
                                }
                            }
                        }
                    }
                }
                country_spinner_progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                country_spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end serverRegister()

    private void initCountrySpinner() {

        initSpinner.setSpinner(spinner_country, mCountriesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(position));
                    showCitySpinner(true);
                    initCitySpinner();
                } else {
                    countryCode = "";
                    cityCode = "";
                    showCitySpinner(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCitySpinner() {
        ArrayList<String> mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);

        for (int i = 0; i < dataCountryList.size(); i++) {
            if (countryCode.equals(String.valueOf(dataCountryList.get(i).getId()))) {
                for (int k = 0; k < dataCountryList.get(i).getCities().size(); k++) {
                    if (getLanguage().equals("ar"))
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_ar());
                    else
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_en());
                    mCitiesIdList.add(dataCountryList.get(i).getCities().get(k).getId());
                }
            }
        }

        initSpinner.setSpinner(spinner_city, mCitiesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    cityCode = String.valueOf(mCitiesIdList.get(position));
                } else {
                    cityCode = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }

    private void showCitySpinner(boolean b) {
        if (b)
            cityLinearLayout.setVisibility(View.VISIBLE);
        else
            cityLinearLayout.setVisibility(View.GONE);
    }

    private String extractStringFromEditTxt(EditText editText) {
        return editText.getText().toString();
    }

    private Call<DefaultResponse> callAddJobOrder() {
        progressBar.setVisibility(View.VISIBLE);
        //photo part

        RequestBody mFile1 = RequestBody.create(MediaType.parse(photoMimeType), file1);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", file1.getName(), mFile1);


        RequestBody mFile2 = RequestBody.create(MediaType.parse(cvMimeType), file2);
        MultipartBody.Part cvPart = MultipartBody.Part.createFormData("cv", file2.getName(), mFile2);

        String name = extractStringFromEditTxt(nameET);
        String email = extractStringFromEditTxt(emailET);
        String phone = extractStringFromEditTxt(phoneET);
        String desc = extractStringFromEditTxt(descET);

        RequestBody namePart = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody emailPart = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody phonePart = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody descPart = RequestBody.create(MediaType.parse("text/plain"), desc);

        RequestBody cityPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody fieldPart = RequestBody.create(MediaType.parse("text/plain"), fieldCode);
        RequestBody disabilityPart = RequestBody.create(MediaType.parse("text/plain"), disabilityCode);

        return apiLinkJobs.addJobOrder(namePart, cityPart, disabilityPart, fieldPart, emailPart,
                phonePart, descPart, cvPart,
                photoPart);
    }

    private Call<DefaultResponse> callAddJobOrderNonImage() {
        progressBar.setVisibility(View.VISIBLE);

        String name = extractStringFromEditTxt(nameET);
        String email = extractStringFromEditTxt(emailET);
        String phone = extractStringFromEditTxt(phoneET);
        String desc = extractStringFromEditTxt(descET);

        return apiLinkJobs.addJobOrderNonImage(name, cityCode, disabilityCode, fieldCode, email, phone, desc);
    }

    @Override
    public void onStart() {
        mainViewsCallBack.serToolbarTitle(getString(R.string.add_job));
        mainViewsCallBack.showAddFab(false);
        super.onStart();
    }

    @Override
    public void onPause() {
        callGetCountry().cancel();
        callGetField().cancel();
        callGetDisability().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    private void setViewData() {
        //progressBars
        progressBar = progressBarList.get(0);
        country_spinner_progressBar = progressBarList.get(1);
        field_spinner_progressBar = progressBarList.get(2);
        disability_spinner_progressBar = progressBarList.get(3);

        //EditText
        nameET = editTextList.get(0);
        emailET = editTextList.get(1);
        phoneET = editTextList.get(2);
        descET = editTextList.get(3);

        //Spinner
        spinner_country = spinnerList.get(0);
        spinner_city = spinnerList.get(1);
        spinner_field = spinnerList.get(2);
        spinner_disability = spinnerList.get(3);
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new JobOrdersFragment());
            }
        });
    }//end back pressed
}