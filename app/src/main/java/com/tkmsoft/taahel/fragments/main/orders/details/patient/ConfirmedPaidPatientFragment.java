package com.tkmsoft.taahel.fragments.main.orders.details.patient;

import android.app.Activity;
import android.app.AlertDialog;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.main.orders.RequestsFragment;
import com.tkmsoft.taahel.fragments.main.orders.SubRequestFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.order.orderDetails.patient.PatientCompleteOrderModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MahmoudAyman on 13/09/2018.
 */
public class ConfirmedPaidPatientFragment extends Fragment {

    @BindView(R.id.codeTV)
    TextView codeTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.timeTV)
    TextView timeTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;
    @BindView(R.id.paymentTV)
    TextView paymentTV;

    @BindView(R.id.completeBtn)
    Button completeBtn;

    String code, date, name, country, city, address, day, time, orderId, description, paymentStatus;

    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private MainViewsCallBack mainViewsCallBack;

    ListSharedPreference listSharedPreference;
    private String rateValue;
    private ApiLink apiLink;
    private String paymentType;
    private MoveToFragment moveToFragment;

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        fireBackButtonEvent();
        try {
            activity = getActivity();
            mainViewsCallBack = (MainViewsCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            code = arguments.getString("code");
            date = arguments.getString("date");
            name = arguments.getString("name");
            country = arguments.getString("country");
            city = arguments.getString("city");
            address = arguments.getString("address");
            day = arguments.getString("day");
            description = arguments.getString("desc");
            paymentStatus = arguments.getString("paymentStatus");
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < arguments.getInt("timeSize"); i++) {
                stringBuilder.append(getArguments().getString("time" + i));
                if (i < arguments.getInt("timeSize") - 1) {
                    stringBuilder.append(" ,\n");
                }
            }
            time = stringBuilder.toString();
            orderId = arguments.getString("orderId");
            String trackerId = arguments.getString("trackerId");
        }
        apiLink = MyRetrofitClient.getOrder().create(ApiLink.class);
    }


    public ConfirmedPaidPatientFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_confirmed_paid_patient, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
//        nameTV.setPaintFlags(nameTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        nameTV.setText(name);
        codeTV.setText(code);
        dateTV.setText(date);
        countryTV.setText(country);
        cityTV.setText(city);
        addressTV.setText(address);
        dayTV.setText(day);
        timeTV.setText(time);
        descriptionTV.setText(description);
        switch (paymentStatus) {
            case "undefined":
                paymentTV.setText(getString(R.string.not_paid_yet));
                break;
            case "cash":
                paymentTV.setText(getString(R.string.cash));
                break;
            case "visa":
                paymentTV.setText(getString(R.string.visa));
                break;
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.confirmed_order));
    }

    EditText reasonET;

    @OnClick(R.id.completeBtn)
    protected void onCompleteBtnClick() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View popUpView = li.inflate(R.layout.complete_order_patient_layout, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // create alert dialog

        alertDialogBuilder.setView(popUpView);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        // set prompts.xml to alertDialog builder
        final ImageButton imageButton = popUpView.findViewById(R.id.close_button);
        reasonET = popUpView.findViewById(R.id.noteET);
        final RatingBar ratingBar = popUpView.findViewById(R.id.rateBar);
        ratingBar.setOnRatingBarChangeListener((ratingBar1, v, b) -> rateValue = String.valueOf(v));
        final Button sendBtn = popUpView.findViewById(R.id.sendBtn);
        final ProgressBar progressBar = popUpView.findViewById(R.id.progressBar);

        imageButton.setOnClickListener
                (view -> alertDialog.dismiss());

        sendBtn.setOnClickListener(view -> {
            if (isTxtEmpty(reasonET))
                serverComplete(progressBar, alertDialog);
            else
                Toast.makeText(mContext, "" + getString(R.string.plz_enter_note), Toast.LENGTH_SHORT).show();
        });
        // show it
        alertDialog.show();
    }

    private boolean isTxtEmpty(EditText etText) {
        return etText.getText().toString().trim().length() != 0;
    }

    private void serverComplete(final ProgressBar progressBar, final AlertDialog alertDialog) {
        progressBar.setVisibility(View.VISIBLE);
        completeOrder().enqueue(new Callback<PatientCompleteOrderModel>() {
            @Override
            public void onResponse(@NonNull Call<PatientCompleteOrderModel> call, @NonNull Response<PatientCompleteOrderModel> response) {
                try {
                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        if (response.body().getStatus() != null)
                            if (response.body().getStatus().getType().equals("success")) {
                                Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                getFragmentManager().beginTransaction()
                                        .replace(R.id.main_frameLayout, new RequestsFragment())
                                        .commit();
                            } else
                                Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception exc) {
                    Log.d(TAG, "onResponse: " + exc.getMessage());
//                    Toast.makeText(mContext, ""+exc.getMessage(), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
                alertDialog.dismiss();
            }

            @Override
            public void onFailure(@NonNull Call<PatientCompleteOrderModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
//                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(mContext, "" + R.string.error_msg_no_internet, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Call<PatientCompleteOrderModel> completeOrder() {
        String reason;
        if (!reasonET.getText().toString().isEmpty())
            reason = reasonET.getText().toString();
        else
            reason = null;
        return apiLink.patientCompleteOrderModel(orderId, rateValue, reason);
    }


    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new SubRequestFragment());
            }
        });
    }//end back pressed
}
