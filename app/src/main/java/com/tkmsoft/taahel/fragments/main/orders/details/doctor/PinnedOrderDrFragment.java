package com.tkmsoft.taahel.fragments.main.orders.details.doctor;


import android.app.Activity;
import android.app.AlertDialog;
import androidx.fragment.app.Fragment;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.google.gson.reflect.TypeToken;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.fragments.main.orders.RequestsFragment;
import com.tkmsoft.taahel.fragments.main.orders.SubRequestFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.interfaces.SendPatientProfData;
import com.tkmsoft.taahel.model.api.order.orderDetails.dr.accept.AcceptDrModel;
import com.tkmsoft.taahel.network.Links;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;

public class PinnedOrderDrFragment extends Fragment {

    private MainViewsCallBack mainViewsCallBack;
    SendPatientProfData sendPatientProfData;
    @BindView(R.id.codeTV)
    TextView codeTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.dayTV)
    TextView dayTV;
    @BindView(R.id.timeTV)
    TextView timeTV;
    @BindView(R.id.descriptionTV)
    TextView descriptionTV;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.paymentTV)
    TextView paymentTV;

    String code, date, name, country, city, address, day, time, orderId;
    String pName, pGender, pBirthDate, pAvatar, pEmail, pPhone, pCountry, pCity, pId, paymentStatus;

    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private String desc;
    private AlertDialog alertDialog;
    private MoveToFragment moveToFragment;

    public PinnedOrderDrFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            sendPatientProfData = (SendPatientProfData) context;
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        ListSharedPreference listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        ConnectionDetector connectionDetector = new ConnectionDetector(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        code = arguments.getString("code");
        date = arguments.getString("date");
        name = arguments.getString("name");
        country = arguments.getString("country");
        city = arguments.getString("city");
        address = arguments.getString("address");
        day = arguments.getString("day");
        desc = arguments.getString("desc");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < arguments.getInt("timeSize"); i++) {
            stringBuilder.append(getArguments().getString("time" + i));
            if (i < arguments.getInt("timeSize") - 1) {
                stringBuilder.append(" ,\n");
            }
        }

        time = stringBuilder.toString();
        orderId = arguments.getString("orderId");

        pGender = arguments.getString("pGender");
        pAvatar = arguments.getString("pAvatar");
        pBirthDate = arguments.getString("pBirthDate");
        pName = arguments.getString("pName");
        pEmail = arguments.getString("pEmail");
        pPhone = arguments.getString("pPhone");
        pCountry = arguments.getString("pCountry");
        pCity = arguments.getString("pCity");
        pId = arguments.getString("pId");

        paymentStatus = arguments.getString("paymentStatus");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_pinned_order_dr, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        nameTV.setPaintFlags(nameTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        nameTV.setText(name);
        codeTV.setText(code);
        dateTV.setText(date);
        countryTV.setText(country);
        cityTV.setText(city);
        addressTV.setText(address);
        dayTV.setText(day);
        timeTV.setText(time);
        descriptionTV.setText(desc);

        switch (paymentStatus) {
            case "undefined":
                paymentTV.setText(getString(R.string.not_paid_yet));
                break;
            case "cash":
                paymentTV.setText(getString(R.string.cash));
                break;
            case "visa":
                paymentTV.setText(getString(R.string.visa));
                break;
        }
    }

    @OnClick(R.id.nameTV)
    protected void onNameTVClick() {
        sendPatientProfData.sendData(pId, pGender, pAvatar, pBirthDate, pName, pEmail, pPhone, pCountry, pCity);
    }

    @OnClick(R.id.acceptBtn)
    protected void onAcceptedBtnClick() {
        serverAccept();
    }

    private void serverAccept() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            AndroidNetworking.get(Links.Order.acceptOrderDr())
                    .addPathParameter("id", orderId) // posting java object
                    .setOkHttpClient(client)
                    .setTag("acceptOrderDr")
                    .build()
                    .getAsParsed(new TypeToken<AcceptDrModel>() {
                    }, new ParsedRequestListener<AcceptDrModel>() {
                        @Override
                        public void onResponse(AcceptDrModel response) {
                            try {
                                if (!response.getStatus().getType().equals("error")) {
                                    Toast.makeText(mContext, "" + response.getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                    getFragmentManager().beginTransaction()
                                            .replace(R.id.main_frameLayout, new RequestsFragment())
                                            .commit();
                                } else
                                    Toast.makeText(mContext, "" + response.getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            } catch (Exception exc) {
                                Log.d(TAG, "onResponse: " + exc.getMessage());
                            }
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(ANError error) {
                            if (error.getErrorCode() != 0) {
                                Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                                Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            } else {
                                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            }
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception exc) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(mContext, "failed, try to use another number", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.refuseBtn)
    protected void onRefuseClick() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        final View popUpView = li.inflate(R.layout.refuse_order_layout, null);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        // create alert dialog

        alertDialogBuilder.setView(popUpView);
        alertDialog = alertDialogBuilder.create();
        // set prompts.xml to alertdialog builder
        final EditText reasonET = popUpView.findViewById(R.id.reasonET);
        ImageButton imageButton = popUpView.findViewById(R.id.close_button);
        Button sendBtn = popUpView.findViewById(R.id.sendBtn);

        imageButton.setOnClickListener
                (view -> alertDialog.dismiss());

        sendBtn.setOnClickListener(view -> {
            if (isTxtEmpty(reasonET))
                serverRefuse(reasonET.getText().toString());
            else
                reasonET.setError(getString(R.string.plz_enter_reason));
        });
        // show it
        alertDialog.show();
    }


    private boolean isTxtEmpty(EditText etText) {
        return etText.getText().toString().trim().length() != 0;
    }

    private void serverRefuse(String reason) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            AndroidNetworking.post(Links.Order.refuseOrderDr())
                    .addPathParameter("id", orderId)     //posting java object
                    .addBodyParameter("reason", reason)
                    .setOkHttpClient(client)
                    .setTag("refuseOrderDr")
                    .build()
                    .getAsParsed(new TypeToken<AcceptDrModel>() {
                    }, new ParsedRequestListener<AcceptDrModel>() {
                        @Override
                        public void onResponse(AcceptDrModel response) {
                            try {
                                if (!response.getStatus().getType().equals("error")) {
                                    Toast.makeText(mContext, "" + response.getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                                    moveToFragment.moveInMain(new RequestsFragment());
                                    alertDialog.dismiss();
                                } else
                                    Toast.makeText(mContext, "" + response.getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            } catch (Exception exc) {
                                Log.d(TAG, "onResponse: " + exc.getMessage());
                            }
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(ANError error) {
                            if (error.getErrorCode() != 0) {
                                Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                                Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            } else {
                                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            }
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception exc) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(mContext, "failed, try to use another number", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.pinned_order));
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {

                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, new SubRequestFragment())
                        .commit();
            }
        });
    }//end back pressed

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }


}
