package com.tkmsoft.taahel.fragments.main.profile.doctor;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.reflect.TypeToken;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.StringAdapter;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.profile.updatedoctor.UpdateDoctorModel;
import com.tkmsoft.taahel.model.api.specialization.Specialization;
import com.tkmsoft.taahel.model.api.specialization.SpecializationsModel;
import com.tkmsoft.taahel.model.body.UpdateDoctorBody;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.model.spinners.CurrencyModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.Links;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditDoctorFragment extends Fragment {

    MainViewsCallBack mainViewsCallBack;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.refreshBtn)
    ImageButton refreshBtn;
    @BindView(R.id.phoneCode_spinner)
    Spinner phoneCode_spinner;
    @BindView(R.id.mobileET)
    EditText mobileET;
    @BindView(R.id.nameET)
    EditText nameET;
    @BindView(R.id.userNameET)
    EditText userNameET;
    @BindView(R.id.emailET)
    EditText emailET;
    @BindView(R.id.educationET)
    EditText educationET;
    @BindView(R.id.experienceET)
    EditText experienceET;
    @BindView(R.id.aboutET)
    EditText aboutET;
    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.old_passET)
    EditText old_passET;
    @BindView(R.id.new_passET)
    EditText new_passET;
    @BindView(R.id.new_pass_confirmET)
    EditText new_pass_confirmET;
    @BindView(R.id.priceET)
    EditText priceET;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.updateBtn)
    Button updateBtn;
    @BindView(R.id.spinner_currency)
    Spinner spinner_currency;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.maleRB)
    RadioButton maleRB;
    @BindView(R.id.femaleRB)
    RadioButton femaleRB;
    @BindView(R.id.deptTV)
    TextView deptTv;
    @BindView(R.id.progressUpdate)
    ProgressBar progressUpdate;
    ListSharedPreference listSharedPreference;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    private ArrayList<String> mCitiesArrayList, mCountriesArrayList, mCurrencyArrayList, mPhoneCodeList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mCurrencyIdList;
    String countryCode, cityCode;
    private String currencyCode;
    List<String> specialities;
    private String phoneCode;
    private View rootView;
    private List<String> uSpecializationsList;


    public EditDoctorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        fireBackButtonEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_edit_doctor, container, false);
        listSharedPreference = new ListSharedPreference(mContext);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        initCountrySpinner(0);
        mPhoneCodeList = new ArrayList<>();
        mPhoneCodeList.add(0, getString(R.string.code));
        initPhoneCodeSpinner();
        mCurrencyArrayList = new ArrayList<>();
        mCurrencyIdList = new ArrayList<>();
        mCurrencyArrayList.add(0, getString(R.string.currency));
        mCurrencyIdList.add(0, 0);
        initCurrencySpinner();
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getUserInfo();
    }

    private void initCountrySpinner(final Integer cityId) {
        spinner_country.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCountriesArrayList);
        spinner_country.setAdapter(adapter);
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(i));
                    cityRelative.setVisibility(View.VISIBLE);
                    serverCitiesSpinner(i, cityId);
                } else
                    cityRelative.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinner_country);
            }
        });
    }

    private void initCitiesSpinner() {
        spinner_city.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCitiesArrayList);
        spinner_city.setAdapter(adapter);
        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cityCode = String.valueOf(mCitiesIdList.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void serverCitiesSpinner(final int countryId, final Integer cityId) {
        mCitiesArrayList = new ArrayList<>();
        mCitiesIdList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList.add(0, 0);
        initCitiesSpinner();
        spinner_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> citiesCall = retrofit.getCountries();
        citiesCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {

                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                            }
                            Log.d(TAG, "cityId: " + mCitiesIdList);
                        } else {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                        }//end else
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                    initCitiesSpinner();
                }
                for (int j = 0; j < mCitiesIdList.size(); j++) {
                    if (mCitiesIdList.get(j).equals(cityId)) {
                        spinner_city.setSelection(j);
                        break;
                    }
                }
                spinner_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void serverCountrySpinner(final Integer countryId, final Integer cityId, final String phoneKey) {
        progressBar.setVisibility(View.VISIBLE);
        //important to not repeating the items in spinner
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);
        mPhoneCodeList = new ArrayList<>();
        mPhoneCodeList.add(0, getString(R.string.code));

        try {
            ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
            Call<CountriesModel> registerCall = retrofit.getCountries();
            registerCall.enqueue(new Callback<CountriesModel>() {
                @Override
                public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                    progressBar.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            //Country
                            for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                                mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                                mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                            }
                            //Phone Code
                            for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                                mPhoneCodeList.add(response.body().getData().getCountries().get(i).getCode());
                            }
                            Log.d(TAG, "" + mCountriesIdList);
                            initCountrySpinner(cityId);
                            initPhoneCodeSpinner();
                        }
                    } else {
                        assert response.body() != null;
                        Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                    for (int j = 0; j < mCountriesIdList.size(); j++) {
                        if (mCountriesIdList.get(j).equals(countryId)) {
                            spinner_country.setSelection(j);
                            //return to stop looping and cause index exception
                            break;
                        }
                    }
                    for (int j = 0; j < mPhoneCodeList.size(); j++) {
                        if (mPhoneCodeList.get(j).equals(phoneKey)) {
                            phoneCode_spinner.setSelection(j);
                            //return to stop looping and cause index exception
                            break;
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            });
        } catch (Exception exc) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(mContext, "" + exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }//end serverRegister()

    private void initPhoneCodeSpinner() {
        phoneCode_spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity(), mPhoneCodeList);
        phoneCode_spinner.setAdapter(adapter);

        phoneCode_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    if (listSharedPreference.getLanguage().equals("ar")) {
                        String string = mPhoneCodeList.get(i);
                        String[] parts = string.split("\\+");
                        String part12 = parts[1];
                        phoneCode = "+" + part12;
                        //Toast.makeText(mContext, "" + phoneCode, Toast.LENGTH_SHORT).show();
                    } else {
                        phoneCode = mPhoneCodeList.get(i);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void serverCurrencySpinner(final Integer currencyId) {
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CurrencyModel> currencyCall = retrofit.getCurrency();
        currencyCall.enqueue(new Callback<CurrencyModel>() {
            @Override
            public void onResponse(@NonNull Call<CurrencyModel> call, @NonNull Response<CurrencyModel> response) {
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCurrencies().size(); i++) {
                                mCurrencyArrayList.add(response.body().getData().getCurrencies().get(i).getName_ar());
                                mCurrencyIdList.add(response.body().getData().getCurrencies().get(i).getId());
                                initCurrencySpinner();
                            }
                        } else {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                        }//end else
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
                for (int j = 0; j < mCurrencyIdList.size(); j++) {
                    if (mCurrencyIdList.get(j).equals(currencyId)) {
                        spinner_currency.setSelection(j);
                        //return to stop looping and cause index exception
                        break;
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CurrencyModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCurrencySpinner()

    private void initCurrencySpinner() {

        spinner_currency.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(getActivity().getApplicationContext(), mCurrencyArrayList);
        spinner_currency.setAdapter(adapter);
        spinner_currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                currencyCode = String.valueOf(mCurrencyIdList.get(i));
                // Toast.makeText(mContext, "" + currencyCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void getUserInfo() {
        specialities = new ArrayList<>();
        String uName = listSharedPreference.getUName();
        String uEmail = listSharedPreference.getUEmail();
        String uEducation = listSharedPreference.getUEducation();
        uSpecializationsList = listSharedPreference.getUSpecialists();
        Log.d(TAG, "getUserInfo: "+uSpecializationsList);
        String uExp = listSharedPreference.getUExperience();
        String uPrice = listSharedPreference.getUPrice();
        String uGender = listSharedPreference.getUGender();
        String uAbout = listSharedPreference.getUAbout();
        String uPhone = listSharedPreference.getUPhone();
        String uPhoneCode = listSharedPreference.getUPhoneKey();
        int countryId = listSharedPreference.getUCountryId();
        int cityId = listSharedPreference.getUCityId();
        int uCurrencyId = listSharedPreference.getUCurrencyId();
        if (uGender.equals("0")) {
            maleRB.setChecked(true);
        } else
            femaleRB.setChecked(true);
        nameET.setText(uName);
        emailET.setText(uEmail);
        educationET.setText(uEducation);
        experienceET.setText(uExp);
        aboutET.setText(uAbout);
        mobileET.setText(uPhone);
        priceET.setText(uPrice);

        serverCountrySpinner(countryId, cityId, uPhoneCode);
        serverCurrencySpinner(uCurrencyId);


        serverSpecialization();
    }

    private void serverSpecialization() {
        try {
            progressBar.setVisibility(View.VISIBLE);

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            AndroidNetworking.get(Links.Base.getSpecialization())
                    .setTag("getSpecialization")
                    .setOkHttpClient(client)
                    .build()
                    .getAsParsed(new TypeToken<SpecializationsModel>() {
                    }, new ParsedRequestListener<SpecializationsModel>() {
                        @Override
                        public void onResponse(SpecializationsModel response) {
                            try {
                                for (int i = 0; i < response.getData().getSpecializations().size(); i++) {
                                    if (i < uSpecializationsList.size()) {
                                        listSharedPreference.setDeptCheck("checkBox:" + i, true);
                                        specialities.add(i, String.valueOf(i + 1));
                                    } else
                                        listSharedPreference.setDeptCheck("checkBox:" + i, false);
                                }
                                initAdapter(response.getData().getSpecializations());
                            } catch (Exception exc) {
                                Toast.makeText(mContext, "" + exc, Toast.LENGTH_SHORT).show();
                            }
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(ANError error) {
                            if (error.getErrorCode() != 0) {
                                Log.d(TAG, "onError errorCode : " + error.getErrorCode());
                                Log.d(TAG, "onError errorBody : " + error.getErrorBody());
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            } else {
                                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                                Log.d(TAG, "onError errorDetail : " + error.getErrorDetail());
                            }
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception exc) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(mContext, getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void initAdapter(List<Specialization> specializations) {
        StringAdapter stringAdapter = new StringAdapter(getActivity().getApplicationContext(), specializations,
                new StringAdapter.ListAllListeners() {
                    @Override
                    public void onCheck(Specialization stringModel, int position) {
                        if (!specialities.contains(String.valueOf(stringModel.getId())))
                            specialities.add(String.valueOf(stringModel.getId()));
//                        Toast.makeText(mContext, "" + specialities, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onUnCheck(Specialization stringModel, int position) {
                        specialities.remove(String.valueOf(stringModel.getId()));
//                        Toast.makeText(mContext, "" + specialities, Toast.LENGTH_SHORT).show();
                    }
                });
        recyclerView.setAdapter(stringAdapter);
        stringAdapter.notifyDataSetChanged();
//        Toast.makeText(mContext, "" + specialities, Toast.LENGTH_SHORT).show();
//        Toast.makeText(mContext, "u: " + uSpecializationsList, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.updateBtn)
    protected void updateBtnOnClick() {
        serverUpdate();
    }

    private void serverUpdate() {
        progressUpdate.setVisibility(View.VISIBLE);
        UpdateDoctorBody updateDoctorBody = new UpdateDoctorBody();
        updateDoctorBody.setName(nameET.getText().toString());
        updateDoctorBody.setUsername(nameET.getText().toString()+"username");
        updateDoctorBody.setCity_id(cityCode);
        updateDoctorBody.setEmail(emailET.getText().toString());
        updateDoctorBody.setPhone(mobileET.getText().toString());
        updateDoctorBody.setPhoneKey(phoneCode);
        updateDoctorBody.setGender(getGender());
        updateDoctorBody.setLat("123456");
        updateDoctorBody.setxLong("123456");
        updateDoctorBody.setOld_password(old_passET.getText().toString());
        updateDoctorBody.setPassword(new_passET.getText().toString());
        updateDoctorBody.setPassword_confirmation(new_pass_confirmET.getText().toString());
        updateDoctorBody.setPrice(priceET.getText().toString());
        updateDoctorBody.setCurrency_id(currencyCode);
        updateDoctorBody.setExperiences(experienceET.getText().toString());
        updateDoctorBody.setEducation(educationET.getText().toString());
        updateDoctorBody.setAbout(aboutET.getText().toString());
        updateDoctorBody.setSpecialtiesList(specialities);
        ApiLink retrofit = MyRetrofitClient.getProfile().create(ApiLink.class);
        Call<UpdateDoctorModel> currencyCall = retrofit.updateDoctor(listSharedPreference.getToken(), updateDoctorBody);
        currencyCall.enqueue(new Callback<UpdateDoctorModel>() {
            @Override
            public void onResponse(@NonNull Call<UpdateDoctorModel> call, @NonNull Response<UpdateDoctorModel> response) {
                if (response.isSuccessful()) {
                    try {
                        if (response.body().getStatus().getType().equals("success")) {

                            Toast.makeText(mContext, "" + response.body().getStatus().getTitle()
                                    , Toast.LENGTH_SHORT).show();

                            saveUserData(response.body().getData().getMemberData().getApiToken(),
                                    response.body().getData().getMemberData().getName(),
                                    response.body().getData().getMemberData().getUsername(),
                                    response.body().getData().getMemberData().getEmail(),
                                    response.body().getData().getMemberData().getAvatar(),
                                    response.body().getData().getMemberData().getCity().getNameAr(),
                                    response.body().getData().getMemberData().getCountry().getNameAr(),
                                    response.body().getData().getMemberData().getGender(),
                                    response.body().getData().getMemberData().getPhone(),
                                    response.body().getData().getMemberData().getPhoneKey(),
                                    response.body().getData().getMemberData().getCountry().getId(),
                                    response.body().getData().getMemberData().getCity().getId());

                            saveDrData(response.body().getData().getMemberData().getDoctor().getAbout(),
                                    response.body().getData().getMemberData().getDoctor().getEducation(),
                                    response.body().getData().getMemberData().getDoctor().getExperiences(),
                                    response.body().getData().getMemberData().getDoctor().getLicense(),
                                    response.body().getData().getMemberData().getDoctor().getPrice(),
                                    response.body().getData().getMemberData().getSpecializations(),
                                    response.body().getData().getMemberData().getCurrency().getNameAr(),
                                    response.body().getData().getMemberData().getCurrency().getId());

                            getFragmentManager().beginTransaction()
                                    .replace(R.id.main_frameLayout, new HomeFragment())
                                    .commit();
                        } else
                            Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
                progressUpdate.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<UpdateDoctorModel> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                progressUpdate.setVisibility(View.GONE);
            }
        });
    }

    private void saveUserData(String token,String name, String userName, String email, String avatar,
                              String city, String country, String gender,
                              String phone, String phoneKey, Integer countryId, Integer cityId) {
        listSharedPreference.setToken(token);
        listSharedPreference.setUName(name);
        listSharedPreference.setUUserName(userName);
        listSharedPreference.setUEmail(email);
        listSharedPreference.setUAvatar(avatar);
        listSharedPreference.setUCity(city);
        listSharedPreference.setUCountry(country);
        listSharedPreference.setUGender(gender);
        listSharedPreference.setUPhone(phone);
        listSharedPreference.setUPhoneKey(phoneKey);
        listSharedPreference.setUCountryId(countryId);
        listSharedPreference.setUCityId(cityId);
    }

    private void saveDrData(String about, String education, String experiences, String license,
                            String price, List<com.tkmsoft.taahel.model.api.profile.updatedoctor.Specialization> specializations, String currency, Integer currencyId) {
        listSharedPreference.setUAbout(about);
        listSharedPreference.setUEducation(education);
        listSharedPreference.setUExperience(experiences);
        listSharedPreference.setULicense(license);
        listSharedPreference.setUPrice(price);
        List<String> specialistsStringList = new ArrayList<>();
        for (int i = 0; i < specializations.size(); i++) {
            specialistsStringList.add(specializations.get(i).getNameAr());
        }
        listSharedPreference.setUSpecialists(specialistsStringList);
        listSharedPreference.setUCurrency(currency);
        listSharedPreference.setUCurrencyId(currencyId);
    }

    @OnClick(R.id.deptTV)
    protected void onDeptClick() {
        if (recyclerView.getVisibility() == View.GONE) {
            recyclerView.setVisibility(View.VISIBLE);
            if (listSharedPreference.getLanguage().equals("en"))
                deptTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_accent_up_24dp_wrapper, 0);
            else
                deptTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_accent_up_24dp_wrapper, 0, 0, 0);
        } else {
            recyclerView.setVisibility(View.GONE);
            if (listSharedPreference.getLanguage().equals("en"))
                deptTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_accent_arrow_down_24dp_wrapper, 0);
            else
                deptTv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_accent_arrow_down_24dp_wrapper, 0, 0, 0);
        }
    }

    private String getGender() {
        int genderId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = rootView.findViewById(genderId);
        switch (radioButton.getId()) {
            case R.id.maleRB:
                return "0";
            case R.id.femaleRB:
                return "1";
            default:
                return "0";
        }
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.main_frameLayout, new HomeFragment())
                            .commit();
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed
}
