package com.tkmsoft.taahel.fragments.main.centers;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MapsActivity;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.helper.MyPermissionManager;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.drugs.AddCenterModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static androidx.core.content.PermissionChecker.PERMISSION_GRANTED;

public class AddCentersFrag extends Fragment {

    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.cityRelLayout)
    RelativeLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.addBtn)
    Button addBtn;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.name_arET)
    EditText name_arET;
    @BindView(R.id.name_enET)
    EditText name_enET;
    @BindView(R.id.address_ar_ET)
    EditText address_ar_ET;
    @BindView(R.id.address_en_ET)
    EditText address_en_ET;
    @BindView(R.id.staticPhone_ET)
    EditText staticPhone_ET;
    @BindView(R.id.mobileET)
    EditText mobileET;
    @BindView(R.id.linkET)
    EditText linkET;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    private View rootView;
    @BindView(R.id.locationTV)
    TextView locationTV;

    private MainViewsCallBack mainViewsCallBack;
    private ListSharedPreference listSharedPreference;
    private ArrayList<String> mCountriesArrayList, mCitiesArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList;
    private final int IMAGE_CODE = 2001;
    private String image_path = "";
    private String countryCode, cityCode;
    private FragmentActivity mContext;
    private MoveToFragment moveToFragment;
    private MyPermissionManager myPermissionManager;
    private String TAG = getClass().getName();
    private double lat = 0.0, longX = 0.0;
    private ApiLink apiLinkDrugs;

    public AddCentersFrag() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        myPermissionManager = new MyPermissionManager(mContext, TAG);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkDrugs = MyRetrofitClient.getDrug().create(ApiLink.class);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_centers, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        mCountriesArrayList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList = new ArrayList<>();
        mCountriesIdList.add(0, 0);
        serverCountrySpinner();
        initCountrySpinner();
    }

    private Integer getTypeId() {

        int typeId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = rootView.findViewById(typeId);
        switch (radioButton.getId()) {
            case R.id.governmentalRB:
                return 1;
            case R.id.privateRB:
                return 2;
            default:
                return null;
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    ///////////////Spinners///////////////
    private void serverCountrySpinner() {
        initCountrySpinner();
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> registerCall = retrofit.getCountries();
        registerCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        for (int i = 0; i < response.body().getData().getCountries().size(); i++) {
                            mCountriesArrayList.add(response.body().getData().getCountries().get(i).getName_ar());
                            mCountriesIdList.add(response.body().getData().getCountries().get(i).getId());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }//end serverCountry()

    private void serverCitiesSpinner(final int countryId) {
        initCitiesSpinner();
        spinner_progressBar.setVisibility(View.VISIBLE);
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        Call<CountriesModel> citiesCall = retrofit.getCountries();
        citiesCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                spinner_progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    try {
                        if (response.body() != null) {
                            for (int i = 0; i < response.body().getData().getCountries().get(countryId - 1).getCities().size(); i++) {
                                mCitiesArrayList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getName_ar());
                                mCitiesIdList.add(response.body().getData().getCountries().get(countryId - 1).getCities().get(i).getId());
                            }
                        }
                    } catch (Exception exc) {
                        Toast.makeText(getActivity(), "" + exc, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                spinner_progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }//end serverCitiesSpinner()

    private void initCountrySpinner() {
        spinner_country.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(mContext, mCountriesArrayList);
        spinner_country.setAdapter(adapter);
        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(i));
                    cityRelative.setVisibility(View.VISIBLE);
                    serverCitiesSpinner(i);
                } else {
                    cityRelative.setVisibility(View.GONE);
                    countryCode = null;
                    cityCode = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(spinner_country);
            }
        });
    }

    private void initCitiesSpinner() {
        mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);

        spinner_city.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        final DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(mContext, mCitiesArrayList);
        spinner_city.setAdapter(adapter);

        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    cityCode = String.valueOf(mCitiesIdList.get(i));
                else
                    cityCode = null;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                YoYo.with(Techniques.Shake).playOn(cityRelative);
            }
        });
    }
    ///////////////////////////////////////////////////////

    private void serverAddCenter() {
        progressBar.setVisibility(View.VISIBLE);

        registerCall().enqueue(new Callback<AddCenterModel>() {
            @Override
            public void onResponse(@NonNull Call<AddCenterModel> call, @NonNull Response<AddCenterModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            moveToFragment.moveInMain(new CenterGovernmentFragment());
                        } else
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddCenterModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private Call<AddCenterModel> registerCall() {
        File file1;
        file1 = new File(image_path);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);

        String token = listSharedPreference.getToken();
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", file1.getName(), mFile1);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(getTypeId()));
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody address_arPart = RequestBody.create(MediaType.parse("text/plain"), address_ar_ET.getText().toString());
        RequestBody address_enPart = RequestBody.create(MediaType.parse("text/plain"), address_en_ET.getText().toString());
        RequestBody linkPart = RequestBody.create(MediaType.parse("text/plain"), linkET.getText().toString());
        RequestBody static_phonePart = RequestBody.create(MediaType.parse("text/plain"), staticPhone_ET.getText().toString());
        RequestBody phonePart = RequestBody.create(MediaType.parse("text/plain"), mobileET.getText().toString());
        RequestBody latPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(lat));
        RequestBody longXPart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longX));

        return apiLinkDrugs.addCenter(token,
                photoPart,
                name_arPart, name_enPart, type_idPart,
                city_idPart, address_arPart, address_enPart,
                linkPart, static_phonePart, phonePart,
                latPart, longXPart);
    }

    ///////////////On Click///////////////
    @OnClick(R.id.imageView)
    void imageOnClick() {
        checkForPermission();
    }

    @OnClick(R.id.locationTV)
    void onLocationTVClick() {
        startActivity(new Intent(MyApp.getContext().getApplicationContext(), MapsActivity.class));
    }

    private void checkForPermission() {
        myPermissionManager.ask(new String[]{READ_EXTERNAL_STORAGE});
        requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, 112);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult: ");
        switch (requestCode) {
            case 112:
                if (grantResults.length > 0 &&
                        grantResults[0] == PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(intent, IMAGE_CODE);
                } else
                    Log.d(TAG, "onRequestPermissionsResult: denied");
                break;
        }
    }


    @OnClick(R.id.addBtn)
    protected void onNextBtnClick() {
        if (!image_path.equals(""))
            if (countryCode != null)
                if (cityCode != null)
                    if (lat != 0.0 && longX != 0.0)
                        serverAddCenter();
                    else
                        Toast.makeText(mContext, "" + getString(R.string.choose_location), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + getString(R.string.plz_choose_pic), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, getActivity());
            showImageInView(uri);
        }//end if check for data

    }//end onActivityResult

    private void showImageInView(Uri image_path) {
        Picasso.get().load(image_path).into(imageView);
    }

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.add_center));
        if (listSharedPreference.getLat() != 0.0) {
            if (listSharedPreference.getLanguage().equals("ar"))
                locationTV.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_location_on_green_700_24dp, 0);
            else
                locationTV.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_location_on_green_700_24dp, 0, 0, 0);
        }
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new CenterGovernmentFragment());
            }
        });
    }//end back pressed
}
