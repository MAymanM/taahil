package com.tkmsoft.taahel.fragments.main.adds.store;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.addings.store.SubMyAddsStoreAdapter;
import com.tkmsoft.taahel.fragments.main.adds.MyAddsFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.adds.store.Data;
import com.tkmsoft.taahel.model.api.adds.store.Datum;
import com.tkmsoft.taahel.model.api.adds.store.MyStoreModel;
import com.tkmsoft.taahel.model.api.adds.store.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by MahmoudAyman on 28/01/2019.
 */
public class SubMyAddsStoreFragment extends Fragment implements SubMyAddsStoreAdapter.ListAllClickListeners {
    private MainViewsCallBack mainViewsCallBack;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    ListSharedPreference listSharedPreference;

    private int currentPage;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLink;
    ConnectionDetector connectionDetector;
    private boolean isLastPage = false;
    SubMyAddsStoreAdapter subActivityAdapter;
    MoveToFragment moveToFragment;

    public SubMyAddsStoreFragment() {

    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        fireBackButtonEvent();
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        connectionDetector = new ConnectionDetector(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.getProfile().create(ApiLink.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sub_my_adds_store, container, false);
        ButterKnife.bind(this, rootView);
        currentPage = 1;
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initPullRefreshLayout();
        initAdapter();
        initRecyclerView();
        if (connectionDetector.isConnectingToInternet())
            loadFirstPage();
        else
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
    }


    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(subActivityAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void initAdapter() {
        subActivityAdapter = new SubMyAddsStoreAdapter(mContext, new ArrayList<>(), this);
    }


    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);
        Log.d(TAG, "loadFirstPage: ");
        // To ensure list is visible when retry button in error view is clicked

        callGetMyCenters().enqueue(new Callback<MyStoreModel>() {
            @Override
            public void onResponse(@NonNull Call<MyStoreModel> call, @NonNull Response<MyStoreModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> datumList = data.getData();

                                if (datumList != null) {
                                    if (!datumList.isEmpty()) {
                                        List<Datum> result = new ArrayList<>();
                                        for (int i = 0; i < datumList.size(); i++) {
                                            if (datumList.get(i).getApprove().equals("1"))
                                                result.add(datumList.get(i));
                                        }
                                        subActivityAdapter.replaceData(result);
                                    }
                                } else
                                    isLastPage = true;
                            } else
                                isLastPage = true;
                        } else {
                            isLastPage = true;
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                    pullRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyStoreModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);
        callGetMyCenters().enqueue(new Callback<MyStoreModel>() {
            @Override
            public void onResponse(@NonNull Call<MyStoreModel> call, @NonNull Response<MyStoreModel> response) {

                assert response.body() != null;
                Status status = response.body().getStatus();
                if (status != null) {
                    if (status.getType().equals("success")) {
                        Data data = response.body().getData();
                        if (data != null) {
                            List<Datum> datumList = data.getData();
                            if (datumList != null) {
                                if (!datumList.isEmpty()) {
                                    List<Datum> result = new ArrayList<>();
                                    for (int i = 0; i < datumList.size(); i++) {
                                        if (datumList.get(i).getApprove().equals("1"))
                                            result.add(datumList.get(i));
                                    }
                                    subActivityAdapter.updateData(result);
                                }
                            } else
                                isLastPage = true;
                        } else
                            isLastPage = true;
                    } else {
                        isLastPage = true;
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<MyStoreModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private Call<MyStoreModel> callGetMyCenters() {
        return apiLink.getMyAddsStore(listSharedPreference.getToken(),
                listSharedPreference.getAddsType(),
                currentPage);
    }


    private void initPullRefreshLayout() {
        pullRefreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefreshLayout.setOnRefreshListener(() -> loadFirstPage());
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.serToolbarTitle(getString(R.string.store));
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.showAddFab(false);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }


    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new MyAddsFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed


    @Override
    public void onItemClick(Datum data, int pos) {
        Fragment fragment = new EditStoreFragment();
        Bundle bundle = new Bundle();
        bundle.putString("pId", String.valueOf(data.getId()));
        bundle.putString("photo1", data.getMainPhoto());
        bundle.putString("photo2", data.getSecondPhoto());
        bundle.putString("photo3", data.getThirdPhoto());
        bundle.putString("nameAr", data.getNameAr());
        bundle.putString("nameEn", data.getNameEn());
        bundle.putInt("typeId", data.getCategory().getId());
        bundle.putString("descAr", data.getDescAr());
        bundle.putString("descEn", data.getDescEn());
        bundle.putInt("countryId", data.getCountry().getId());
        bundle.putInt("cityId", data.getCity().getId());
        bundle.putInt("currencyId", Integer.parseInt(data.getCurrencyId()));
        bundle.putString("price", data.getPrice());
        bundle.putString("ageFrom", data.getAgeFrom());
        bundle.putString("ageTo", data.getAgeTo());
        bundle.putString("size", data.getSize());
        fragment.setArguments(bundle);

        moveToFragment.moveInMain(fragment);
    }
}
