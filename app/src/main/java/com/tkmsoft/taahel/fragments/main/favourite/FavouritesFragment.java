package com.tkmsoft.taahel.fragments.main.favourite;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.favs.FavouritesAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.fragments.main.store.StoreDetail;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.favourite.change.ChangeFavModel;
import com.tkmsoft.taahel.model.api.favourite.view.Comment;
import com.tkmsoft.taahel.model.api.favourite.view.Data;
import com.tkmsoft.taahel.model.api.favourite.view.Datum;
import com.tkmsoft.taahel.model.api.favourite.view.FavouritesModel;
import com.tkmsoft.taahel.model.api.favourite.view.Medical;
import com.tkmsoft.taahel.model.api.favourite.view.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.PaginationScrollListener;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavouritesFragment extends Fragment implements FavouritesAdapter.ListAllListeners {

    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    FavouritesAdapter favAdapter;
    private MainViewsCallBack mMainViewsCallBack;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private MoveToFragment moveToFragment;
    private FragmentActivity mContext;
    private ListSharedPreference listSharedPreference;
    private ApiLink apiLinkBase,apiLinkStore;
    private boolean isLastPage = false;
    private int currentPage = 1;
    private String mLanguage;

    public FavouritesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        try {
            mMainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);
        apiLinkStore = MyRetrofitClient.getStore().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_favourites, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
        loadFirstPage();
        initPullRefreshLayout();
        mLanguage = listSharedPreference.getLanguage();
    }

    private void initPullRefreshLayout() {
        pullRefreshLayout.setOnRefreshListener(() -> moveToFragment.moveInMain(new FavouritesFragment()));
    }

    private void initRecyclerView() {
        favAdapter = new FavouritesAdapter(new ArrayList<>(), this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 1);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(favAdapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    private void loadFirstPage() {
        progressBar.setVisibility(View.VISIBLE);

        callGetFav().enqueue(new Callback<FavouritesModel>() {
            @Override
            public void onResponse(@NonNull Call<FavouritesModel> call, @NonNull Response<FavouritesModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> datumList = data.getData();
                                if (datumList != null) {
                                    favAdapter.replaceData(datumList);
                                }
                            }else
                                isLastPage = true;
                        } else
                            isLastPage = true;
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<FavouritesModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });

    }//end server

    private void loadNextPage() {
        callGetFav().enqueue(new Callback<FavouritesModel>() {
            @Override
            public void onResponse(@NonNull Call<FavouritesModel> call, @NonNull Response<FavouritesModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Datum> datumList = data.getData();
                                if (datumList != null) {
                                    favAdapter.updateData(datumList);
                                }
                            }else
                                isLastPage = true;
                        } else
                            isLastPage = true;
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<FavouritesModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    private Call<FavouritesModel> callGetFav() {
        return apiLinkBase.getWishLists(listSharedPreference.getToken(), currentPage);
    }

    @Override
    public void onPause() {
        callGetFav().cancel();
        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        mMainViewsCallBack.showAddFab(false);
        mMainViewsCallBack.showFilterBtn(false);
        mMainViewsCallBack.serToolbarTitle(getString(R.string.my_favourite));
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
            }
        });
    }//end back pressed

    @Override
    public void onItemViewClick(Medical datumList, int adapterPosition) {
        listSharedPreference.setBackStack("fav");
        Fragment fragment = new StoreDetail();
        Bundle bundle = new Bundle();
        bundle.putInt("pId", datumList.getId());
        bundle.putString("image", datumList.getMainPhoto());
        bundle.putString("price", datumList.getPrice());
        if (mLanguage.equals("ar")) {
            bundle.putString("name", datumList.getNameAr());
            bundle.putString("currency", datumList.getCurrency().getNameAr());
            bundle.putString("country", datumList.getCountry().getNameAr());
            bundle.putString("city", datumList.getCity().getNameAr());
            bundle.putString("dept", datumList.getCategory().getNameAr());
            bundle.putString("desc", datumList.getDescAr());
        } else {
            bundle.putString("name", datumList.getNameEn());
            bundle.putString("currency", datumList.getCurrency().getNameEn());
            bundle.putString("country", datumList.getCountry().getNameEn());
            bundle.putString("city", datumList.getCity().getNameEn());
            bundle.putString("dept", datumList.getCategory().getNameEn());
            bundle.putString("desc", datumList.getDescEn());
        }
        bundle.putString("views", datumList.getViews());
        bundle.putString("favs", datumList.getCountWishlists());
        bundle.putString("code", String.valueOf(datumList.getId()));

        bundle.putFloat("rate", datumList.getCountRate());
        bundle.putString("size", datumList.getSize());
        bundle.putString("ageFrom", datumList.getAgeFrom());
        bundle.putString("ageTo", datumList.getAgeTo());

        bundle.putStringArrayList("commentsContent", getCommentsContent(datumList.getComments()));
        bundle.putStringArrayList("commentsRate", getCommentsRate(datumList.getComments()));

        bundle.putBoolean("isFav", true);
        bundle.putBoolean("isCart", datumList.getInCart());

        fragment.setArguments(bundle);
        moveToFragment.moveInMain(fragment);
    }
    private ArrayList<String> getCommentsContent(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getContent());
        }
        return sList;
    }

    private ArrayList<String> getCommentsRate(List<Comment> comments) {
        ArrayList<String> sList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            sList.add(comments.get(i).getRate());
        }
        return sList;
    }
    @Override
    public void onFavBtnClick(Medical datum, int pos, ImageButton imageButton) {
        if (getLogged()) {
            try {
                serverChangeFav(datum.getId());
            }catch (Exception ignore){}
        } else {
            goToLogin();
        }
    }

    private void serverChangeFav(Integer id) {
        progressBar.setVisibility(View.VISIBLE);
        callGetChangeFav(id).enqueue(new Callback<ChangeFavModel>() {
            @Override
            public void onResponse(@NonNull Call<ChangeFavModel> call, @NonNull Response<ChangeFavModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.favourite.change.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            moveToFragment.moveInMain(new FavouritesFragment());
                        } else
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChangeFavModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private Call<ChangeFavModel> callGetChangeFav(Integer pId) {
        return apiLinkStore.changeWichlist(getToken(), pId);
    }

    private String getToken() {
        return listSharedPreference.getToken();
    }

    private boolean getLogged() {
        return listSharedPreference.getIsLogged();
    }

    private void goToLogin() {
        Toast.makeText(mContext, R.string.please_login_first, Toast.LENGTH_SHORT).show();
        Intent myIntent = new Intent(MyApp.getContext().getApplicationContext(), LoginActivity.class);
        startActivity(myIntent);
        mContext.finish();
    }

}
