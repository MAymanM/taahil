package com.tkmsoft.taahel.fragments.main.cart;

import android.app.AlertDialog;
import androidx.fragment.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.cart.CartAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.cart.change.ChangeCartModel;
import com.tkmsoft.taahel.model.api.cart.view.Cart;
import com.tkmsoft.taahel.model.api.cart.view.CartModel;
import com.tkmsoft.taahel.model.api.cart.view.Data;
import com.tkmsoft.taahel.model.api.cart.view.Product;
import com.tkmsoft.taahel.model.api.cart.view.Status;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */

public class CartFragment extends Fragment implements CartAdapter.ListAllClickListeners {

    private FragmentActivity mContext;
    private ListSharedPreference listSharedPreference;
    private MainViewsCallBack mainViewsCallBack;
    private MoveToFragment moveToFragment;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private CartAdapter cartAdapter;
    private String TAG = getClass().getSimpleName();
    private ConnectionDetector connectionDetector;
    private ApiLink apiLink;
    private String totalPriceAfter;
    @BindView(R.id.totalTV)
    TextView totalTV;
    private String totalAll;
    private ArrayList<String> quantityList;

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
        super.onAttach(context);
        try {
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        connectionDetector = new ConnectionDetector(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLink = MyRetrofitClient.getBase().create(ApiLink.class);
        String mLanguage = listSharedPreference.getLanguage();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        ArrayList<Cart> mDataList = new ArrayList<>();
        cartAdapter = new CartAdapter(mDataList, this);
        initRecyclerView();
        quantityList = new ArrayList<>();
    }


    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(cartAdapter);
    }

    private void serverGetSections() {
        progressBar.setVisibility(View.VISIBLE);
        callCart().enqueue(new Callback<CartModel>() {
            @Override
            public void onResponse(@NonNull Call<CartModel> call, @NonNull Response<CartModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            Status status = response.body().getStatus();
                            if (status.getType().equals("1")) {
                                Data data = response.body().getData();
                                if (data != null) {
                                    List<Cart> cart = response.body().getData().getCart();
                                    if (cart != null) {
                                        cartAdapter.replaceData(cart);
                                        for (int i = 0; i < cart.size(); i++) {
                                            quantityList.add("1");
                                        }
                                    }
                                    totalAll = data.getTotalPrice();
                                    totalTV.setText(totalAll);
                                }
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<CartModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
//                serverGetSections();
            }
        });
    }


    private Call<CartModel> callCart() {
        return apiLink.get_cart(listSharedPreference.getToken());
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.serToolbarTitle(getString(R.string.cart));
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (connectionDetector.isConnectingToInternet()) {
            serverGetSections();
        } else
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onPause() {
        callCart().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new HomeFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

    @Override
    public void onItemClick(Product datum, int pos) {
        //do nothing
    }

    private Call<ChangeCartModel> callCartDelete(Long id) {
        return apiLink.changeCart(listSharedPreference.getToken(), id);
    }

    private void serverChangeCart(Product datum) {
        progressBar.setVisibility(View.VISIBLE);
        callCartDelete(datum.getId()).enqueue(new Callback<ChangeCartModel>() {
            @Override
            public void onResponse(@NonNull Call<ChangeCartModel> call, @NonNull Response<ChangeCartModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            com.tkmsoft.taahel.model.api.cart.change.Status status = response.body().getStatus();
                            if (status.getType().equals("1")) {
                                //refresh
                                moveToFragment.moveInMain(new CartFragment());
                            } else {
                                if (listSharedPreference.getLanguage().equals("en")) {
                                    Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChangeCartModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onRemoveCartClick(final Product datum, int adapterPosition) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setMessage(getString(R.string.delete_cart_prompt));
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), (arg0, arg1)
                -> serverChangeCart(datum));
        alertDialogBuilder.setNegativeButton(getString(R.string.no), (dialog, which)
                -> dialog.dismiss());
        alertDialogBuilder.show();
    }

    @Override
    public void onAddQuantityClick(int counter, String total, int price, int pos) {
//        //set Total
        int prevText = Integer.valueOf(totalTV.getText().toString());
        String result = String.valueOf((price + prevText));
//        Log.d(TAG, "onAddQuantityClick: \n" + "qunatity: " + counter + "\ntotal: " + total + "\noriginal: " + originalPrice + "\nprevText: " + prevText);
        totalTV.setText(result);

        //set Quantity data to array
        Log.d(TAG, "onAddQuantityClick: \n" + "prev all list: " + quantityList + "\nquantity: " + counter);
        quantityList.remove(pos);
        quantityList.add(pos, String.valueOf(counter));
        Log.d(TAG, "onAddQuantityClick: \n" + "all list: " + quantityList);
    }

    @Override
    public void onRemoveQuantityClick(int counter, String total, int price, int pos) {
        int prevText = Integer.valueOf(totalTV.getText().toString());
        String result = String.valueOf((prevText - price));
        totalTV.setText(result);
        Log.d(TAG, "onAddQuantityClick: \n" + "qunatity: " + counter + "\ntotal: " + total + "\noriginal: " + price + "\nprevText: " + prevText);

        //remove Quantity data to array
        Log.d(TAG, "onAddQuantityClick: \n" + "prev all list: " + quantityList + "\nquantity: " + counter);
        quantityList.remove(pos);
        quantityList.add(pos, String.valueOf(counter));
        Log.d(TAG, "onAddQuantityClick: \n" + "all list: " + quantityList);


    }


    @OnClick(R.id.proceedBtn)
    protected void onProceddBtnClick() {
            Fragment fragment = new CheckOutFragment();
            Bundle bundle = new Bundle();
            bundle.putString("totalAll", totalTV.getText().toString());
            bundle.putStringArrayList("quntityArray", quantityList);
            fragment.setArguments(bundle);
            moveToFragment.moveInMain(fragment);

    }

}