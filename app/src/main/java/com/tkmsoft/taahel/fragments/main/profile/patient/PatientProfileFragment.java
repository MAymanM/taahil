package com.tkmsoft.taahel.fragments.main.profile.patient;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.core.widget.NestedScrollView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.reports.ReportsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class PatientProfileFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.nestedScrollView_reports)
    NestedScrollView nestedScrollView;
    @BindView(R.id.rel_reports_header)
    RelativeLayout relativeLayout;
    @BindView(R.id.staticTxt_reports)
    TextView staticTxt_reports;
    @BindView(R.id.arrow_expand)
    ImageView arrow_expand;
    @BindView(R.id.card_reports)
    CardView cardView_rpt;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.refreshBtn)
    ImageButton refreshBtn;
    @BindView(R.id.imageView)
    CircleImageView imageView;
    @BindView(R.id.genderTV)
    TextView genderTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.emailTV)
    TextView emailTV;
    @BindView(R.id.mobileTV)
    TextView mobileTV;
    @BindView(R.id.countryTV)
    TextView countryTV;
    @BindView(R.id.cityTV)
    TextView cityTV;


    MainViewsCallBack mainViewsCallBack;
    private ReportsAdapter reportsAdapter;
    private String TAG = getClass().getSimpleName();
    View rootView;
    private ListSharedPreference listSharedPreference;

    public PatientProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile_patient, container, false);
        ButterKnife.bind(this,rootView);
        listSharedPreference = new ListSharedPreference(PatientProfileFragment.this.getActivity().getApplicationContext());

        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
//        initAdapter();
        setUserData(listSharedPreference.getUAvatar(),
                listSharedPreference.getUName(),
                listSharedPreference.getUCity(),
                listSharedPreference.getUCountry(),
                listSharedPreference.getUGender(),
                listSharedPreference.getUBirthDate(),
                listSharedPreference.getUEmail(),
                listSharedPreference.getUPhone());

    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }
    }


    private void setUserData(String avatar, String name, String city, String country, String gender,
                             String birthday, String email, String phone) {
        progressBar.setVisibility(View.VISIBLE);

        try {
            if (URLUtil.isValidUrl(avatar))
                Glide.with(rootView).load(avatar).into(imageView);
            else
                imageView.setImageResource(R.drawable.place_holder);
        } catch (Exception ignore) {
        }

        nameTV.setText(name);
        cityTV.setText(city);
        if (gender.equals("0"))
            genderTV.setText(R.string.male);
        else
            genderTV.setText(R.string.female);
        emailTV.setText(email);
        countryTV.setText(country);
        dateTV.setText(birthday);
        mobileTV.setText(phone);

        progressBar.setVisibility(View.GONE);
    }


//    private void initAdapter() {
//        mDataList = new ArrayList<>();
//        for (int i = 0; i <= 6; i++) {
//            ReportsModel th;
//            if (i == 5 || i == 2)
//                th = new ReportsModel("Book" + i + ".pdf", getString(R.string.description_));
//            else
//                th = new ReportsModel("", "dummy Description 2");
//            mDataList.add(i, th);
//        }
//
//        reportsAdapter = new ReportsAdapter(getActivitiesCategs().getApplicationContext(), mDataList,
//                new ReportsAdapter.ListAllListeners() {
//                    @Override
//                    public void onItemDrOrderClick(ReportsModel reportsModel, int adapterPosition) {
//                        Toast.makeText(mContext, "downloading...", Toast.LENGTH_SHORT).show();
//                    }
//                });
//        recyclerView.setAdapter(reportsAdapter);
//        reportsAdapter.notifyDataSetChanged();
//    }

    @OnClick(R.id.card_reports)
    protected void onCardRptClick() {
        if (nestedScrollView.getVisibility() == View.GONE) {
            nestedScrollView.setVisibility(View.VISIBLE);
            arrow_expand.setImageResource(R.drawable.ic_arrow_up_24dp);
        } else {
            nestedScrollView.setVisibility(View.GONE);
            arrow_expand.setImageResource(R.drawable.ic_arrow_down_24dp);
        }
    }

    @OnClick(R.id.arrow_expand)
    protected void onArrowReportsClick() {
        onCardRptClick();
    }

    @OnClick(R.id.staticTxt_reports)
    protected void onStaticTextReportsClick() {
        onCardRptClick();
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            FragmentActivity mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            context = getActivity();
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                onDetach();
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, new HomeFragment())
                        .commit();
            }
        });
    }//end back pressed

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.profile));
        mainViewsCallBack.showFilterBtn(false);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }
}