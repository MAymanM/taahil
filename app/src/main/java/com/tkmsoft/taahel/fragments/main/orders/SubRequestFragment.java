package com.tkmsoft.taahel.fragments.main.orders;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.requests.mostafed.SubRequestAdapterPatient;
import com.tkmsoft.taahel.adapters.requests.doctor.SubRequestAdapterDr;
import com.tkmsoft.taahel.fragments.main.orders.details.doctor.CanceledOrderDrFragment;
import com.tkmsoft.taahel.fragments.main.orders.details.doctor.ConfirmedNonPaidDrFragment;
import com.tkmsoft.taahel.fragments.main.orders.details.doctor.ConfirmedPaidDrFragment;
import com.tkmsoft.taahel.fragments.main.orders.details.doctor.PinnedOrderDrFragment;
import com.tkmsoft.taahel.fragments.main.orders.details.patient.CanceledOrderPatientFrag;
import com.tkmsoft.taahel.fragments.main.orders.details.patient.CompletedOrderPatientFrag;
import com.tkmsoft.taahel.fragments.main.orders.details.patient.ConfirmedNonPaidPatientFragment;
import com.tkmsoft.taahel.fragments.main.orders.details.patient.ConfirmedPaidPatientFragment;
import com.tkmsoft.taahel.fragments.main.orders.details.patient.PinnedOrderPatientFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.order.drs.DoctorOrdersModel;
import com.tkmsoft.taahel.model.api.order.drs.Order;
import com.tkmsoft.taahel.model.api.order.drs.Status;
import com.tkmsoft.taahel.model.api.order.patient.PatientOrderModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubRequestFragment extends Fragment implements SubRequestAdapterPatient.ListAllListeners, SubRequestAdapterDr.ListAllListeners {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private MainViewsCallBack mainViewsCallBack;
    ListSharedPreference listSharedPreference;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.notFoundTV)
    TextView notFoundTV;
    private String TAG = getClass().getSimpleName();
    private ApiLink apiLinkOrder;
    private FragmentActivity mContext;
    private MoveToFragment moveToFragment;

    public SubRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            context = getActivity();
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkOrder = MyRetrofitClient.getOrder().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sub_request, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
        if (listSharedPreference.getUType().equals("0"))
            serverRequestsDr();
        else
            serverRequestsPatients();
    }


    private void serverRequestsDr() {
        progressBar.setVisibility(View.VISIBLE);
        callDrOrders().enqueue(new Callback<DoctorOrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<DoctorOrdersModel> call, @NonNull Response<DoctorOrdersModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            com.tkmsoft.taahel.model.api.order.drs.Data data = response.body().getData();
                            if (data != null) {
                                List<Order> order = data.getOrders();
                                if (order != null) {
                                    initAdapterDr(order);
                                }
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DoctorOrdersModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }//end server

    private void serverRequestsPatients() {
        progressBar.setVisibility(View.VISIBLE);
        callPatientOrders().enqueue(new Callback<PatientOrderModel>() {
            @Override
            public void onResponse(@NonNull Call<PatientOrderModel> call, @NonNull Response<PatientOrderModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.order.patient.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            com.tkmsoft.taahel.model.api.order.patient.Data data = response.body().getData();
                            if (data != null) {
                                List<com.tkmsoft.taahel.model.api.order.patient.Order> order = data.getOrders();
                                if (order != null) {
                                    initAdapterPatient(order);
                                }
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<PatientOrderModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private Call<PatientOrderModel> callPatientOrders() {
        return apiLinkOrder.getPatientOrders(getToken(), listSharedPreference.getOrderType());
    }

    private String getToken() {
        return listSharedPreference.getToken();
    }

    private Call<DoctorOrdersModel> callDrOrders() {
        return apiLinkOrder.getDrOrders(getToken(), listSharedPreference.getOrderType());
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }
    }

    private void initAdapterPatient(List<com.tkmsoft.taahel.model.api.order.patient.Order> orderList) {

        SubRequestAdapterPatient subRequestAdapter = new SubRequestAdapterPatient(mContext, orderList, this);
        recyclerView.setAdapter(subRequestAdapter);
        subRequestAdapter.notifyDataSetChanged();
    }

    private String getTime(String time) {
        switch (time) {
            case "0":
                return getString(R.string.am12_1);
            case "1":
                return getString(R.string.am1_2);
            case "2":
                return getString(R.string.am2_3);
            case "3":
                return getString(R.string.am3_4);
            case "4":
                return getString(R.string.am4_5);
            case "5":
                return getString(R.string.am5_6);
            case "6":
                return getString(R.string.am6_7);
            case "7":
                return getString(R.string.am7_8);
            case "8":
                return getString(R.string.am8_9);
            case "9":
                return getString(R.string.am9_10);
            case "10":
                return getString(R.string.am10_11);
            case "11":
                return getString(R.string.am11_12);
            case "12":
                return getString(R.string.pm12_1);
            case "13":
                return getString(R.string.pm1_2);
            case "14":
                return getString(R.string.pm2_3);
            case "15":
                return getString(R.string.pm3_4);
            case "16":
                return getString(R.string.pm4_5);
            case "17":
                return getString(R.string.pm5_6);
            case "18":
                return getString(R.string.pm6_7);
            case "19":
                return getString(R.string.pm7_8);
            case "20":
                return getString(R.string.pm8_9);
            case "21":
                return getString(R.string.pm9_10);
            case "22":
                return getString(R.string.pm10_11);
            case "23":
                return getString(R.string.pm11_12);
            default:
                return null;
        }
    }

    private void initAdapterDr(List<Order> orderList) {
        if (getActivity() != null) {
            SubRequestAdapterDr subRequestAdapterDr = new SubRequestAdapterDr(getActivity(),
                    orderList, this);
            recyclerView.setAdapter(subRequestAdapterDr);
            subRequestAdapterDr.notifyDataSetChanged();
        }
    }

    private String getGender(String gender) {
        switch (gender) {
            case "0":
                return getString(R.string.male);
            case "1":
                return getString(R.string.female);
            default:
                return "-1";
        }
    }

    private String getDay(String dayNum) {
        switch (dayNum) {
            case "0":
                return getString(R.string.sunday);
            case "1":
                return getString(R.string.monday);
            case "2":
                return getString(R.string.tuesday);
            case "3":
                return getString(R.string.wednesday);
            case "4":
                return getString(R.string.thursday);
            case "5":
                return getString(R.string.friday);
            case "6":
                return getString(R.string.saturday);
            default:
                return "null";
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);

        switch (getOrderType()) {
            case "0":
                mainViewsCallBack.serToolbarTitle(getString(R.string.pinned_order));
                break;
            case "1":
                mainViewsCallBack.serToolbarTitle(getString(R.string.confirmed_order_not_paid));
                break;
            case "2":
                mainViewsCallBack.serToolbarTitle(getString(R.string.confirmed_order_paid));
                break;
            case "3":
                mainViewsCallBack.serToolbarTitle(getString(R.string.completed_order));
                break;
            case "-1":
                mainViewsCallBack.serToolbarTitle(getString(R.string.cancled_order));
                break;
            default:
                mainViewsCallBack.serToolbarTitle(getString(R.string.my_requests));
                break;

        }
    }

    private String getOrderType() {
        return listSharedPreference.getOrderType();
    }

    private void fireBackButtonEvent() {
        ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new RequestsFragment());
            }
        });
    }//end back pressed

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
        notFoundTV.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        callDrOrders().cancel();
        callPatientOrders().cancel();
        super.onPause();
    }

    @Override
    public void onItemPatientOrderClick(com.tkmsoft.taahel.model.api.order.patient.Order order, int adapterPosition) {
        Fragment fragment;
        Bundle bundle = new Bundle();
        bundle.putString("code", String.valueOf(order.getId()));
        bundle.putString("orderId", String.valueOf(order.getId()));
        bundle.putString("date", order.getDate());
        bundle.putString("name", order.getOrderDay().getMember().getName());
        bundle.putString("country", listSharedPreference.getUCountry());
        bundle.putString("city", listSharedPreference.getUCity());
        bundle.putString("address", order.getAddress());
        bundle.putString("desc", order.getDesc());
        bundle.putString("reason", order.getReason());
        bundle.putString("paymentStatus", order.getPaymentStatus());
        bundle.putString("trackerId", String.valueOf(order.getPaymentTracker().getId()));
        bundle.putInt("timeSize", order.getTimeSelected().size());
        bundle.putString("total", order.getPaymentTracker().getPrice());
        for (int i = 0; i < order.getTimeSelected().size(); i++) {
            bundle.putString("time" + i, getTime(order.getTimeSelected().get(i).getTime()));
        }
        switch (order.getOrderDay().getDay()) {
            case "0":
                bundle.putString("day", getString(R.string.sunday));
                break;
            case "1":
                bundle.putString("day", getString(R.string.monday));
                break;
            case "2":
                bundle.putString("day", getString(R.string.tuesday));
                break;
            case "3":
                bundle.putString("day", getString(R.string.wednesday));
                break;
            case "4":
                bundle.putString("day", getString(R.string.thursday));
                break;
            case "5":
                bundle.putString("day", getString(R.string.friday));
                break;
            case "6":
                bundle.putString("day", getString(R.string.saturday));
                break;
            default:
                bundle.putString("day", "day");
                break;
        }
        if (order.getReason() != null)
            bundle.putString("reason", order.getReason());

        switch (listSharedPreference.getOrderType()) {
            case "0":
                fragment = new PinnedOrderPatientFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, fragment)
                        .commit();
                break;
            case "1":
                fragment = new ConfirmedNonPaidPatientFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, fragment)
                        .commit();
                break;
            case "2":
                fragment = new ConfirmedPaidPatientFragment();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, fragment)
                        .commit();
                break;
            case "3":
                fragment = new CompletedOrderPatientFrag();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, fragment)
                        .commit();
                break;
            case "-1":
                fragment = new CanceledOrderPatientFrag();
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, fragment)
                        .commit();
                break;
        }
    }

    @Override
    public void onItemDrOrderClick(Order order, int adapterPosition) {
        Fragment fragment;
        Bundle bundle = new Bundle();
        bundle.putString("code", String.valueOf(order.getId()));
        bundle.putString("date", order.getDate());
        bundle.putString("name", order.getMember().getName());
        bundle.putString("country", order.getMember().getCountry().getNameAr());
        bundle.putString("city", order.getMember().getCity().getNameAr());
        bundle.putString("address", order.getAddress());
        bundle.putString("desc", order.getDesc());
        bundle.putString("day", getDay(order.getOrderDay().getDay()));
        bundle.putString("orderId", String.valueOf(order.getId()));
        bundle.putString("reason", order.getReason());
        bundle.putInt("timeSize", order.getTimeSelected().size());
        for (int i = 0; i < order.getTimeSelected().size(); i++) {
            bundle.putString("time" + i, getTime(order.getTimeSelected().get(i).getTime()));
        }
        bundle.putString("paymentStatus", order.getPaymentStatus());
        //PatientInfo
        bundle.putString("pId", String.valueOf(order.getMember().getId()));
        bundle.putString("pGender", getGender(order.getMember().getGender()));
        bundle.putString("pAvatar", order.getMember().getAvatar());
        bundle.putString("pName", order.getMember().getName());
        bundle.putString("pEmail", order.getMember().getEmail());
        bundle.putString("pPhone", order.getMember().getPhone());
        bundle.putString("pCountry", order.getMember().getCountry().getNameAr());
        bundle.putString("pCity", order.getMember().getCity().getNameAr());
        bundle.putString("pBirthDate", order.getMember().getGender());

        switch (getOrderType()) {
            case "0":
                fragment = new PinnedOrderDrFragment();
                fragment.setArguments(bundle);
                moveToFragment.moveInMain(fragment);
                break;
            case "1": //confirmed not paid
                fragment = new ConfirmedNonPaidDrFragment();
                fragment.setArguments(bundle);
                moveToFragment.moveInMain(fragment);
                break;
            case "2": //confirmed paid
                fragment = new ConfirmedPaidDrFragment();
                fragment.setArguments(bundle);
                moveToFragment.moveInMain(fragment);
                break;
            case "3": //completed
                fragment = new CompletedOrderPatientFrag();
                fragment.setArguments(bundle);
                moveToFragment.moveInMain(fragment);
                break;
            case "-1":
                fragment = new CanceledOrderDrFragment();
                fragment.setArguments(bundle);
                moveToFragment.moveInMain(fragment);
                break;
        }
    }
}
