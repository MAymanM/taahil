package com.tkmsoft.taahel.fragments.main.jobs;


import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobDetailFragment extends Fragment {

    @BindViews({R.id.nameTV, R.id.fieldTV, R.id.countryTV, R.id.cityTV, R.id.emailTV, R.id.descTV, R.id.phoneTV,
            R.id.feature1TV, R.id.feature2TV, R.id.feature3TV, R.id.feature4TV,R.id.addressTV})
    List<TextView> textViewList;
    @BindView(R.id.imageView)
    ImageView imageView;
    TextView nameTV, fieldTV, countryTV, cityTV, emailTV, descTV, phoneTV, feature1TV, feature2TV, feature3TV, feature4TV, addressTV;
    private FragmentActivity mContext;
    private MainViewsCallBack mainViewsCallBack;
    private MoveToFragment moveToFragment;

    private String name, field, country, city, email, desc, phone, feature1, feature2, feature3, feature4,address, image;

    public JobDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        fireBackButtonEvent();
        ListSharedPreference listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString("name");
            field = getArguments().getString("field");
            country = getArguments().getString("country");
            city = getArguments().getString("city");
            email = getArguments().getString("email");
            desc = getArguments().getString("desc");
            phone = getArguments().getString("phone");
            feature1 = getArguments().getString("feature1");
            feature2 = getArguments().getString("feature2");
            feature3 = getArguments().getString("feature3");
            feature4 = getArguments().getString("feature4");
            address = getArguments().getString("address");
            image = getArguments().getString("image");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_job_detail, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        nameTV = textViewList.get(0);
        fieldTV = textViewList.get(1);
        countryTV = textViewList.get(2);
        cityTV = textViewList.get(3);
        emailTV = textViewList.get(4);
        descTV = textViewList.get(5);
        phoneTV = textViewList.get(6);
        feature1TV = textViewList.get(7);
        feature2TV = textViewList.get(8);
        feature3TV = textViewList.get(9);
        feature4TV = textViewList.get(10);
        addressTV = textViewList.get(11);


        nameTV.setText(name);
        fieldTV.setText(field);
        countryTV.setText(country);
        cityTV.setText(city);
        emailTV.setText(email);
        descTV.setText(desc);
        phoneTV.setText(phone);
        feature1TV.setText("- " + feature1);
        feature2TV.setText("- " +feature2);
        feature3TV.setText("- " +feature3);
        feature4TV.setText("- " +feature4);
        addressTV.setText(address);

        Picasso.get().load(image).error(R.drawable.place_holder).into(imageView);
    }

    @Override
    public void onStart() {
        super.onStart();
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(name);
        mainViewsCallBack.showAddFab(false);
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new JobsFragment());
            }
        });
    }//end back pressed
}
