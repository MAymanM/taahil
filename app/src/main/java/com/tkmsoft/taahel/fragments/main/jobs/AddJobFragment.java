package com.tkmsoft.taahel.fragments.main.jobs;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.helper.InitSpinner;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.defaultRes.DefaultResponse;
import com.tkmsoft.taahel.model.api.defaultRes.Title;
import com.tkmsoft.taahel.model.api.jobs.fields.Datum;
import com.tkmsoft.taahel.model.api.jobs.fields.FieldsJobModel;
import com.tkmsoft.taahel.model.api.jobs.fields.Status;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddJobFragment extends Fragment {

    private static final int IMAGE_CODE = 1002;
    private MainViewsCallBack mainViewsCallBack;
    @BindViews({R.id.progressBar, R.id.country_spinner_progressBar, R.id.field_spinner_progressBar})
    List<ProgressBar> progressBarList;
    ProgressBar progressBar, country_spinner_progressBar, field_spinner_progressBar;

    @BindViews({R.id.name_arET, R.id.name_enET, R.id.address_ar_ET, R.id.address_en_ET, R.id.emailET,
            R.id.phoneET, R.id.descAR_ET, R.id.descEN_ET, R.id.feature1AR_ET, R.id.feature1EN_ET, R.id.feature2AR_ET, R.id.feature2EN_ET,
            R.id.feature3AR_ET, R.id.feature3EN_ET, R.id.feature4AR_ET, R.id.feature4EN_ET})
    List<EditText> editTextList;
    EditText name_arET, name_enET, address_ar_ET, address_en_ET, emailET, phoneET, descAR_ET, descEN_ET,
            feature1AR_ET, feature1EN_ET, feature2AR_ET, feature2EN_ET,
            feature3AR_ET, feature3EN_ET, feature4AR_ET, feature4EN_ET;

    @BindViews({R.id.spinner_country, R.id.spinner_city, R.id.spinner_field})
    List<Spinner> spinnerList;
    Spinner spinner_country, spinner_city, spinner_field;

    @BindView(R.id.cityLinearLayout)
    LinearLayout cityLinearLayout;

    ListSharedPreference listSharedPreference;
    @BindView(R.id.imageView)
    ImageView imageView;
    private FragmentActivity mContext;
    private String TAG = getClass().getSimpleName();
    ApiLink apiLinkJobs, apiLinkBase;
    ConnectionDetector connectionDetector;
    private MoveToFragment moveToFragment;
    private InitSpinner initSpinner;

    private ArrayList<String> mCountriesArrayList;
    private ArrayList<String> mFieldArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mFieldIdList;
    private List<CountriesModel.DataBean.CountriesBean> dataCountryList;

    private String countryCode = "", cityCode = "", fieldCode = "";
    private String image_path = "";

    public AddJobFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }

        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        initSpinner = new InitSpinner(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiLinkJobs = MyRetrofitClient.getJobs().create(ApiLink.class);
        apiLinkBase = MyRetrofitClient.getBase().create(ApiLink.class);

    }

    private String extractStringFromEditTxt(EditText editText) {
        return editText.getText().toString();
    }

    private Call<DefaultResponse> callAddJob() {
        progressBar.setVisibility(View.VISIBLE);
        //photo part
        File file1;
        file1 = new File(image_path);
        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);
        MultipartBody.Part photoPart = MultipartBody.Part.createFormData("photo", file1.getName(), mFile1);

        String nameAR = extractStringFromEditTxt(name_arET);
        String nameEn = extractStringFromEditTxt(name_enET);
        String addressAR = extractStringFromEditTxt(address_ar_ET);
        String addressEn = extractStringFromEditTxt(address_en_ET);
        String email = extractStringFromEditTxt(emailET);
        String phone = extractStringFromEditTxt(phoneET);
        String descAR = extractStringFromEditTxt(descAR_ET);
        String descEN = extractStringFromEditTxt(descEN_ET);
        String feature1AR = extractStringFromEditTxt(feature1AR_ET);
        String feature1En = extractStringFromEditTxt(feature1EN_ET);
        String feature2Ar = extractStringFromEditTxt(feature2AR_ET);
        String feature2En = extractStringFromEditTxt(feature2EN_ET);
        String feature3Ar = extractStringFromEditTxt(feature3AR_ET);
        String feature3En = extractStringFromEditTxt(feature3EN_ET);
        String feature4AR = extractStringFromEditTxt(feature4AR_ET);
        String feature4En = extractStringFromEditTxt(feature4EN_ET);

        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), nameAR);
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), nameEn);
        RequestBody addressArPart = RequestBody.create(MediaType.parse("text/plain"), addressAR);
        RequestBody addressEnPart = RequestBody.create(MediaType.parse("text/plain"), addressEn);
        RequestBody emailPart = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody phonePart = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody descArPart = RequestBody.create(MediaType.parse("text/plain"), descAR);
        RequestBody descEnPart = RequestBody.create(MediaType.parse("text/plain"), descEN);
        RequestBody feature1ARPart = RequestBody.create(MediaType.parse("text/plain"), feature1AR);
        RequestBody feature1ENPart = RequestBody.create(MediaType.parse("text/plain"), feature1En);
        RequestBody feature2ARPart = RequestBody.create(MediaType.parse("text/plain"), feature2Ar);
        RequestBody feature2ENPart = RequestBody.create(MediaType.parse("text/plain"), feature2En);
        RequestBody feature3ARPart = RequestBody.create(MediaType.parse("text/plain"), feature3Ar);
        RequestBody feature3ENPart = RequestBody.create(MediaType.parse("text/plain"), feature3En);
        RequestBody feature4ARPart = RequestBody.create(MediaType.parse("text/plain"), feature4AR);
        RequestBody feature4ENPart = RequestBody.create(MediaType.parse("text/plain"), feature4En);

        RequestBody cityPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody fieldPart = RequestBody.create(MediaType.parse("text/plain"), fieldCode);

        return apiLinkJobs.addJob(name_arPart, name_enPart, cityPart, addressArPart, addressEnPart, fieldPart, emailPart, phonePart, descArPart, descEnPart,
                feature1ARPart, feature1ENPart, feature2ARPart, feature2ENPart, feature3ARPart, feature3ENPart, feature4ARPart, feature4ENPart,
                photoPart);
    }

    private Call<DefaultResponse> callAddJobNonImage() {
        progressBar.setVisibility(View.VISIBLE);

        String nameAR = extractStringFromEditTxt(name_arET);
        String nameEn = extractStringFromEditTxt(name_enET);
        String addressAR = extractStringFromEditTxt(address_ar_ET);
        String addressEn = extractStringFromEditTxt(address_en_ET);
        String email = extractStringFromEditTxt(emailET);
        String phone = extractStringFromEditTxt(phoneET);
        String descAR = extractStringFromEditTxt(descAR_ET);
        String descEN = extractStringFromEditTxt(descEN_ET);
        String feature1AR = extractStringFromEditTxt(feature1AR_ET);
        String feature1En = extractStringFromEditTxt(feature1EN_ET);
        String feature2Ar = extractStringFromEditTxt(feature2AR_ET);
        String feature2En = extractStringFromEditTxt(feature2EN_ET);
        String feature3Ar = extractStringFromEditTxt(feature3AR_ET);
        String feature3En = extractStringFromEditTxt(feature3EN_ET);
        String feature4AR = extractStringFromEditTxt(feature4AR_ET);
        String feature4En = extractStringFromEditTxt(feature4EN_ET);

        return apiLinkJobs.addJobNonImage(nameAR, nameEn, cityCode, addressAR, addressEn, fieldCode, email, phone, descAR, descEN,
                feature1AR, feature1En, feature2Ar, feature2En, feature3Ar, feature3En, feature4AR, feature4En);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_job_add, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        setViewData();
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);

        initCountrySpinner();

        mFieldArrayList = new ArrayList<>();
        mFieldIdList = new ArrayList<>();
        mFieldArrayList.add(0, getString(R.string.field));
        mFieldIdList.add(0, 0);

        initFieldSpinner();
        serverCountrySpinner();
        serverFieldSpinner();
    }


    @OnClick(R.id.addBtn)
    protected void onAddClick() {
        attemptToAdd();
    }

    @OnClick(R.id.imageView)
    protected void onImageClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(mContext,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(mContext,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                openImageIntent();
            }
        } else {
            openImageIntent();
        }
    }

    private void openImageIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, IMAGE_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
            image_path = getImagePathFromUri(uri, getActivity());
            showImageInView(uri);
        }//end if check for data

    }//end onActivityResult

    private void showImageInView(Uri image_path) {
        Picasso.get().load(image_path).resize(500, 300).into(imageView);
    }

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    private void attemptToAdd() {

        if (!countryCode.equals("")) {
            if (!cityCode.equals("")) {
                if (!fieldCode.equals("")) {
                    if (!image_path.equals("")) {
                        serverAddJob();
                    } else {
                        serverAddJobNonImage();
                    }
                } else
                    Toast.makeText(mContext, "" + getString(R.string.enter_field), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();

    }


    private void serverAddJob() {
        progressBar.setVisibility(View.VISIBLE);
        callAddJob().enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(@NonNull Call<DefaultResponse> call, @NonNull Response<DefaultResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.defaultRes.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Title title = status.getTitle();
                            if (title != null) {
                                if (getLanguage().equals("ar"))
                                    Toast.makeText(mContext, "" + status.getTitle().getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + status.getTitle().getEn(), Toast.LENGTH_SHORT).show();
                                moveToFragment.moveInMain(new JobsFragment());
                            }
                        } else {
                            Title title = status.getTitle();
                            if (title != null) {
                                if (getLanguage().equals("ar"))
                                    Toast.makeText(mContext, "" + status.getTitle().getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + status.getTitle().getEn(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DefaultResponse> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void serverAddJobNonImage() {
        progressBar.setVisibility(View.VISIBLE);
        callAddJobNonImage().enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(@NonNull Call<DefaultResponse> call, @NonNull Response<DefaultResponse> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.defaultRes.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Title title = status.getTitle();
                            if (title != null) {
                                if (getLanguage().equals("ar"))
                                    Toast.makeText(mContext, "" + status.getTitle().getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + status.getTitle().getEn(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Title title = status.getTitle();
                            if (title != null) {
                                if (getLanguage().equals("ar"))
                                    Toast.makeText(mContext, "" + status.getTitle().getAr(), Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "" + status.getTitle().getEn(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DefaultResponse> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private Call<FieldsJobModel> callGetField() {
        return apiLinkBase.getJobFields();
    }

    private void initFieldSpinner() {

        initSpinner.setSpinner(spinner_field, mFieldArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    fieldCode = String.valueOf(mFieldIdList.get(position));
                } else {
                    fieldCode = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void serverFieldSpinner() {
        field_spinner_progressBar.setVisibility(View.VISIBLE);
        callGetField().enqueue(new Callback<FieldsJobModel>() {
            @Override
            public void onResponse(@NonNull Call<FieldsJobModel> call, @NonNull Response<FieldsJobModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                List<Datum> dataList = response.body().getData();
                                if (dataList != null) {
                                    for (int i = 0; i < dataList.size(); i++) {
                                        if (getLanguage().equals("ar")) {
                                            mFieldArrayList.add(dataList.get(i).getNameAr());
                                        } else {
                                            mFieldArrayList.add(dataList.get(i).getNameEn());
                                        }
                                        mFieldIdList.add(dataList.get(i).getId());
                                    }//end for

                                    initFieldSpinner();
                                }
                            }
                        }
                    }
                }
                field_spinner_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<FieldsJobModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                field_spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end()

    private Call<CountriesModel> callGetCountry() {
        return apiLinkBase.getCountries();
    }

    private void serverCountrySpinner() {
        country_spinner_progressBar.setVisibility(View.VISIBLE);
        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CountriesModel.StatusBean status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                CountriesModel.DataBean data = response.body().getData();
                                if (data != null) {
                                    dataCountryList = data.getCountries();
                                    if (dataCountryList != null && !dataCountryList.isEmpty()) {
                                        for (int i = 0; i < dataCountryList.size(); i++) {
                                            mCountriesArrayList.add(dataCountryList.get(i).getName_ar());
                                            mCountriesIdList.add(dataCountryList.get(i).getId());
                                        }//end for

                                        initCountrySpinner();
                                    }
                                }
                            }
                        }
                    }
                }
                country_spinner_progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                country_spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end serverRegister()

    private void initCountrySpinner() {

        initSpinner.setSpinner(spinner_country, mCountriesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(position));
                    showCitySpinner(true);
                    initCitySpinner();
                } else {
                    countryCode = "";
                    cityCode = "";
                    showCitySpinner(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCitySpinner() {
        ArrayList<String> mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);

        for (int i = 0; i < dataCountryList.size(); i++) {
            if (countryCode.equals(String.valueOf(dataCountryList.get(i).getId()))) {
                for (int k = 0; k < dataCountryList.get(i).getCities().size(); k++) {
                    if (getLanguage().equals("ar"))
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_ar());
                    else
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_en());
                    mCitiesIdList.add(dataCountryList.get(i).getCities().get(k).getId());
                }
            }
        }

        initSpinner.setSpinner(spinner_city, mCitiesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    cityCode = String.valueOf(mCitiesIdList.get(position));
                } else {
                    cityCode = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }

    private void showCitySpinner(boolean b) {
        if (b)
            cityLinearLayout.setVisibility(View.VISIBLE);
        else
            cityLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        mainViewsCallBack.serToolbarTitle(getString(R.string.add_job));
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        super.onStart();
    }

    @Override
    public void onPause() {
        callGetCountry().cancel();
        callGetField().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    private void setViewData() {
        //progressBars
        progressBar = progressBarList.get(0);
        country_spinner_progressBar = progressBarList.get(1);
        field_spinner_progressBar = progressBarList.get(2);

        //EditText
        name_arET = editTextList.get(0);
        name_enET = editTextList.get(1);
        address_ar_ET = editTextList.get(2);
        address_en_ET = editTextList.get(3);
        emailET = editTextList.get(4);
        phoneET = editTextList.get(5);
        descAR_ET = editTextList.get(6);
        descEN_ET = editTextList.get(7);
        feature1AR_ET = editTextList.get(8);
        feature1EN_ET = editTextList.get(9);
        feature2AR_ET = editTextList.get(10);
        feature2EN_ET = editTextList.get(11);
        feature3AR_ET = editTextList.get(12);
        feature3EN_ET = editTextList.get(13);
        feature4AR_ET = editTextList.get(14);
        feature4EN_ET = editTextList.get(15);

        //Spinner
        spinner_country = spinnerList.get(0);
        spinner_city = spinnerList.get(1);
        spinner_field = spinnerList.get(2);
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new JobsFragment());
            }
        });
    }//end back pressed

}
