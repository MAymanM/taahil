package com.tkmsoft.taahel.fragments.main.activities;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.activities.ActivitiesCategAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.activities.view.category.ActivityCategModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitiesFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    private MainViewsCallBack mainViewsCallBack;
    private ListSharedPreference listSharedPreference;
    private FragmentActivity mContext;
    private ApiLink apiLink;
    MoveToFragment moveToFragment;

    public ActivitiesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        fireBackButtonEvent();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_activities, container, false);
        ButterKnife.bind(this, rootView);
        apiLink = MyRetrofitClient.getBaseActivity().create(ApiLink.class);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
        pullRefreshLayout.setOnRefreshListener(() -> serverGetActivitiesCateg());
        setupAddButton();
    }

    private void setupAddButton() {
        FloatingActionButton fab = mContext.findViewById(R.id.main_fab);
        fab.setOnClickListener(view -> {
                    listSharedPreference.deleteSharedPref("lat");
                    listSharedPreference.deleteSharedPref("longX");
                    moveToFragment.moveInMain(new AddActivitiesFrag());
                }
        );
    }

    private void serverGetActivitiesCateg() {
        progressBar.setVisibility(View.VISIBLE);
        callGetActivitiesCategs().enqueue(new Callback<ActivityCategModel>() {
            @Override
            public void onResponse(@NonNull Call<ActivityCategModel> call, @NonNull Response<ActivityCategModel> response) {
                progressBar.setVisibility(View.GONE);
                pullRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    if (response.body().getStatus() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            if (response.body().getData() != null) {
                                initAdapter(response.body().getData().getCategories());
                            } else
                                initAdapter(new ArrayList<>());
                        } else
                            Toast.makeText(mContext, "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ActivityCategModel> call, @NonNull Throwable t) {
                pullRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(mContext, "" + R.string.network_error, Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private Call<ActivityCategModel> callGetActivitiesCategs() {
        return apiLink.getActivitiesCategs();
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
    }

    private void initAdapter(List<ActivityCategModel.DataBean.CategoriesBean> categories) {
        Integer[] images = {
                R.drawable.events,
                R.drawable.seminar,
                R.drawable.courses,
                R.drawable.conferances,
                R.drawable.workshops,
                R.drawable.lectures};
        List<Integer> imagesList = new ArrayList<>(Arrays.asList(images));

        ActivitiesCategAdapter activitiesCategAdapter = new ActivitiesCategAdapter(getActivity(), imagesList,
                categories, (categoriesBean, adapterPosition) -> {
                    listSharedPreference.setActivityTypeID(categoriesBean.getId());
                    listSharedPreference.setActivityName(categoriesBean.getName_ar());

                    moveToFragment.moveInMain(new SubActivityFragment());
                });
        recyclerView.setAdapter(activitiesCategAdapter);
        activitiesCategAdapter.notifyDataSetChanged();
    }


    @Override
    public void onStart() {
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.activities));
        mainViewsCallBack.showAddFab(true);
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        serverGetActivitiesCateg();
    }

    @Override
    public void onPause() {
        callGetActivitiesCategs().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {

        super.onDetach();
        mainViewsCallBack = null;
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                getFragmentManager().beginTransaction()
                        .replace(R.id.main_frameLayout, new HomeFragment())
                        .commit();
            }
        });
    }//end back pressed
}