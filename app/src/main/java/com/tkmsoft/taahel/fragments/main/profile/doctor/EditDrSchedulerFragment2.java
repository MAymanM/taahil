package com.tkmsoft.taahel.fragments.main.profile.doctor;


import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.DaysListAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.DaysModel;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditDrSchedulerFragment2 extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.register_done_Btn)
    Button register_done_Btn;
    private FragmentActivity mContext;

    private MoveToFragment moveToFragment;

    public EditDrSchedulerFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        if (activity instanceof FragmentActivity) {
            mContext = (FragmentActivity) activity;
        }
        super.onAttach(activity);
        moveToFragment = new MoveToFragment(mContext);
        ListSharedPreference listSharedPreference = new ListSharedPreference(mContext);
        fireBackButtonEvent();
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_dr_scheduler_fragment2, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
        initAdapter();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        }
    }

    private void initAdapter() {
        ArrayList<DaysModel> mDataList = new ArrayList<>();
        String[] daysList = {getString(R.string.saturday), getString(R.string.sunday), getString(R.string.monday),
                getString(R.string.tuesday), getString(R.string.wednesday),
                getString(R.string.thursday), getString(R.string.friday)};

        int[] daysNum = {6,0,1,2,3,4,5};

        for (int i = 0; i < daysList.length; i++) {
            DaysModel th = new DaysModel(daysList[i], daysNum[i]);
            mDataList.add(i, th);
        }

        DaysListAdapter daysListAdapter = new DaysListAdapter(mContext, mDataList,
                (daysModel, adapterPosition) -> {
            //on click
                    Fragment fr = new EditDrSchedulerFragment3();
                    Bundle args = new Bundle();
                    args.putString("day", daysModel.getDay());
                    args.putInt("dayNum", daysModel.getDayNum());
                    fr.setArguments(args);
                    moveToFragment.moveInMain(fr);
                });

        recyclerView.setAdapter(daysListAdapter);
        daysListAdapter.notifyDataSetChanged();
    }


    private void fireBackButtonEvent() {

        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;
                moveToFragment.moveInMain(new HomeFragment());
            }
        });
    }//end back pressed

    @OnClick(R.id.register_done_Btn)
    void onRegister_done_Btn(){
        moveToFragment.moveInMain(new HomeFragment());
    }


}