package com.tkmsoft.taahel.fragments.main.adds.store;

import android.Manifest;
import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.helper.InitSpinner;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.adds.edit.EditModel;
import com.tkmsoft.taahel.model.api.adds.edit.Status;
import com.tkmsoft.taahel.model.api.store.spinner.type.Category;
import com.tkmsoft.taahel.model.api.store.spinner.type.Data;
import com.tkmsoft.taahel.model.api.store.spinner.type.TypeStoreModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by MahmoudAyman on 28/01/2019.
 */
public class EditStoreFragment extends Fragment {
    @BindView(R.id.spinner_city)
    Spinner spinner_city;
    @BindView(R.id.spinner_country)
    Spinner spinner_country;
    @BindView(R.id.spinner_currency)
    Spinner spinner_currency;
    @BindView(R.id.spinner_type)
    Spinner spinner_type;
    @BindView(R.id.cityRelLayout)
    LinearLayout cityRelative;
    @BindView(R.id.spinner_progressBar)
    ProgressBar spinner_progressBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.spinner_type_progressBar)
    ProgressBar spinner_type_progressBar;
    @BindView(R.id.name_arET)
    EditText name_arET;
    @BindView(R.id.name_enET)
    EditText name_enET;
    @BindView(R.id.descAR_ET)
    EditText desc_ar_ET;
    @BindView(R.id.descEN_ET)
    EditText desc_en_ET;
    @BindView(R.id.ageFromET)
    EditText ageFromET;
    @BindView(R.id.ageToET)
    EditText ageToET;
    @BindView(R.id.priceET)
    EditText priceET;
    @BindView(R.id.sizeET)
    EditText sizeET;
    @BindViews({R.id.imageView1, R.id.imageView2, R.id.imageView3})
    List<ImageView> imageViewList;
    ImageView imageView1, imageView2, imageView3;
    MainViewsCallBack mainViewsCallBack;
    ListSharedPreference listSharedPreference;
    private ArrayList<String> mCountriesArrayList;
    private ArrayList<String> mCurrencyArrayList;
    private ArrayList<String> mTypeArrayList;
    private ArrayList<Integer> mCountriesIdList, mCitiesIdList, mCurrencyIdList, mTypeIdList;
    private final int IMAGE_CODE1 = 2001, IMAGE_CODE2 = 2002, IMAGE_CODE3 = 2003;
    private String image_path1 = "", image_path2 = "", image_path3 = "";
    private String countryCode, cityCode, currencyCode, typeCode;
    private FragmentActivity mContext;
    MoveToFragment moveToFragment;

    String pId;
    String image1, image2, image3;
    String nameAr, nameEn;
    String descAr, descEn;
    String ageTo;
    String size;
    String price;
    String ageFrom;
    Integer countryId, cityId, typeId, currencyId;
    private InitSpinner initSpinner;
    private ConnectionDetector connectionManager;
    private ApiLink apiBase, apiStore;
    private List<CountriesModel.DataBean.CountriesBean> dataCountryList;
    private String TAG = getClass().getSimpleName();

    boolean isInserted = false, isInsertedCurrency = false;

    public EditStoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        fireBackButtonEvent();
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        initSpinner = new InitSpinner(mContext);
        connectionManager = new ConnectionDetector(mContext);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            pId = bundle.getString("pId");
            image1 = bundle.getString("photo1");
            image2 = bundle.getString("photo2");
            image3 = bundle.getString("photo3");
            nameAr = bundle.getString("nameAr");
            nameEn = bundle.getString("nameEn");
            typeId = bundle.getInt("typeId");
            descAr = bundle.getString("descAr");
            descEn = bundle.getString("descEn");
            countryId = bundle.getInt("countryId");
            cityId = bundle.getInt("cityId");
            currencyId = bundle.getInt("currencyId");
            price = bundle.getString("price");
            ageFrom = bundle.getString("ageFrom");
            ageTo = bundle.getString("ageTo");
            size = bundle.getString("size");
        }
        apiBase = MyRetrofitClient.getBase().create(ApiLink.class);
        apiStore = MyRetrofitClient.getStore().create(ApiLink.class);
    }//end onCreate

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_store, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        setData();
        initCountrySpinner();
        initCurrencySpinner();
        initTypeSpinner();
    }

    private void setData() {
        name_arET.setText(nameAr);
        name_enET.setText(nameEn);
        desc_ar_ET.setText(descAr);
        desc_en_ET.setText(descEn);
        ageFromET.setText(ageFrom);
        ageToET.setText(ageTo);
        sizeET.setText(size);
        priceET.setText(price);

        imageView1 = imageViewList.get(0);
        imageView2 = imageViewList.get(1);
        imageView3 = imageViewList.get(2);

        setImages(image1, imageView1);

    }

    private void setImages(String image, ImageView imageView) {
        Picasso.get().load(image).resize(500, 500).error(R.drawable.place_holder).into(imageView);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    private boolean isConnected() {
        return connectionManager.isConnectingToInternet();
    }

    ///////////////Spinners///////////////

    private Call<CountriesModel> callGetCountry() {
        return apiBase.getCountries();
    }

    private Call<TypeStoreModel> callGetType() {
        return apiStore.getStoreTypes();
    }

    private void initTypeSpinner() {
        mTypeArrayList = new ArrayList<>();
        mTypeIdList = new ArrayList<>();
        mTypeArrayList.add(0, getString(R.string.type));
        mTypeIdList.add(0, 0);

        initSpinner.setSpinner(spinner_type, mTypeArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    typeCode = String.valueOf(mTypeIdList.get(position));
                } else {
                    typeCode = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void serverTypeSpinner() {
        spinner_type_progressBar.setVisibility(View.VISIBLE);
        callGetType().enqueue(new Callback<TypeStoreModel>() {
            @Override
            public void onResponse(@NonNull Call<TypeStoreModel> call, @NonNull Response<TypeStoreModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        com.tkmsoft.taahel.model.api.store.spinner.type.Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {

                                Data data = response.body().getData();
                                if (data != null) {
                                    List<Category> categoryList = data.getCategories();
                                    if (categoryList != null) {
                                        for (int i = 0; i < categoryList.size(); i++) {
                                            if (getLanguage().equals("ar")) {
                                                mTypeArrayList.add(categoryList.get(i).getNameAr());
                                            } else {
                                                mTypeArrayList.add(categoryList.get(i).getNameEn());
                                            }
                                            mTypeIdList.add(categoryList.get(i).getId());
                                        }//end for
                                        setSpinnerValue(mTypeIdList, typeId, spinner_type);
                                    }
                                }
                            }
                        }
                    }
                }
                spinner_type_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<TypeStoreModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                spinner_type_progressBar.setVisibility(View.GONE);
            }
        });
    }//end()

    private void serverCountrySpinner() {

        callGetCountry().enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CountriesModel.StatusBean status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                CountriesModel.DataBean data = response.body().getData();
                                if (data != null) {
                                    dataCountryList = data.getCountries();
                                    if (dataCountryList != null && !dataCountryList.isEmpty()) {
                                        for (int i = 0; i < dataCountryList.size(); i++) {
                                            if (getLanguage().equals("ar")) {
                                                mCountriesArrayList.add(dataCountryList.get(i).getName_ar());
                                                mCurrencyArrayList.add(dataCountryList.get(i).getCurrency().getName_ar());
                                            } else {
                                                mCountriesArrayList.add(dataCountryList.get(i).getName_en());
                                                mCurrencyArrayList.add(dataCountryList.get(i).getCurrency().getName_en());
                                            }
                                            mCurrencyIdList.add(dataCountryList.get(i).getCurrency().getId());
                                            mCountriesIdList.add(dataCountryList.get(i).getId());
                                        }//end for

                                        setSpinnerValue(mCountriesIdList, countryId, spinner_country);
                                        setSpinnerValue(mCurrencyIdList, currencyId, spinner_currency);

                                    }
                                }
                            }
                        }
                    }
                }
                spinner_progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                spinner_progressBar.setVisibility(View.GONE);
            }
        });
    }//end()

    private void setSpinnerValue(ArrayList<Integer> integerArrayList, Integer id, Spinner spinner) {
        int j = 0;
        while (j < integerArrayList.size()) {
            if (integerArrayList.get(j).equals(id)) {
                spinner.setSelection(j);
                break;
            } else
                j++;
        }
    }

    private void initCountrySpinner() {
        mCountriesArrayList = new ArrayList<>();
        mCountriesIdList = new ArrayList<>();
        mCountriesArrayList.add(0, getString(R.string.country));
        mCountriesIdList.add(0, 0);

        initSpinner.setSpinner(spinner_country, mCountriesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    countryCode = String.valueOf(mCountriesIdList.get(position));
                    showCitySpinner();
                    initCitySpinner();
                } else {
                    countryCode = null;
                    hideCitySpinner();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCitySpinner() {
        ArrayList<String> mCitiesArrayList = new ArrayList<>();
        mCitiesArrayList.add(0, getString(R.string.city));
        mCitiesIdList = new ArrayList<>();
        mCitiesIdList.add(0, 0);


        for (int i = 0; i < dataCountryList.size(); i++) {
            if (countryCode.equals(String.valueOf(dataCountryList.get(i).getId()))) {
                for (int k = 0; k < dataCountryList.get(i).getCities().size(); k++) {
                    if (getLanguage().equals("ar"))
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_ar());
                    else
                        mCitiesArrayList.add(dataCountryList.get(i).getCities().get(k).getName_en());
                    mCitiesIdList.add(dataCountryList.get(i).getCities().get(k).getId());
                }
            }
        }

        initSpinner.setSpinner(spinner_city, mCitiesArrayList).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    cityCode = String.valueOf(mCitiesIdList.get(position));
                } else {
                    cityCode = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (!isInserted) {
            int j = 0;
            while (j < mCitiesIdList.size()) {
                if (mCitiesIdList.get(j).equals(cityId)) {
                    spinner_city.setSelection(j);
                    isInserted = true;
                    Log.d(TAG, "onCitySelect: " + j);
                    break;
                } else
                    j++;
            }
        }

    }

    private void initCurrencySpinner() {
        mCurrencyArrayList = new ArrayList<>();
        mCurrencyArrayList.add(0, getString(R.string.currency));
        mCurrencyIdList = new ArrayList<>();
        mCurrencyIdList.add(0, 0);

        initSpinner.setSpinner(spinner_currency, mCurrencyArrayList).setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position > 0) {
                            currencyCode = String.valueOf(mCurrencyIdList.get(position));
                        } else {
                            currencyCode = null;
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

        if (!isInsertedCurrency) {
            int j = 0;
            while (j < mCurrencyIdList.size()) {
                if (mCurrencyIdList.get(j).equals(currencyId)) {
                    spinner_currency.setSelection(j);
                    isInsertedCurrency = true;
                    break;
                } else
                    j++;
            }
        }

    }


    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }

    private void hideCitySpinner() {
        cityRelative.setVisibility(View.GONE);
    }

    private void showCitySpinner() {
        cityRelative.setVisibility(View.VISIBLE);
    }

    /////////////////////end spinners////////////////

    private void serverEditStoreAll() {
        progressBar.setVisibility(View.VISIBLE);

        callEditAll().enqueue(new Callback<EditModel>() {
            @Override
            public void onResponse(@NonNull Call<EditModel> call, @NonNull Response<EditModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().getType().equals("success")) {
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                            moveToFragment.moveInMain(new SubMyAddsStoreFragment());
                        } else
                            Toast.makeText(getActivity(), "" + response.body().getStatus().getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<EditModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
//                Toast.makeText(mContext, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void serverEditStoreNonImage() {
        progressBar.setVisibility(View.VISIBLE);

        callEditNonImage().enqueue(new Callback<EditModel>() {
            @Override
            public void onResponse(@NonNull Call<EditModel> call, @NonNull Response<EditModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                                moveToFragment.moveInMain(new SubMyAddsStoreFragment());
                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<EditModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
            }
        });
    }

    private Call<EditModel> callEditAll() {
        File file1 = new File(image_path1);
        File file2 = new File(image_path2);
        File file3 = new File(image_path3);

        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);
        MultipartBody.Part photoPart1 = MultipartBody.Part.createFormData("main_photo", file1.getName(), mFile1);

        RequestBody mFile2 = RequestBody.create(MediaType.parse("image/*"), file2);
        MultipartBody.Part photoPart2 = MultipartBody.Part.createFormData("second_photo", file2.getName(), mFile2);

        RequestBody mFile3 = RequestBody.create(MediaType.parse("image/*"), file3);
        MultipartBody.Part photoPart3 = MultipartBody.Part.createFormData("third_photo", file3.getName(), mFile3);

        RequestBody idPart = RequestBody.create(MediaType.parse("text/plain"), pId);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody currency_idPart = RequestBody.create(MediaType.parse("text/plain"), currencyCode);
        RequestBody pricePart = RequestBody.create(MediaType.parse("text/plain"), priceET.getText().toString());
        RequestBody sizePart = RequestBody.create(MediaType.parse("text/plain"), sizeET.getText().toString());
        RequestBody ageFromPart = RequestBody.create(MediaType.parse("text/plain"), ageFromET.getText().toString());
        RequestBody ageToPart = RequestBody.create(MediaType.parse("text/plain"), ageToET.getText().toString());
        RequestBody desc_arPart = RequestBody.create(MediaType.parse("text/plain"), desc_ar_ET.getText().toString());
        RequestBody desc_enPart = RequestBody.create(MediaType.parse("text/plain"), desc_en_ET.getText().toString());

        return apiStore.editStoreAll(idPart, photoPart1,
                photoPart2, photoPart3, name_arPart, name_enPart, type_idPart, city_idPart,
                currency_idPart, pricePart, sizePart, ageFromPart, ageToPart, desc_arPart, desc_enPart);
    }

    private Call<EditModel> callEditOne(String image_path, int pos) {
        File file = new File(image_path);
        MultipartBody.Part photoPart;
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        if (pos == 1) {
            photoPart = MultipartBody.Part.createFormData("main_photo", file.getName(), mFile);
        } else if (pos == 2) {
            photoPart = MultipartBody.Part.createFormData("second_photo", file.getName(), mFile);
        } else {
            photoPart = MultipartBody.Part.createFormData("third_photo", file.getName(), mFile);
        }
        RequestBody idPart = RequestBody.create(MediaType.parse("text/plain"), pId);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody currency_idPart = RequestBody.create(MediaType.parse("text/plain"), currencyCode);
        RequestBody pricePart = RequestBody.create(MediaType.parse("text/plain"), priceET.getText().toString());
        RequestBody sizePart = RequestBody.create(MediaType.parse("text/plain"), sizeET.getText().toString());
        RequestBody ageFromPart = RequestBody.create(MediaType.parse("text/plain"), ageFromET.getText().toString());
        RequestBody ageToPart = RequestBody.create(MediaType.parse("text/plain"), ageToET.getText().toString());
        RequestBody desc_arPart = RequestBody.create(MediaType.parse("text/plain"), desc_ar_ET.getText().toString());
        RequestBody desc_enPart = RequestBody.create(MediaType.parse("text/plain"), desc_en_ET.getText().toString());

        return apiStore.editStore1(idPart,
                photoPart,
                name_arPart,
                name_enPart,
                type_idPart,
                city_idPart,
                currency_idPart, pricePart, sizePart, ageFromPart, ageToPart, desc_arPart, desc_enPart);
    }

    private Call<EditModel> callEditTwo(String image_path1, String image_path2, int pos) {
        File file1 = new File(image_path1);
        File file2 = new File(image_path2);
        MultipartBody.Part photoPart1, photoPart2;
        RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), file1);
        RequestBody mFile2 = RequestBody.create(MediaType.parse("image/*"), file2);
        if (pos == 12) {
            photoPart1 = MultipartBody.Part.createFormData("main_photo", file1.getName(), mFile1);
            photoPart2 = MultipartBody.Part.createFormData("second_photo", file2.getName(), mFile2);
        } else if (pos == 13) {
            photoPart1 = MultipartBody.Part.createFormData("main_photo", file1.getName(), mFile1);
            photoPart2 = MultipartBody.Part.createFormData("third_photo", file2.getName(), mFile2);
        } else {
            photoPart1 = MultipartBody.Part.createFormData("second_photo", file1.getName(), mFile1);
            photoPart2 = MultipartBody.Part.createFormData("third_photo", file2.getName(), mFile2);
        }
        RequestBody idPart = RequestBody.create(MediaType.parse("text/plain"), pId);
        RequestBody name_arPart = RequestBody.create(MediaType.parse("text/plain"), name_arET.getText().toString());
        RequestBody name_enPart = RequestBody.create(MediaType.parse("text/plain"), name_enET.getText().toString());
        RequestBody type_idPart = RequestBody.create(MediaType.parse("text/plain"), typeCode);
        RequestBody city_idPart = RequestBody.create(MediaType.parse("text/plain"), cityCode);
        RequestBody currency_idPart = RequestBody.create(MediaType.parse("text/plain"), currencyCode);
        RequestBody pricePart = RequestBody.create(MediaType.parse("text/plain"), priceET.getText().toString());
        RequestBody sizePart = RequestBody.create(MediaType.parse("text/plain"), sizeET.getText().toString());
        RequestBody ageFromPart = RequestBody.create(MediaType.parse("text/plain"), ageFromET.getText().toString());
        RequestBody ageToPart = RequestBody.create(MediaType.parse("text/plain"), ageToET.getText().toString());
        RequestBody desc_arPart = RequestBody.create(MediaType.parse("text/plain"), desc_ar_ET.getText().toString());
        RequestBody desc_enPart = RequestBody.create(MediaType.parse("text/plain"), desc_en_ET.getText().toString());

        return apiStore.editStore2(idPart,
                photoPart1, photoPart2,
                name_arPart,
                name_enPart,
                type_idPart,
                city_idPart,
                currency_idPart, pricePart, sizePart, ageFromPart, ageToPart, desc_arPart, desc_enPart);
    }

    private Call<EditModel> callEditNonImage() {
        String name_arPart = name_arET.getText().toString();
        String name_enPart = name_enET.getText().toString();
        String desc_arPart = desc_ar_ET.getText().toString();
        String desc_enPart = desc_en_ET.getText().toString();
        String ageFromPart = ageFromET.getText().toString();
        String ageToPart = ageToET.getText().toString();
        String pricePart = priceET.getText().toString();
        String sizePart = sizeET.getText().toString();

        return apiStore.editStoreNonImage(pId, name_arPart,
                name_enPart, typeCode,
                cityCode, currencyCode,
                pricePart, sizePart,
                desc_arPart,
                desc_enPart, ageFromPart,
                ageToPart);
    }

    ///////////////On Click///////////////
    @OnClick({R.id.imageView1, R.id.imageView2, R.id.imageView3})
    protected void imageOnClick(ImageView imageView) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        int id = imageView.getId();
        switch (id) {
            case R.id.imageView1:
                startActivityForResult(intent, IMAGE_CODE1);
                break;
            case R.id.imageView2:
                startActivityForResult(intent, IMAGE_CODE2);
                break;
            case R.id.imageView3:
                startActivityForResult(intent, IMAGE_CODE3);
                break;
        }
    }

    @OnClick(R.id.addBtn)
    protected void onNextBtnClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            112);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                // Permission has already been granted
                addStoreNow();
            }
        } else {
            addStoreNow();
        }
    }

    @OnClick({R.id.closeIB1, R.id.closeIB2, R.id.closeIB3})
    protected void imageOnClick(ImageButton imageButton) {
        int id = imageButton.getId();
        switch (id) {
            case R.id.closeIB1:
                Picasso.get().load(R.drawable.place_holder).into(imageView1);
                image_path1 = "";
                break;
            case R.id.closeIB2:
                Picasso.get().load(R.drawable.place_holder).into(imageView2);
                image_path2 = "";
                break;
            case R.id.closeIB3:
                Picasso.get().load(R.drawable.place_holder).into(imageView3);
                image_path3 = "";
                break;
        }
    }

    private void addStoreNow() {
        if (countryCode != null) {
            if (cityCode != null) {
                if (!image_path1.equals("")) {
                    if (!image_path2.equals("")) {
                        if (!image_path3.equals("")) {
                            serverEditStoreAll();
                        }//end add all
                        else {
                            serverEditStore2(image_path1, image_path2, 12);
                        }//end 1 & 2
                    } else if (!image_path3.equals("")) {
                        serverEditStore2(image_path1, image_path3, 13);
                    }//end add only 1 & 3
                    else {
                        serverEditStore1(image_path1, 1);
                    }//end only add 1
                } else if (!image_path2.equals("")) {
                    if (!image_path3.equals("")) {
                        serverEditStore2(image_path2, image_path3, 23);
                    }//end  2 & 3
                    else {
                        serverEditStore1(image_path2, 2);
                    }//end add only 2
                } else if (!image_path3.equals("")) {
                    serverEditStore1(image_path3, 3);
                }//end only 3
                else {
                    serverEditStoreNonImage();
                }//edn no image selected
            } else
                Toast.makeText(mContext, "" + getString(R.string.enter_city), Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(mContext, "" + getString(R.string.enter_country), Toast.LENGTH_SHORT).show();
    }

    private void serverEditStore1(String image_path1, int pos) {
        progressBar.setVisibility(View.VISIBLE);
        callEditOne(image_path1, pos).enqueue(new Callback<EditModel>() {
            @Override
            public void onResponse(@NonNull Call<EditModel> call, @NonNull Response<EditModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("1")) {
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                            moveToFragment.moveInMain(new SubMyAddsStoreFragment());
                        } else
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<EditModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void serverEditStore2(String image_path1, String image_path2, int pos) {
        progressBar.setVisibility(View.VISIBLE);
        callEditTwo(image_path1, image_path2, pos).enqueue(new Callback<EditModel>() {
            @Override
            public void onResponse(@NonNull Call<EditModel> call, @NonNull Response<EditModel> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("1")) {
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                            moveToFragment.moveInMain(new SubMyAddsStoreFragment());
                        } else
                            Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<EditModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_CODE1 && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            assert uri != null;
        }
        if (resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    switch (requestCode) {
                        case IMAGE_CODE1:
                            image_path1 = getImagePathFromUri(uri, getActivity());
                            showImageInView(uri, imageView1);
                            break;
                        case IMAGE_CODE2:
                            image_path2 = getImagePathFromUri(uri, getActivity());
                            showImageInView(uri, imageView2);
                            break;
                        case IMAGE_CODE3:
                            image_path3 = getImagePathFromUri(uri, getActivity());
                            showImageInView(uri, imageView3);
                            break;
                    }
                }
            }
        }
    }//end onActivityResult

    private void showImageInView(Uri image_path, ImageView imageView) {
        Picasso.get().load(image_path).resize(500, 500).error(R.drawable.place_holder).into(imageView);
    }

    private static String getImagePathFromUri(Uri contentURI, Context context) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            return imagePath;
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public void onStart() {
        if (isConnected()) {
            serverCountrySpinner();
            serverTypeSpinner();
        } else
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        callGetCountry().cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.edit));
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new SubMyAddsStoreFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

}
