package com.tkmsoft.taahel.fragments.login;

import androidx.fragment.app.Fragment;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.adapters.spinners.DefaultSpinnerAdapter;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.api.auth.register.Data;
import com.tkmsoft.taahel.model.api.auth.register.RegisterModel;
import com.tkmsoft.taahel.model.api.auth.register.Status;
import com.tkmsoft.taahel.model.api.auth.register.Title;
import com.tkmsoft.taahel.model.api.auth.updatePlayerId.UpdatePlayerIDModel;
import com.tkmsoft.taahel.model.spinners.CountriesModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment {

    @BindView(R.id.nameET)
    EditText nameET;
    @BindView(R.id.mobileET)
    EditText mobileET;
    @BindView(R.id.passwordET)
    EditText passwordET;
    @BindView(R.id.password_confirmET)
    EditText password_confirmET;
    @BindView(R.id.phoneCode_spinner)
    Spinner spinner;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tV)
    TextView textView;

    private String phoneCode;
    private ListSharedPreference listSharedPreference;
    private String TAG = getClass().getName();
    private FragmentActivity mContext;
    private ArrayList<String> phoneCodeList;
    private ConnectionDetector connectionDetector;
    private MoveToFragment moveToFragment;
    private ApiLink apiLinkAuth;
    private OSPermissionSubscriptionState status;
    private boolean regDone = false;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        connectionDetector = new ConnectionDetector(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);

        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkAuth = MyRetrofitClient.auth().create(ApiLink.class);
        status = OneSignal.getPermissionSubscriptionState();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        phoneCodeList = new ArrayList<>();
        phoneCodeList.add(0, getString(R.string.code));
        serverPhoneCodeSpinner();
        initPhoneCodeSpinner();
        mobileET.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                textView.setVisibility(View.VISIBLE);
                textView.setText("055" + " " + "XXX XXXX");
            } else {
                textView.setVisibility(View.GONE);
            }
        });
    }

    private void initPhoneCodeSpinner() {
        spinner.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        DefaultSpinnerAdapter adapter = new DefaultSpinnerAdapter(mContext, phoneCodeList);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    if (listSharedPreference.getLanguage().equals("ar")) {
                        String string = phoneCodeList.get(i);
                        String[] parts = string.split("\\+");
                        String part12 = parts[1];
                        phoneCode = "+" + part12;
                    } else {
                        phoneCode = phoneCodeList.get(i);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void serverPhoneCodeSpinner() {
        ApiLink retrofit = MyRetrofitClient.getBase().create(ApiLink.class);
        final Call<CountriesModel> registerCall = retrofit.getCountries();
        registerCall.enqueue(new Callback<CountriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CountriesModel> call, @NonNull Response<CountriesModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        CountriesModel.StatusBean statusBean = response.body().getStatus();
                        if (statusBean != null) {
                            if (statusBean.getType().equals("success")) {
                                CountriesModel.DataBean dataBean = response.body().getData();
                                if (dataBean != null) {
                                    for (int i = 0; i < dataBean.getCountries().size(); i++) {
                                        phoneCodeList.add(dataBean.getCountries().get(i).getCode());
                                    }
                                }
                            } else
                                Toast.makeText(mContext, "" + statusBean.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountriesModel> call, @NonNull Throwable t) {
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                serverPhoneCodeSpinner();
            }
        });
    }//end serverSpinner()

    private void serverReg() {
        progressBar.setVisibility(View.VISIBLE);
        callRegister().enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(@NonNull Call<RegisterModel> call, @NonNull Response<RegisterModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("1")) {
                            regDone = true;
                            Data data = response.body().getData();
                            if (data != null) {
                                saveData(data);
                            }
                        } else {
                            Title title = status.getTitle();
                            if (title != null)
                                setResponseMessage(title.getAr(), title.getEn());

                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<RegisterModel> call, @NonNull Throwable t) {
                Toast.makeText(mContext, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void serverUpdatePlayerId() {
        callUpdatePlayerId().enqueue(new Callback<UpdatePlayerIDModel>() {
            @Override
            public void onResponse(@NonNull Call<UpdatePlayerIDModel> call, @NonNull Response<UpdatePlayerIDModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        com.tkmsoft.taahel.model.api.auth.updatePlayerId.Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {
                                moveToFragment.moveInLogin(new CodeVerifyFragment());
                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdatePlayerIDModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private Call<UpdatePlayerIDModel> callUpdatePlayerId() {
        return apiLinkAuth.updatePlayerId(
                listSharedPreference.getToken(),//token
                status.getSubscriptionStatus().getUserId()//player_id
        );
    }

    private String getLanguage() {
        return listSharedPreference.getLanguage();
    }

    private void setResponseMessage(String ar, String en) {
        if (getLanguage().equals("ar"))
            Toast.makeText(mContext, "" + ar, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + en, Toast.LENGTH_SHORT).show();
    }

    private Call<RegisterModel> callRegister() {
        final String type = listSharedPreference.getUType();
        final String name = nameET.getText().toString();
        final String phone = mobileET.getText().toString();
        final String phoneKey = phoneCode;
        final String password = passwordET.getText().toString();
        final String passConfirm = password_confirmET.getText().toString();

        return apiLinkAuth.register(type, name, phone, phoneKey, password, passConfirm);
    }

    private void saveData(Data data) {
        listSharedPreference.setToken(data.getApiToken());
        serverUpdatePlayerId();
        listSharedPreference.setUName(data.getName());
        listSharedPreference.setUUserName(data.getUsername());
        listSharedPreference.setPassword(passwordET.getText().toString());
        listSharedPreference.setUPhone(data.getPhone());
        listSharedPreference.setUPhoneKey(data.getPhoneKey());
        listSharedPreference.setUType(data.getType());
        listSharedPreference.setUConfirm(data.getConfirm());
        listSharedPreference.setUApprove(data.getApprove());
    }

    @OnClick(R.id.sign_up_button)
    void onSignUpClick() {
        if (connectionDetector.isConnectingToInternet()) {
            if (!regDone)
                serverReg();
            else
                serverUpdatePlayerId();
        } else
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
    }

    private void fireBackButtonEvent() {
        ((LoginActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                assert getFragmentManager() != null;

                getFragmentManager().beginTransaction()
                        .replace(R.id.login_frame, new RegSelectorFragment())
                        .commit();
            }
        });
    }//end back pressed

}