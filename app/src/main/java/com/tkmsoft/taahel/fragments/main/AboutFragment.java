package com.tkmsoft.taahel.fragments.main;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends Fragment {

    private final static String MOBILE_NUM = "+966597201828";
    private final static String EMAIL = "info@taahel.com";


    //    @BindViews({R.id.relativeLayout1, R.id.relativeLayout2})
//    List<RelativeLayout> relativeLayoutList;
    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            FragmentActivity mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        MainViewsCallBack mMainViewsCallBack;
        try {
            mMainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        fireBackButtonEvent();
        mMainViewsCallBack.serToolbarTitle(getString(R.string.about_us));
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnClick(R.id.relativeLayout2)
    protected void onMailClick(){

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL  , new String[] { EMAIL });
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.taahel_subject));
        startActivity(Intent.createChooser(intent, getString(R.string.email_via)));
    }

    @OnClick(R.id.relativeLayout1)
    protected void onPhoneClick(){
        Intent dialIntent = new Intent(Intent.ACTION_DIAL);
        dialIntent.setData(Uri.parse("tel:" + MOBILE_NUM));
        startActivity(dialIntent);

    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) getActivity()).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    getFragmentManager().beginTransaction()
                            .replace(R.id.main_frameLayout, new HomeFragment())
                            .commit();
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

}
