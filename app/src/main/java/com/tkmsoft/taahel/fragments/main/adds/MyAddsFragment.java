package com.tkmsoft.taahel.fragments.main.adds;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.addings.MyAddsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.fragments.main.adds.activity.SubMyAddsActivityFragment;
import com.tkmsoft.taahel.fragments.main.adds.center.SubMyAddsCenterFragment;
import com.tkmsoft.taahel.fragments.main.adds.edu.SubMyAddsEduFragment;
import com.tkmsoft.taahel.fragments.main.adds.store.SubMyAddsStoreFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyAddsFragment extends Fragment implements MyAddsAdapter.ListAllListeners {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.pullRefreshLayout)
    PullRefreshLayout pullRefreshLayout;
    private MainViewsCallBack mainViewsCallBack;
    private ListSharedPreference listSharedPreference;
    private FragmentActivity mContext;
    private MoveToFragment moveToFragment;

    public MyAddsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity)
            mContext = (FragmentActivity) context;
        super.onAttach(context);
        if (context instanceof MainViewsCallBack) {
            mainViewsCallBack = (MainViewsCallBack) context;
        } else {
            throw new RuntimeException(context.toString()
                    + "error");
        }
        moveToFragment = new MoveToFragment(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        fireBackButtonEvent();
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_adds, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
        initAdapter();
    }


    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
    }

    private void initAdapter() {
        Integer[] images = {
                R.drawable.place_holder,
                R.drawable.place_holder,
                R.drawable.place_holder,
                R.drawable.place_holder};
        List<Integer> imagesList = new ArrayList<>(Arrays.asList(images));

        String[] names = {
                getString(R.string.store),
                getString(R.string.centers),
                getString(R.string.education_center),
                getString(R.string.activities)};

        List<String> namesList = new ArrayList<>(Arrays.asList(names));


        MyAddsAdapter myAddsAdapter = new MyAddsAdapter(mContext, imagesList, namesList, this);
        recyclerView.setAdapter(myAddsAdapter);
        myAddsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        mainViewsCallBack.showFilterBtn(false);
        mainViewsCallBack.serToolbarTitle(getString(R.string.my_adding_items));
        mainViewsCallBack.showAddFab(false);
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {

        super.onDetach();
        mainViewsCallBack = null;
    }

    private void fireBackButtonEvent() {
        try {
            ((MainActivity) Objects.requireNonNull(getActivity())).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
                @Override
                public void onBackPressed() {
                    moveToFragment.moveInMain(new HomeFragment());
                }
            });
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }//end back pressed

    @Override
    public void onItemViewClick(String name, int adapterPosition) {
        listSharedPreference.setAddsType(String.valueOf(adapterPosition));
        switch (adapterPosition) {
            case 0:
            moveToFragment.moveInMain(new SubMyAddsStoreFragment());
            break;
            case 1:
                moveToFragment.moveInMain(new SubMyAddsCenterFragment());
                break;
            case 2:
                moveToFragment.moveInMain(new SubMyAddsEduFragment());
                break;
            case 3:
                moveToFragment.moveInMain(new SubMyAddsActivityFragment());
                break;
        }
    }
}