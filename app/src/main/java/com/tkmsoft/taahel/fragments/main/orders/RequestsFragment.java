package com.tkmsoft.taahel.fragments.main.orders;


import android.app.Activity;
import androidx.fragment.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.adapters.requests.RequestsAdapter;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.interfaces.MainViewsCallBack;
import com.tkmsoft.taahel.model.api.order.RequestsModel;
import com.tkmsoft.taahel.model.api.order.drs.Data;
import com.tkmsoft.taahel.model.api.order.drs.DoctorOrdersModel;
import com.tkmsoft.taahel.model.api.order.drs.Order;
import com.tkmsoft.taahel.model.api.order.drs.OrderCounter;
import com.tkmsoft.taahel.model.api.order.drs.Status;
import com.tkmsoft.taahel.model.api.order.patient.PatientOrderModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class RequestsFragment extends Fragment implements RequestsAdapter.ListAllListeners {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    RequestsAdapter requestsAdapter;
    private MainViewsCallBack mainViewsCallBack;

    private ArrayList<RequestsModel> mDataList;

    private ArrayList<Integer> counterList = new ArrayList<>();


    ListSharedPreference listSharedPreference;
    private String TAG = getClass().getName();
    private FragmentActivity mContext;
    private MoveToFragment moveToFragment;
    private ApiLink apiLinkOrder;

    public RequestsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        try {
            mainViewsCallBack = (MainViewsCallBack) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "error");
        }
        listSharedPreference = new ListSharedPreference(mContext);
        moveToFragment = new MoveToFragment(mContext);
        ConnectionDetector connectionDetector = new ConnectionDetector(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkOrder = MyRetrofitClient.getOrder().create(ApiLink.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_requests, container, false);
        ButterKnife.bind(this, rootView);
        initUI(rootView);
        return rootView;
    }

    private void initUI(View rootView) {
        initRecyclerView();
    }

    private String getType() {
        return listSharedPreference.getUType();
    }

    private void serverPatientRequests() {
        progressBar.setVisibility(View.VISIBLE);
        callPatientOrders().enqueue(new Callback<PatientOrderModel>() {
            @Override
            public void onResponse(@NonNull Call<PatientOrderModel> call, @NonNull Response<PatientOrderModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    com.tkmsoft.taahel.model.api.order.patient.Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            com.tkmsoft.taahel.model.api.order.patient.Data data = response.body().getData();
                            if (data != null) {
                                List<com.tkmsoft.taahel.model.api.order.patient.Order> order = data.getOrders();
                                com.tkmsoft.taahel.model.api.order.patient.OrderCounter orderCounter = data.getOrderCounter();
                                if (orderCounter != null) {
                                    counterList.add(orderCounter.getPinned());
                                    counterList.add(orderCounter.getConfirmedNotPaid());
                                    counterList.add(orderCounter.getConfirmedPaid());
                                    counterList.add(orderCounter.getCompleted());
                                    counterList.add(orderCounter.getCanceled());
                                    Log.v(TAG, counterList + "");
                                }
                                if (order != null) {
                                    initAdapter();
                                }
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<PatientOrderModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    private Call<PatientOrderModel> callPatientOrders() {
        return apiLinkOrder.getPatientOrders(getToken(), "4");
    }

    private Call<DoctorOrdersModel> callDrOrders() {
        return apiLinkOrder.getDrOrders(getToken(), "4");
    }

    private String getToken() {
        return listSharedPreference.getToken();
    }

    private void initRecyclerView() {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        }
    }

    private void serverDrRequests() {
        progressBar.setVisibility(View.VISIBLE);
        callDrOrders().enqueue(new Callback<DoctorOrdersModel>() {
            @Override
            public void onResponse(@NonNull Call<DoctorOrdersModel> call, @NonNull Response<DoctorOrdersModel> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        if (status.getType().equals("success")) {
                            Data data = response.body().getData();
                            if (data != null) {
                                List<Order> order = data.getOrders();
                                OrderCounter orderCounter = data.getOrderCounter();
                                if (orderCounter != null) {
                                    counterList.add(orderCounter.getPinned());
                                    counterList.add(orderCounter.getConfirmedNotPaid());
                                    counterList.add(orderCounter.getConfirmedPaid());
                                    counterList.add(orderCounter.getCompleted());
                                    counterList.add(orderCounter.getCanceled());
                                    Timber.tag(TAG).v(counterList + "");
                                }
                                if (order != null) {
                                    initAdapter();
                                }
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<DoctorOrdersModel> call, @NonNull Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }//end server

    private void setData() {
        mDataList = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        names.add(getString(R.string.pinned_order));
        names.add(getString(R.string.confirmed_order_not_paid));
        names.add(getString(R.string.confirmed_order_paid));
        names.add(getString(R.string.completed_order));
        names.add(getString(R.string.cancled_order));

        for (int i = 0; i < names.size(); i++) {
            RequestsModel th = new RequestsModel(names.get(i), String.valueOf(counterList.get(i)));
            mDataList.add(i, th);
        }
    }

    private void initAdapter() {
        try {
            setData();
            requestsAdapter = new RequestsAdapter(mContext, mDataList, this);
            recyclerView.setAdapter(requestsAdapter);
            requestsAdapter.notifyDataSetChanged();
        } catch (Exception ignore) {
        }
    }

    @Override
    public void onStart() {
        if (getType().equals("0"))
            serverDrRequests();
        else
            serverPatientRequests();
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainViewsCallBack.showAddFab(false);
        mainViewsCallBack.showFilterBtn(false);
        if (getType().equals("1"))
            mainViewsCallBack.serToolbarTitle(getString(R.string.my_requests));
        else
            mainViewsCallBack.serToolbarTitle(getString(R.string.patient_orders));
    }

    private void fireBackButtonEvent() {
        ((MainActivity) mContext).setOnBackPressedListener(new BaseBackPressedListener(getActivity()) {
            @Override
            public void onBackPressed() {
                moveToFragment.moveInMain(new HomeFragment());
                listSharedPreference.deleteOrderType();
            }
        });
    }//end back pressed

    @Override
    public void onPause() {
        callDrOrders().cancel();
        callPatientOrders().cancel();
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainViewsCallBack = null;
    }

    @Override
    public void onItemViewClick(RequestsModel requestsModel, int adapterPosition) {
        switch (adapterPosition) {
            case 0:
                //pinned_0
                listSharedPreference.setOrderType("0");
                break;
            case 1:
                //confirmed not paid
                listSharedPreference.setOrderType("1");
                break;
            case 2:
                //confirmed paid
                listSharedPreference.setOrderType("2");
                break;
            case 3:
                //completed
                listSharedPreference.setOrderType("3");
                break;
            case 4:
                //canceled
                listSharedPreference.setOrderType("-1");
                break;
        }
        moveToFragment.moveInMain(new SubRequestFragment());
    }
}
