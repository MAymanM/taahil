package com.tkmsoft.taahel.fragments.login;

import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.MainActivity;
import com.tkmsoft.taahel.fragments.login.resetpassword.ForgetPasswordFragment;
import com.tkmsoft.taahel.helper.BaseBackPressedListener;
import com.tkmsoft.taahel.model.api.auth.login.Data;
import com.tkmsoft.taahel.model.api.auth.login.Doctor;
import com.tkmsoft.taahel.model.api.auth.login.LoginModel;
import com.tkmsoft.taahel.model.api.auth.login.Member;
import com.tkmsoft.taahel.model.api.auth.login.Patient;
import com.tkmsoft.taahel.model.api.auth.login.Specialization;
import com.tkmsoft.taahel.model.api.auth.login.Status;
import com.tkmsoft.taahel.model.api.auth.updatePlayerId.UpdatePlayerIDModel;
import com.tkmsoft.taahel.network.ApiLink;
import com.tkmsoft.taahel.network.MyRetrofitClient;
import com.tkmsoft.taahel.util.ConnectionDetector;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.application.MyApp;
import com.tkmsoft.taahel.util.PlayAnimation;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment {

    @BindView(R.id.mobileET)
    EditText mobileET;
    @BindView(R.id.passwordET)
    EditText passwordET;
    @BindView(R.id.btn_pass_visibility)
    ImageButton btn_pass_visibility;
    @BindView(R.id.sign_in_button)
    Button btn_signin;
    @BindView(R.id.register_button)
    Button btn_register;
    @BindView(R.id.skip_button)
    Button btn_skip;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.forgot_passwordTV)
    TextView forgot_passwordTV;

    private ListSharedPreference listSharedPreference;
    private boolean isPasswordShown;
    private ConnectionDetector connectionDetector;

    private String TAG = getClass().getSimpleName();
    private FragmentActivity mContext;
    private MoveToFragment moveToFragment;
    private ApiLink apiLinkAuth;
    private PlayAnimation playAnimation;
    private OSPermissionSubscriptionState status;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        if (context instanceof FragmentActivity) {
            mContext = (FragmentActivity) context;
        }
        super.onAttach(context);
        connectionDetector = new ConnectionDetector(mContext);
        listSharedPreference = new ListSharedPreference(mContext);
        playAnimation = new PlayAnimation(mContext);
        moveToFragment = new MoveToFragment(mContext);
        fireBackButtonEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiLinkAuth = MyRetrofitClient.auth().create(ApiLink.class);
        status = OneSignal.getPermissionSubscriptionState();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, rootView);
        initUI();
        return rootView;
    }

    private void initUI() {
        passwordET.setOnFocusChangeListener((view, b) -> {
            if (b) {
                if (passwordET.length() > 0 || passwordET.length() == 0) {
                    passwordET.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            } else {

                if (passwordET.length() > 0) {
                    passwordET.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);

                } else {
                    passwordET.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            if (listSharedPreference.getLanguage().equals("ar")) {
                mobileET.setGravity(Gravity.END | Gravity.CENTER);
                passwordET.setGravity(Gravity.START | Gravity.CENTER);
            }

    }


    @OnClick(R.id.btn_pass_visibility)
    void onBtnPassVisibleClick() {

        if (isPasswordShown) {
            btn_pass_visibility.setImageResource(R.drawable.ic_visibility_accent_24dp);
            passwordET.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
            isPasswordShown = false;
        } else {
            btn_pass_visibility.setImageResource(R.drawable.ic_visibility_off_accent_24dp);
            passwordET.setInputType(InputType.TYPE_CLASS_TEXT);
            isPasswordShown = true;
        }
    }

    @OnClick(R.id.sign_in_button)
    void onLoginClick() {
        if (connectionDetector.isConnectingToInternet()) {
            serverLogin();
        } else {
            Toast.makeText(mContext, "" + getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.forgot_passwordTV)
    void onForgot_passwordTVClick() {
        moveToFragment.moveInLogin(new ForgetPasswordFragment());
    }

    private void serverLogin() {
        playAnimation.showAnimationFullScreen(R.raw.load, getString(R.string.loading_login), 100);
        callLogin().enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {
                playAnimation.stopAnimation();
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Status status = response.body().getStatus();
                    if (status != null) {
                        switch (status.getType()) {
                            case "1": {

                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());

                                Data data = response.body().getData();
                                if (data != null) {
                                    Member member = data.getMember();
                                    if (member != null) {
                                        String memberType = member.getType();
                                        saveUserData(member);

                                        if (memberType.equals("0")) {
                                            Doctor doctor = member.getDoctor();
                                            List<Specialization> specialization = member.getSpecializations();
                                            if (doctor != null && specialization != null)
                                                saveDrData(member, doctor, specialization);
                                        } else {
                                            Patient patient = member.getPatient();
                                            if (patient != null)
                                                savePatientData(patient);
                                        }

                                        listSharedPreference.setLoginStatus(true);
                                        //begin transaction
                                        goToMainActivity();
                                    }
                                }
                                serverUpdatePlayerId();
                                break;
                            }
                            case "01": {
                                //not confirmed

                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());

                                Data data = response.body().getData();
                                if (data != null) {
                                    Member member = data.getMember();
                                    if (member != null) {
                                        goToConfirmPage(member);
                                    }
                                }
                                break;
                            }
                            case "02": {
                                //not completed register

                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());

                                Data data = response.body().getData();
                                if (data != null) {
                                    Member member = data.getMember();
                                    if (member != null) {
                                        listSharedPreference.setToken(member.getApiToken()); // important to set token
                                        if (member.getType().equals("0"))
                                            moveToFragment.moveInLogin(new TherapistRegFragment());
                                        else
                                            moveToFragment.moveInLogin(new MostafedRegFragment());
                                    }
                                }
                                break;
                            }
                            case "03": {
                                //not completed schedule

                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());
                                
                                Data data = response.body().getData();
                                if (data != null) {
                                    Member member = data.getMember();
                                    if (member != null) {
                                        listSharedPreference.deleteAllSharedPref();
                                        listSharedPreference.setToken(member.getApiToken()); // important to set token
                                        moveToFragment.moveInLogin(new TherapistRegFragment2());
                                    }
                                }
                                break;
                            }
                            default:
                                setResponseMessage(status.getTitle().getAr(), status.getTitle().getEn());
                                break;
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginModel> call, @NonNull Throwable t) {
                t.printStackTrace();
                playAnimation.stopAnimation();
            }
        });
    }

    private void serverUpdatePlayerId() {
        callUpdatePlayerId().enqueue(new Callback<UpdatePlayerIDModel>() {
            @Override
            public void onResponse(@NonNull Call<UpdatePlayerIDModel> call, @NonNull Response<UpdatePlayerIDModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        com.tkmsoft.taahel.model.api.auth.updatePlayerId.Status status = response.body().getStatus();
                        if (status != null) {
                            if (status.getType().equals("success")) {

                            } else
                                Toast.makeText(mContext, "" + status.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdatePlayerIDModel> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private Call<UpdatePlayerIDModel> callUpdatePlayerId() {
        return apiLinkAuth.updatePlayerId(
                listSharedPreference.getToken(),//token
                status.getSubscriptionStatus().getUserId()//player_id
        );
    }

    private void setResponseMessage(String ar, String en) {
        if (getLang().equals("ar"))
            Toast.makeText(mContext, "" + ar, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(mContext, "" + en, Toast.LENGTH_SHORT).show();
    }

    private Call<LoginModel> callLogin() {
        String phone = mobileET.getText().toString();
        String password = passwordET.getText().toString();
        return apiLinkAuth.login(phone, password);
    }

    private String getLang() {
        return listSharedPreference.getLanguage();
    }

    private void goToConfirmPage(Member data) {
        listSharedPreference.setToken(data.getApiToken());
        listSharedPreference.setUType(data.getType());
        listSharedPreference.setUName(data.getName());
        listSharedPreference.setUUserName(data.getUsername());
        listSharedPreference.setUAvatar(data.getAvatar());
        listSharedPreference.setUId(data.getId());
        listSharedPreference.setUPhone(data.getPhone());
        listSharedPreference.setUPhoneKey(data.getPhoneKey());
        moveToFragment.moveInLogin(new CodeVerifyFragment());
    }

    private void saveDrData(Member member, Doctor doctor, List<Specialization> specializations) {
        listSharedPreference.setUAbout(doctor.getAbout());
        listSharedPreference.setUEducation(doctor.getEducation());
        listSharedPreference.setUExperience(doctor.getExperiences());
        listSharedPreference.setULicense(doctor.getLicense());
        listSharedPreference.setUPrice(doctor.getPrice());
        ArrayList<String> specialistsStringList = new ArrayList<>();
        for (int i = 0; i < specializations.size(); i++) {
            specialistsStringList.add(specializations.get(i).getNameAr());
        }
        listSharedPreference.setUSpecialists(specialistsStringList);

        if (member.getCurrency() != null) {
            if (getLang().equals("ar"))
                listSharedPreference.setUCurrency(member.getCurrency().getNameAr());
            else
                listSharedPreference.setUCurrency(member.getCurrency().getNameEn());
            listSharedPreference.setUCurrencyId(member.getCurrency().getId());
        }
    }


    private void savePatientData(Patient patient) {
        listSharedPreference.setUBirthDate(patient.getBirthday());
        listSharedPreference.setUReport(patient.getReport());
    }

    private void saveUserData(Member member) {
        listSharedPreference.setToken(member.getApiToken());
        listSharedPreference.setUType(member.getType());
        listSharedPreference.setUName(member.getName());
        listSharedPreference.setUUserName(member.getUsername());
        listSharedPreference.setUEmail(member.getEmail());
        listSharedPreference.setUAvatar(member.getAvatar());
        if (getLang().equals("ar")) {
            listSharedPreference.setUCity(member.getCity().getNameAr());
            listSharedPreference.setUCountry(member.getCountryAr());
        } else {
            listSharedPreference.setUCity(member.getCity().getNameEn());
            listSharedPreference.setUCountry(member.getCountryEn());
        }
        listSharedPreference.setUGender(member.getGender());
        listSharedPreference.setUId(member.getId());
        listSharedPreference.setULat(member.getLat());
        listSharedPreference.setULong(member.getLong());
        listSharedPreference.setUPhone(member.getPhone());
        listSharedPreference.setUPhoneKey(member.getPhoneKey());
        listSharedPreference.setUCountryId(member.getCountry().getId());
        listSharedPreference.setUCityId(member.getCity().getId());
    }


    @OnClick(R.id.register_button)
    void onRegisterClick() {
        assert getFragmentManager() != null;
        listSharedPreference.deleteAllSharedPref();
        Log.d(TAG, "Deleted ALL Shared Pref");
        moveToFragment.moveInLogin(new RegSelectorFragment());
    }

    @OnClick(R.id.skip_button)
    void onSkipClick() {
        listSharedPreference.setLoginStatus(false);
        listSharedPreference.setUType("-1");
        goToMainActivity();
    }

    private void goToMainActivity() {
        Intent myIntent = new Intent();
        myIntent.setClassName(MyApp.getContext().getApplicationContext(), Objects.requireNonNull(MainActivity.class.getCanonicalName()));
        startActivity(myIntent);
        mContext.finish();
    }


    private void fireBackButtonEvent() {
        ((LoginActivity) mContext).setOnBackPressedListener(
                new BaseBackPressedListener(mContext) {
            @Override
            public void onBackPressed() {
                mContext.finish();
            }
        });
    }//end back pressed

}//end class LoginFragment