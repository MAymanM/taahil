package com.tkmsoft.taahel.model.api.activities.view.category;

import java.util.List;

/**
 * Created by MahmoudAyman on 10/11/2018.
 **/
public class ActivityCategModel {

    /**
     * status : {"type":"success","title":"انواع الانشطه"}
     * data : {"categories":[{"id":1,"name_ar":"فعاليات","name_en":"Events","slug":"Events","created_at":"2018-06-17 15:00:00","updated_at":"2018-06-17 15:00:00"},{"id":2,"name_ar":"ندوات","name_en":"Seminars","slug":"Seminars","created_at":"2018-06-17 15:00:00","updated_at":"2018-06-17 15:00:00"},{"id":3,"name_ar":"دورات","name_en":"Courses","slug":"Courses","created_at":"2018-06-17 15:00:00","updated_at":"2018-06-17 15:00:00"},{"id":4,"name_ar":"مؤتمرات","name_en":"Conferences","slug":"Conferences","created_at":"2018-06-17 15:00:00","updated_at":"2018-06-17 15:00:00"},{"id":5,"name_ar":"ورش عمل","name_en":"Workshops","slug":"Workshops","created_at":"2018-06-17 15:00:00","updated_at":"2018-06-17 15:00:00"},{"id":6,"name_ar":"محاضرات","name_en":"Lectures","slug":"Lectures","created_at":"2018-06-17 15:00:00","updated_at":"2018-06-17 15:00:00"}]}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : انواع الانشطه
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        private List<CategoriesBean> categories;

        public List<CategoriesBean> getCategories() {
            return categories;
        }

        public void setCategories(List<CategoriesBean> categories) {
            this.categories = categories;
        }

        public static class CategoriesBean {
            /**
             * id : 1
             * name_ar : فعاليات
             * name_en : Events
             * slug : Events
             * created_at : 2018-06-17 15:00:00
             * updated_at : 2018-06-17 15:00:00
             */

            private int id;
            private String name_ar;
            private String name_en;
            private String slug;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName_ar() {
                return name_ar;
            }

            public void setName_ar(String name_ar) {
                this.name_ar = name_ar;
            }

            public String getName_en() {
                return name_en;
            }

            public void setName_en(String name_en) {
                this.name_en = name_en;
            }

            public String getSlug() {
                return slug;
            }

            public void setSlug(String slug) {
                this.slug = slug;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
