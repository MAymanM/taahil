
package com.tkmsoft.taahel.model.api.order.patient;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("caculations")
    @Expose
    private Integer caculations;
    @SerializedName("orders")
    @Expose
    private List<Order> orders = null;
    @SerializedName("orderCounter")
    @Expose
    private OrderCounter orderCounter;
    public Integer getCaculations() {
        return caculations;
    }

    public void setCaculations(Integer caculations) {
        this.caculations = caculations;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public OrderCounter getOrderCounter() {
        return orderCounter;
    }

    public void setOrderCounter(OrderCounter orderCounter) {
        this.orderCounter = orderCounter;
    }
}
