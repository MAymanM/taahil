
package com.tkmsoft.taahel.model.api.order.patient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pivot_ {

    @SerializedName("patient_order_id")
    @Expose
    private String patientOrderId;
    @SerializedName("job_time_id")
    @Expose
    private String jobTimeId;

    public String getPatientOrderId() {
        return patientOrderId;
    }

    public void setPatientOrderId(String patientOrderId) {
        this.patientOrderId = patientOrderId;
    }

    public String getJobTimeId() {
        return jobTimeId;
    }

    public void setJobTimeId(String jobTimeId) {
        this.jobTimeId = jobTimeId;
    }

}
