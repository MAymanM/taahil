package com.tkmsoft.taahel.model.spinners;

/**
 * Created by MahmoudAyman on 23/05/2018.
 */
public class GenderSpinnerModel {
    private String gender;

    public GenderSpinnerModel(String gender) {

        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
