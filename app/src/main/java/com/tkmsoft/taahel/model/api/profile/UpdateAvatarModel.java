package com.tkmsoft.taahel.model.api.profile;

public class UpdateAvatarModel {

    /**
     * status : {"type":"success","title":"avatar"}
     * data : {"avatar_path":"http://tohfa.net/storage/uploads/members/avatars/15343484981.png"}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : avatar
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        /**
         * avatar_path : http://tohfa.net/storage/uploads/members/avatars/15343484981.png
         */

        private String avatar_path;

        public String getAvatar_path() {
            return avatar_path;
        }

        public void setAvatar_path(String avatar_path) {
            this.avatar_path = avatar_path;
        }
    }
}
