package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 14/05/2018.
 */
public class TherapistScheduleModel {
    private String time;
    private boolean checked;
    private boolean free;
    private boolean paid;

    public TherapistScheduleModel(String time, boolean checked, boolean free, boolean paid) {
        this.time = time;
        this.checked = checked;
        this.free = free;
        this.paid = paid;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked= checked;
    }
}
