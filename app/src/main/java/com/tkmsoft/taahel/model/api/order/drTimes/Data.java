
package com.tkmsoft.taahel.model.api.order.drTimes;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("job_date_id")
    @Expose
    private Integer jobDateId;
    @SerializedName("dayTimes")
    @Expose
    private List<DayTime> dayTimes = null;
    @SerializedName("date")
    @Expose
    private String date;

    public Integer getJobDateId() {
        return jobDateId;
    }

    public void setJobDateId(Integer jobDateId) {
        this.jobDateId = jobDateId;
    }

    public List<DayTime> getDayTimes() {
        return dayTimes;
    }

    public void setDayTimes(List<DayTime> dayTimes) {
        this.dayTimes = dayTimes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
