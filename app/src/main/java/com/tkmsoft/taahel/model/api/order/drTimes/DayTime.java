
package com.tkmsoft.taahel.model.api.order.drTimes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DayTime {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("free")
    @Expose
    private String free;
    @SerializedName("job_date_id")
    @Expose
    private String jobDateId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getJobDateId() {
        return jobDateId;
    }

    public void setJobDateId(String jobDateId) {
        this.jobDateId = jobDateId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
