package com.tkmsoft.taahel.model.api;

import java.util.List;

/**
 * Created by MahmoudAyman on 10/19/2018.
 **/
public class PrivacyAndConditionModel {

    /**
     * status : {"type":"success","title":"conditions "}
     * data : {"conditions":{"current_page":1,"data":[{"id":2,"con_ar":"جميع محتويات هذه الخدمة هى حقوق ملكية فكرية محفوظة لتأهيل ويمنع إستخدامها باى شكل من الأشكال من أطراف أخرى","con_en":"جميع محتويات هذه الخدمة هى حقوق ملكية فكرية محفوظة لتأهيل ويمنع إستخدامها باى شكل من الأشكال من أطراف أخرى","created_at":"2018-06-26 12:43:26","updated_at":"2018-06-26 12:43:26"},{"id":1,"con_ar":"تتعهد بعدم بخس أى سلعة","con_en":"تتعهد بعدم بخس أى سلعة","created_at":"2018-06-26 12:42:49","updated_at":"2018-06-26 12:42:49"}],"first_page_url":"https://taahel.com/api/conditions?page=1","from":1,"last_page":1,"last_page_url":"https://taahel.com/api/conditions?page=1","next_page_url":null,"path":"https://taahel.com/api/conditions","per_page":10,"prev_page_url":null,"to":2,"total":2}}
     */

    private StatusBean status;
    private DataBeanX data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : conditions
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBeanX {
        /**
         * conditions : {"current_page":1,"data":[{"id":2,"con_ar":"جميع محتويات هذه الخدمة هى حقوق ملكية فكرية محفوظة لتأهيل ويمنع إستخدامها باى شكل من الأشكال من أطراف أخرى","con_en":"جميع محتويات هذه الخدمة هى حقوق ملكية فكرية محفوظة لتأهيل ويمنع إستخدامها باى شكل من الأشكال من أطراف أخرى","created_at":"2018-06-26 12:43:26","updated_at":"2018-06-26 12:43:26"},{"id":1,"con_ar":"تتعهد بعدم بخس أى سلعة","con_en":"تتعهد بعدم بخس أى سلعة","created_at":"2018-06-26 12:42:49","updated_at":"2018-06-26 12:42:49"}],"first_page_url":"https://taahel.com/api/conditions?page=1","from":1,"last_page":1,"last_page_url":"https://taahel.com/api/conditions?page=1","next_page_url":null,"path":"https://taahel.com/api/conditions","per_page":10,"prev_page_url":null,"to":2,"total":2}
         */

        private ConditionsBean conditions;

        public ConditionsBean getConditions() {
            return conditions;
        }

        public void setConditions(ConditionsBean conditions) {
            this.conditions = conditions;
        }

        public static class ConditionsBean {
            /**
             * current_page : 1
             * data : [{"id":2,"con_ar":"جميع محتويات هذه الخدمة هى حقوق ملكية فكرية محفوظة لتأهيل ويمنع إستخدامها باى شكل من الأشكال من أطراف أخرى","con_en":"جميع محتويات هذه الخدمة هى حقوق ملكية فكرية محفوظة لتأهيل ويمنع إستخدامها باى شكل من الأشكال من أطراف أخرى","created_at":"2018-06-26 12:43:26","updated_at":"2018-06-26 12:43:26"},{"id":1,"con_ar":"تتعهد بعدم بخس أى سلعة","con_en":"تتعهد بعدم بخس أى سلعة","created_at":"2018-06-26 12:42:49","updated_at":"2018-06-26 12:42:49"}]
             * first_page_url : https://taahel.com/api/conditions?page=1
             * from : 1
             * last_page : 1
             * last_page_url : https://taahel.com/api/conditions?page=1
             * next_page_url : null
             * path : https://taahel.com/api/conditions
             * per_page : 10
             * prev_page_url : null
             * to : 2
             * total : 2
             */

            private int current_page;
            private String first_page_url;
            private int from;
            private int last_page;
            private String last_page_url;
            private Object next_page_url;
            private String path;
            private int per_page;
            private Object prev_page_url;
            private int to;
            private int total;
            private List<DataBean> data;

            public int getCurrent_page() {
                return current_page;
            }

            public void setCurrent_page(int current_page) {
                this.current_page = current_page;
            }

            public String getFirst_page_url() {
                return first_page_url;
            }

            public void setFirst_page_url(String first_page_url) {
                this.first_page_url = first_page_url;
            }

            public int getFrom() {
                return from;
            }

            public void setFrom(int from) {
                this.from = from;
            }

            public int getLast_page() {
                return last_page;
            }

            public void setLast_page(int last_page) {
                this.last_page = last_page;
            }

            public String getLast_page_url() {
                return last_page_url;
            }

            public void setLast_page_url(String last_page_url) {
                this.last_page_url = last_page_url;
            }

            public Object getNext_page_url() {
                return next_page_url;
            }

            public void setNext_page_url(Object next_page_url) {
                this.next_page_url = next_page_url;
            }

            public String getPath() {
                return path;
            }

            public void setPath(String path) {
                this.path = path;
            }

            public int getPer_page() {
                return per_page;
            }

            public void setPer_page(int per_page) {
                this.per_page = per_page;
            }

            public Object getPrev_page_url() {
                return prev_page_url;
            }

            public void setPrev_page_url(Object prev_page_url) {
                this.prev_page_url = prev_page_url;
            }

            public int getTo() {
                return to;
            }

            public void setTo(int to) {
                this.to = to;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public List<DataBean> getData() {
                return data;
            }

            public void setData(List<DataBean> data) {
                this.data = data;
            }

            public static class DataBean {
                /**
                 * id : 2
                 * con_ar : جميع محتويات هذه الخدمة هى حقوق ملكية فكرية محفوظة لتأهيل ويمنع إستخدامها باى شكل من الأشكال من أطراف أخرى
                 * con_en : جميع محتويات هذه الخدمة هى حقوق ملكية فكرية محفوظة لتأهيل ويمنع إستخدامها باى شكل من الأشكال من أطراف أخرى
                 * created_at : 2018-06-26 12:43:26
                 * updated_at : 2018-06-26 12:43:26
                 */

                private int id;
                private String con_ar;
                private String con_en;
                private String created_at;
                private String updated_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getCon_ar() {
                    return con_ar;
                }

                public void setCon_ar(String con_ar) {
                    this.con_ar = con_ar;
                }

                public String getCon_en() {
                    return con_en;
                }

                public void setCon_en(String con_en) {
                    this.con_en = con_en;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }
            }
        }
    }
}
