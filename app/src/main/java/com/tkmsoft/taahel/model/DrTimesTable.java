package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 23/06/2018.
 */
public class DrTimesTable {
    private String time;
    private String free;

    public DrTimesTable(String time, String free) {
        this.time = time;
        this.free = free;
    }

    public String getTime() {

        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }
}
