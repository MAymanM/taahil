package com.tkmsoft.taahel.model.spinners;

public class DepartmentSpinnerModel {
    private String deptName;

    public DepartmentSpinnerModel(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
}
