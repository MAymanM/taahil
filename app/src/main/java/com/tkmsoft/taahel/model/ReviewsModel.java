package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 06/05/2018.
 */
public class ReviewsModel {
    private String name;
    private String rate;
    private String image;
    private String comment;

    public ReviewsModel(String name, String rate, String image, String comment) {
        this.name = name;
        this.rate = rate;
        this.image = image;
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
