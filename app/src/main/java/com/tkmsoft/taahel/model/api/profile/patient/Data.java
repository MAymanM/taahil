
package com.tkmsoft.taahel.model.api.profile.patient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("member_data")
    @Expose
    private MemberData memberData;

    public MemberData getMemberData() {
        return memberData;
    }

    public void setMemberData(MemberData memberData) {
        this.memberData = memberData;
    }

}
