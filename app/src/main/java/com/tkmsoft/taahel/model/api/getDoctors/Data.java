
package com.tkmsoft.taahel.model.api.getDoctors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("doctors")
    @Expose
    private Doctors doctors;

    public Doctors getDoctors() {
        return doctors;
    }

    public void setDoctors(Doctors doctors) {
        this.doctors = doctors;
    }

}
