package com.tkmsoft.taahel.model.addingItems;

/**
 * Created by MahmoudAyman on 23/05/2018.
 */
public class AddingItemsModel {
    private String name;
    private int image;

    public AddingItemsModel(int image, String name) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
