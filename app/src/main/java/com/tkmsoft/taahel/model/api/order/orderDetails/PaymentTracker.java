
package com.tkmsoft.taahel.model.api.order.orderDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentTracker {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("order_id")
    @Expose
    private Long orderId;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("payment_status")
    @Expose
    private Long paymentStatus;
    @SerializedName("doctor_status")
    @Expose
    private Long doctorStatus;
    @SerializedName("patient_status")
    @Expose
    private Long patientStatus;
    @SerializedName("price")
    @Expose
    private Long price;
    @SerializedName("is_free")
    @Expose
    private Long isFree;
    @SerializedName("doctor_id")
    @Expose
    private Long doctorId;
    @SerializedName("collected")
    @Expose
    private Long collected;
    @SerializedName("collector")
    @Expose
    private Object collector;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Long getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Long paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Long getDoctorStatus() {
        return doctorStatus;
    }

    public void setDoctorStatus(Long doctorStatus) {
        this.doctorStatus = doctorStatus;
    }

    public Long getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(Long patientStatus) {
        this.patientStatus = patientStatus;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getIsFree() {
        return isFree;
    }

    public void setIsFree(Long isFree) {
        this.isFree = isFree;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public Long getCollected() {
        return collected;
    }

    public void setCollected(Long collected) {
        this.collected = collected;
    }

    public Object getCollector() {
        return collector;
    }

    public void setCollector(Object collector) {
        this.collector = collector;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
