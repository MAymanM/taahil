package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 28/04/2018.
 */
public class GovernmentModel {
    private int image;
    private String name;

    public GovernmentModel(int image, String name) {
        this.image = image;
        this.name = name;
    }

    public int getImage() {

        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

