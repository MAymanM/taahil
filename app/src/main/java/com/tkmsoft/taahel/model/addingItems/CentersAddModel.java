package com.tkmsoft.taahel.model.addingItems;

/**
 * Created by MahmoudAyman on 27/05/2018.
 */
public class CentersAddModel {
    private String code;
    private String name;
    private String kind;
    private String country;
    private String city;

    public CentersAddModel(String code, String name, String kind, String country, String city) {
        this.code    = code;
        this.name    = name;
        this.kind    = kind;
        this.country = country;
        this.city    = city;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}