package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 13/05/2018.
 */
public class DaysModel {
    private String name;
    private int dayNum;

    public String getName() {
        return name;
    }

    public int getDayNum() {
        return dayNum;
    }

    public void setDayNum(int dayNum) {
        this.dayNum = dayNum;
    }

    public DaysModel(String name, int dayNum) {

        this.name = name;
        this.dayNum = dayNum;
    }

    public String getDay() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
