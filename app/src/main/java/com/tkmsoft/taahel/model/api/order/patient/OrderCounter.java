package com.tkmsoft.taahel.model.api.order.patient;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by MahmoudAyman on 02/02/2019.
 */
public class OrderCounter {
    @SerializedName("all")
    @Expose
    private Integer all;
    @SerializedName("pinned")
    @Expose
    private Integer pinned;
    @SerializedName("confirmedNotPaid")
    @Expose
    private Integer confirmedNotPaid;
    @SerializedName("confirmedPaid")
    @Expose
    private Integer confirmedPaid;
    @SerializedName("completed")
    @Expose
    private Integer completed;
    @SerializedName("canceled")
    @Expose
    private Integer canceled;

    public Integer getAll() {
        return all;
    }

    public void setAll(Integer all) {
        this.all = all;
    }

    public Integer getPinned() {
        return pinned;
    }

    public void setPinned(Integer pinned) {
        this.pinned = pinned;
    }

    public Integer getCompleted() {
        return completed;
    }

    public void setCompleted(Integer completed) {
        this.completed = completed;
    }

    public Integer getCanceled() {
        return canceled;
    }

    public void setCanceled(Integer canceled) {
        this.canceled = canceled;
    }

    public Integer getConfirmedNotPaid() {
        return confirmedNotPaid;
    }

    public void setConfirmedNotPaid(Integer confirmedNotPaid) {
        this.confirmedNotPaid = confirmedNotPaid;
    }

    public Integer getConfirmedPaid() {
        return confirmedPaid;
    }

    public void setConfirmedPaid(Integer confirmedPaid) {
        this.confirmedPaid = confirmedPaid;
    }

}
