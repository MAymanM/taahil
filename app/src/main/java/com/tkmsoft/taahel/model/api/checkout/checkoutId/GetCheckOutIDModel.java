
package com.tkmsoft.taahel.model.api.checkout.checkoutId;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetCheckOutIDModel implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("data")
    @Expose
    private Data data;
    public final static Creator<GetCheckOutIDModel> CREATOR = new Creator<GetCheckOutIDModel>() {


        public GetCheckOutIDModel createFromParcel(Parcel in) {
            return new GetCheckOutIDModel(in);
        }

        public GetCheckOutIDModel[] newArray(int size) {
            return (new GetCheckOutIDModel[size]);
        }

    }
    ;

    protected GetCheckOutIDModel(Parcel in) {
        this.status = ((Status) in.readValue((Status.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public GetCheckOutIDModel() {
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return  0;
    }

}
