
package com.tkmsoft.taahel.model.api.order.drs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Caculations {

    @SerializedName("doctor_finance")
    @Expose
    private Integer doctorFinance;
    @SerializedName("taaheel_finance")
    @Expose
    private Integer taaheelFinance;

    public Integer getDoctorFinance() {
        return doctorFinance;
    }

    public void setDoctorFinance(Integer doctorFinance) {
        this.doctorFinance = doctorFinance;
    }

    public Integer getTaaheelFinance() {
        return taaheelFinance;
    }

    public void setTaaheelFinance(Integer taaheelFinance) {
        this.taaheelFinance = taaheelFinance;
    }

}
