package com.tkmsoft.taahel.model.spinners;

import java.util.List;

public class CurrencyModel {

    /**
     * status : {"type":"success","title":"العملات "}
     * data : {"currencies":[{"id":1,"name_ar":"الجنيه المصرى","name_en":"Egyption pound","created_at":"2018-06-18 06:22:51","updated_at":"2018-06-18 06:22:51"}]}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : العملات
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        private List<CurrenciesBean> currencies;

        public List<CurrenciesBean> getCurrencies() {
            return currencies;
        }

        public void setCurrencies(List<CurrenciesBean> currencies) {
            this.currencies = currencies;
        }

        public static class CurrenciesBean {
            /**
             * id : 1
             * name_ar : الجنيه المصرى
             * name_en : Egyption pound
             * created_at : 2018-06-18 06:22:51
             * updated_at : 2018-06-18 06:22:51
             */

            private int id;
            private String name_ar;
            private String name_en;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName_ar() {
                return name_ar;
            }

            public void setName_ar(String name_ar) {
                this.name_ar = name_ar;
            }

            public String getName_en() {
                return name_en;
            }

            public void setName_en(String name_en) {
                this.name_en = name_en;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
