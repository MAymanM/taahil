package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 23/06/2018.
 */
public class StringModel {
    private String name;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    private boolean checked;

    public StringModel(String name) {
        this.name = name;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
