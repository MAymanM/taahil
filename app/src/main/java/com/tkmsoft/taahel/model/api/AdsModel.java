package com.tkmsoft.taahel.model.api;

import java.util.List;

/**
 * Created by MahmoudAyman on 23/07/2018.
 */
public class AdsModel {

    /**
     * status : {"type":"success","title":"ads"}
     * data : {"ads_info":[{"id":8,"photo":"https://taahel.com/storage/uploads/ads/9471534421177.jpg","type":"0","created_at":"2018-10-14 09:13:54","updated_at":"2018-10-14 09:13:54"},{"id":9,"photo":"https://taahel.com/storage/uploads/ads/9471534421177.jpg","type":"0","created_at":"2018-10-14 09:14:04","updated_at":"2018-10-14 09:14:04"}]}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : ads
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        private List<AdsInfoBean> ads_info;

        public List<AdsInfoBean> getAds_info() {
            return ads_info;
        }

        public void setAds_info(List<AdsInfoBean> ads_info) {
            this.ads_info = ads_info;
        }

        public static class AdsInfoBean {
            /**
             * id : 8
             * photo : https://taahel.com/storage/uploads/ads/9471534421177.jpg
             * type : 0
             * created_at : 2018-10-14 09:13:54
             * updated_at : 2018-10-14 09:13:54
             */

            private int id;
            private String photo;
            private String type;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
