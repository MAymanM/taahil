package com.tkmsoft.taahel.model.api;

/**
 * Created by MahmoudAyman on 18/09/2018.
 */
public class OpinionModel {

    /**
     * status : {"type":"success","title":"تم ارسال رايك بنجاح "}
     * data : {"opinion":{"opinion":"sdfsdfsdfsdfd","updated_at":"2018-09-18 17:58:46","created_at":"2018-09-18 17:58:46","id":5}}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : تم ارسال رايك بنجاح
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        /**
         * opinion : {"opinion":"sdfsdfsdfsdfd","updated_at":"2018-09-18 17:58:46","created_at":"2018-09-18 17:58:46","id":5}
         */

        private OpinionBean opinion;

        public OpinionBean getOpinion() {
            return opinion;
        }

        public void setOpinion(OpinionBean opinion) {
            this.opinion = opinion;
        }

        public static class OpinionBean {
            /**
             * opinion : sdfsdfsdfsdfd
             * updated_at : 2018-09-18 17:58:46
             * created_at : 2018-09-18 17:58:46
             * id : 5
             */

            private String opinion;
            private String updated_at;
            private String created_at;
            private int id;

            public String getOpinion() {
                return opinion;
            }

            public void setOpinion(String opinion) {
                this.opinion = opinion;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }
    }
}
