package com.tkmsoft.taahel.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by MahmoudAyman on 13/08/2018.
 */
public class DrTimesBody {

    private @SerializedName("day")
    String day;
    private @SerializedName("type")
    List<String> typeList;
    private @SerializedName("times")
    List<String> timesList;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public List<String> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<String> typeList) {
        this.typeList = typeList;
    }

    public List<String> getTimesList() {
        return timesList;
    }

    public void setTimesList(List<String> timesList) {
        this.timesList = timesList;
    }
}