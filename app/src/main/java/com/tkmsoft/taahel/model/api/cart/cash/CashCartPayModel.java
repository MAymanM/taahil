
package com.tkmsoft.taahel.model.api.cart.cash;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CashCartPayModel implements Parcelable
{

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("url")
    @Expose
    private String url;
    public final static Creator<CashCartPayModel> CREATOR = new Creator<CashCartPayModel>() {


        public CashCartPayModel createFromParcel(Parcel in) {
            return new CashCartPayModel(in);
        }

        public CashCartPayModel[] newArray(int size) {
            return (new CashCartPayModel[size]);
        }

    }
    ;

    protected CashCartPayModel(Parcel in) {
        this.response = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.url = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CashCartPayModel() {
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(response);
        dest.writeValue(message);
        dest.writeValue(url);
    }

    public int describeContents() {
        return  0;
    }

}
