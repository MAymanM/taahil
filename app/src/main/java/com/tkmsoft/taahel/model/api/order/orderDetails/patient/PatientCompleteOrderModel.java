package com.tkmsoft.taahel.model.api.order.orderDetails.patient;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MahmoudAyman on 20/09/2018.
 */
public class PatientCompleteOrderModel {

    /**
     * status : {"type":"success","title":"تم اكمال الطلب بنجاح"}
     * data : {"order":{"id":73,"code":"gipaQycG06","member_id":"235","job_date_id":"103","date":"2018-10-17 10:01:20","status":"1","doctor_complete":"0","patient_complete":1,"desc":"hh","address":"hh","reason":null,"created_at":"2018-10-17 17:01:06","updated_at":"2018-10-17 18:09:28","order_day":{"id":103,"day":"3","member_id":"233","created_at":"2018-10-15 12:42:41","updated_at":"2018-10-15 12:42:41","member":{"id":233,"name":"د كريم","username":"d-krymcz0cs19","phone":"1","phoneKey":"+966","code":null,"confirm":"1","approve":"1","type":"0","city_id":"6","email":"karim@tkmsoft.com","gender":"0","avatar":"blog5.jpg","address":null,"lat":"25.911729","long":"29.763218999999935","api_token":"eBkqxpAS980RgzQhioaVT7C8J5GqbxU1j6tqqkRPGywOZmGk5Gg7RRRK6InZ","created_at":"2018-10-15 12:34:19","updated_at":"2018-10-15 13:17:57"}},"member":{"id":235,"name":"مستفيد","username":"mstfyd9k1dt15","phone":"11","phoneKey":"+966","code":null,"confirm":"1","approve":"1","type":"1","city_id":"3","email":"k@k.com","gender":"0","avatar":"blog1.jpg","address":null,"lat":"25.911729","long":"29.763219","api_token":"XkMpMe8CF3WakZt5JeRAUJK4WxeNeX8NYurH9uCMS8k7RAUYSJ7AMI64jSKF","created_at":"2018-10-15 12:39:15","updated_at":"2018-10-17 17:00:07"}}}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : تم اكمال الطلب بنجاح
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        /**
         * order : {"id":73,"code":"gipaQycG06","member_id":"235","job_date_id":"103","date":"2018-10-17 10:01:20","status":"1","doctor_complete":"0","patient_complete":1,"desc":"hh","address":"hh","reason":null,"created_at":"2018-10-17 17:01:06","updated_at":"2018-10-17 18:09:28","order_day":{"id":103,"day":"3","member_id":"233","created_at":"2018-10-15 12:42:41","updated_at":"2018-10-15 12:42:41","member":{"id":233,"name":"د كريم","username":"d-krymcz0cs19","phone":"1","phoneKey":"+966","code":null,"confirm":"1","approve":"1","type":"0","city_id":"6","email":"karim@tkmsoft.com","gender":"0","avatar":"blog5.jpg","address":null,"lat":"25.911729","long":"29.763218999999935","api_token":"eBkqxpAS980RgzQhioaVT7C8J5GqbxU1j6tqqkRPGywOZmGk5Gg7RRRK6InZ","created_at":"2018-10-15 12:34:19","updated_at":"2018-10-15 13:17:57"}},"member":{"id":235,"name":"مستفيد","username":"mstfyd9k1dt15","phone":"11","phoneKey":"+966","code":null,"confirm":"1","approve":"1","type":"1","city_id":"3","email":"k@k.com","gender":"0","avatar":"blog1.jpg","address":null,"lat":"25.911729","long":"29.763219","api_token":"XkMpMe8CF3WakZt5JeRAUJK4WxeNeX8NYurH9uCMS8k7RAUYSJ7AMI64jSKF","created_at":"2018-10-15 12:39:15","updated_at":"2018-10-17 17:00:07"}}
         */

        private OrderBean order;

        public OrderBean getOrder() {
            return order;
        }

        public void setOrder(OrderBean order) {
            this.order = order;
        }

        public static class OrderBean {
            /**
             * id : 73
             * code : gipaQycG06
             * member_id : 235
             * job_date_id : 103
             * date : 2018-10-17 10:01:20
             * status : 1
             * doctor_complete : 0
             * patient_complete : 1
             * desc : hh
             * address : hh
             * reason : null
             * created_at : 2018-10-17 17:01:06
             * updated_at : 2018-10-17 18:09:28
             * order_day : {"id":103,"day":"3","member_id":"233","created_at":"2018-10-15 12:42:41","updated_at":"2018-10-15 12:42:41","member":{"id":233,"name":"د كريم","username":"d-krymcz0cs19","phone":"1","phoneKey":"+966","code":null,"confirm":"1","approve":"1","type":"0","city_id":"6","email":"karim@tkmsoft.com","gender":"0","avatar":"blog5.jpg","address":null,"lat":"25.911729","long":"29.763218999999935","api_token":"eBkqxpAS980RgzQhioaVT7C8J5GqbxU1j6tqqkRPGywOZmGk5Gg7RRRK6InZ","created_at":"2018-10-15 12:34:19","updated_at":"2018-10-15 13:17:57"}}
             * member : {"id":235,"name":"مستفيد","username":"mstfyd9k1dt15","phone":"11","phoneKey":"+966","code":null,"confirm":"1","approve":"1","type":"1","city_id":"3","email":"k@k.com","gender":"0","avatar":"blog1.jpg","address":null,"lat":"25.911729","long":"29.763219","api_token":"XkMpMe8CF3WakZt5JeRAUJK4WxeNeX8NYurH9uCMS8k7RAUYSJ7AMI64jSKF","created_at":"2018-10-15 12:39:15","updated_at":"2018-10-17 17:00:07"}
             */

            private int id;
            private String code;
            private String member_id;
            private String job_date_id;
            private String date;
            private String status;
            private String doctor_complete;
            private int patient_complete;
            private String desc;
            private String address;
            private Object reason;
            private String created_at;
            private String updated_at;
            private OrderDayBean order_day;
            private MemberBeanX member;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getMember_id() {
                return member_id;
            }

            public void setMember_id(String member_id) {
                this.member_id = member_id;
            }

            public String getJob_date_id() {
                return job_date_id;
            }

            public void setJob_date_id(String job_date_id) {
                this.job_date_id = job_date_id;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDoctor_complete() {
                return doctor_complete;
            }

            public void setDoctor_complete(String doctor_complete) {
                this.doctor_complete = doctor_complete;
            }

            public int getPatient_complete() {
                return patient_complete;
            }

            public void setPatient_complete(int patient_complete) {
                this.patient_complete = patient_complete;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public Object getReason() {
                return reason;
            }

            public void setReason(Object reason) {
                this.reason = reason;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public OrderDayBean getOrder_day() {
                return order_day;
            }

            public void setOrder_day(OrderDayBean order_day) {
                this.order_day = order_day;
            }

            public MemberBeanX getMember() {
                return member;
            }

            public void setMember(MemberBeanX member) {
                this.member = member;
            }

            public static class OrderDayBean {
                /**
                 * id : 103
                 * day : 3
                 * member_id : 233
                 * created_at : 2018-10-15 12:42:41
                 * updated_at : 2018-10-15 12:42:41
                 * member : {"id":233,"name":"د كريم","username":"d-krymcz0cs19","phone":"1","phoneKey":"+966","code":null,"confirm":"1","approve":"1","type":"0","city_id":"6","email":"karim@tkmsoft.com","gender":"0","avatar":"blog5.jpg","address":null,"lat":"25.911729","long":"29.763218999999935","api_token":"eBkqxpAS980RgzQhioaVT7C8J5GqbxU1j6tqqkRPGywOZmGk5Gg7RRRK6InZ","created_at":"2018-10-15 12:34:19","updated_at":"2018-10-15 13:17:57"}
                 */

                private int id;
                private String day;
                private String member_id;
                private String created_at;
                private String updated_at;
                private MemberBean member;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }

                public String getMember_id() {
                    return member_id;
                }

                public void setMember_id(String member_id) {
                    this.member_id = member_id;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public MemberBean getMember() {
                    return member;
                }

                public void setMember(MemberBean member) {
                    this.member = member;
                }

                public static class MemberBean {
                    /**
                     * id : 233
                     * name : د كريم
                     * username : d-krymcz0cs19
                     * phone : 1
                     * phoneKey : +966
                     * code : null
                     * confirm : 1
                     * approve : 1
                     * type : 0
                     * city_id : 6
                     * email : karim@tkmsoft.com
                     * gender : 0
                     * avatar : blog5.jpg
                     * address : null
                     * lat : 25.911729
                     * long : 29.763218999999935
                     * api_token : eBkqxpAS980RgzQhioaVT7C8J5GqbxU1j6tqqkRPGywOZmGk5Gg7RRRK6InZ
                     * created_at : 2018-10-15 12:34:19
                     * updated_at : 2018-10-15 13:17:57
                     */

                    private int id;
                    private String name;
                    private String username;
                    private String phone;
                    private String phoneKey;
                    private Object code;
                    private String confirm;
                    private String approve;
                    private String type;
                    private String city_id;
                    private String email;
                    private String gender;
                    private String avatar;
                    private Object address;
                    private String lat;
                    @SerializedName("long")
                    private String longX;
                    private String api_token;
                    private String created_at;
                    private String updated_at;

                    public int getId() {
                        return id;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getUsername() {
                        return username;
                    }

                    public void setUsername(String username) {
                        this.username = username;
                    }

                    public String getPhone() {
                        return phone;
                    }

                    public void setPhone(String phone) {
                        this.phone = phone;
                    }

                    public String getPhoneKey() {
                        return phoneKey;
                    }

                    public void setPhoneKey(String phoneKey) {
                        this.phoneKey = phoneKey;
                    }

                    public Object getCode() {
                        return code;
                    }

                    public void setCode(Object code) {
                        this.code = code;
                    }

                    public String getConfirm() {
                        return confirm;
                    }

                    public void setConfirm(String confirm) {
                        this.confirm = confirm;
                    }

                    public String getApprove() {
                        return approve;
                    }

                    public void setApprove(String approve) {
                        this.approve = approve;
                    }

                    public String getType() {
                        return type;
                    }

                    public void setType(String type) {
                        this.type = type;
                    }

                    public String getCity_id() {
                        return city_id;
                    }

                    public void setCity_id(String city_id) {
                        this.city_id = city_id;
                    }

                    public String getEmail() {
                        return email;
                    }

                    public void setEmail(String email) {
                        this.email = email;
                    }

                    public String getGender() {
                        return gender;
                    }

                    public void setGender(String gender) {
                        this.gender = gender;
                    }

                    public String getAvatar() {
                        return avatar;
                    }

                    public void setAvatar(String avatar) {
                        this.avatar = avatar;
                    }

                    public Object getAddress() {
                        return address;
                    }

                    public void setAddress(Object address) {
                        this.address = address;
                    }

                    public String getLat() {
                        return lat;
                    }

                    public void setLat(String lat) {
                        this.lat = lat;
                    }

                    public String getLongX() {
                        return longX;
                    }

                    public void setLongX(String longX) {
                        this.longX = longX;
                    }

                    public String getApi_token() {
                        return api_token;
                    }

                    public void setApi_token(String api_token) {
                        this.api_token = api_token;
                    }

                    public String getCreated_at() {
                        return created_at;
                    }

                    public void setCreated_at(String created_at) {
                        this.created_at = created_at;
                    }

                    public String getUpdated_at() {
                        return updated_at;
                    }

                    public void setUpdated_at(String updated_at) {
                        this.updated_at = updated_at;
                    }
                }
            }

            public static class MemberBeanX {
                /**
                 * id : 235
                 * name : مستفيد
                 * username : mstfyd9k1dt15
                 * phone : 11
                 * phoneKey : +966
                 * code : null
                 * confirm : 1
                 * approve : 1
                 * type : 1
                 * city_id : 3
                 * email : k@k.com
                 * gender : 0
                 * avatar : blog1.jpg
                 * address : null
                 * lat : 25.911729
                 * long : 29.763219
                 * api_token : XkMpMe8CF3WakZt5JeRAUJK4WxeNeX8NYurH9uCMS8k7RAUYSJ7AMI64jSKF
                 * created_at : 2018-10-15 12:39:15
                 * updated_at : 2018-10-17 17:00:07
                 */

                private int id;
                private String name;
                private String username;
                private String phone;
                private String phoneKey;
                private Object code;
                private String confirm;
                private String approve;
                private String type;
                private String city_id;
                private String email;
                private String gender;
                private String avatar;
                private Object address;
                private String lat;
                @SerializedName("long")
                private String longX;
                private String api_token;
                private String created_at;
                private String updated_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public String getPhone() {
                    return phone;
                }

                public void setPhone(String phone) {
                    this.phone = phone;
                }

                public String getPhoneKey() {
                    return phoneKey;
                }

                public void setPhoneKey(String phoneKey) {
                    this.phoneKey = phoneKey;
                }

                public Object getCode() {
                    return code;
                }

                public void setCode(Object code) {
                    this.code = code;
                }

                public String getConfirm() {
                    return confirm;
                }

                public void setConfirm(String confirm) {
                    this.confirm = confirm;
                }

                public String getApprove() {
                    return approve;
                }

                public void setApprove(String approve) {
                    this.approve = approve;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getCity_id() {
                    return city_id;
                }

                public void setCity_id(String city_id) {
                    this.city_id = city_id;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getAvatar() {
                    return avatar;
                }

                public void setAvatar(String avatar) {
                    this.avatar = avatar;
                }

                public Object getAddress() {
                    return address;
                }

                public void setAddress(Object address) {
                    this.address = address;
                }

                public String getLat() {
                    return lat;
                }

                public void setLat(String lat) {
                    this.lat = lat;
                }

                public String getLongX() {
                    return longX;
                }

                public void setLongX(String longX) {
                    this.longX = longX;
                }

                public String getApi_token() {
                    return api_token;
                }

                public void setApi_token(String api_token) {
                    this.api_token = api_token;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }
            }
        }
    }
}
