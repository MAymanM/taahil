package com.tkmsoft.taahel.model.api.drugs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by MahmoudAyman on 12/07/2018.
 */
public class AddCenterModel {

    /**
     * status : {"type":"success","title":"تم اضافه البيانات بنجاح بنتظار موافقه الادمن"}
     * data : {"drug":{"name_ar":"sadasdsd","name_en":"mahmoud","slug":"mahmoud20","code":"zwJiKmWp20","type_id":"2","city_id":"1","address_ar":"asdsd","address_en":"xzcasdd","web":"www.google.com","static_phone":"145445","phone":"22445","photo":"https://taahel.com/storage/uploads/members/drugs/153918548043255884_10156832285728970_910687470826291200_n.jpg","lat":"8455417","long":"745415","member_id":18,"updated_at":"2018-10-10 15:31:20","created_at":"2018-10-10 15:31:20","id":27,"countRate":0,"city":{"id":1,"name_ar":"القاهرة","name_en":"Cairo","country_id":"1","transport_price":"50","created_at":"2018-06-18 06:23:54","updated_at":"2018-09-17 12:51:14"},"category":{"id":2,"name_ar":"أهلية","name_en":"Private\r\n","slug":"Private","created_at":"2018-06-17 22:27:00","updated_at":"2018-06-17 15:00:00"},"comments":[]}}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : تم اضافه البيانات بنجاح بنتظار موافقه الادمن
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        /**
         * drug : {"name_ar":"sadasdsd","name_en":"mahmoud","slug":"mahmoud20","code":"zwJiKmWp20","type_id":"2","city_id":"1","address_ar":"asdsd","address_en":"xzcasdd","web":"www.google.com","static_phone":"145445","phone":"22445","photo":"https://taahel.com/storage/uploads/members/drugs/153918548043255884_10156832285728970_910687470826291200_n.jpg","lat":"8455417","long":"745415","member_id":18,"updated_at":"2018-10-10 15:31:20","created_at":"2018-10-10 15:31:20","id":27,"countRate":0,"city":{"id":1,"name_ar":"القاهرة","name_en":"Cairo","country_id":"1","transport_price":"50","created_at":"2018-06-18 06:23:54","updated_at":"2018-09-17 12:51:14"},"category":{"id":2,"name_ar":"أهلية","name_en":"Private\r\n","slug":"Private","created_at":"2018-06-17 22:27:00","updated_at":"2018-06-17 15:00:00"},"comments":[]}
         */

        private DrugBean drug;

        public DrugBean getDrug() {
            return drug;
        }

        public void setDrug(DrugBean drug) {
            this.drug = drug;
        }

        public static class DrugBean {
            /**
             * name_ar : sadasdsd
             * name_en : mahmoud
             * slug : mahmoud20
             * code : zwJiKmWp20
             * type_id : 2
             * city_id : 1
             * address_ar : asdsd
             * address_en : xzcasdd
             * web : www.google.com
             * static_phone : 145445
             * phone : 22445
             * photo : https://taahel.com/storage/uploads/members/drugs/153918548043255884_10156832285728970_910687470826291200_n.jpg
             * lat : 8455417
             * long : 745415
             * member_id : 18
             * updated_at : 2018-10-10 15:31:20
             * created_at : 2018-10-10 15:31:20
             * id : 27
             * countRate : 0
             * city : {"id":1,"name_ar":"القاهرة","name_en":"Cairo","country_id":"1","transport_price":"50","created_at":"2018-06-18 06:23:54","updated_at":"2018-09-17 12:51:14"}
             * category : {"id":2,"name_ar":"أهلية","name_en":"Private\r\n","slug":"Private","created_at":"2018-06-17 22:27:00","updated_at":"2018-06-17 15:00:00"}
             * comments : []
             */

            private String name_ar;
            private String name_en;
            private String slug;
            private String code;
            private String type_id;
            private String city_id;
            private String address_ar;
            private String address_en;
            private String web;
            private String static_phone;
            private String phone;
            private String photo;
            private String lat;
            @SerializedName("long")
            private String longX;
            private int member_id;
            private String updated_at;
            private String created_at;
            private int id;
            private int countRate;
            private CityBean city;
            private CategoryBean category;
            private List<?> comments;

            public String getName_ar() {
                return name_ar;
            }

            public void setName_ar(String name_ar) {
                this.name_ar = name_ar;
            }

            public String getName_en() {
                return name_en;
            }

            public void setName_en(String name_en) {
                this.name_en = name_en;
            }

            public String getSlug() {
                return slug;
            }

            public void setSlug(String slug) {
                this.slug = slug;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getType_id() {
                return type_id;
            }

            public void setType_id(String type_id) {
                this.type_id = type_id;
            }

            public String getCity_id() {
                return city_id;
            }

            public void setCity_id(String city_id) {
                this.city_id = city_id;
            }

            public String getAddress_ar() {
                return address_ar;
            }

            public void setAddress_ar(String address_ar) {
                this.address_ar = address_ar;
            }

            public String getAddress_en() {
                return address_en;
            }

            public void setAddress_en(String address_en) {
                this.address_en = address_en;
            }

            public String getWeb() {
                return web;
            }

            public void setWeb(String web) {
                this.web = web;
            }

            public String getStatic_phone() {
                return static_phone;
            }

            public void setStatic_phone(String static_phone) {
                this.static_phone = static_phone;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLongX() {
                return longX;
            }

            public void setLongX(String longX) {
                this.longX = longX;
            }

            public int getMember_id() {
                return member_id;
            }

            public void setMember_id(int member_id) {
                this.member_id = member_id;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getCountRate() {
                return countRate;
            }

            public void setCountRate(int countRate) {
                this.countRate = countRate;
            }

            public CityBean getCity() {
                return city;
            }

            public void setCity(CityBean city) {
                this.city = city;
            }

            public CategoryBean getCategory() {
                return category;
            }

            public void setCategory(CategoryBean category) {
                this.category = category;
            }

            public List<?> getComments() {
                return comments;
            }

            public void setComments(List<?> comments) {
                this.comments = comments;
            }

            public static class CityBean {
                /**
                 * id : 1
                 * name_ar : القاهرة
                 * name_en : Cairo
                 * country_id : 1
                 * transport_price : 50
                 * created_at : 2018-06-18 06:23:54
                 * updated_at : 2018-09-17 12:51:14
                 */

                private int id;
                private String name_ar;
                private String name_en;
                private String country_id;
                private String transport_price;
                private String created_at;
                private String updated_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName_ar() {
                    return name_ar;
                }

                public void setName_ar(String name_ar) {
                    this.name_ar = name_ar;
                }

                public String getName_en() {
                    return name_en;
                }

                public void setName_en(String name_en) {
                    this.name_en = name_en;
                }

                public String getCountry_id() {
                    return country_id;
                }

                public void setCountry_id(String country_id) {
                    this.country_id = country_id;
                }

                public String getTransport_price() {
                    return transport_price;
                }

                public void setTransport_price(String transport_price) {
                    this.transport_price = transport_price;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }
            }

            public static class CategoryBean {
                /**
                 * id : 2
                 * name_ar : أهلية
                 * name_en : Private

                 * slug : Private
                 * created_at : 2018-06-17 22:27:00
                 * updated_at : 2018-06-17 15:00:00
                 */

                private int id;
                private String name_ar;
                private String name_en;
                private String slug;
                private String created_at;
                private String updated_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName_ar() {
                    return name_ar;
                }

                public void setName_ar(String name_ar) {
                    this.name_ar = name_ar;
                }

                public String getName_en() {
                    return name_en;
                }

                public void setName_en(String name_en) {
                    this.name_en = name_en;
                }

                public String getSlug() {
                    return slug;
                }

                public void setSlug(String slug) {
                    this.slug = slug;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }
            }
        }
    }
}
