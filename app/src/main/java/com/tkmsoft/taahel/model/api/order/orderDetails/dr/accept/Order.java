
package com.tkmsoft.taahel.model.api.order.orderDetails.dr.accept;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("member_id")
    @Expose
    private String memberId;
    @SerializedName("job_date_id")
    @Expose
    private String jobDateId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("doctor_complete")
    @Expose
    private String doctorComplete;
    @SerializedName("patient_complete")
    @Expose
    private String patientComplete;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("member")
    @Expose
    private Member member;
    @SerializedName("order_day")
    @Expose
    private OrderDay orderDay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getJobDateId() {
        return jobDateId;
    }

    public void setJobDateId(String jobDateId) {
        this.jobDateId = jobDateId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDoctorComplete() {
        return doctorComplete;
    }

    public void setDoctorComplete(String doctorComplete) {
        this.doctorComplete = doctorComplete;
    }

    public String getPatientComplete() {
        return patientComplete;
    }

    public void setPatientComplete(String patientComplete) {
        this.patientComplete = patientComplete;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public OrderDay getOrderDay() {
        return orderDay;
    }

    public void setOrderDay(OrderDay orderDay) {
        this.orderDay = orderDay;
    }

}
