package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 16/05/2018.
 */
public class PurchaseDetailModel {
    private String name;
    private String quantity;
    private int image;
    private String total;
    private String price;


    public PurchaseDetailModel(String name, String quantity, int image, String total, String price) {
        this.name = name;
        this.quantity = quantity;
        this.image = image;
        this.total = total;
        this.price = price;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
