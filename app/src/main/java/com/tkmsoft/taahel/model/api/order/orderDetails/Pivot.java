
package com.tkmsoft.taahel.model.api.order.orderDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pivot {

    @SerializedName("patient_order_id")
    @Expose
    private Long patientOrderId;
    @SerializedName("job_time_id")
    @Expose
    private Long jobTimeId;

    public Long getPatientOrderId() {
        return patientOrderId;
    }

    public void setPatientOrderId(Long patientOrderId) {
        this.patientOrderId = patientOrderId;
    }

    public Long getJobTimeId() {
        return jobTimeId;
    }

    public void setJobTimeId(Long jobTimeId) {
        this.jobTimeId = jobTimeId;
    }

}
