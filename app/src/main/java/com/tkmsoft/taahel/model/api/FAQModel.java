package com.tkmsoft.taahel.model.api;

import java.util.List;

/**
 * Created by MahmoudAyman on 10/19/2018.
 **/
public class FAQModel {

    /**
     * status : {"type":"success","title":"faqs "}
     * data : {"faqs":[{"id":19,"question":"أين المقر الرئيسي لتأهيل","answer":"المقر الرئيسي لمنصة تأهيل المملكة العربية السعودية - الرياض","created_at":"2018-09-24 12:14:28","updated_at":"2018-09-24 12:14:28"},{"id":17,"question":"ماهي الصفة الرسمية  لتأهيل","answer":"منصة تأهيل أحد مشاريع مؤسسة تأهيل لتقنية المعلومات","created_at":"2018-09-19 23:18:13","updated_at":"2018-09-19 23:18:13"}]}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : faqs
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        private List<FaqsBean> faqs;

        public List<FaqsBean> getFaqs() {
            return faqs;
        }

        public void setFaqs(List<FaqsBean> faqs) {
            this.faqs = faqs;
        }

        public static class FaqsBean {
            /**
             * id : 19
             * question : أين المقر الرئيسي لتأهيل
             * answer : المقر الرئيسي لمنصة تأهيل المملكة العربية السعودية - الرياض
             * created_at : 2018-09-24 12:14:28
             * updated_at : 2018-09-24 12:14:28
             */

            private int id;
            private String question;
            private String answer;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getQuestion() {
                return question;
            }

            public void setQuestion(String question) {
                this.question = question;
            }

            public String getAnswer() {
                return answer;
            }

            public void setAnswer(String answer) {
                this.answer = answer;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }
}
