
package com.tkmsoft.taahel.model.api.auth.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientOrderComplete {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("patient_order_id")
    @Expose
    private String patientOrderId;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatientOrderId() {
        return patientOrderId;
    }

    public void setPatientOrderId(String patientOrderId) {
        this.patientOrderId = patientOrderId;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
