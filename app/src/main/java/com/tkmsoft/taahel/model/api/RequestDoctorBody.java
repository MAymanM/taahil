package com.tkmsoft.taahel.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by MahmoudAyman on 19/08/2018.
 */
public class RequestDoctorBody {
    private @SerializedName("job_date_id")
    int job_date_id;
    private @SerializedName("date")
    String date;
    private @SerializedName("desc")
    String desc;
    private @SerializedName("address")
    String address;
    private @SerializedName("times")
    List<String> timesList;

    public int getJob_date_id() {
        return job_date_id;
    }

    public void setJob_date_id(int job_date_id) {
        this.job_date_id = job_date_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getTimesList() {
        return timesList;
    }

    public void setTimesList(List<String> timesList) {
        this.timesList = timesList;
    }
}
