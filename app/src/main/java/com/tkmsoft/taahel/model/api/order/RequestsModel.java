package com.tkmsoft.taahel.model.api.order;

/**
 * Created by MahmoudAyman on 12/09/2018.
 */
public class RequestsModel {
    private String name;
    private String count;

    public RequestsModel(String name, String count) {
        this.name = name;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
