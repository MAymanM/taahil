package com.tkmsoft.taahel.model.api.order;

/**
 * Created by MahmoudAyman on 26/09/2018.
 */
public class GetPermissionModel {

    /**
     * status : {"type":"success","title":"تم ارسال الطلب بنجاح بنتظار موافقه المستفيد"}
     */

    private StatusBean status;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : تم ارسال الطلب بنجاح بنتظار موافقه المستفيد
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
