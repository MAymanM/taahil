package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 29/05/2018.
 */
public class TherapistDetailDeptsModel {
    private String dept;

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public TherapistDetailDeptsModel(String dept) {

        this.dept = dept;
    }
}
