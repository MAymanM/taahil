
package com.tkmsoft.taahel.model.api.cart.view;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("totalPrice")
    @Expose
    private String totalPrice;
    @SerializedName("totalOldPrice")
    @Expose
    private String totalOldPrice;
    @SerializedName("cart")
    @Expose
    private List<Cart> cart = null;

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTotalOldPrice() {
        return totalOldPrice;
    }

    public void setTotalOldPrice(String totalOldPrice) {
        this.totalOldPrice = totalOldPrice;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public void setCart(List<Cart> cart) {
        this.cart = cart;
    }

}
