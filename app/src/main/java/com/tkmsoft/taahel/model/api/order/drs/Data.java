
package com.tkmsoft.taahel.model.api.order.drs;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("caculations")
    @Expose
    private Caculations caculations;
    @SerializedName("orders")
    @Expose
    private List<Order> orders = null;
    @SerializedName("orderCounter")
    @Expose
    private OrderCounter orderCounter;

    public Caculations getCaculations() {
        return caculations;
    }

    public void setCaculations(Caculations caculations) {
        this.caculations = caculations;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public OrderCounter getOrderCounter() {
        return orderCounter;
    }

    public void setOrderCounter(OrderCounter orderCounter) {
        this.orderCounter = orderCounter;
    }
}
