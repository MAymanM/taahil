package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 15/05/2018.
 */
public class PurchasesModel {
    private String code;
    private String total;

    public PurchasesModel(String code, String total) {
        this.code = code;
        this.total = total;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
