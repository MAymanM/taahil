package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 30/05/2018.
 */
public class SelectDayModel {
    private String day;
    private String type;

    public SelectDayModel(String day, String type) {
        this.day = day;
        this.type = type;
    }

    public String getDay() {

        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
