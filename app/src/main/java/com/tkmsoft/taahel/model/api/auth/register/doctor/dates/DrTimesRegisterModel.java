
package com.tkmsoft.taahel.model.api.auth.register.doctor.dates;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrTimesRegisterModel {

    @SerializedName("status")
    @Expose
    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
