package com.tkmsoft.taahel.model.api;

import java.util.List;

/**
 * Created by MahmoudAyman on 19/07/2018.
 */
public class SliderModel {


    /**
     * status : {"type":"success","title":"sliders "}
     * data : {"sliders":[{"photo":"http://tohfa.net/storage/uploads/sliders/g2.jpg"}]}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : sliders
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        private List<SlidersBean> sliders;

        public List<SlidersBean> getSliders() {
            return sliders;
        }

        public void setSliders(List<SlidersBean> sliders) {
            this.sliders = sliders;
        }

        public static class SlidersBean {
            /**
             * photo : http://tohfa.net/storage/uploads/sliders/g2.jpg
             */

            private String photo;

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }
        }
    }
}
