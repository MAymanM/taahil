
package com.tkmsoft.taahel.model.api.jobs.view;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tkmsoft.taahel.model.api.drugs.view.City;
import com.tkmsoft.taahel.model.api.drugs.view.Country;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name_ar")
    @Expose
    private String nameAr;
    @SerializedName("name_en")
    @Expose
    private String nameEn;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("address_ar")
    @Expose
    private String addressAr;
    @SerializedName("address_en")
    @Expose
    private String addressEn;
    @SerializedName("field_id")
    @Expose
    private String fieldId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("desc_ar")
    @Expose
    private String descAr;
    @SerializedName("desc_en")
    @Expose
    private String descEn;
    @SerializedName("feature1_ar")
    @Expose
    private String feature1Ar;
    @SerializedName("feature1_en")
    @Expose
    private String feature1En;
    @SerializedName("feature2_ar")
    @Expose
    private String feature2Ar;
    @SerializedName("feature2_en")
    @Expose
    private String feature2En;
    @SerializedName("feature3_ar")
    @Expose
    private String feature3Ar;
    @SerializedName("feature3_en")
    @Expose
    private String feature3En;
    @SerializedName("feature4_ar")
    @Expose
    private String feature4Ar;
    @SerializedName("feature4_en")
    @Expose
    private String feature4En;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("country")
    @Expose
    private Country country;
    @SerializedName("field")
    @Expose
    private Field field;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAddressAr() {
        return addressAr;
    }

    public void setAddressAr(String addressAr) {
        this.addressAr = addressAr;
    }

    public String getAddressEn() {
        return addressEn;
    }

    public void setAddressEn(String addressEn) {
        this.addressEn = addressEn;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescAr() {
        return descAr;
    }

    public void setDescAr(String descAr) {
        this.descAr = descAr;
    }

    public String getDescEn() {
        return descEn;
    }

    public void setDescEn(String descEn) {
        this.descEn = descEn;
    }

    public String getFeature1Ar() {
        return feature1Ar;
    }

    public void setFeature1Ar(String feature1Ar) {
        this.feature1Ar = feature1Ar;
    }

    public String getFeature1En() {
        return feature1En;
    }

    public void setFeature1En(String feature1En) {
        this.feature1En = feature1En;
    }

    public String getFeature2Ar() {
        return feature2Ar;
    }

    public void setFeature2Ar(String feature2Ar) {
        this.feature2Ar = feature2Ar;
    }

    public String getFeature2En() {
        return feature2En;
    }

    public void setFeature2En(String feature2En) {
        this.feature2En = feature2En;
    }

    public String getFeature3Ar() {
        return feature3Ar;
    }

    public void setFeature3Ar(String feature3Ar) {
        this.feature3Ar = feature3Ar;
    }

    public String getFeature3En() {
        return feature3En;
    }

    public void setFeature3En(String feature3En) {
        this.feature3En = feature3En;
    }

    public String getFeature4Ar() {
        return feature4Ar;
    }

    public void setFeature4Ar(String feature4Ar) {
        this.feature4Ar = feature4Ar;
    }

    public String getFeature4En() {
        return feature4En;
    }

    public void setFeature4En(String feature4En) {
        this.feature4En = feature4En;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }
}
