
package com.tkmsoft.taahel.model.api.deletecart;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteCartModel implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Status status;
    public final static Creator<DeleteCartModel> CREATOR = new Creator<DeleteCartModel>() {


        public DeleteCartModel createFromParcel(Parcel in) {
            return new DeleteCartModel(in);
        }

        public DeleteCartModel[] newArray(int size) {
            return (new DeleteCartModel[size]);
        }

    }
    ;

    protected DeleteCartModel(Parcel in) {
        this.status = ((Status) in.readValue((Status.class.getClassLoader())));
    }

    public DeleteCartModel() {
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
    }

    public int describeContents() {
        return  0;
    }

}
