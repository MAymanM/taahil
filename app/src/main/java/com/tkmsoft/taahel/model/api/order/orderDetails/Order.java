
package com.tkmsoft.taahel.model.api.order.orderDetails;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("member_id")
    @Expose
    private Long memberId;
    @SerializedName("job_date_id")
    @Expose
    private Long jobDateId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("doctor_complete")
    @Expose
    private Long doctorComplete;
    @SerializedName("patient_complete")
    @Expose
    private Long patientComplete;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("member")
    @Expose
    private Member member;
    @SerializedName("order_day")
    @Expose
    private OrderDay orderDay;
    @SerializedName("time_selected")
    @Expose
    private List<TimeSelected> timeSelected = null;
    @SerializedName("payment_tracker")
    @Expose
    private PaymentTracker paymentTracker;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getJobDateId() {
        return jobDateId;
    }

    public void setJobDateId(Long jobDateId) {
        this.jobDateId = jobDateId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getDoctorComplete() {
        return doctorComplete;
    }

    public void setDoctorComplete(Long doctorComplete) {
        this.doctorComplete = doctorComplete;
    }

    public Long getPatientComplete() {
        return patientComplete;
    }

    public void setPatientComplete(Long patientComplete) {
        this.patientComplete = patientComplete;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public OrderDay getOrderDay() {
        return orderDay;
    }

    public void setOrderDay(OrderDay orderDay) {
        this.orderDay = orderDay;
    }

    public List<TimeSelected> getTimeSelected() {
        return timeSelected;
    }

    public void setTimeSelected(List<TimeSelected> timeSelected) {
        this.timeSelected = timeSelected;
    }

    public PaymentTracker getPaymentTracker() {
        return paymentTracker;
    }

    public void setPaymentTracker(PaymentTracker paymentTracker) {
        this.paymentTracker = paymentTracker;
    }

}
