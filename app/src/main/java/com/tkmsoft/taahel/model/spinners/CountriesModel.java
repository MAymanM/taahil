package com.tkmsoft.taahel.model.spinners;

import java.util.List;

/**
 * Created by MahmoudAyman on 22/04/2018.
 */
public class CountriesModel {

    /**
     * status : {"type":"success","title":"الدول "}
     * data : {"countries":[{"id":1,"name_ar":"مصر","name_en":"Egypt","alias":"Egy","code":"+20","currency_id":"1","created_at":"2018-06-18 06:23:26","updated_at":"2018-06-18 06:23:26","cities":[{"id":1,"name_ar":"القاهره","name_en":"cairo","country_id":"1","transport_price":"50","created_at":"2018-06-18 06:23:54","updated_at":"2018-06-18 06:23:54"},{"id":2,"name_ar":"الاسكندريه","name_en":"alex","country_id":"1","transport_price":"70","created_at":"2018-06-18 06:24:15","updated_at":"2018-06-18 06:24:15"},{"id":5,"name_ar":"أسيوط","name_en":"اسيوط","country_id":"1","transport_price":"100","created_at":"2018-08-09 11:35:47","updated_at":"2018-08-09 11:35:47"}],"currency":{"id":1,"name_ar":"ريال سعودي","name_en":"Saudi riyal\r\n","created_at":"2018-06-18 06:22:51","updated_at":"2018-06-26 12:39:08"}},{"id":2,"name_ar":"المملكة العربية السعودية","name_en":"Saudi","alias":"sa","code":"+966","currency_id":"1","created_at":"2018-06-25 09:46:42","updated_at":"2018-06-25 09:46:42","cities":[{"id":3,"name_ar":"الرياض","name_en":"riyadh","country_id":"2","transport_price":"6523","created_at":"2018-06-25 09:47:02","updated_at":"2018-06-25 09:47:02"},{"id":4,"name_ar":"جدة","name_en":"jedah","country_id":"2","transport_price":"8543","created_at":"2018-06-26 12:34:35","updated_at":"2018-06-26 12:34:35"}],"currency":{"id":1,"name_ar":"ريال سعودي","name_en":"Saudi riyal\r\n","created_at":"2018-06-18 06:22:51","updated_at":"2018-06-26 12:39:08"}}]}
     */

    private StatusBean status;
    private DataBean data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class StatusBean {
        /**
         * type : success
         * title : الدول
         */

        private String type;
        private String title;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class DataBean {
        private List<CountriesBean> countries;

        public List<CountriesBean> getCountries() {
            return countries;
        }

        public void setCountries(List<CountriesBean> countries) {
            this.countries = countries;
        }

        public static class CountriesBean {
            /**
             * id : 1
             * name_ar : مصر
             * name_en : Egypt
             * alias : Egy
             * code : +20
             * currency_id : 1
             * created_at : 2018-06-18 06:23:26
             * updated_at : 2018-06-18 06:23:26
             * cities : [{"id":1,"name_ar":"القاهره","name_en":"cairo","country_id":"1","transport_price":"50","created_at":"2018-06-18 06:23:54","updated_at":"2018-06-18 06:23:54"},{"id":2,"name_ar":"الاسكندريه","name_en":"alex","country_id":"1","transport_price":"70","created_at":"2018-06-18 06:24:15","updated_at":"2018-06-18 06:24:15"},{"id":5,"name_ar":"أسيوط","name_en":"اسيوط","country_id":"1","transport_price":"100","created_at":"2018-08-09 11:35:47","updated_at":"2018-08-09 11:35:47"}]
             * currency : {"id":1,"name_ar":"ريال سعودي","name_en":"Saudi riyal\r\n","created_at":"2018-06-18 06:22:51","updated_at":"2018-06-26 12:39:08"}
             */

            private int id;
            private String name_ar;
            private String name_en;
            private String alias;
            private String code;
            private String currency_id;
            private String created_at;
            private String updated_at;
            private CurrencyBean currency;
            private List<CitiesBean> cities;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName_ar() {
                return name_ar;
            }

            public void setName_ar(String name_ar) {
                this.name_ar = name_ar;
            }

            public String getName_en() {
                return name_en;
            }

            public void setName_en(String name_en) {
                this.name_en = name_en;
            }

            public String getAlias() {
                return alias;
            }

            public void setAlias(String alias) {
                this.alias = alias;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getCurrency_id() {
                return currency_id;
            }

            public void setCurrency_id(String currency_id) {
                this.currency_id = currency_id;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public CurrencyBean getCurrency() {
                return currency;
            }

            public void setCurrency(CurrencyBean currency) {
                this.currency = currency;
            }

            public List<CitiesBean> getCities() {
                return cities;
            }

            public void setCities(List<CitiesBean> cities) {
                this.cities = cities;
            }

            public static class CurrencyBean {
                /**
                 * id : 1
                 * name_ar : ريال سعودي
                 * name_en : Saudi riyal

                 * created_at : 2018-06-18 06:22:51
                 * updated_at : 2018-06-26 12:39:08
                 */

                private int id;
                private String name_ar;
                private String name_en;
                private String created_at;
                private String updated_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName_ar() {
                    return name_ar;
                }

                public void setName_ar(String name_ar) {
                    this.name_ar = name_ar;
                }

                public String getName_en() {
                    return name_en;
                }

                public void setName_en(String name_en) {
                    this.name_en = name_en;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }
            }

            public static class CitiesBean {
                /**
                 * id : 1
                 * name_ar : القاهره
                 * name_en : cairo
                 * country_id : 1
                 * transport_price : 50
                 * created_at : 2018-06-18 06:23:54
                 * updated_at : 2018-06-18 06:23:54
                 */

                private int id;
                private String name_ar;
                private String name_en;
                private String country_id;
                private String transport_price;
                private String created_at;
                private String updated_at;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName_ar() {
                    return name_ar;
                }

                public void setName_ar(String name_ar) {
                    this.name_ar = name_ar;
                }

                public String getName_en() {
                    return name_en;
                }

                public void setName_en(String name_en) {
                    this.name_en = name_en;
                }

                public String getCountry_id() {
                    return country_id;
                }

                public void setCountry_id(String country_id) {
                    this.country_id = country_id;
                }

                public String getTransport_price() {
                    return transport_price;
                }

                public void setTransport_price(String transport_price) {
                    this.transport_price = transport_price;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }
            }
        }
    }
}
