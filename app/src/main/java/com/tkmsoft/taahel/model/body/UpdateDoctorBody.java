package com.tkmsoft.taahel.model.body;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by MahmoudAyman on 29/08/2018.
 */
public class UpdateDoctorBody {

    @SerializedName("name")
    String name;
    @SerializedName("username")
    String username;
    @SerializedName("city_id")
    String city_id;
    @SerializedName("email")
    String email;
    @SerializedName("phone")
    String phone;
    @SerializedName("phoneKey")
    String phoneKey;
    @SerializedName("gender")
    String gender;
    @SerializedName("lat")
    String lat;
    @SerializedName("long")
    String xLong;
    @SerializedName("old_password")
    String old_password;
    @SerializedName("password")
    String password;
    @SerializedName("password_confirmation")
    String password_confirmation;
    @SerializedName("price")
    String price;
    @SerializedName("currency_id")
    String currency_id;
    @SerializedName("experiences")
    String experiences;
    @SerializedName("education")
    String education;
    @SerializedName("about")
    String about;
    @SerializedName("specialties")
    List<String> specialtiesList;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneKey() {
        return phoneKey;
    }

    public void setPhoneKey(String phoneKey) {
        this.phoneKey = phoneKey;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getxLong() {
        return xLong;
    }

    public void setxLong(String xLong) {
        this.xLong = xLong;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(String currency_id) {
        this.currency_id = currency_id;
    }

    public String getExperiences() {
        return experiences;
    }

    public void setExperiences(String experiences) {
        this.experiences = experiences;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public List<String> getSpecialtiesList() {
        return specialtiesList;
    }

    public void setSpecialtiesList(List<String> specialtiesList) {
        this.specialtiesList = specialtiesList;
    }

}


