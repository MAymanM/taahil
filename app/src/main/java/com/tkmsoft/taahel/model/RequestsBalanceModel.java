package com.tkmsoft.taahel.model;

/**
 * Created by MahmoudAyman on 19/05/2018.
 */
public class RequestsBalanceModel {
    private String code;
    private String patientName;
    private String department;
    private String total;

    public RequestsBalanceModel(String code, String patientName, String department, String total) {
        this.code = code;
        this.patientName = patientName;
        this.department = department;
        this.total = total;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
