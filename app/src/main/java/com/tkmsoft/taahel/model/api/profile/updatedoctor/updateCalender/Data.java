
package com.tkmsoft.taahel.model.api.profile.updatedoctor.updateCalender;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("day_info")
    @Expose
    private List<DayInfo> dayInfo = null;

    public List<DayInfo> getDayInfo() {
        return dayInfo;
    }

    public void setDayInfo(List<DayInfo> dayInfo) {
        this.dayInfo = dayInfo;
    }

}
