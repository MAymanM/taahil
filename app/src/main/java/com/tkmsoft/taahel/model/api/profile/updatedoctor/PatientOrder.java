
package com.tkmsoft.taahel.model.api.profile.updatedoctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientOrder {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("member_id")
    @Expose
    private String memberId;
    @SerializedName("job_date_id")
    @Expose
    private String jobDateId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("doctor_complete")
    @Expose
    private String doctorComplete;
    @SerializedName("patient_complete")
    @Expose
    private String patientComplete;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("patient_order_complete")
    @Expose
    private Object patientOrderComplete;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getJobDateId() {
        return jobDateId;
    }

    public void setJobDateId(String jobDateId) {
        this.jobDateId = jobDateId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDoctorComplete() {
        return doctorComplete;
    }

    public void setDoctorComplete(String doctorComplete) {
        this.doctorComplete = doctorComplete;
    }

    public String getPatientComplete() {
        return patientComplete;
    }

    public void setPatientComplete(String patientComplete) {
        this.patientComplete = patientComplete;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getPatientOrderComplete() {
        return patientOrderComplete;
    }

    public void setPatientOrderComplete(Object patientOrderComplete) {
        this.patientOrderComplete = patientOrderComplete;
    }

}
