package com.tkmsoft.taahel.interfaces;

/**
 * Created by MahmoudAyman on 21/04/2018.
 */
public interface MainViewsCallBack {

    void serToolbarTitle(String title);

    void showFilterBtn(boolean visibility);

    void showAddFab(boolean visibility);

    void showSendRequestView(boolean visibility);

    void filterDoctor(String countryCode, String cityCode, String genderCode, String specialtyId, String rate, boolean isFilter);

    void filterCenter(String countryCode, String cityCode, String rate, String categCode, boolean isFilter);

    void sendCategNum(String categNum,String type);

    void filterEdu(String countryCode, String cityCode, String rate, String categCode, boolean isFilter);

    void filterActivity(String countryCode, String cityCode, String rate, boolean isFilter);

    void filterStore(Integer countryCode, Integer cityCode, Float rate, Integer fieldCode, boolean isFilter);
}
