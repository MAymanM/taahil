package com.tkmsoft.taahel.interfaces;

/**
 * Created by ${USER_NAME} on 10/1/2018.
 **/
public interface PaginationAdapterCallback {
    void retryPageLoad();
}
