package com.tkmsoft.taahel.interfaces;

/**
 * Created by MahmoudAyman on 31/05/2018.
 */
public interface SendDateCallBack {
    void setDate(String fullDate, String date, int dateNum);
}
