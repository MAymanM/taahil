package com.tkmsoft.taahel.interfaces;

/**
 * Created by MahmoudAyman on 26/09/2018.
 */
public interface SendPatientProfData {
    void sendData(String id,String gender, String avatar, String birthDate, String name,
                  String email, String phone, String country, String city);
}
