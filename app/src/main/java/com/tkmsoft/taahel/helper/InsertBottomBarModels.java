package com.tkmsoft.taahel.helper;

import android.content.Intent;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;

import com.tkmsoft.taahel.R;
import com.tkmsoft.taahel.activities.LoginActivity;
import com.tkmsoft.taahel.fragments.main.HomeFragment;
import com.tkmsoft.taahel.fragments.main.activities.ActivitiesFragment;
import com.tkmsoft.taahel.fragments.main.centers.CenterGovernmentFragment;
import com.tkmsoft.taahel.fragments.main.education.EduGovernmentFragment;
import com.tkmsoft.taahel.fragments.main.store.StoreFragment;
import com.tkmsoft.taahel.util.MoveToFragment;
import com.tkmsoft.taahel.util.sharedpreference.ListSharedPreference;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;

/**
 * Created by MahmoudAyman on 10/27/2018.
 **/
public class InsertBottomBarModels {

    private AppCompatActivity context;
    private MoveToFragment moveToFragment;
    private String[] colors;

    public InsertBottomBarModels(AppCompatActivity context) {
        this.context = context;
        ListSharedPreference listSharedPreference = new ListSharedPreference(context);
        moveToFragment = new MoveToFragment(context);
    }

    private void loginFirst() {
        Intent myIntent = new Intent(context, LoginActivity.class);
        context.startActivity(myIntent);
        context.finish();
    }

    public void modelsInEnglish(ArrayList<NavigationTabBar.Model> models, String[] colors) {
        models.add(
                getIconModel(R.drawable.ic_home_white_24dp)
        );
        models.add(
                getIconModel(R.drawable.ic_shopping_white_24dp)
        );
        models.add(
                getIconModel(R.drawable.ic_centers_white_24dp)
        );
        models.add(
                getIconModel(R.drawable.ic_school_white_24dp)
        );

        models.add(
                getIconModel(R.drawable.ic_sports_white_24dp)
        );
    }

    public void beginFragmentTransactionsEnglish(int index) {
        switch (index) {
            case 0:
                moveToFragment.moveInMain(new HomeFragment());
                break;
            case 1:
                moveToFragment.moveInMain(new StoreFragment());
                break;
            case 2:
                moveToFragment.moveInMain(new CenterGovernmentFragment());
                break;
            case 3:
                moveToFragment.moveInMain(new EduGovernmentFragment());

                break;
            case 4:
                moveToFragment.moveInMain(new ActivitiesFragment());
                break;
            default:
                break;
        }//end switch
    }


    public void modelsInArabic(ArrayList<NavigationTabBar.Model> models, String[] colors) {
        this.colors = colors;
        models.add(getIconModel(R.drawable.ic_sports_white_24dp));
        models.add(
                getIconModel(R.drawable.ic_school_white_24dp)
        );
        models.add(
                getIconModel(R.drawable.ic_centers_white_24dp)
        );

        models.add(
                getIconModel(R.drawable.ic_shopping_white_24dp)
        );
        models.add(
                getIconModel(R.drawable.ic_home_white_24dp)
        );
    }

    private NavigationTabBar.Model getIconColorModel(int iconDrawable, String color) {
        return new NavigationTabBar.Model.Builder(
                AppCompatResources.getDrawable(context, iconDrawable),
                Color.parseColor(colors[2]))
                .build();
    }

    private NavigationTabBar.Model getIconModel(int iconDrawable) {
        return new NavigationTabBar.Model.Builder(
                AppCompatResources.getDrawable(context, iconDrawable),
                Color.parseColor(colors[2]))
                .build();
    }

    public void beginFragmentTransactionsArabic(int index) {
        switch (index) {
            case 4:
                moveToFragment.moveInMain(new HomeFragment());
                break;
            case 0:
                moveToFragment.moveInMain(new ActivitiesFragment());
                break;
            case 1:
                moveToFragment.moveInMain(new EduGovernmentFragment());
                break;
            case 2:
                moveToFragment.moveInMain(new CenterGovernmentFragment());
                break;
            case 3:
                moveToFragment.moveInMain(new StoreFragment());
                break;
            default:
                break;
        }//end switch
    }

}
