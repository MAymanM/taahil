package com.tkmsoft.taahel.helper;

import android.os.Build;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import static androidx.core.content.PermissionChecker.PERMISSION_GRANTED;

/**
 * Created by MahmoudAyman on 3/8/2019.
 **/
public class MyPermissionManager {
    private final String tag;
    private FragmentActivity mContext;

    public MyPermissionManager(FragmentActivity mContext, String tag) {
        this.mContext = mContext;
        this.tag = tag;
    }

    public void ask(String[] permissions) {
        if (isAppSDkMarshmallow()) {
            ActivityCompat.requestPermissions(mContext,
                    permissions,
                    112);
            Log.d(tag, "ask: request");
        }
    }

    public boolean isPermissionGranted(String permission){
        if (isAppSDkMarshmallow()) {
            return mContext.checkSelfPermission(permission) == PERMISSION_GRANTED;
        }else
            return false;
    }

    private boolean isAppSDkMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }
}
