package com.tkmsoft.taahel.helper;

import android.content.Context;
import com.google.android.material.snackbar.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.tkmsoft.taahel.R;

/**
 * Created by MahmoudAyman on 25/06/2018.
 */
public class SnackBarSet {
    private Snackbar snackbar;
    private View layout;
    private Context context;

    public SnackBarSet(Context context, View layout) {
        this.context = context;
        this.layout = layout;
    }

    public void basicSnackBar(String message) {
        snackbar = Snackbar.make(layout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public void basicSnackBar(String message, int color) {
        snackbar = Snackbar
                .make(layout, message, Snackbar.LENGTH_LONG);

        // Changing text color
        snackbar.setActionTextColor(context.getResources().getColor(color));
        snackbar.setDuration(1000);
        snackbar.show();
    }

    public void buttonSnackBar(String message, String btnText, int messageColor, int btnColor, View.OnClickListener onClickListener) {
        snackbar = Snackbar
                .make(layout, message, Snackbar.LENGTH_LONG)
                .setAction(btnText, onClickListener);

        // Changing text color
        snackbar.setActionTextColor(context.getResources().getColor(messageColor));
        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(btnColor);
        snackbar.show();
    }
}